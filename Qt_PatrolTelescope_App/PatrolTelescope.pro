#-------------------------------------------------
#
# Project created by QtCreator 2016-04-22T16:08:18
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SolarPatrolTelescope
TEMPLATE = app
CONFIG += c++11
QMAKE_CXXFLAGS_EXCEPTIONS_ON = /EHa
QMAKE_CXXFLAGS_STL_ON = /EHa

INCLUDEPATH += include
INCLUDEPATH += "C:/Program Files/JAI/SDK/library/CPP/include"
DEPENDPATH += "C:/Program Files/JAI/SDK/library/CPP/include"

MOC_DIR = tmp/moc
OBJECTS_DIR = tmp/obj
UI_DIR = tmp/ui
RCC_DIR = tmp/rcc
DESTDIR = bin

LIBS += -L"./lib/" -lImageLib
LIBS += -L"./lib/" -lPvAPI
LIBS += -L"./lib/" -lJai_Factory
LIBS += -L"./lib/" -lcfitsio
LIBS += -lws2_32

SOURCES += source/main.cpp\
    source/mainwindow.cpp \
    source/camera.cpp \
    source/camera_avt.cpp \
    source/camera_jai.cpp \
    source/xmlconfig.cpp \
    source/QSM_CNLR_Ethernet.cpp \
    source/Dlg_Coelostat.cpp \
    source/Dlg_Coelostat_Alpha_GoTo.cpp \
    source/Dlg_Coelostat_Delta_GoTo.cpp \
    source/QSM_Dlg_InitControllers.cpp \
    source/Dlg_Coelostat_Scans.cpp \
    source/QSM_InitControllers.cpp \
    source/sunpos.cpp \
    source/Point3.cpp \
    source/guide.cpp \
    source/dlg_guide.cpp \
    source/dlg_telescopemotors.cpp \
    source/scanner.cpp \
    source/dlg_scanner.cpp \
    source/dlg_spectra.cpp \
    source/sehtranslator.cpp \
    source/observation_log.cpp

HEADERS  += source/mainwindow.h \
    source/camera.h \
    source/camera_avt.h \
    source/camera_jai.h \
    source/xmlconfig.h \
    source/QSM_CNLR_Ethernet.h \
    source/Dlg_Coelostat.h \
    source/Dlg_Coelostat_Alpha_GoTo.h \
    source/Dlg_Coelostat_Delta_GoTo.h \
    source/QSM_Dlg_InitControllers.h \
    source/Dlg_Coelostat_Scans.h \
    source/QSM_InitControllers.h \
    source/sunpos.h \
    source/Point3.h \
    source/guide.h \
    source/dlg_guide.h \
    source/dlg_telescopemotors.h \
    source/scanner.h \
    source/dlg_scanner.h \
    source/dlg_spectra.h \
    source/sehtranslator.h \
    source/init_controllers_macros.h \
    source/controllers_ethernet_macros.h \
    source/observation_log.h

FORMS    += gui/mainwindow.ui \
    gui/Dlg_Coelostat.ui \
    gui/Dlg_Coelostat_Alpha_GoTo.ui \
    gui/Dlg_Coelostat_Delta_GoTo.ui \
    gui/QSM_Dlg_InitControllers.ui \
    gui/Dlg_Coelostat_Scans.ui \
    gui/dlg_guide.ui \
    gui/dlg_telescopemotors.ui \
    gui/dlg_scanner.ui \
    gui/dlg_spectra.ui \
    gui/observation_log.ui

#win32:!win32-g++:
PRE_TARGETDEPS += $$PWD/lib/Jai_Factory.lib
#PRE_TARGETDEPS += $$PWD/lib/ImageLib.lib
#else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libJai_Factory.a

win32: RC_ICONS = res/icons/telescope_64x64.ico
