#include "scanner.h"
#include "camera.h"
#include "QSM_InitControllers.h"
#include "QSM_CNLR_Ethernet.h"

#include <QTime>
#include <QApplication>
#include <QTimerEvent>
#include <QStringList>
#include <QMessageBox>
#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QThread>
#include <QTimer>
#include <QProcess>


Scanner::Scanner(QObject *parent) : QObject(parent)
{
    setObjectName("Scanner");
    guideOnSlit = false;
    pCamera = nullptr;
    pCamera2 = nullptr;
    isFirstCameraExist = false;
    pMotorController = nullptr;
    pInitControllers = nullptr;
    pScanTimer = nullptr;
    counter = 0;
    nScans = 0;
    currentKSpeed = 0;
    startPosition = 0;
    scanning = false;
    cameraSaving = false;
    scanPause = false;
    procImageCreateFinished = true;
    scanWasBreaked = false;
    debug_counter = 0;
    timeForWaitSolarImageFinished = 0;

    // Таймер для контроля сделанных двигателем шагов (запуск в Start() )
    pScanTimer = new QTimer(this);
    connect(pScanTimer, SIGNAL(timeout()), this, SLOT(on_Timer_Scan()));

    // Процесс для запуска программы сборки
    processSolarImage = new QProcess(this);
    connect(processSolarImage, SIGNAL(finished(int)),
            this, SLOT(on_Proc_Finished(int)));


    // Процесс для загрузки изображений на сайт
    processUpload = new QProcess(this);
}

Scanner::~Scanner()
{
//    qDebug() << "~Scanner";
    Stop();
    while (currentKSpeed != 0)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);

    pScanTimer->stop();
    ClearDisk();
//    if(pScanTimer > 0) delete pScanTimer;
    processSolarImage->kill();
    processUpload->kill();
}

//------------------------------------------------------------------------------------------------
//  Проверка на сканирование в данный момент
//
bool Scanner::isScanning() { return scanning; }

//------------------------------------------------------------------------------------------------
//  Проверка на сохранение кадров в данный момент
//
bool Scanner::isSaving() { return cameraSaving; }

bool Scanner::isScanWasBreaked() { return scanWasBreaked; }

//------------------------------------------------------------------------------------------------
// Метод для передачи указателя на объект камеры (выбирается изходя из значения флага isFirstCameraExist
//
bool Scanner::SetCamera(Camera *camera)
{
    bool res = false;
    Camera *current_camera;
    if(camera)
    {
        if (!isFirstCameraExist)
        {
            pCamera = camera;
            current_camera =  pCamera;
            isFirstCameraExist = true;
        }
        else
        {
            pCamera2 = camera;
            current_camera = pCamera2;
        }

        // Подключение сканирования камеры к сигналам
        res = connect(this, SIGNAL(sigStartCameraScan(bool)), current_camera, SLOT(SetSaveMode(bool)));
    }

    if (res)
    {
        qDebug() << "SolarScanner::SetCamera. Камера передана." << current_camera->parameters.cameraName;
        emit sigStatusMessage("Сканер готов к запуску!");
    }
    else
    {
        if (!pCamera)
        {
            qDebug() << "SolarScanner::SetCamera. Камера не передана.";
            emit sigStatusMessage("Камера не передана!");
        }
    }
    return res;
}


//------------------------------------------------------------------------------------------------
// Метод для инициализации контроллера и оси для сканирования
//
bool Scanner::SetMotorController(CInitControllers *motorControllers)
{
    if(motorControllers){
        pInitControllers = motorControllers;
        pMotorController = pInitControllers->m_apCNLRs[pInitControllers->m_anFunCNLR[FUN_SCAN_SPECTR]];
        scanAxis = pInitControllers->m_anFunAxis[FUN_SCAN_SPECTR];
        maxSpeed = pMotorController->m_anSM_Speed_max[scanAxis - 1];
        accelerSpeed = pMotorController->m_anSM_Acceler[scanAxis - 1];
        qDebug() << "SolarScanner::SetMotorController. Контроллер передан";
        return true;
    }
    else
    {
        qDebug() << "SolarScanner::SetMotorController. Контроллер не передан";
        emit sigStatusMessage("SolarScanner::SetMotorController. Контроллер не передан");
        pInitControllers = 0;
        pMotorController = 0;
//        scanAxis = pInitControllers->m_anFunAxis[FUN_SCAN_SPECTR];
//        maxSpeed = pMotorController->m_anSM_Speed_max[scanAxis - 1];
//        accelerSpeed = pMotorController->m_anSM_Acceler[scanAxis - 1];
        return false;
    }
}


//------------------------------------------------------------------------------------------------
// Метод для установки режима запуска автоматической сборки
//
void Scanner::SetAutoCreateImage(bool b)
{
    scanSettings.autoCreateImage = b;
}

//------------------------------------------------------------------------------------------------
// Метод для установки режима конвертации данных в JPEG
//
void Scanner::SetConvertToJpeg(bool b)
{
    scanSettings.convertToJpeg = b;
}

//------------------------------------------------------------------------------------------------
// Запуск программы сканирования
//
void Scanner::Start()
{
    scanWasBreaked = false;
    if (scanSettings.scanMode == SCANNER_TO_START)
        return;

    emit sigScanWasStarted(true);
    qDebug() << "Scanner: start!";
//    emit sigStartTime();
    if(pScanTimer->isActive() == false)
        pScanTimer->start(SCANNER_TIMER_MSEC);

    // Начало движения
    startPosition = pMotorController->GetPosAxes(scanAxis);
    qDebug() << "StartPosition =" << startPosition;
    qDebug() << "ScanPause =" << scanPause;
    if(scanPause)
    {
        scanSettings.scanMode = SCANNER_TO_FINISH;
    }
    else
    {
        currentKSpeed = - SCANNER_K_MAX_SPEED;
        pMotorController->SM_Forvard(response, currentKSpeed, scanAxis);
        scanSettings.scanMode = SCANNER_TO_START;
        if (guideOnSlit == true)
            scanning = true; //true (нельзя гидировать)

    }

}

//------------------------------------------------------------------------------------------------
// Остановка программы сканирования
//
void Scanner::Stop()
{
    if(scanSettings.scanMode == SCANNER_STOP)
        return;

    emit sigScanWasStarted(false);
    scanWasBreaked = true;
    qDebug() << "Scanner: stop!";
    nScans = 0;
    emit sigProgressBarCounter(0);
    emit sigStatusMessage("Остановка...");
    emit sigStartCameraScan(false);
    cameraSaving = false;

    emit sigFinishTime();

    if (guideOnSlit == false)
        //для гида на щели сканирования - true (можно гидировать)
        scanning = false;
    //scanning = true; //для гида до сканирования - false (можно гидировать)

    double position = pMotorController->GetPosAxes(scanAxis);
    // Если позиция больше или меньше нуля - двигаемся в ту или иную сторону
    if ((position) > startPosition)
    {
        currentKSpeed = - SCANNER_K_MAX_SPEED;
        pMotorController->SM_Forvard(response, currentKSpeed, scanAxis);
    }
    if ((position) < startPosition)
    {
        currentKSpeed = SCANNER_K_MAX_SPEED;
        pMotorController->SM_Forvard(response, currentKSpeed, scanAxis);
    }
    scanSettings.scanMode = SCANNER_STOP;
    DeleteScan();
    return;
}

//------------------------------------------------------------------------------------------------
//  Расчет погрешности в шагах (из-за таймера)
//
double Scanner::ErrorSteps(double speed)
{
    return SCANNER_TIMER_MSEC * speed / (1000.0 * 0.40);
}

//------------------------------------------------------------------------------------------------
// Таймер сканирования
//
void Scanner::on_Timer_Scan()
{
    // Проверка на достижение концевиков
    MaxMinCheck();

    // Текущие координаты
    double position  = pMotorController->GetPosAxes(scanAxis);

    // Выбор дальнейших действий в зависимости от установленного режима сканирования
    switch(scanSettings.scanMode)
    {
        case SCANNER_TO_START:         // Если режим отмотки на край диска Солнца
        {
            GoToStart(position);
//            if (debug_counter % 20 == 0)
//                qDebug() << "SCANNER_TO_START: scanning = " << scanning;
            break;
        }
        case SCANNER_ON:               // Если режим сканирования
        {
            GoScan(position);
//            if (debug_counter % 10 == 0)
//                qDebug() << "SCANNER_ON: scanning = " << scanning;
            break;
        }
        case SCANNER_TO_FINISH:        // Если режим перемотки на центр
        {
            bool b = GoToFinish(position);
//            if (debug_counter % 20 == 0) {
//                qDebug() << "SCANNER_TO_FINISH: scanning = " << scanning;
//                qDebug() <<  "SCANNER_TO_FINISH: readyToScan = " << b ;
//                qDebug() <<  "SCANNER_TO_FINISH: procFinished =" << procImageCreateFinished;
//            }
            break;
        }
        case SCANNER_STOP:             // Если режим остановки (с выводом в центр)
        {
            GoToStop(position);
//            if (debug_counter % 100 == 0)
//                qDebug() << "SCANNER_STOP: scanning = " << scanning;
            break;
        }
    }
//    debug_counter++;
}

//------------------------------------------------------------------------------------------------
// Проверка концевиков на достижение пределов подвижки
//
void Scanner::MaxMinCheck()
{
    motorMax = pInitControllers->Get_On_Port_MAX_MIN(FUN_SCAN_SPECTR, PORT_ON_MAX);
    motorMin = pInitControllers->Get_On_Port_MAX_MIN(FUN_SCAN_SPECTR, PORT_ON_MIN);

    if(motorMax || motorMin)
    {
        pMotorController->SM_Stop(response,scanAxis);
        scanSettings.scanMode = SCANNER_STOP;
        emit sigStatusMessage("<FONT COLOR=#990000>Подвижка достигла концевика!</FONT>");
    }
}

//------------------------------------------------------------------------------------------------
// Движение в начальное положение и остановка.
// вызывается по таймеру в on_Timer_Scan()
void Scanner::GoToStop(double position)
{
    double step2stop = pMotorController->SM_StepToNextSpeed(maxSpeed*currentKSpeed, 0, accelerSpeed, SCANNER_TIMER_MSEC);

    if ( (currentKSpeed > 0.0) && (position > startPosition - step2stop)
         || (currentKSpeed < 0.0) && (position < startPosition + step2stop) )
    {
        pMotorController->SM_Stop(response, scanAxis);
        currentKSpeed = 0;
        qDebug() << "SolarScanner::GoToStop Позиция остановки =" << position + step2stop;
        emit sigStatusMessage("Остановлено");

        if (guideOnSlit == true)
        {
            emit sigGuideRestart();
            scanning = false; //для гида на щели сканирования - false (можно гидировать)
        }

//        if(ExistsScanPath() && !procFinished)
//        {
//            CopyFiles(previousScanPath, previousSavePath);
//            DeleteFiles(previousScanPath);
//        }
    }
    else
    {
        // Счетчик для полосы загрузки
        double arcsecToStop = scanSettings.arcsecScanRadius / 2;
        int progressBarCounter = 100; // - (int)( ( fabs(position - startPosition) - step2stop)/ (arcsecToStop * scanSettings.kArcsec2Steps) * 100.0f );
//        qDebug() << position << startPosition << step2stop << arcsecToStop * scanSettings.kArcsec2Steps << progressBarCounter;
        emit sigProgressBarCounter(progressBarCounter);
        ++counter;
    }

}

//-------------------------------------------------------------------------------------------------
//  Проверка на достижение точки начала сканирования, запуск режима сканирования
//  вызывается по таймеру в on_Timer_Scan()
void Scanner::GoToStart(double position)
{
    double arcsecToStart = -scanSettings.arcsecScanRadius / 2;
    emit sigStatusMessage("Перемотка к началу сканирования...");

    double step2stop = pMotorController->SM_StepToNextSpeed(maxSpeed*currentKSpeed, 0, accelerSpeed, SCANNER_TIMER_MSEC);
    double positionToStart = arcsecToStart * scanSettings.kArcsec2Steps + startPosition;

    // Счетчик для полосы загрузки
    int progressBarCounter = (position - step2stop - startPosition)/ (arcsecToStart * scanSettings.kArcsec2Steps) * 100.0f;
//    qDebug() << position << step2stop << arcsecToStart * scanSettings.kArcsec2Steps << progressBarCounter;
    emit sigProgressBarCounter(progressBarCounter);
    ++counter;

    // Условие выполнения необходимого количества шагов
    if ((position - step2stop) < positionToStart)
    {
        scanSettings.scanMode = SCANNER_ON;
        //scanning = false; //для гида до сканирования - false (можно гидировать)
        //scanning = true; //для гида на щели сканирования - true (нельзя гидировать)

        double scanSpeed = CalcScanSpeed() / maxSpeed;
//        qDebug() << "SolarScanner::GoToStart, scanSpeed = " << scanSpeed;
        // Начало движения сканирования
        pMotorController->SM_Forvard(response, scanSpeed, scanAxis);
        currentKSpeed = scanSpeed;
    }
}

//------------------------------------------------------------------------------------------------
//  Расчет скорости сканирования
//
double Scanner::CalcScanSpeed()
{
    double secToScan = scanSettings.msecToScan / 1000.0;
    return scanSettings.arcsecScanRadius * scanSettings.kArcsec2Steps / secToScan;
}

//------------------------------------------------------------------------------------------------
// Проверка на точку окончания сканирования, запуск сохранения кадров с камеры,
//                                           запуск движения обратной перемотки
// вызывается по таймеру в on_Timer_Scan()
//
void Scanner::GoScan(double position)
{
    double arcsecToStart = -scanSettings.arcsecScanRadius / 2;
    double arcsecToFinish = scanSettings.arcsecScanRadius / 2;
    double step2stop = pMotorController->SM_StepToNextSpeed(maxSpeed*currentKSpeed, 0, accelerSpeed, SCANNER_TIMER_MSEC);
    double positionToFinish = arcsecToFinish * scanSettings.kArcsec2Steps + startPosition;
    double positionToStart = arcsecToStart * scanSettings.kArcsec2Steps + startPosition;

    // Счетчик для полосы загрузки
    int progressBarCounter = (float)++counter * (float)SCANNER_TIMER_MSEC / (float)scanSettings.msecToScan * 100.0f;
    emit sigProgressBarCounter(progressBarCounter);

    //  Запуск сохранения изображений с камеры
    if ( !cameraSaving && (position > positionToStart) )
    {
        emit sigStatusMessage("Сканирование");
        scanning = true;
        if (!StartSaveCamera())
        {
            Stop();
            return;
        }
//            SetScanPause();
    }
    //  Условие завершения сканирования и запуск обратной перемотки
    if ( (position + step2stop) > positionToFinish )
    {
        StopSaveCamera();
        if (guideOnSlit == false)
            scanning = false;
        // Переход в режим обратной перемотки
        scanSettings.scanMode = SCANNER_TO_FINISH;
        currentKSpeed = -SCANNER_K_MAX_SPEED;
        pMotorController->SM_Forvard(response, currentKSpeed, scanAxis);
        counter = 0;

        //  Переименование файлов в директории сканирования для зеркального отражения в изображении
        if (SCANNER_FLIP_RENAMEFILE)
            RenameFiles_OnFlip();
    }
}

//------------------------------------------------------------------------------------------------
// Проверка на достижение точки центра (к центру диска Солнца), остановка.
// вызывается по таймеру в on_Timer_Scan()
bool Scanner::GoToFinish(double position)
{
    // Если сканирование было прервано по сигналу из гида
    if (scanWasBreaked)
    {
        emit sigStatusMessage("Прерывание сканирования");
        scanSettings.isAlreadyCreatedUploaded = true;
//        scanWasBreaked = false;
    }
    bool result = 0;
//    emit sigStatusMessage("Обратная перемотка...");
    
    // Если загрузка и сборка еще не были проведены
    if (!scanSettings.isAlreadyCreatedUploaded)
    {
        // Если сборка была завершена
        if (procImageCreateFinished)
        {
            debug_counter = 0;
            // Запуск программы сборки изображения
            StartCreateImageProgram(currentScanPath, currentSavePath);

            // Загрузка на сайт
            if (scanSettings.autoUpload)
                StartUploadProgram(previousSavePath);

            // Загрузка и сборка уже были запущены
            scanSettings.isAlreadyCreatedUploaded = true;
        }
        else
        {
            emit sigStatusMessage("Ожидаем завершения программы сборки...");

            if (SCANNER_TIMER_MSEC * debug_counter++ > timeForWaitSolarImageFinished)
            {
                processSolarImage->kill();
                qDebug() << "kill SolarImage";
                debug_counter = 0;
                DeleteFiles(previousScanPath);
                DeleteFiles(previousSavePath);
            }
        }
    }

    GoToStop(position);

    // Если сборка и загрузка уже были запущены
    if (scanSettings.isAlreadyCreatedUploaded)
    {
        emit sigStatusMessage("Ожидаем завершения гидирования...");

        // Проверка на режим остановки и режим паузы в сканировании
        if( result = ReadyToScan() == true )
        {
            scanWasBreaked = false;
            // Для нового скана загрузка и сборка еще не были выполнены
            scanSettings.isAlreadyCreatedUploaded = false;

//            qDebug() << "ready to next scan!";
//            qDebug() << procImageCreateFinished;
            scanSettings.scanMode = SCANNER_TO_START;
            currentKSpeed = -SCANNER_K_MAX_SPEED;
            pMotorController->SM_Forvard(response, currentKSpeed, scanAxis);
            counter = 0;

//          if (guideOnSlit == true)
//              scanning = false;   //для гида до сканирования - false (можно гидировать)
        }
    }
    return result;
}

//------------------------------------------------------------------------------------------------
// Проверка на готовность к следующему сканированию
//
bool Scanner::ReadyToScan()
{
//    int err = processSolarImage->error();
//    if (debug_counter % 1000)
//        qDebug() << "ProcessError = " << err;
//    debug_counter++;

    bool result = (scanSettings.scanMode != SCANNER_STOP) && !scanPause;
    return result;
}

//------------------------------------------------------------------------------------------------
// Метод для запуска сохранения кадров с камеры
//
bool Scanner::StartSaveCamera()
{
    if (!pCamera || !pCamera->isOpened())
    {
        qDebug() << "Camera don't open!";
        return false;
    }


    if (cameraSaving)
        return true;

    nScans++;
    previousScanPath = currentScanPath;
    previousSavePath = currentSavePath;
    currentScanPath = CreateDir(scanSettings.scanPath);
    if ( scanSettings.convertToJpeg == true && nScans % 5 != 0)
    {
        QString savePathJpg(scanSettings.savePath);
        savePathJpg.chop(1);
        savePathJpg.append("_jpg/");
        currentSavePath = CreateDir(savePathJpg);
    }
    else
        currentSavePath = CreateDir(scanSettings.savePath);

//    qDebug() << "PATH=" << currentScanPath << currentSavePath;

    if (currentSavePath == "" || currentScanPath == "" )
        return true;

    pCamera->parameters.savePath = currentScanPath;

    if(pCamera2 != NULL)
    {
        if (!pCamera2->isOpened())
            return false;
        pCamera2->parameters.savePath = currentScanPath;
    }
    //scanning = true;
    cameraSaving = true;
    emit sigStartCameraScan(true);
    counter = 0;
    emit sigStatusMessage("Сканирование...");
//    qDebug() << "SolarScanner::GoScan" << "ScanNumber = " << nScans;
    sigStatusMessage(nScans);
    return true;
}

//------------------------------------------------------------------------------------------------
// Метод для остановки сохранения кадров с камеры
//
void Scanner::StopSaveCamera()
{
    if(cameraSaving)
    {
        emit sigStartCameraScan(false);
        cameraSaving = false;
        //scanning = false;
        sigProgressBarCounter(100);

        // Получаем время наблюдения
        if (scanWasBreaked)
            return;
        QString time = QTime::currentTime().toString("hh:mm");
        qDebug() << time;

        // Запись в журнал наблюдений
        if (nScans == 1)
            emit sigStartTime(time);

        emit sigCurrentTime(time);
    }
}

//------------------------------------------------------------------------------------------------
//  Очистка RAM-диска
//
void Scanner::ClearDisk()
{
    emit sigStatusMessage("Очистка RAM-диска...");
    QString scanDisc = scanSettings.scanPath;
    // Берем только букву диска
    scanDisc.remove(2,scanDisc.size() - 3);
    // Очищаем весь диск
    QDir tempDir(scanDisc);
    tempDir.removeRecursively();
    return;
}

//------------------------------------------------------------------------------------------------
//  Переименование файлов в директории сканирования для зеркального отражения в изображении
//
void Scanner::RenameFiles_OnFlip()
{
    emit sigStatusMessage("Переименование файлов RAM-диска...");
    QDir dir(currentScanPath);// передаём в конструктор строку пути
    QStringList nameFilter; // имя фильтра
    // можно задать интересующие расширения nameFilter << "*.png" << "*.jpg" << "*.gif";
    nameFilter << "*.*" ; //<< "*.fit"<< "*.jpg";// разные
    QFileInfoList listFN = dir.entryInfoList( nameFilter, QDir::Files );// интересуют только файлы
    QFileInfo fileinfo; // сюда будем считывать эжлементы
    long i, nF = listFN.length(); //число файлов в директории
    QString sFN_out;
    //Переименовали с "+" в обратном порядке
    for(i=0; i<nF ;i++)
    {
        sFN_out=listFN[nF-1-i].path() + "/+" +listFN[nF-1-i].fileName();
        QFile::rename(listFN[i].filePath(), sFN_out);
    }
    nameFilter.clear();
    nameFilter<< "+*.*" ; //<< "*.fit"<< "*.jpg";// c "+"
    listFN = dir.entryInfoList( nameFilter, QDir::Files );// интересуют только файлы
    //Переименовали для обработки
    for(i=0; i<nF ;i++)
    {
        sFN_out=listFN[i].path()+ "/" + listFN[i].fileName().right(listFN[i].fileName().length()-1);
        QFile::rename (listFN[i].filePath(), sFN_out);
    }
    //emit sigStatusMessage(sFN_out);
    return;
}



//------------------------------------------------------------------------------------------------
//  Проверка существования папки для сканирования
//
bool Scanner::ExistsScanPath()
{
    if(previousScanPath == "")
        return false;
    return QDir(previousScanPath).exists();
}

//------------------------------------------------------------------------------------------------
//  Создание папки (формата: дата, время) в заданной директории
//
QString Scanner::CreateDir(QString path)
{
    // Создание имени для папки на диске
    QString diskName = path[0];
    path.remove(0, 1);
    QString folderPath = diskName + QDateTime::currentDateTimeUtc().toString(path);
    // Создание папки
    QDir dir;
    if(dir.exists(folderPath))
        folderPath.append("(2)");
    if(dir.mkpath(folderPath))
        return folderPath;
    else
    {
        qDebug() << "Не удалось создать директорию по заданному пути:" << folderPath;
//        emit sigWarningMessage("Не удалось создать директорию по заданному пути!");
        return "";
    }
}

//------------------------------------------------------------------------------------------------
// Запуск программы сборки изображения
//
void Scanner::StartCreateImageProgram(QString pathToScan, QString pathToSave)
{
    if(scanSettings.autoCreateImage == true)
    {
        if(pathToScan == "" || pathToSave == "")
        {
            qDebug() << "Scanner::StartCreateImageProgram: Не задан путь сборки! ";
            return;
        }
//        qDebug() << "Scanner::StartCreateImageProgram: " << pathToScan << pathToSave ;

        QStringList args;
        args << scanSettings.dopplerMode << pathToScan << pathToSave << QString().setNum(nScans);



        int len = scanSettings.programCreateImagePath.lastIndexOf("/");
        QString path(scanSettings.programCreateImagePath.mid(0, len+1));
        processSolarImage->setWorkingDirectory(path);

        if (scanSettings.dopplerMode == "-d")
            timeForWaitSolarImageFinished = 80000;
        else
            timeForWaitSolarImageFinished = 20000;

        processSolarImage->start(scanSettings.programCreateImagePath, args);
        procImageCreateFinished = false;
    }
}

//------------------------------------------------------------------------------------------------
// Запуск программы загрузки на сайт
//
void Scanner::StartUploadProgram(const QString &pathToImage)
{
//    qDebug() << "upload called!";
    if (pathToImage != "")
    {
        // Параметры для программы загрузки
        QString ftp_type("/gas_megalog");
        QString ftp_addr("'ftp://ftp.solarstation.ru/www/solarstation.ru/lastdata/'");
        QString saveDir = scanSettings.savePath.left(14);
        saveDir.prepend("'");
        saveDir.insert(4, "'");        // имеет вид 'C:/'yyyy/mm/dd/

        QString coreName("yyyy-MM-dd'_??.??.??_.jpg'");
        QString dopplerName = QString("'d'").append(coreName);  // 'd'yyyy-MM-dd'_??.??.??_.jpg'

        // Загружаем на сайт
        if (scanSettings.autoCreateImage)
        {
            QStringList args;
            args << ftp_type << saveDir << coreName << ftp_addr << scanSettings.core_name_ftp;
//            qDebug() << args;

            if (processUpload->state() != 0)
                // Ждем завершения процесса
                if (!processUpload->waitForFinished(10000))
                    processUpload->kill();

            processUpload->start(scanSettings.programUploadImagePath, args);
        }
        if (scanSettings.dopplerMode == "-d")
        {
            QStringList args;
            args << ftp_type << saveDir << dopplerName << ftp_addr << scanSettings.doppler_name_ftp;

            if (!processUpload->waitForFinished(20000))
                processUpload->kill();

            processUpload->start(scanSettings.programUploadImagePath, args);
        }
    }
}

void Scanner::DeleteScan()
{
    qDebug() << currentScanPath << currentSavePath;
    DeleteFiles(currentScanPath);
    DeleteFiles(currentSavePath);
}

//-----------------------------------------------------------------------------
// Копирование файлов с RAM-диска
//
void Scanner::CopyFiles(QString source, QString dest)
{
    emit sigStatusMessage("Копирование файлов...");
    QDir tempDir(source);
    QStringList fileList = tempDir.entryList(QDir::Files, QDir::Name);
    int nFiles = fileList.count();

    for (int i = 0; i < nFiles; i++)
    {
        QCoreApplication::processEvents( QEventLoop::AllEvents, 25 );
        QString fileName(source);
        fileName.append(fileList.at(i));
        QString newName(dest);
        newName.append(fileList.at(i));
        if (QFile::exists(fileName)){
            if(!QFile(fileName).copy(newName))
                qDebug("Файл не скопирован");
        }
    }
    return;
}

//------------------------------------------------------------------------------------------------
//  Удаление скана с диска
//
void Scanner::DeleteFiles(QString scanPath)
{
    if (scanPath == "")
        return;
    QDir tempDir(scanPath);
    if(tempDir.exists())
        emit sigStatusMessage("Удаление файлов...");
        tempDir.removeRecursively();
    return;
}

//------------------------------------------------------------------------------------------------
// Проверка на завершение процесса обработки (сборки)
//
void Scanner::on_Proc_Finished(int code)
{
    this->thread()->msleep(1000); //Пауза для закрытия программы
    procImageCreateFinished = true;
    if(code != 0)
    {
        int errorFinished =  processSolarImage->error();
        qDebug() << "error finished code = " << errorFinished;
        emit sigStatusMessage(QString("Процесс завершился с ошибкой "));
    }
//    qDebug() << "on_Proc_Finished: " << procImageCreateFinished;
}


//------------------------------------------------------------------------------------------------
// Установка флага на паузу при ожидании следующего сканирования
//
void Scanner::SetNextScanPause(bool needPause)  { scanPause = needPause;}

//------------------------------------------------------------------------------------------------
// Установка флага на прерывание текущего сканирования
//
void Scanner::SetScanPause()
{
    qDebug() << "SetScanPause called";
    qDebug() << "break position" << pMotorController->GetPosAxes(scanAxis) << "start position" << startPosition;
    scanWasBreaked = true;

    sigStatusMessage(--nScans);
    StopSaveCamera();

    if (guideOnSlit == false)
        scanning = false;
    // Переход в режим обратной перемотки
    scanSettings.scanMode = SCANNER_TO_FINISH;
    currentKSpeed = SCANNER_K_MAX_SPEED;
    pMotorController->SM_Forvard(response, currentKSpeed, scanAxis);
    DeleteScan();

    SetNextScanPause(true);

}

//------------------------------------------------------------------------------------------------
// Установка режима сборки
//
void Scanner::SetDopplerMode(bool doppler)
{
    if (doppler)
        scanSettings.dopplerMode = "-d";
    else
        scanSettings.dopplerMode = "-c";
}

//------------------------------------------------------------------------------------------------
// Установка режима загрузки на сайт (авто или вручную)
//
void Scanner::SetUploadMode(bool upload)
{
    if (upload)
        scanSettings.autoUpload = true;
    else
        scanSettings.autoUpload = false;
}

// Вместо деструктора, вызывается по сигналу из DlgScanner
void Scanner::DeleteScanner()
{
    this->~Scanner();
}

