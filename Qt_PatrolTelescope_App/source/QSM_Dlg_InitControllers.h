#ifndef QSM_Dlg_InitControllers_H
#define QQSM_Dlg_InitControllers_H

#include <QDialog>
#include <QDateTime>
#include "QSM_InitControllers.h"

namespace Ui {
class QSM_Dlg_InitControllers;
}

class QSM_Dlg_InitControllers : public QDialog
{
    Q_OBJECT

public:
    CInitControllers  *m_pAll_CNLRs_SM; //Ссылка на класс инициализации всех рабочих контоллеров ШД
    //bool m_On_Cur_Time; //Работаем в режиме поиска положения по текущему времени

public:
    explicit QSM_Dlg_InitControllers(QWidget *parent = 0);
    ~QSM_Dlg_InitControllers();

private slots:


    void on_btn_Refresh_clicked();

private:
    Ui::QSM_Dlg_InitControllers *ui;
};

#endif // QSM_Dlg_InitControllers

