#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#define _WINSOCKAPI_

#include <QMainWindow>
#include <QDebug>

class QSplashScreen;
class QMdiArea;
class Dlg_Guide;
class Guide;
class Dlg_Spectra;
class Camera;
class Dlg_Scanner;
class Scanner;
class Dlg_TelescopeMotors;
class Dlg_Coelostat;
class QSM_Dlg_InitControllers;
class CInitControllers;
class XMLConfig;
class ObservationLog;

namespace Ui {
class MainWindow;
}

/**
 * @brief Класс главного окна программы. Размещает графические элементы управления телескопом в мультиоконном режиме и
 * проводит начальную инициализацию системы.
 * @details Для размещения графических элементов используется объект класса QMdiArea.
 * Графические элементы (диалоги) проводят инициализацию оборудования. Создается журнал наблюдений.
 *
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QString confDir, QString logPath, QWidget *parent = 0);
    ~MainWindow();
    virtual void closeEvent(QCloseEvent *event);

private:
    Ui::MainWindow* ui;
    QMdiArea* mdiArea;                              // Область, содержащая все окошки
    QString configDirectory;
    QString observLogPath;
    Dlg_Guide* dlg_Guide;                           // Окно управления гидированием
    Dlg_Spectra* dlg_Spectra;                       // Окно управление камерой спектра
    Dlg_Spectra* dlg_Spectra2;                      // Окно управления второй камерой спектра
    CInitControllers*  all_Controllers;             // Класс инициализации всех рабочих контоллеров ШД
    Dlg_Coelostat* dlg_Coelostat;                   // Окно управления целостатом
    QSM_Dlg_InitControllers* dlg_InitControllers;   // Окно инициализации всех контроллеров
    Dlg_Scanner* dlg_Scanner;                       // Окно сканирования
    Dlg_TelescopeMotors* dlg_TelescopeMotors;       // Окно управления подвижками телескопа
    XMLConfig* xml;
    ObservationLog* log;
    QString config_name;
    QString config_second_spectral_camera;

    void CreateObservationLog();

    // Подключение сигналов к слотам
    bool ConnectScannerAndSpectra(Scanner *scanner, Camera *camera);
    bool ConnectCoelostatAndScanner();
    bool ConnectCoelostatAndGuide();
    bool ConnectScannerAndGuide();
    bool ConnectGuideAndSpectra(Camera *camera);
    void ConnectScannerAndLog();


    void SaveConfig();
    void LoadConfig();
    Camera *ConnectSecondSpectralCamera();

signals:
    void sigExit();
    void sigStopThreads();
    void sigLogCreate();
};

#endif // MAINWINDOW_H
