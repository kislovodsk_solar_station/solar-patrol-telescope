#include "xmlconfig.h"
#include <QDebug>
#include <QFile>

XMLConfig::XMLConfig()
{
    m_XML.setAutoFormatting(true);
    p_XMLFile = new QFile("./config.xml");
    if ( p_XMLFile->exists())
        p_XMLFile->remove();
    p_XMLFile->open(QIODevice::ReadWrite);
    m_XML.setDevice(p_XMLFile);
    qDebug() << "XML файл создан";
}

bool XMLConfig::WriteFile()
{
    m_XML.writeStartDocument();
    m_XML.writeDTD("<!DOCTYPE config>");
    m_XML.writeStartElement("Конфигурационный файл");
    m_XML.writeAttribute("version", "1.0");
    m_XML.writeEndElement();
    m_XML.writeStartElement("Камера");
    m_XML.writeAttribute("Экспозиция", "2000");
    m_XML.writeAttribute("Ширина кадра", "2500");
    m_XML.writeEndElement();
    m_XML.writeEndDocument();
    qDebug() << "XML записан";
    p_XMLFile->close();
    return true;
}

