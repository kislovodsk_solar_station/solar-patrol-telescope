//  CInitControllers--- QSM_InitControllers.h
#ifndef QSM_INITCONTROLLERS_H
#define QSM_INITCONTROLLERS_H

#include <QString>
#include "init_controllers_macros.h"


//Сверху и снизу данные функций контроллеров должны быть согласованными
static QString g_asFunControllers[] =
{"Alpha",           // Управление на целостате альфой
 "Alpha two,half v",// Управление на целостате альфой
 "Delta" ,          // Управление на целостате дельтой
 "Scan coelostat",	// Сканирование с помощью целостата
 "Scan spectr",     // Сканирование внутри спектрографа
 "Focus slit",      // Фокус на щели
 "Focus spectr",    // Фокус спектра
 "Focus guide",     // Фокус гида
 "Grating",         // Вращение дифракционной решетки
 "Collimate focus"  // Управление коллиматорным зеркалом
};

//В файле конфигурации поля для работы с контроллером для каждой функций
static QString g_asFun_Field_CNLR[] =
{"CNLR name",       // В файле конфигурации имя контроллера. Если = S_NO_CNLR функция отключена
 "Axis number" ,    // В файле конфигурации ось
 "Max speed   ",    // В файле конфигурации максимальная скорость
 "Acceleration",    // В файле конфигурации ускорение
 "Microstep",       // В файле конфигурации микрошаг
 "N port min",      // В файле номер порта для концевиков - минимального положения
 "N port max",      // В файле номер порта для концевиков - максимального положения
};

typedef unsigned char BYTE;

class CControllerEthernet;
class CInitControllers //: QObject
{
//    Q_OBJECT
public:
    //конструктор CInitControllers
    explicit CInitControllers(QString configPath);

    //QString m_asAll_CNLR_IP_Name_load[N_MAX_CNLR]; //IP + имена контроллеров загруженные из CFG
    QString m_asAll_CNLR_IP_Name_cur [N_MAX_CNLR]; //IP + имена контроллеров определенные в сети

    BYTE m_anFunCNLR     [FUN_NUMBER_ALL]; //Контроллеры для реализации заложенных Функции. Если = NO_CNLR функция отключена
    BYTE m_anFunAxis     [FUN_NUMBER_ALL]; //Оси контроллера для реализации заложенных Функции
    long m_anFunMaxSpeed [FUN_NUMBER_ALL]; //Максимальная скорость оси контроллера (ш/сек) для реализации заложенных Функции
    long m_anFunAcceler  [FUN_NUMBER_ALL]; //Ускорение оси контроллера (ш/(сек^2)) для реализации заложенных Функции
    BYTE m_anFunMicrostep[FUN_NUMBER_ALL]; //Микрошаг оси контроллера (может не поддерживатся) для реализации заложенных Функции

    int m_anFunNumInPorts_MIN[FUN_NUMBER_ALL];   //Номера входных портов контроллера для концевиков - минимального положения
    int m_anFunNumInPorts_MAX[FUN_NUMBER_ALL];   //Номера входных портов контроллера для концевиков - максимального положения

    CControllerEthernet* m_apCNLRs[N_MAX_CNLR]; //Массив контроллеров

    ~CInitControllers();

public:
    bool isInitSuccess();
    bool GetAll_CNLR_Names();                    // Взять имена всех доступных контрллеров
    int Load_Controllers_CFG(bool bMessageOn);   // Создание или чтение файла конфигурации
    int Save_Controllers_CFG(bool bMessageOn);   // Сохранение файла конфигурации
    bool Init_All_CNLRs();                       // Инициализация имеющихся контроллеров
    CControllerEthernet * Init_One_CNLR(QString sIP_Name_CNLR);   // Инициализация одного контроллера
    int GetN_CNLR(CControllerEthernet * pCNLR);  // Определить номер контроллера в списке m_asAll_CNLR_IP_Name_cur

public:

    bool Get_On_Port_MAX_MIN(int nFun, int nMAX_MIN); //для концевиков определение максимального или минимального положения
    QString funGet_IP_CNRL   (QString sIP_Name_CNRL); //символное IP  CNRL из IP+имя контроллера
    QString funGet_Name_CNRL (QString sIP_Name_CNRL); //символное имя CNRL из IP+имя контроллера

protected:
    bool initSuccess;       // Флаг успешности инициализации всех контроллеров
    bool terminated;
    QString config_init_controllers;

//signals:
//    void sigSetCNLRs(CInitControllers*);
};


#endif // QSM_INITCONTROLLERS_H
