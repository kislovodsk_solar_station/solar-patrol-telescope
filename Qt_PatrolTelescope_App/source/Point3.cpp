﻿//---------------------------------------------------------------------------
#include <math.h>
//#include "qmath.h"
#include <QImage>
#include <QColor>
#include <QPainter>
#include <QDebug>
#include "sunpos.h"
#include "Point3.h"

int random(int limit)
{
    return rand()%limit;
}

float Circle_Three_Points_Rand(int n,int iter,float x[],float y[],int *xc,int *yc)
{//трехточечный алгоритм поиска центра окружности  по удаленым точкам
   //n-число точек с координатами xc,yc
   int step = n / 3;
   iter = step;

   int i,j,k,it=0,it1=0;
   double sx=0,sy=0,q=0,del,xcijk,ycijk,r=0;
   //float eps=0.1f;
   float xcf=0,ycf=0,dd,d_avr=0,d1,d2,d3;;
//   qDebug() << x[1] << y[1];
//   if(iter<10)
//    iter=10;

//   *xc=0; *yc=0;
   if(n<3)
      return 0;
   //ищем расстояние меду точками
   d_avr=k=0;
   for(i=k=0;i<n-5;i++)
    for(j=i;j<n;j++)
     { dd=sqrt((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j]));
       d_avr+=dd;
       k++;
     }
   d_avr/=0.01+k;

   it=it1=0;
   do{//перебор по удаленым точкам
//       i=random(n-1);
//       j=random(n-1);
//       k=random(n-1);
       i = it + 0 * step;
       j = it + 1 * step;
       k = it + 2 * step;
       if  (k >= n) break;

       d1=sqrt((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j]));
       d2=sqrt((x[i]-x[k])*(x[i]-x[k])+(y[i]-y[k])*(y[i]-y[k]));
       d3=sqrt((x[k]-x[j])*(x[k]-x[j])+(y[k]-y[j])*(y[k]-y[j]));

       it1++;
//       if(!(d1>d_avr & d2>d_avr & d3>d_avr))
//         continue;
               del=(x[k]-x[j])*(y[j]-y[i])-(x[j]-x[i])*(y[k]-y[j]);
               if(fabs(del)<1e-6)
                 continue;
               xcijk=(y[k]-y[j])*(x[i]*x[i]+y[i]*y[i])+(y[i]-y[k])*(x[j]*x[j]+y[j]*y[j])+(y[j]-y[i])*(x[k]*x[k]+y[k]*y[k]);
               xcijk=0.5*xcijk/del;
               ycijk=(x[k]-x[j])*(x[i]*x[i]+y[i]*y[i])+(x[i]-x[k])*(x[j]*x[j]+y[j]*y[j])+(x[j]-x[i])*(x[k]*x[k]+y[k]*y[k]);
               ycijk=-0.5*ycijk/del;
               sx=sx+xcijk;
               sy=sy+ycijk;
               q+=1.0;
       it++;
     }while((it<iter) & (it1<1e5));

        if(fabs(q)<1e-6)
              return 0;

      xcf=sx/q;
      ycf=sy/q;
      *xc=xcf+0.4;
      *yc=ycf+0.4;
      r=0;
     for(i=0;i<n;i++)
      r+=sqrt((x[i]-xcf)*(x[i]-xcf)+(y[i]-ycf)*(y[i]-ycf));
     r/=(n+0.01);

     return r;
  }

//float Circle_Three_Points_Rand(int n,int iter,float x[],float y[],int *xc,int *yc)
//{//трехточечный алгоритм поиска центра окружности  по удаленым точкам
//   //n-число точек с координатами xc,yc
//   step =
//   int i,j,k,it=0,it1=0;
//   double sx=0,sy=0,q=0,del,xcijk,ycijk,r=0;
//   float eps=0.1;
//   float xcf=0,ycf=0,dd,d_avr=0,d1,d2,d3;;
////   qDebug() << x[1] << y[1];
//   if(iter<10)
//    iter=10;

////   *xc=0; *yc=0;
//   if(n<3)
//      return 0;
//   //ищем расстояние меду точками
//   d_avr=k=0;
//   for(i=k=0;i<n-5;i++)
//    for(j=i;j<n;j++)
//     { dd=sqrt((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j]));
//       d_avr+=dd;
//       k++;
//     }
//   d_avr/=0.01+k;

//   it=it1=0;
//   do{//перебор по удаленым точкам
//       i=random(n-1);
//       j=random(n-1);
//       k=random(n-1);
//       d1=sqrt((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j]));
//       d2=sqrt((x[i]-x[k])*(x[i]-x[k])+(y[i]-y[k])*(y[i]-y[k]));
//       d3=sqrt((x[k]-x[j])*(x[k]-x[j])+(y[k]-y[j])*(y[k]-y[j]));

//       it1++;
//       if(!(d1>d_avr & d2>d_avr & d3>d_avr))
//         continue;
//               del=(x[k]-x[j])*(y[j]-y[i])-(x[j]-x[i])*(y[k]-y[j]);
//               if(fabs(del)<1e-6)
//                 continue;
//               xcijk=(y[k]-y[j])*(x[i]*x[i]+y[i]*y[i])+(y[i]-y[k])*(x[j]*x[j]+y[j]*y[j])+(y[j]-y[i])*(x[k]*x[k]+y[k]*y[k]);
//               xcijk=0.5*xcijk/del;
//               ycijk=(x[k]-x[j])*(x[i]*x[i]+y[i]*y[i])+(x[i]-x[k])*(x[j]*x[j]+y[j]*y[j])+(x[j]-x[i])*(x[k]*x[k]+y[k]*y[k]);
//               ycijk=-0.5*ycijk/del;
//               sx=sx+xcijk;
//               sy=sy+ycijk;
//               q+=1.0;
//       it++;
//     }while(it<iter & it1<1e5);

//        if(fabs(q)<1e-6)
//              return 0;

//      xcf=sx/q;
//      ycf=sy/q;
//      *xc=xcf+0.4;
//      *yc=ycf+0.4;
//      r=0;
//     for(i=0;i<n;i++)
//      r+=sqrt((x[i]-xcf)*(x[i]-xcf)+(y[i]-ycf)*(y[i]-ycf));
//     r/=(n+0.01);

//     return r;
//  }


//  float Circle_Conjugate_Gradient(int n,float x[],float y[],int *xc,int *yc)
//  {//градиентый метод поиска центра окружности
//   //n-число точек с координатами xc,yc

//   int i,it,xc1 = 0,yc1 = 0,imax=20;
//   double d,ds,r=0;
//   double J,dJx,dJy,dJ,sum1,sum2,sum3,Jp,dJp,dJxp,dJyp,ux=0,uy=0,bet,uxp,uyp,Jin,Lam,dJLam,d2JLam;
//   float eps1=0.01f,eps2=0.01f;
//   float xcf=0,ycf=0,R1=0;


//   //*xc=0; *yc=0;
//   if(n<3)
//      return 0;
// /* //тест
//   n=5;
//   x[0]=30;  y[0]=68;
//   x[1]=50;  y[1]=-6;
//   x[2]=110; y[2]=-20;
//   x[3]=35;  y[3]=15;
//   x[4]=45;  y[4]=97;
//*/
//     //начальное приближение
//  //   R1=Circle_Three_Points(n,x,y,&xc1,&yc1);

//     xcf=xc1; ycf=yc1;


//     for(i=r=0;i<n;i++)//вычисляем r  (5)
//     { d=sqrt((x[i]-xcf)*(x[i]-xcf)+(y[i]-ycf)*(y[i]-ycf));
//       r+=d;
//     }
//     r/=n;   R1=r;

//     for(i=sum1=ds=0;i<n;i++) //вычисляем J  (6)
//     { d=sqrt((x[i]-xcf)*(x[i]-xcf)+(y[i]-ycf)*(y[i]-ycf));
//       sum1+=d;
//       ds+=d*d;
//     }
//     J=ds-sum1*sum1/n;

//      for(i=sum1=sum2=0;i<n;i++) //вычисляем dJ  (7)
//        { d=sqrt((double)(x[i]-xcf)*(x[i]-xcf)+(y[i]-ycf)*(y[i]-ycf));
//          if(d<0.1) d=1;
//          sum1+=(xcf-x[i])*(d-R1)/d;
//          sum2+=(ycf-y[i])*(d-R1)/d;
//     }
//      dJx=2*sum1;
//      dJy=2*sum2;
//      dJ=sqrt(dJx*dJx+dJy*dJy);
//      if(fabs(J)<eps1 || dJ<eps2)
//       { *xc=xc1; *yc=yc1;
//         return (float)R1;
//       }
//       Jp=J; dJp=dJ; dJxp=dJx; dJyp=dJy;
//       for(it=0;it<imax;it++) //итерации
//        {//поиск направлений
//          ux=-dJx; uy=-dJy;
//          if(it>0)
//           { bet=(dJx*(dJx-dJxp)+dJy*(dJy-dJyp))/(dJp*dJp);    // Polak-Ribiere coefficient
//             bet=(dJ*dJ)/(dJp*dJp); //Fletcher–Reeves
//             ux=ux+uxp*bet;
//             uy=uy+uyp*bet;
//           }

//          dJp=dJ; dJxp=dJx; dJyp=dJy;
//          uxp=ux;
//          uyp=uy;
//          int kol_it=0;
//          do{// rough minimization along the search direction (a few Newton steps)
//             Jin=J;
//             //compute dJ=dl using equation (9)
//             dJLam=0;
//             for(i=0;i<n;i++)
//              { d=sqrt((double)(x[i]-xcf)*(x[i]-xcf)+(y[i]-ycf)*(y[i]-ycf));
//                if(d<0.1) d=1;
//                dJLam+=((xcf-x[i])*ux+(ycf-y[i])*uy)*(d-R1)/d;
//              }
//              dJLam=2*dJLam;
//             //compute d2J=dl2 using equation (10)
//             d2JLam=sum1=sum2=sum3=0;
//             for(i=0;i<n;i++)
//              { d=sqrt((double)(x[i]-xcf)*(x[i]-xcf)+(y[i]-ycf)*(y[i]-ycf));
//                if(d<0.1) d=1;
//                sum1+=2*(ux*ux+uy*uy)*(d-R1)/d;
//                sum2+=((xcf-x[i])*ux+(ycf-y[i])*uy)/d;
//                sum3+=((xcf-x[i])*ux+(ycf-y[i])*uy)*((xcf-x[i])*ux+(ycf-y[i])*uy)/(d*d*d);
//              }
//              sum2=sum2*sum2*2/n;
//              sum3=sum3*2*R1;
//              d2JLam=sum1-sum2+sum3;
//              Lam=-dJLam/d2JLam;
//              xcf=xcf+Lam*ux;
//              ycf=ycf+Lam*uy;

//              for(i=r=0;i<n;i++)//вычисляем r  (5)
//                  { d=sqrt((double)(x[i]-xcf)*(x[i]-xcf)+(y[i]-ycf)*(y[i]-ycf));
//                   r+=d;
//                  }
//                  R1=r/n;

//              for(i=sum1=0;i<n;i++) //вычисляем J  (6)
//                { d=sqrt((double)(x[i]-xcf)*(x[i]-xcf)+(y[i]-ycf)*(y[i]-ycf));
//                  sum1+=d;
//                   ds+=d*d;
//                 }
//              J=ds-sum1*sum1/n;

//              for(i=sum1=sum2=0;i<n;i++) //вычисляем dJ  (7)
//                { d=sqrt((double)(x[i]-xcf)*(x[i]-xcf)+(y[i]-ycf)*(y[i]-ycf));
//                  if(d<0.1) d=1;
//                  sum1+=(xcf-x[i])*(d-R1)/d;
//                  sum2+=(ycf-y[i])*(d-R1)/d;
//                  }
//               dJx=2*sum1;
//               dJy=2*sum2;
//               dJ=sqrt(dJx*dJx+dJy*dJy);
//               kol_it++;
//          } while((kol_it<1) & (it<imax) & (fabs(J-Jin)/J > eps2) & ((fabs(J)+dJ)<1e10) );

//          if(fabs(J-Jp)/J<eps1 || dJ<eps2 || dJ>1e10 )
//           break;
//        } //итерации i
//        *xc=xcf+0.4;
//        *yc=ycf+0.4;
//        if( (fabs(J)>10*eps1) & (dJ>10*eps2))
//          *xc=*yc=R1=0;

//         return R1;
//  }

//int CircleFit(int N, TPoint P[], double *pa, double *pb, double *pr)
//{
//  /* user-selected parameters */
//  const int maxIterations = 256;
//  const double tolerance = 1e-06;

//  double a, b, r;

//  /* compute the average of the data points */
//  int i, j;
//  double xAvr = 0.0;
//  double yAvr = 0.0;

//  for (i = 0; i < N; i++) {
//    xAvr += P[i].x;
//    yAvr += P[i].y;
//  }
//  xAvr /= N;
//  yAvr /= N;

//  /* initial guess */
//  a = xAvr;
//  b = yAvr;

//  for (j = 0; j < maxIterations; j++) {
//    /* update the iterates */
//    double a0 = a;
//    double b0 = b;

//    /* compute average L, dL/da, dL/db */
//    double LAvr = 0.0;
//    double LaAvr = 0.0;
//    double LbAvr = 0.0;

//    for (i = 0; i < N; i++) {
//      double dx = P[i].x - a;
//      double dy = P[i].y - b;
//      double L = sqrt(dx * dx + dy * dy);
//      if (fabs(L) > tolerance) {
//        LAvr += L;
//        LaAvr -= dx / L;
//        LbAvr -= dy / L;
//      }
//    }
//    LAvr /= N;
//    LaAvr /= N;
//    LbAvr /= N;

//    a = xAvr + LAvr * LaAvr;
//    b = yAvr + LAvr * LbAvr;
//    r = LAvr;

//    if (fabs(a - a0) <= tolerance && fabs(b - b0) <= tolerance)
//      break;
//  }

//  *pa = a;
//  *pb = b;
//  *pr = r;

//  return (j < maxIterations ? j : -1);
//}



//  float Ellipse_Three_Points(int n,float x[],float y[],int *xc,int *yc,float *a,float *b)
//  {//трехточечный алгоритм поиска центра окружности
//   //n-число точек с координатами xc,yc

////   Из этого также следует, что центр эллипса и центр масс исходных точек совпадают. Поэтому для простоты будем считать, что эти центры находятся в начале координат.
////Итак даны 3 точки (x1,y1), (x2,y2), (x3,y3).
////Введём дополнительные переменные:
////x12 = x1 - x2
//// y12 = y1 - y2
//// x23 = x2 - x3
//// y23 = y2 - y3
//// x31 = x3 - x1
//// y31 = y3 - y1
////Эллипс зададим в виде уравнения:
////a*x*x - b*x*y + c*y*y = d
////Тогда:
////a = y12*y12 + y23*y23 + y31*y31 - y31*y12 - y12*y23 - y23*y31
//// b = 3 * ( y12*x12 + y23*x23 + y31*x31 )
//// c = x12*x12 + x23*x23 + x31*x31 - x31*x12 - x12*x23 - x23*x31
//// d = ( y31*x23 - y23*x31 ) ^ 2


//   int i,j,k;
//   float sx=0,sy=0,q=0,del,xcijk,ycijk,r=0;
//   float eps=0.1;
//   float xcf=0,ycf=0;


//   *xc=0; *yc=0;
//   if(n<3)
//      return 0;
//   for(i=0;i<n-2;i++)
//    for(j=i+1;j<n-1;j++)
//        for(k=j+1;k<n;k++)
//         {
//           del=(x[k]-x[j])*(y[j]-y[i])-(x[j]-x[i])*(y[k]-y[j]);
//           if(fabs(del)>eps)
//            {
//               xcijk=(y[k]-y[j])*(x[i]*x[i]+y[i]*y[i])+(y[i]-y[k])*(x[j]*x[j]+y[j]*y[j])+(y[j]-y[i])*(x[k]*x[k]+y[k]*y[k]);
//               xcijk=0.5*xcijk/del;
//               ycijk=(x[k]-x[j])*(x[i]*x[i]+y[i]*y[i])+(x[i]-x[k])*(x[j]*x[j]+y[j]*y[j])+(x[j]-x[i])*(x[k]*x[k]+y[k]*y[k]);
//               ycijk=-0.5*ycijk/del;
//               sx=sx+xcijk;
//               sy=sy+ycijk;
//               q+=1.0;
//            }

//         }

//        if(fabs(q)<1e-6)
//              return 0;

//      xcf=sx/q;
//      ycf=sy/q;
//      *xc=xcf+0.4;
//      *yc=ycf+0.4;
//      r=0;
//     for(i=0;i<n;i++)
//      r+=sqrt((x[i]-xcf)*(x[i]-xcf)+(y[i]-ycf)*(y[i]-ycf));
//     r/=n;

//     return r;
//  }

 //Поиск центра окружности и радиуса по удаленым точкам
 float GetR_XC_YC_3P(int xc_0, int yc_0, int nFindGrad, QImage *pImage, int *x_ret, int *y_ret, float *aSunCirX, float *aSunCirY)
 {
  int nRaysFind = N_POINT_FIND_CIRC; //число лучей
 // float ax_circle[N_POINT_FIND_CIRC];
 // float ay_circle[N_POINT_FIND_CIRC];
  *x_ret = xc_0;
  *y_ret = yc_0;

  // Метод нахождения точек на краю диска Солнца(лучи из начальной точки)
  FindCirclePoints(&nRaysFind, xc_0, yc_0, nFindGrad, pImage, aSunCirX, aSunCirY);

  //трехточечный алгоритм поиска центра окружности  по удаленым точкам
  float Rsun = Circle_Three_Points_Rand(nRaysFind, nRaysFind*1.0, aSunCirX, aSunCirY, x_ret, y_ret);
  //градиентый метод поиска центра окружности
  //float Rsun = Circle_Conjugate_Gradient(nRaysFind, ax_circle, ay_circle, x_ret, y_ret);

  return Rsun;

 }

 //Поиск центра окружности и радиуса по удаленым точкам (буфер камеры)
float GetR_XC_YC_3P(int xc_0, int yc_0, int nFindGrad, unsigned char* imageBuf, int width, int height, int *x_ret, int *y_ret, float *aSunCirX, float *aSunCirY)
{
 float Rsun = 0;
 int nRaysFind = N_POINT_FIND_CIRC; //число лучей
// float ax_circle[N_POINT_FIND_CIRC];
// float ay_circle[N_POINT_FIND_CIRC];
 for (int i=0; i < N_POINT_FIND_CIRC; i++)
    aSunCirX[i] = aSunCirY[i] = -1;
 *x_ret = xc_0;
 *y_ret = yc_0;

 // Метод нахождения точек на краю диска Солнца(лучи из начальной точки)
 FindCirclePoints(&nRaysFind, xc_0, yc_0, nFindGrad, imageBuf, width, height, aSunCirX, aSunCirY);

 if (nRaysFind > 2)
    //трехточечный алгоритм поиска центра окружности  по удаленым точкам
    Rsun = Circle_Three_Points_Rand(nRaysFind, nRaysFind*1.0, aSunCirX, aSunCirY, x_ret, y_ret);
    //градиентый метод поиска центра окружности
    //Rsun = Circle_Conjugate_Gradient(nRaysFind, ax_circle, ay_circle, x_ret, y_ret);
 //qDebug() << nRaysFind;
 //float Rsun = 333;
 //qDebug()<< "N->" << nRaysFind << " R=" << Rsun;
 return Rsun;

}

 //  Метод нахождения точек на краю диска Солнца(лучи из начальной точки)
 void FindCirclePoints(int *n_0, int xc_0, int yc_0, int nFindGrad, QImage *pImage, float *ax_ret, float *ay_ret)
 {//  n - число лучей (угол=360/n) (может измениться и быть меньше заданного, если Солнце за краем)
  //  xc_0 и yc_0 - предварительный центр(должен быть точно внутри диска Солнца)

   int nFindStep = 3;
//   nFindGrad = 22;
   int width  = pImage->width()  - 1;
   int height = pImage->height() - 1;
   int i, nGrad, r, dx, dy, x0, y0, x1, y1;
   int n = *n_0;
   int n_new=0;

   float fAngle;
   for (i = 0; i < n; i++)
   {
       fAngle= 2 * pi_my * i / n;
       nGrad = 0; r = 0;
       x0=xc_0; y0=yc_0;
       x1 = -1; //Условие ошибки
       do {
         r = r + nFindStep;
         dx=int((float)r * cos(fAngle));
         dy=int((float)r * sin(fAngle));
         x1 = xc_0 + dx;
         y1 = yc_0 + dy;
//         qDebug() << x1 << y1;
         if (x1 < 0 || y1 < 0 || x1 > width || y1 > height)
         {
            if ((x1 < 0 && dx > 0) || (y1 < 0 && dy > 0) ||
                (x1 > width && dx < 0) || (y1 > height && dx < 0)) continue;
            x1 = -1; //Условие ошибки
            break;
         }

//         qDebug() << r << qGray(pImage->pixel(x1,y1));
         nGrad = qGray(pImage->pixel(x0,y0)) - qGray(pImage->pixel(x1,y1));
//        qDebug() << r << nGrad;
//        qDebug() << qGray(pImage->pixel(x1,y1)) << qGray(pImage->pixel(x0,y0));
//        pImage->setPixel(x1, y1, 255l + 256l*255l);

         x0 = x1; y0 = y1;
       } while (nGrad < nFindGrad);

       if (x1 == -1) continue; //ошибка

       ax_ret[n_new] = (float) x0 - float(nFindStep) * cos(fAngle) / 2.0;
       ay_ret[n_new] = (float) y0 - float(nFindStep) * sin(fAngle) / 2.0;
//       qDebug() << ax_ret[n_new] << ay_ret[n_new] << r << n_new;
       n_new ++;
   }
//   qDebug() << ax_ret[n_new] << ay_ret[n_new] << r << n_new;
//   qDebug() << qGray(pImage->pixel(width/2, height/2));
//   qDebug() << qGray(pImage->pixel(xc_0, yc_0));


   *n_0 = n_new;
}
// Метод нахождения точек на диске Солнца (буфер камеры)
void FindCirclePoints(int *n_0, int xc_0, int yc_0, int nFindGrad, unsigned char* imageBuf, int width, int height, float *ax_ret, float *ay_ret)
{//  n - число лучей (угол=360/n) (может измениться и быть меньше заданного, если Солнце за краем)
 //  xc_0 и yc_0 - предварительный центр(должен быть точно внутри диска Солнца)

  int nFindStep = 7;
  nFindGrad = 23;
  int R_max = 333 * 2;
  //QTime t;
//  int width  = pImage->width()  - 1;
//  int height = pImage->height() - 1;
  int i, nGrad, r, dx, dy, x0, y0, x1, y1;
  int n = *n_0;
  int n_new=0;

  float fAngle;
  for (i = 0; i < n; i++)
  {
      fAngle= 2 * pi_my * i / n;
      nGrad = 0; r = 0;
      x0=xc_0; y0=yc_0;
      x1 = -1; //Условие ошибки 
     // t.start();
      do {
        r = r + nFindStep;
        dx=int((float)r * cos(fAngle));
        dy=int((float)r * sin(fAngle));
        x1 = xc_0 + dx;
        y1 = yc_0 + dy;
//         qDebug() << x1 << y1;
        if (x1 < 0 || y1 < 0 || x1 > width || y1 > height)
        {
           if (r < R_max)
            if ((x1 < 0 && dx > 0) || (y1 < 0 && dy > 0) ||
                (x1 > width && dx < 0) || (y1 > height && dx < 0)) continue;
            x1 = -1; //Условие ошибки
            break;
        }

//         qDebug() << r << imageBuf[x1 + y1*width];
        nGrad = imageBuf[x0 + y0*width] - imageBuf[x1 + y1*width];
//        qDebug() << r << nGrad;
//        qDebug() << imageBuf[x0 + y0*width] << imageBuf[x1 + y1*width];

        x0 = x1; y0 = y1;
      } while (nGrad < nFindGrad);

//      qDebug("FindCirclePoints времени заняло: %d мс", t.elapsed());

      if (x1 == -1) continue; //ошибка

      ax_ret[n_new] = (float) x0 - float(nFindStep) * cos(fAngle) / 2.0;
      ay_ret[n_new] = (float) y0 - float(nFindStep) * sin(fAngle) / 2.0;
//       qDebug() << ax_ret[n_new] << ay_ret[n_new] << r << n_new;
      n_new ++;
  }
//   qDebug() << ax_ret[n_new] << ay_ret[n_new] << r << n_new;
//   qDebug() << imageBuf[width/2 + height*width/2));

  *n_0 = n_new;
}

 //Вывод окружности на image
 void PlotCirclePix(QImage *pImage, int xc, int yc, int r, uint rgb)
 {//  n - число лучей (угол=360/n) (может измениться и быть меньше заданного, если Солнце за краем)

   float alp, f_step = 1.0 / (2.0*pi_my*r);
   int width  = pImage->width()  - 1;
   int height = pImage->height() - 1;
   int x, y;
   for (alp=0.0; alp <= 2.0*pi_my; alp+=f_step)
   {
     x = xc + r * cos(alp);
     y = yc + r * sin(alp);
     if (x < 0 || y < 0 || x > width || y > height) continue;
     pImage->setPixel(x, y, rgb);
   }
 }

 //Вывод крестика на QPainter
  void PlotCross(QPainter *pPaint, int xc, int yc, int d, QColor color)
 {
      QPen pen;
      pen.setColor(color);
      pen.setWidth(4);
      pPaint->setPen(pen);

      pPaint->drawLine(xc - d/2, yc - d/2, xc + d/2, yc + d/2);
      pPaint->drawLine(xc - d/2, yc + d/2, xc + d/2, yc - d/2);

 }
