#include <QTimer>
#include "Dlg_Coelostat.h"
#include "Dlg_Coelostat_Scans.h"
#include "ui_Dlg_Coelostat_Scans.h"

Dlg_Coelostat_Scans::Dlg_Coelostat_Scans(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dlg_Coelostat_Scans)
{
    setObjectName("Dlg_Coelostat_Scans");
    m_OnStartDialog = false;
    ui->setupUi(this);
    QTimer::singleShot(0, this, SLOT(slotDelayedInit()));

}
void Dlg_Coelostat_Scans::slotDelayedInit()
{
    ui->cmb_start_Znak->addItem(QString('-'));
    ui->cmb_start_Znak->addItem(QString('+'));
    ui->cmb_end_Znak->addItem(QString('-'));
    ui->cmb_end_Znak->addItem(QString('+'));
    ui->doubleSpinBox_speed->setValue(m_dlg_fScan_Speed_MinutOfMinut);
    ui->checkBox_ScanBack->setCheckState(Qt::CheckState(m_dlg_bScan_On_Scan_back));
    ui->checkBox_OnCorrDelta->setCheckState(Qt::CheckState(m_dlg_bScan_On_Delta_Correction));
//    qDebug() << "m_dlg_bScan_On_Scan_back=" << m_dlg_bScan_On_Scan_back <<"m_dlg_bScan_On_Delta_Correction=" << m_dlg_bScan_On_Delta_Correction;

    SetDlgCurItem();//Установка элементов в диалоге по значениям
    m_OnStartDialog = true;
}

Dlg_Coelostat_Scans::~Dlg_Coelostat_Scans()
{
    delete ui;
}

//Установка элементов в диалоге по значениям
void Dlg_Coelostat_Scans::SetDlgCurItem()
{
    QString sMsg;
    cAngle Angle_b=funRadian2Angle((m_dlg_fScan_Angle_Minut_Pos_Beg /60.0) * pi_my /180.0);//Перевод радиан в угол(грд, мин, сек)
    int n;
    //sTmp=m_tDataTime_cur.toString("hh:mm:ss"); //для просмотра в QString
    if( Angle_b.cZnak == '-') ui->cmb_start_Znak->setCurrentIndex(0);
                         else ui->cmb_start_Znak->setCurrentIndex(1);
    n = Angle_b.iMinut;
    if (n < 10) ui->spinBox_start_mm->setPrefix("0"); else ui->spinBox_start_mm->setPrefix("");
    ui->spinBox_start_mm->setValue(n);

    n = int(Angle_b.fSec);
    if (n < 10) ui->spinBox_start_ss->setPrefix("0"); else ui->spinBox_start_ss->setPrefix("");
    ui->spinBox_start_ss->setValue(n);

    cAngle Angle_e=funRadian2Angle((m_dlg_fScan_Angle_Minut_Pos_End /60.0) * pi_my /180.0);//Перевод радиан в угол(грд, мин, сек)
    //qDebug() << m_dlg_fScan_Angle_Minut_Pos_Beg << m_dlg_fScan_Angle_Minut_Pos_End << bool( Angle_e.cZnak == '-');;
    if( Angle_e.cZnak == '-') ui->cmb_end_Znak->setCurrentIndex(0);
                         else ui->cmb_end_Znak->setCurrentIndex(1);
    n = Angle_e.iMinut;
    if (n < 10) ui->spinBox_end_mm->setPrefix("0"); else ui->spinBox_end_mm->setPrefix("");
    ui->spinBox_end_mm->setValue(n);

    n = int(Angle_e.fSec);
    if (n < 10) ui->spinBox_end_ss->setPrefix("0"); else ui->spinBox_end_ss->setPrefix("");
    ui->spinBox_end_ss->setValue(n);
}

void Dlg_Coelostat_Scans::on_cmb_start_Znak_currentIndexChanged(int index)
{
    if (!m_OnStartDialog) return;
    if (index == 0 )m_dlg_fScan_Angle_Minut_Pos_Beg = -abs(m_dlg_fScan_Angle_Minut_Pos_Beg);
               else m_dlg_fScan_Angle_Minut_Pos_Beg = +abs(m_dlg_fScan_Angle_Minut_Pos_Beg);
    SetDlgCurItem();//Установка элементов в диалоге по значениям
}

void Dlg_Coelostat_Scans::on_spinBox_start_mm_valueChanged(int arg1)
{
    if (!m_OnStartDialog) return;
    cAngle Angle=funRadian2Angle((m_dlg_fScan_Angle_Minut_Pos_Beg /60.0) * pi_my /180.0);//Перевод радиан в угол(грд, мин, сек)
    Angle.iMinut=arg1;
    m_dlg_fScan_Angle_Minut_Pos_Beg = funAngle2Radian(Angle) * 60.0 * 180.0 / pi_my;
    SetDlgCurItem();//Установка элементов в диалоге по значениям
}

void Dlg_Coelostat_Scans::on_spinBox_start_ss_valueChanged(int arg1)
{
    if (!m_OnStartDialog) return;
    cAngle Angle=funRadian2Angle((m_dlg_fScan_Angle_Minut_Pos_Beg /60.0) * pi_my /180.0);//Перевод радиан в угол(грд, мин, сек)
    Angle.fSec=arg1;
    m_dlg_fScan_Angle_Minut_Pos_Beg = funAngle2Radian(Angle) * 60.0 * 180.0 / pi_my;
    SetDlgCurItem();//Установка элементов в диалоге по значениям
}

void Dlg_Coelostat_Scans::on_cmb_end_Znak_currentIndexChanged(int index)
{
    if (!m_OnStartDialog) return;
    if (index == 0 )m_dlg_fScan_Angle_Minut_Pos_End = -abs(m_dlg_fScan_Angle_Minut_Pos_End);
               else m_dlg_fScan_Angle_Minut_Pos_End = +abs(m_dlg_fScan_Angle_Minut_Pos_End);
    SetDlgCurItem();//Установка элементов в диалоге по значениям
}

void Dlg_Coelostat_Scans::on_spinBox_end_mm_valueChanged(int arg1)
{
    if (!m_OnStartDialog) return;
    cAngle Angle=funRadian2Angle((m_dlg_fScan_Angle_Minut_Pos_End /60.0) * pi_my /180.0);//Перевод радиан в угол(грд, мин, сек)
    Angle.iMinut=arg1;
    m_dlg_fScan_Angle_Minut_Pos_End = funAngle2Radian(Angle) * 60.0 * 180.0 / pi_my;
    SetDlgCurItem();//Установка элементов в диалоге по значениям
}

void Dlg_Coelostat_Scans::on_spinBox_end_ss_valueChanged(int arg1)
{
    if (!m_OnStartDialog) return;
    cAngle Angle=funRadian2Angle((m_dlg_fScan_Angle_Minut_Pos_End /60.0) * pi_my /180.0);//Перевод радиан в угол(грд, мин, сек)
    Angle.fSec=arg1;
    m_dlg_fScan_Angle_Minut_Pos_End = funAngle2Radian(Angle) * 60.0 * 180.0 / pi_my;
    SetDlgCurItem();//Установка элементов в диалоге по значениям
}

void Dlg_Coelostat_Scans::on_doubleSpinBox_speed_valueChanged(double arg1)
{
    if (!m_OnStartDialog) return;
    m_dlg_fScan_Speed_MinutOfMinut = arg1;
}

void Dlg_Coelostat_Scans::on_checkBox_ScanBack_clicked(bool checked)
{
    if (!m_OnStartDialog) return;
    m_dlg_bScan_On_Scan_back = checked;
//    qDebug() << m_dlg_bScan_On_Scan_back;
}

void Dlg_Coelostat_Scans::on_checkBox_OnCorrDelta_clicked(bool checked)
{
    if (!m_OnStartDialog) return;
    m_dlg_bScan_On_Delta_Correction = checked;
//    qDebug() << m_dlg_bScan_On_Delta_Correction;
}
