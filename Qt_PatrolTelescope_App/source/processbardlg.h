#ifndef PROCESSBARDLG_H
#define PROCESSBARDLG_H

#include <QWidget>
#include <QDialog>

namespace Ui {
class ProcessBarDlg;
}

class ProcessBarDlg : public QDialog
{
    Q_OBJECT
public:
    explicit ProcessBarDlg(QWidget *parent = 0);
    ~ProcessBarDlg();
    void setMaximum(int val);
private:
    Ui::ProcessBarDlg *ui;
public slots:
    void setValue(int val, QString text);
    void showDlg();
};

#endif // PROCESSBARDLG_H
