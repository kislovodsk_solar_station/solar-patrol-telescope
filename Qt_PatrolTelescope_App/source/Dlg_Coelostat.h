#ifndef Dlg_Coelostat_H
#define Dlg_Coelostat_H

#include <QWidget>
#include <QPoint>
#include "sunpos.h"
#include "controllers_ethernet_macros.h"

//#define config_coelostat "f:/NEW TELESCOP/Soft/solar-patrol-telescope/config\\SM_Coelostat.cfg"

//Оси
#define AXIS_ALPHA          0
#define AXIS_DELTA          1
#define AXIS_SCAN_SPECTR    2
#define AXIS_SCAN_COELOSTAT 3

//для значений в массиве m_anCur_SM_SpeedMode[AXIS]
#define SM_STOP       0
#define SM_FORV_HOUR  1
#define SM_SCAN_START 1 /*Для сканирования*/
#define SM_FORV_SLOW  2
#define SM_FORV_FAST  3
#define SM_REVR_SLOW -1
#define SM_REVR_FAST -2

//для значений в массиве m_anCur_SM_InPortMode[AXIS]
#define SM_PORT_OFF     0
#define SM_PORT_ON_MIN -1
#define SM_PORT_ON_MAX  1

//Множители для скоростей режимов SLOW и FAST от максимальной скорости ШД данной оси
#define SM_K_SPEED_SLOW 0.3
#define SM_K_SPEED_FAST 1.0

//Альфа - скорость часового привода солнечной скорости по умолчанию (ш/с) --> (527(микрошаг-16)*4)= 2108(микрошаг-64)
#define SM_ALPHA_SPEED_SUN_INIT 2108

//Альфа -  Коэффициент скорости часового привода для второй оси (если управляем на целостате по альфе двумя приводами)
#define SM_ALPHA_K_SPEED_TWO_HALF_V 1.0000


//Дельта - кол-во шагов ШД для перемещения на 1 градус (шаг/грд) --> (18100(микрошаг-16)*4)= 72400(микрошаг-64)
#define SM_DELTA_STEP_DEGREE_INIT 72400

//Для дельты. Сколько угловых минут от концевика MIN до MAX
#define SM_DELTA_ANGLE_MINUT_MIN2MAX 530

//Для скана на целостате. Шагов ШД для перемещения на 1 градус (шаг/грд)- актуатор(-16'->+16' все 25мм) --> (20000(микрошаг-16)*4)= 80000(микрошаг-64)
//Кол-во шагов ШД для перемещения на 1 градус (шаг/грд) -> эксцентрик на целостате (122500(микрошаг-16)*4)= 490000(микрошаг-64)
#define SM_SCAN_STEP_DEGREE_INIT 20000
//Для скана на целостате. Угол - положения начала сканирования в минутах дуги
#define SM_SCAN_POS_BEG_MINUT_INIT -16
//Для скана на целостате. Угол - положения конца  сканирования в минутах дуги
#define SM_SCAN_POS_END_MINUT_INIT 16
//Для скана на целостате. Скорость сканирования (минут дуги в минуту)
#define SM_SCAN_SPEED_MINUT_OF_MINUT_INIT 32


//Для функции определения суточной на изображении гида. На сколько отводим по дельте.
#define DELTA_ANGLE_MINUT_FOR_DAYLY_SUN  15
//Для функции определения суточной на изображении гида. На сколько останавливаем по альфе. 1мин=15` (90s->~22`)
#define ALPHA_TIME_SECONDS_FOR_DAYLY_SUN 90


//Разница часового пояса к UT
#define TIME_HOUR_LOC2UT +3

//Для штатного выключения телескопа по времени захода Солнца
#define MINUT_MINUS_SUNSET_OFF_TELESCOPE 60


namespace Ui {
class Dlg_Coelostat;
}

class Guide;
class Scanner;
class CInitControllers;
class CControllerEthernet;

class Dlg_Coelostat : public QWidget
{
    Q_OBJECT

public:
    explicit Dlg_Coelostat(CInitControllers *controllers, QString configDir, QWidget *parent = 0);
    ~Dlg_Coelostat();

public:
    Guide *m_pGuide;                          //Ссылка на класс гидирования с помощью камеры
    CInitControllers  *m_pAll_CNLRs_SM;      //Ссылка на класс инициализации всех рабочих контроллеров ШД
    CControllerEthernet *m_pCNLR_view_info;  //Ссылка на контроллер ШД, по которому выводится информация
    Scanner *m_pSolarScanner;

    QPoint m_PosWin;        //Положение окна X,Y

    QTimer *m_pTimer_View ; //Таймер для просмотра диалога
//    QTimer *m_pTimer_CNTL; //Таймер для поддержания работы контроллера ШД

    QDateTime m_tDataTime_Timer_View_pre;  //Время предыдущего срабатывания таймера


    bool m_bool_On_Correction_Alpha;      //Работаем в режиме коррекции положения по гиду Alpha
    bool m_bool_On_Correction_Delta;      //Работаем в режиме коррекции положения по гиду Delta

    //Для альфы
    QDateTime m_tDataTime_Alpha_Go;        //Время как положение куда идем в QTime
    bool m_bool_Alpha_On_Go;               //Работаем в режиме поиска положения Alpha
    bool m_bool_Alpha_On_Go_Cur_Time;      //Работаем в режиме поиска положения по текущему времени Alpha

    double m_fSM_KSpeed_Alpha_Sun;         //Множитель для часовой (Солнечной) скорости от макимальной скорости ШД
    long  m_nSM_Speed_Alpha_Sun_StepPerSec;//Часовая (солнечная) скорость ШД в шагах в секунду
    QDateTime m_Alpha_SysTime_Pos_0;       //Время как текущее положение 0 в SYSTEMTIME
    double m_fAlpha_Steps_From_Pos_0;       //Шагов двигателя от 0 установки времени альфы

    //Для альфы - суточная
    bool m_bool_Get_Daily_Sun_Alpha_On;    //Работаем в режиме определения суточной по изображению Cолнца на гиде. По альфе.
    double m_fAngleDaily_Sun_Alpha;        //Наклон суточной на изображении гида. По альфе.
//    QDateTime m_Alpha_SysTime_Pos_Pre;   //Время как положение альфы куда вернуться. Для определения наклона суточной. Не нужно если вернемся по текущему времени
    double m_afXC_YC_R_Alpha_pre[3];       //Массив параметров положения Солнца на гиде. Для определения наклона суточной по альфе.
    double m_dCorrection_Alpha_Seconds;    //Для работы в режиме коррекции положения по гиду Сколько секунд коррекируем. Если отрицательное значение, то остановка и ждём эти секунды;
    QDateTime m_tDataTime_SunSet;          //Время заката Солнца
    bool m_bool_ParkSunSet;                //Отслеживать положение захода Солнца


    //Для дельты
    double m_fAngle_Delta_Go;              //Угол - положение дельты куда идем в радианах
    bool m_bool_Delta_On_Go;               //Работаем в режиме поиска положения Delta
    bool m_bool_Delta_On_Go_Cur_Time;      //Работаем в режиме поиска положения по текущему времени Delta
    bool m_bool_Delta_Centering;           //Работаем в режиме центрирования дельты от концевиков
    bool m_bool_Delta_Centering_On;        //Для установки в конце центрирования значения текущей координаты Солнца
    double m_fDelta_Angle_Pos_0;           //Угол - текущее положение 0 в радианах
    double m_fDelta_SM_Steps_1deg;         //Кол-во шагов ШД для перемещения на 1 градус (шаг/грд)
    double m_fDelta_Steps_From_Pos_0;      //Шагов двигателя от 0 установки времени дельты
    double m_fDelta_AnglMinut_Min2Max;     //Для дельты. Сколько угловых минут от концевика MIN до MAX

    //Для дельты - суточная
    bool m_bool_Get_Daily_Sun_Delta_On;    //Работаем в режиме определения суточной по изображению Cолнца на гиде. По дельте.
    double m_fAngleDaily_Sun_Delta;        //Наклон суточной на изображении гида. По дельте (радианы).
    double m_fDelta_Angle_Pos_Pre;         //Угол - положение дельты куда вернуться в радианах. Для определения наклона суточной
    double m_afXC_YC_R_Delta_pre[3];       //Массив параметров положения Солнца на гиде. Для определения наклона суточной по дельте.

    //Для скана на целостате
    double m_fAngle_Minut_Scan_Go;         //Угол - положение сканировния куда идем  в минутах дуги
    bool m_bool_Scan_On_Go;                //Работаем в режиме поиска положения для сканировния
    double m_fScan_Angle_Minut_Pos_0;      //Угол - текущее положение 0  в минутах дуги
    double m_fScan_Angle_Minut_Pos_Beg;    //Угол - положения начала сканирования в минутах дуги
    double m_fScan_Angle_Minut_Pos_End;    //Угол - положения конца  сканирования в минутах дуги
    double m_fScan_Speed_MinutOfMinut;     //Скорость сканирования (минут дуги в минуту)
    bool m_bScan_On_Scan_back;             //Работаем в режиме сканирования Солнца в обоих направлениях
    bool m_bScan_On_Delta_Correction;      //Работаем в режиме сканирования Солнца с коррекцией положения по дельте

    double m_fScan_SM_Steps_1deg;          //Кол-во шагов ШД для перемещения на 1 градус (шаг/грд)
    double m_fScan_Steps_From_Pos_0;       //Шагов двигателя от 0

    double m_afSM_KSpeed_cur     [N_MAX_AXIS];     //Множитель для скорости данного режима от максимальной скорости ШД
    int m_anCur_SM_SpeedMode     [N_MAX_AXIS];     //Режим работы для осей
    int m_anCur_SM_SpeedMode_pre [N_MAX_AXIS];     //Режим работы для осей предыдущее значение
    int m_anCur_SM_InPortMode    [N_MAX_AXIS];     //Режим работы по значению входных портов для осей

    bool m_bWinMax;                   //В диалоге большое или маленькое окно

public:
    void OnReset_Button_Info_Alpha();                       //Для Aльфы - изменение отображения кнопок от нажатия и соответствующего режима
    void on_Timer_Alpha();                                  //Для Aльфы - таймер отслеживания изменений положений и контроллера (запускается в on_Timer_View())
    QDateTime SM_Alpha_StepPos2SysTime();                   //Для Aльфы - перевод шагов двигателя от 0 к времени SYSTEMTIME положения Солнца
    int SM_Alpha_SysTime2StepPos(QDateTime tSys);           //Для Aльфы - перевод времени SYSTEMTIME в положение шагов двигателя от 0
    void On_Dlg_Set_Speed_Alpha_Hours(long );               //Для Aльфы - инициализировать часовую скорость и ее отоброжение в диалоге

    void on_Timer_Delta();                                  //Для Дельты - таймер отслеживания изменений положений и контроллера (запускается в on_Timer_View())
    double SM_Delta_StepPos2Angle();                        //Для Дельты - перевод шагов двигателя от 0 к углу (радианы) положения Солнца
    int    SM_Delta_Angle2StepPos(double dAngle);           //Для Дельты - перевод угла(радианы) положения Солнца в положение шагов двигателя от 0

    void OnReset_Button_Info_Scan();                        //Для скана на целостате - изменение отображения кнопок от нажатия и соответствующего режима
    void on_Timer_Scan();                                   //Для скана на целостате - таймер отслеживания изменений положений и контроллера (запускается в on_Timer_View())
    double SM_Scan_StepPos2Angle_Minut();                   //Для скана на целостате - перевод шагов двигателя от 0 к углу (радианы) положения
    int SM_Scan_Angle_Minut2StepPos(double dAngle_Minut);   //Для скана на целостате - перевод угла(радианы) положения в положение шагов двигателя от 0
    void On_Scan_Correction_Delta(double dScanAlpha_VStepsOfSec); //Для скана на целостате - изменение скорости по дельте при изменении скорости по альфе

    void On_Dlg_WinSizeMax(); //Диалог с полным размером
    void On_Dlg_WinSizeMin(); //Сокращенный диалог

    double Get_Daily_Sun_Delta(); //Функция определения суточной по изображению Cолнца на гиде. По дельте.
    double Get_Daily_Sun_Alpha(); //Функция определения суточной по изображению Солнца на гиде. По альфе.

    int Load_Coelostat_CFG(); //Создание или чтение файла конфигурации
    int Save_Coelostat_CFG(bool bMessageOn); //Сохронение файла конфигурации

public slots:
    void On_Correction_AlphaDelta(double fArcsecCorrAlpha, double fArcsecCorrDelta);    //Процедура коррекции положения по гиду
    void StopTimers();


private slots:
    void MyInitDialog();  //Инициализация диалога по таймеру (срабатывает один раз при запуске)
    void on_Timer_View(); //Таймер отслеживания изменений положений и контроллера

    void on_Btn_WinSizeMaxMin_clicked();
    void on_Cmb_CNT_Name_currentIndexChanged_my   (int index);


    void on_Btn_Alpha_START_pressed();
    void on_Btn_Alpha_STOP_pressed();
    void on_Btn_Alpha_FORV_FAST_pressed();
    void on_Btn_Alpha_FORV_SLOW_pressed();
    void on_Btn_Alpha_REV_FAST_pressed();
    void on_Btn_Alpha_REV_SLOW_pressed();

    void OnBtnClickedForvardSlow_Alpha();
    void OnBtnClickedReverseSlow_Alpha();

    void on_spinBox_VHours_valueChanged(int arg1);
    void on_Btn_Alpha_Set_Time_clicked();
    void on_Btn_Alpha_GoTo_clicked();
    void on_Cmb_Alpha_Axis_currentIndexChanged_my (int index);

    void OnBtnClickedStop_Delta();
    void OnBtnClickedForvardSlow_Delta();
    void OnBtnClickedForvardFast_Delta();
    void OnBtnClickedReverseSlow_Delta();
    void OnBtnClickedReverseFast_Delta();

    void on_Btn_Delta_FORV_FAST_pressed();
    void on_Btn_Delta_FORV_SLOW_pressed();
    void on_Btn_Delta_REV_SLOW_pressed();
    void on_Btn_Delta_REV_FAST_pressed();

    void on_Btn_Delta_Set_Delta_clicked();
    void on_Btn_Delta_GoTo_clicked();
    void on_Cmb_Delta_Axis_currentIndexChanged_my (int index);


    void on_Btn_Scan_START_pressed();
    void on_Btn_Scan_STOP_pressed();
    void on_Btn_Scan_NULL_pressed();
    void on_Btn_Scan_FORV_FAST_pressed();
    void on_Btn_Scan_REV_FAST_pressed();

    void on_Btn_Scan_Set_0_pressed();
    void on_Btn_Scan_Init_clicked();
    void on_Cmb_Scans_Axis_currentIndexChanged_my (int index);


    void on_Btn_GetDailyLineSun_Delta_pressed();

    void on_Btn_InitTelescope_pressed();

    void on_Btn_Alpha_REV_CORR_ONE_AXIS_pressed();

    void on_Btn_Alpha_FORV_CORR_ONE_AXIS_pressed();

    void on_Btn_Alpha_FORV_REV_CORR_ONE_AXIS_clicked();

signals:
    void sigCoelostatInit(bool b);
    void sigScanStop();
    void sigGuideStop(bool b);

private:
    Ui::Dlg_Coelostat *ui;
    QString config_coelostat;
    QMutex guideMutex;
};
#endif Dlg_Coelostat_H
