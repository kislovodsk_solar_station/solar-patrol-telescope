#include "guide.h"
#include <QtCore/qmath.h>                   /* sqrtf, sqrt, cos, sin, qAtan */
#include <QImage>
#include <QDateTime>
#include <QFileInfo>
#include <QTimer>
#include <QDebug>

Guide::Guide(Dlg_Coelostat *coelostat, Camera_JAI *camera, QObject *parent) : QObject(parent)
{
    setObjectName("Guide");
    pDlg_Coelostat = coelostat;
    pGuideCamera = camera;
    OnNullAll_OffsetСounter(); // Обнуляем счетчики и смещения
    meanRadiusSun = 0;
    maxIntensity_X = 0;
    maxIntensity_Y = 0;
    posX = 0;
    posY = 0;
    offsetX = 0;
    offsetY = 0;
    shiftX = 0;
    shiftY = 0;
    meanIntensitySun = 0;
    meanIntensityFrame = 0;
    meanOffsetR = 0;
    nOffsets = 0;
    stabilityCounter = 0;
    counterLastFrames = 0;
    arcsecMinOffsetError = GUIDE_ARCSEC_OFFSET_ERROR;
    guiding = false;
    inProcess = false;
    findSunError = false;
    StopScanning = false;
    memset(lastOffsets, 0, sizeof(lastOffsets));
    processTimer = new QTimer(this);
    last_intensities = new QVector<int>;

    connect(this, SIGNAL(sigInProcess()),
            this, SLOT(StartTimer()));
}

Guide::~Guide()
{
    delete last_intensities;
}

//-------------------------------------------------------------------------
//  Метод нахождения центра диска Солнца
//
void Guide::FindCenter(unsigned char* imageBuf, int width, int height)
{

    if(!imageBuf)
    {
        qDebug() << "FindCenter: изображение не получено!";
        return;
    }

    inProcess = true;
    startPosX = width / 2 + shiftX;
    startPosY = height / 2 + shiftY;

    // Находим максимальную яркость по диску + среднюю яркость по кадру
    FindMaxIntensity(imageBuf, width, height);

    //Поиск центра окружности и радиуса по удаленным точкам
    int Xsun_c_pix, Ysun_c_pix;
    float radiusSunPix = 0;
    int nFindGrad = int((float) meanIntensitySun * 0.25);
    radiusSunPix = GetR_XC_YC_3P(posX, posY, nFindGrad, imageBuf, width, height, &Xsun_c_pix, &Ysun_c_pix, aSunCirX, aSunCirY);

//    qDebug() << "R_sun = " << radiusSunPix;

    posX = Xsun_c_pix;
    posY = Ysun_c_pix;

    // Преобразование для отсчета с левого нижнего угла (изначально с верхнего)
    posY = height - posY;
    // Расчет смещений диска относительно начальной позиции
    offsetX = posX - startPosX;
    offsetY = posY - startPosY;
    float offsetR = sqrtf(offsetX*offsetX + offsetY*offsetY);
//    qDebug() << "offSetX = " << offsetX << "offsetY" <<offsetY;

    // Запускаем метод контроля зя облаками
    findSunError = CloudsControl(radiusSunPix, offsetR);
//    qDebug() << findSunError;
    // Проверка необходимости остановки сканирования
    isBreakScanning();


    // Складываем смещения, подсчитываем
    sumOffsetX += offsetX;
    sumOffsetY += offsetY;
    sumRSun += radiusSunPix;
    radius = radiusSunPix;
    counter++;

    inProcess = false;

//    bool b = guiding && (findSunError || NeedCorrection());
//    qDebug() << "findSunError = " << findSunError;
//    qDebug() << "sigFindSunError = " << b;

    emit sigFindSunError(guiding && (findSunError || NeedCorrection()));
    emit sigFindCenterFinished(QImage(imageBuf,width,height,QImage::Format_Grayscale8,0,0));
    return;
}

void Guide::isBreakScanning()
{
    if (pScanner->isSaving() && guiding)
    {
        if (counterLastFrames < 5 * pGuideCamera->parameters.fps)
        {
            StopScanning = StopScanning || findSunError;
            ++counterLastFrames;
            return;
        }
        else
        {
            if (StopScanning)
            {
                qDebug() << "SigScanBreak!";
                emit sigScanBreak();
//                isScanStopFromGuide = true;
                StopScanning = false;
                counterLastFrames = 0;
                return;
            }
            else
            {
                --counterLastFrames;
//                if (isScanStopFromGuide)
//                {
//                    qDebug() << "SigScanStart!";
//                    emit sigScanStart();
//                    isScanStopFromGuide = false;
//                }
            }
        }
    }
}

//-------------------------------------------------------------------------
//  Метод нахождения максимальной интенсивности по диску Солнца
//
void Guide::FindMaxIntensity(unsigned char* imageBuf, int width, int height)
{
    int curIntensity = 0, counterIntensitySun = 0, counterIntensityFrame = 0;
    int nGradSunMin = 90;
    int x,y;
    int posX1, posY1, posX2, posY2;
    maxIntensity_X = 0;
    maxIntensity_Y = 0;
    intensity_X = new int[width];
    intensity_Y = new int[height];

    // Находим значения средних интенсивностей в столбцах (по у)
    // и среднее значение интенсивности по всем столбцам (по х)
    for ( x=0; x < width; x++ )
    {
        intensity_X[x]=0;
        for ( y=0; y < height; y++ )
        {
            curIntensity=imageBuf[x + y*width];//intensity_X[x] += imageBuf[y*width];//Ошибка
            intensity_X[x] += curIntensity; //+= imageBuf[x + y*width];
            if (curIntensity > nGradSunMin)
            {
                meanIntensitySun+= curIntensity;
                ++counterIntensitySun;
            }
            // Суммируем интенсивность всего кадра
            meanIntensityFrame+= curIntensity;
            ++counterIntensityFrame;
        }
        intensity_X[x] /= height;

        if ( maxIntensity_X < intensity_X[x] )
        {
            posX1 = posX2 = x;
            maxIntensity_X = intensity_X[x];
        }
        else
        if ( maxIntensity_X == intensity_X[x] )
        {
            posX2 = x;
        }
    }
    posX = (posX2 + posX1) / 2;
    if (counterIntensitySun > 0) meanIntensitySun /= counterIntensitySun;
    if (counterIntensityFrame > 0) meanIntensityFrame /= counterIntensityFrame;

    // Находим значения средних интенсивностей в столбцах (по у)
    // и среднее значение интенсивности по всем столбцам (по х)
    for ( y=0; y < height; y++)
    {
        intensity_Y[y] = 0;
        for ( x = 0; x < width; x++ )
        {
            intensity_Y[y] += imageBuf[x + y*width];
        }
        intensity_Y[y] /= width;

        if ( maxIntensity_Y < intensity_Y[y])
        {
            posY1 = posY2 = y;
            maxIntensity_Y = intensity_Y[y];
        }
        else
        if ( maxIntensity_Y == intensity_Y[y])
        {
            posY2 = y;
        }
    }
    posY = (posY2 + posY1) / 2;

    delete [] intensity_X;
    delete [] intensity_Y;
}


//---------------------------------------------------------------------------------------
//  Метод контроля облаков :)
//
bool Guide::CloudsControl(float R_sun, float offsetR)
{
    return FindSunDeviation(R_sun, offsetR);

}


//---------------------------------------------------------------------------------------
//  Метод проверки стабильности найденной координаты центра диска Солнца
//
bool Guide::FindSunDeviation(float R_sun, float offsetR)
{
    // Пороговое значение радиуса
    if (R_sun < GUIDE_SUN_DIAMETER || R_sun > GUIDE_SUN_DIAMETER + 30)
        return true;
    if (IsCorrection())
        return false;

    // Записываем последние значения смещения
    if(nOffsets == GUIDE_NUM_LAST_OFFSET)
        nOffsets = 0;
    lastOffsets[nOffsets] = offsetR;
    nOffsets++;

    // Находим среднее смещение
    float meanOffset = 0;
    for (int i = 0; i < GUIDE_NUM_LAST_OFFSET; i++)
    {
        meanOffset += lastOffsets[i];
    }
    meanOffset /= GUIDE_NUM_LAST_OFFSET;

    // Находим дисперсию смещения
    float offsetSigma = 0;
    for (int i = 0; i < GUIDE_NUM_LAST_OFFSET; i++)
    {
        offsetSigma += (lastOffsets[i] - meanOffset) * (lastOffsets[i] - meanOffset);
    }
    offsetSigma /= GUIDE_NUM_LAST_OFFSET*GUIDE_NUM_LAST_OFFSET;
//    qDebug() << " offsetSigma = " << offsetSigma << " counterStability = " << stabilityCounter;

    // Проверка на превышение последними значениями заданного лимита
    if (offsetSigma < 3 * GUIDE_PIX_OFFSET_LIMIT)
    {
        if (++stabilityCounter >= GUIDE_STABILITY_NUMBER)
        {
//             qDebug() << "++stabilityCounter >= GUIDE_STABILITY_NUMBER" << stabilityCounter;
             stabilityCounter = GUIDE_STABILITY_NUMBER - 1;
             return false;
        }
        else
        {
//            qDebug() << "++stabilityCounter LESS GUIDE_STABILITY_NUMBER" << stabilityCounter;
            return true;
        }
    }
    else
    {
        stabilityCounter = 0;
//        qDebug() << "offsetSigma >= 3 * GUIDE_PIX_OFFSET_LIMIT" << offsetSigma;
        return true;
    }
}


void Guide::IntensityDeviation(float minIntensitySumm, int sec)
{
    int framesNumber =  sec * pGuideCamera->parameters.fps;
//    qDebug() << "CloudsControl: framesNumber =" << framesNumber << ", minIntensity =" << minIntensitySumm;

    // Если набрали нужное число кадров
    if (counterLastFrames == framesNumber)
    {
//        qDebug() << "CloudsControl: counterFrames =" << counterLastFrames;
        summLastFramesIntensity /= counterLastFrames;

        if (summLastFramesIntensity < minIntensitySumm)
        {
            if (!pScanner->isSaving())
                return;
//            emit sigScanStop();
            isScanStopFromGuide = true;

            qDebug() << "CloudsControl: облака набежали:" << summLastFramesIntensity;
            summLastFramesIntensity = 0;
            counterLastFrames = 0;
//            last_intensities->clear();
        }
        else
        {
            if (isScanStopFromGuide)
            {
//                emit sigScanStart();
                isScanStopFromGuide = false;
            }

//            last_intensities->mid(1);
            --counterLastFrames;
            qDebug() << "CloudsControl: облака НЕ набежали:" << summLastFramesIntensity;
            summLastFramesIntensity -= meanIntensityFrame;
        }
    }
    else
    {
//        last_intensities->push_back(meanIntensityFrame);
        summLastFramesIntensity += meanIntensityFrame;

        ++counterLastFrames;
//        qDebug() << "summ_last_intensities =" << summLastFramesIntensity;
    }
}



//---------------------------------------------------------------------------------------
//  Метод подсчета усредненного значения смещения солнечного диска
//
void Guide::CalcSolarShift()
{
    if(counter == 0)
        return;

    //qDebug() << "On Guide::CalcSolarShift() ";

    if (inProcess)
        return;

//    while(inProcess)
//    {
//        emit sigInProcess();
//        this->thread()->sleep(5);
//    }

    // Находим усреднённые смещения
    meanOffsetX = sumOffsetX / counter;
    meanOffsetY = sumOffsetY / counter;
    meanOffsetR = sqrt(meanOffsetX*meanOffsetX + meanOffsetY*meanOffsetY);
    meanRadiusSun = sumRSun / counter;

    // Сбрасываем счетчик
    counter = 0;
    sumOffsetX = 0;
    sumOffsetY = 0;
    sumRSun = 0;
}

//-------------------------------------------------------------------------------------------------
//  Метод проверяет необходимость коррекции, и запускает её
//
void Guide::Guiding()
{
    if( IsCorrection() )
        return;

    CalcSolarShift();

    bool guideReady = guiding && !pScanner->isSaving() && !pScanner->isScanning() && !findSunError;
    //qDebug() << "Guide::Guiding(): guideReady = " << guideReady;
    if (guideReady)
        GuideCorrection();
}

//-------------------------------------------------------------------------------------------------
//  Проверяется, достигло ли смещение порогового значения, если да -
//          выполненяются необходимые движения для коррекции
//
void Guide::GuideCorrection()
{
    if (!NeedCorrection())
        return;

    double angleError;
    double arcsecOnPixel = CAMERA_ARCESC_ON_PIXEL;
    double dailyAngle = pDlg_Coelostat->m_fAngleDaily_Sun_Delta;

    // Суточный угол передаётся в камеру для записи в fits-файл
    emit sigDailyAngle(dailyAngle);

    if (meanOffsetX != 0)
        angleError = qAtan( (qreal) meanOffsetY / meanOffsetX);
    else if (meanOffsetY > 0)
        angleError = pi_my/2;
    else
        angleError = - pi_my/2;

    if (meanOffsetX < 0)
        angleError = angleError + pi_my;

    double ArcsecOffsetAlpha = (double) meanOffsetR * cos(angleError - dailyAngle) * arcsecOnPixel;
    double ArcsecOffsetDelta = (double) meanOffsetR * sin(angleError - dailyAngle) * arcsecOnPixel;

    //Процедура коррекции положения по гиду
    emit sigCorrectionAlphaDelta(-ArcsecOffsetAlpha, -ArcsecOffsetDelta);
}

//-------------------------------------------------------------------------------------------------
//  Проверка нужна ли корреция целостата
//
bool Guide::NeedCorrection()
{
    double arcsecOnPixel = CAMERA_ARCESC_ON_PIXEL;
    if ((double) meanOffsetR*arcsecOnPixel < (double) arcsecMinOffsetError)
        return false;
    else
    {
//        stabilityCounter = 0;
//        qDebug() << "Need a correction!" << meanOffsetR*arcsecOnPixel;
        return true;
    }
}

//-------------------------------------------------------------------------------------------------
//  Метод включения/выключения режима гидирования
//
void Guide::SetGuideMode(bool b)
{
    guiding = b;
    //Всегда отключаем режим коррекции положения по гиду Alpha и Delta
    pDlg_Coelostat->m_bool_On_Correction_Alpha = false;
    pDlg_Coelostat->m_bool_On_Correction_Delta = false;
    qDebug() << "Guide: guide mode = " << b;
}

// Установка смещений из диалога
void Guide::SetShiftX(int x)
{
    shiftX = x;
    qDebug() << "Смещение по горизонтали " << shiftX;
}

void Guide::SetShiftY(int y)
{
    shiftY = y;
    qDebug() << "Смещение по вертикали " << shiftY;
}

//-------------------------------------------------------------------------------------------------
//  Запуск таймера
//
void Guide::StartTimer()
{
//    processTimer->start(1)
}

//-------------------------------------------------------------------------------------------------
//  Проверка идет ли коррекция по гиду
//
bool Guide::IsCorrection()
{
    return pDlg_Coelostat->m_bool_On_Correction_Alpha || pDlg_Coelostat->m_bool_On_Correction_Delta;
}

// Обнуляем счетчики и смещения
//
void Guide::OnNullAll_OffsetСounter()
{
    qDebug() << "OnNullAll_OffsetСounter()!";
    meanOffsetX = 0;
    meanOffsetY = 0;
    meanOffsetR = 0;

    // Сбрасываем последние значение смещения
    for (int i = 0; i != GUIDE_NUM_LAST_OFFSET - 1; ++i)
        lastOffsets[i] = lastOffsets[GUIDE_NUM_LAST_OFFSET-1];

    // Сбрасываем счетчик
    counter = 0;
    sumOffsetX = 0;
    sumOffsetY = 0;
    sumRSun = 0;
}


