//---------------------------------------------------------------------------------------
//  SolarScannerWdgt - класс графического интерфейса - окна сканирования,
//                  объединяющий классы и функции для работы сканирования
//  Разработка: Чернов Ярослав
//---------------------------------------------------------------------------------------

#ifndef DLG_SCANNER_H
#define DLG_SCANNER_H

#include <QWidget>
class Scanner;

namespace Ui {
class Dlg_Scanner;
}

class Dlg_Scanner : public QWidget
{
    Q_OBJECT

public:
    explicit Dlg_Scanner(QString configDir, QWidget *parent = 0);
    ~Dlg_Scanner();

    void SetSavePath(QString path);
    Scanner *scanner;

private:
    Ui::Dlg_Scanner *ui;
    QThread *pScannerThread;
    QString config_scanner;

private slots:
    void ShowWarningMessage(QString strMsg);
    void ShowStatusMessage(QString strMsg);
    void ShowStatusMessage(int number);    
    void SaveConfig();
    bool LoadConfig();
    void ShowSettings(bool b);
    void SetScanTime();
    void SetScanRadius();
    void on_testSolarImageButton_clicked(bool);

signals:
    void sigStartCreateImageTest(QString, QString);
    void sigStartUploadTest(QString);
    void sigDeleteScanner();
};

#endif // DLG_SCANNER_H
