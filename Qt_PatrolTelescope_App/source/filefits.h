#ifndef FILEFITS_H
#define FILEFITS_H

#include <QObject>

#include <string.h>
#include <stdlib.h>
#include <fitsio.h>
#include <stdint.h>
#include <vector>
#include "image.h"

class FileFits : public Image
{
    fitsfile *fptr; /* pointer to the FITS file; defined in fitsio.h */
    int status ;
    long fpixel;
    int naxis;
    long exposure;
    long naxes[2];
public:
    FileFits(Image & img);
    FileFits();
    virtual ~FileFits();
    bool Save(QString fname);
    bool Open(QString fname, int hdunum = 1);
    void AddImage(QString fname, Image & img);
    void operator =(const Image & img);
    QString GetParam(QString fname, QString param);
signals:

public slots:
};

#endif // FILEFITS_H
