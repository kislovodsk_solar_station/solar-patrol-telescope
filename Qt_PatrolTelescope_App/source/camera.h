//---------------------------------------------------------------------------------------
//  Camera - абстрактный родительский класс для работы с камерами
//  Разработка: Чернов Ярослав
//---------------------------------------------------------------------------------------

#ifndef CAMERA_H
#define CAMERA_H

#include <QObject>
#include <QPixmap>

typedef void* HANDLE;

class Camera : public QObject
{
    Q_OBJECT

public:
    explicit Camera(QObject *parent = 0);
    virtual ~Camera();

    struct CameraSettings_t
    {
        int width;
        int height;
        int binningX;
        int binningY;
        int offsetX;
        int offsetY;
        double gain;
        int exposure;
        double fps;
        QString savePath;
        QString cameraName;

        CameraSettings_t()
        {
            width = 0;
            height = 0;
            binningX = 0;
            binningY = 0;
            offsetX = 0;
            offsetY = 0;
            exposure = 0;
            fps = 0;
            gain = 0;
            cameraName = "";
        }
    }
    parameters;

    inline bool isOpened()  {return opened;}
    inline bool isStarted() {return started;}
    inline bool isSaving()  {return saving;}
    inline bool isReady()   {return frameReady;}
    virtual int GetExposure() const = 0;
    virtual int GetFrameWidth() const = 0;
    virtual int GetFrameHeight() const = 0;
    virtual int GetImageSize() const = 0;
    virtual unsigned char* GetImageBuf() = 0;
    virtual QImage GetImage() = 0;
    void SetCameraModel(QString model);

public slots:
    virtual bool Open() = 0;
    virtual bool Start() = 0;
    virtual bool Stop() = 0;
    virtual bool Close() = 0;
    virtual bool Reset() = 0;
    virtual void Restart() = 0;
    virtual int SaveImage() = 0;
    virtual bool SetParams() = 0;
    virtual bool SetAutoExposure(bool autoMode) = 0;
    virtual bool SetExposure(int val) = 0;
    virtual void SetDefaultSettings() = 0;
    void isScanStart(bool b);
    virtual void SetDailyAngle(double angle);

protected:
    unsigned char* buf8bit;
    bool opened;
    bool showing;
    bool saving;
    bool frameReady;
    bool started;
    bool scanWasStarted;
    double dailyAngle;

signals:
    void sigCameraStarted();
    void sigCameraOpened();
    void sigCameraReset();
    void sigStatusMessage(QString);
    void sigWarningMessage(QString);
    void sigPixmapGot(QPixmap);
    void sigImageGot(unsigned char* buf, int width, int height);
    void sigScanPause();
    void sigScanStart();
    void sigStopTimer();
};

#endif // CAMERA_H
