#ifndef XMLCONFIG_H
#define XMLCONFIG_H

#include <QXmlStreamWriter>

class QFile;

class XMLConfig
{
public:
    XMLConfig();
    bool WriteFile();
   // bool ReadFile();

private:
    QXmlStreamWriter m_XML;
    QFile* p_XMLFile;
};

#endif // XMLCONFIG_H
