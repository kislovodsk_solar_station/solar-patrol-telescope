// This file is available in electronic form at http://www.psa.es/sdg/sunpos.htm
#include "sunpos.h"
//#include <math.h>
#include <QDebug>


void sunpos(QDateTime udtTime,cLocation udtLocation, cSunCoordinates *udtSunCoordinates)
{
	// Main variables
	double dElapsedJulianDays;
	double dDecimalHours;
	double dEclipticLongitude;
	double dEclipticObliquity;
	double dRightAscension;
	double dDeclination;

	// Auxiliary variables
	double dY;
	double dX;

	// Calculate difference in days between the current Julian Day 
	// and JD 2451545.0, which is noon 1 January 2000 Universal Time
	{
		double dJulianDate;
		long int liAux1;
		long int liAux2;
		// Calculate time of the day in UT decimal hours
        dDecimalHours = (double) udtTime.time().hour() + (udtTime.time().minute()
            + udtTime.time().second() / 60.0 ) / 60.0;
		// Calculate current Julian Day
        liAux1 =(udtTime.date().month()-14)/12;
        liAux2=(1461*(udtTime.date().year() + 4800 + liAux1))/4 + (367*(udtTime.date().month()
            - 2-12*liAux1))/12- (3*((udtTime.date().year() + 4900
        + liAux1)/100))/4+udtTime.date().day()-32075;
		dJulianDate=(double)(liAux2)-0.5+dDecimalHours/24.0;
		// Calculate difference between current Julian Day and JD 2451545.0 
		dElapsedJulianDays = dJulianDate-2451545.0;
	}

	// Calculate ecliptic coordinates (ecliptic longitude and obliquity of the 
	// ecliptic in radians but without limiting the angle to be less than 2*Pi 
	// (i.e., the result may be greater than 2*Pi)
	{
		double dMeanLongitude;
		double dMeanAnomaly;
		double dOmega;
        dOmega= (double) 2.1429-0.0010394594*dElapsedJulianDays;
        dMeanLongitude = (double)4.8950630+ 0.017202791698*dElapsedJulianDays; // Radians
        dMeanAnomaly = (double) 6.2400600+ 0.0172019699*dElapsedJulianDays;
		dEclipticLongitude = dMeanLongitude + 0.03341607*sin( dMeanAnomaly ) 
			+ 0.00034894*sin( 2*dMeanAnomaly )-0.0001134
			-0.0000203*sin(dOmega);
        dEclipticObliquity = (double) 0.4090928 - 6.2140e-9*dElapsedJulianDays
			+0.0000396*cos(dOmega);
	}

	// Calculate celestial coordinates ( right ascension and declination ) in radians 
	// but without limiting the angle to be less than 2*Pi (i.e., the result may be 
	// greater than 2*Pi)
	{
		double dSin_EclipticLongitude;
		dSin_EclipticLongitude= sin( dEclipticLongitude );
		dY = cos( dEclipticObliquity ) * dSin_EclipticLongitude;
		dX = cos( dEclipticLongitude );
		dRightAscension = atan2( dY,dX );
		if( dRightAscension < 0.0 ) dRightAscension = dRightAscension + twopi;
		dDeclination = asin( sin( dEclipticObliquity )*dSin_EclipticLongitude );
	}

	// Calculate local coordinates ( azimuth and zenith angle ) in degrees
	{
		double dGreenwichMeanSiderealTime;
		double dLocalMeanSiderealTime;
		double dLatitudeInRadians;
		double dHourAngle;
		double dCos_Latitude;
		double dSin_Latitude;
		double dCos_HourAngle;
		double dParallax;
        dGreenwichMeanSiderealTime = (double) 6.6974243242 +
			0.0657098283*dElapsedJulianDays 
			+ dDecimalHours;
		dLocalMeanSiderealTime = (dGreenwichMeanSiderealTime*15 
			+ udtLocation.dLongitude)*rad;
		dHourAngle = dLocalMeanSiderealTime - dRightAscension;
		dLatitudeInRadians = udtLocation.dLatitude*rad;
		dCos_Latitude = cos( dLatitudeInRadians );
		dSin_Latitude = sin( dLatitudeInRadians );
		dCos_HourAngle= cos( dHourAngle );
		udtSunCoordinates->m_dZenithAngle = (acos( dCos_Latitude*dCos_HourAngle
			*cos(dDeclination) + sin( dDeclination )*dSin_Latitude));
		dY = -sin( dHourAngle );
		dX = tan( dDeclination )*dCos_Latitude - dSin_Latitude*dCos_HourAngle;
		udtSunCoordinates->m_dAzimuth = atan2( dY, dX );
		if ( udtSunCoordinates->m_dAzimuth < 0.0 ) 
			udtSunCoordinates->m_dAzimuth = udtSunCoordinates->m_dAzimuth + twopi;
		udtSunCoordinates->m_dAzimuth = udtSunCoordinates->m_dAzimuth/rad;
		// Parallax Correction
		dParallax=(dEarthMeanRadius/dAstronomicalUnit)
			*sin(udtSunCoordinates->m_dZenithAngle);
		udtSunCoordinates->m_dZenithAngle=(udtSunCoordinates->m_dZenithAngle 
			+ dParallax)/rad;
		udtSunCoordinates->m_dHourAngle=dHourAngle;
		udtSunCoordinates->m_dDeclination=dDeclination;
	}
}

//Координаты Солнца для одного положения по времени SYSTEMTIME и разнице UT
cSunCoordinates funSunPos_curLoc_tSys(QDateTime udtTime_in, int nTimeHourLoc2UT)
{
    cLocation udtLocation;
    udtLocation.dLatitude  = LOC_LATITUDE;
    udtLocation.dLongitude = LOC_LONGITUDE;
    QDateTime udtTime = udtTime_in;
    udtTime.setTime(udtTime.time().addSecs( - 60 * 60 * nTimeHourLoc2UT));
    //qDebug() << udtTime;
    cSunCoordinates udtSunCoordinates;
    sunpos(udtTime, udtLocation, &udtSunCoordinates);
    return udtSunCoordinates;

}
//Перевод радиан в угол(грд, мин, сек)
cAngle funRadian2Angle(double dRadian)
{ 
	cAngle retAngle;
    double dRad1=dRadian, d2pi=double(2.0 * pi_my);
    if (dRad1 > 0)
        while (dRad1 >  pi_my)
            dRad1=dRad1-d2pi;

    if (dRad1 < 0)
        while (dRad1 < -pi_my)
            dRad1=dRad1+d2pi;

    if (dRad1 >= 0)
        retAngle.cZnak='+';
    else
        retAngle.cZnak='-';

    double dDegres= fabs(dRad1) * 180.0 / pi_my;
//    qDebug() << dRadian << dDegres;
	retAngle.iDeg=int(dDegres);
	dDegres=  dDegres - double(retAngle.iDeg);
    retAngle.iMinut=int(dDegres*60.0000001);
    dDegres=dDegres - double(retAngle.iMinut / 60.0000001);
    retAngle.fSec=dDegres*double(60.0*60.00000001);

    if (retAngle.cZnak == '-') retAngle.iDeg= - retAngle.iDeg;
    return  retAngle;
}

//Перевод угла(грд, мин, сек) в радианы
double funAngle2Radian(cAngle Angle)
{
	double dDeg;
    if(Angle.fSec < 0.0 ) {Angle.fSec=59.0; Angle.iMinut--;}
	if(Angle.fSec > 59.0) {Angle.fSec=0.0 ; Angle.iMinut++;}
	
//	if (Angle.iDeg < 0 )  Angle.cZnak = '-';
//	if (Angle.iDeg == 0)  Angle.cZnak = '+';
//	if (Angle.cZnak == '-') Angle.iDeg= - abs(Angle.iDeg);
	
	if(Angle.iMinut < 0.0 ) {Angle.iMinut=59.0; Angle.iDeg--;}
	if(Angle.iMinut > 59.0) {Angle.iMinut=0.0 ; Angle.iDeg++;}

	if(Angle.iDeg < -179.0) Angle.iDeg= 179.0;
	if(Angle.iDeg >  179.0) Angle.iDeg=-179.0;

	
    if (Angle.cZnak == '+') dDeg =  Angle.iDeg + double(Angle.iMinut)/60.0 + double(Angle.fSec)/(60.0 *60.0);
    if (Angle.cZnak == '-') dDeg =  Angle.iDeg - double(Angle.iMinut)/60.0 - double(Angle.fSec)/(60.0 *60.0);
    return  dDeg * pi_my / 180.0;
}

//Вывод радианы в символьный вид (грд, мин, сек)
QString funRAngle2String(double dAngle_radian)
{
    QString sMsg;
    cAngle Angle=funRadian2Angle(dAngle_radian);//Перевод радиан в угол(грд, мин, сек)
    sMsg=QString("%1%2°%3'%4.%5\"").arg(Angle.cZnak)
        .arg(abs(Angle.iDeg),      2, 10, QChar('0'))
        .arg(Angle.iMinut,    2, 10, QChar('0'))
        .arg(int(Angle.fSec), 2, 10, QChar('0'))
        .arg(int((Angle.fSec-float(int(Angle.fSec))+0.0002)*10.0)); //для просмотра в QString
    return sMsg;
}

//Вывод радианы в символьный вид (мин, сек). Без градусов - для малых углов.
QString funRAngle2String_Minut(double dAngle_radian)
{
    QString sMsg;
    cAngle Angle=funRadian2Angle(dAngle_radian);//Перевод радиан в угол(грд, мин, сек)
    sMsg=QString("%1%2'%3.%4\"").arg(Angle.cZnak)
        .arg(Angle.iMinut,    2, 10, QChar('0'))
        .arg(int(Angle.fSec), 2, 10, QChar('0'))
        .arg(int((Angle.fSec-float(int(Angle.fSec))+0.0002)*10.0)); //для просмотра в QString
    return sMsg;
}

//Время захода Солнца для одного положения по дате и разнице UT
QDateTime funGetTime_SunSet(QDateTime udtTime_beg, int nTimeHourLoc2UT)
{
    cLocation udtLocation;
    udtLocation.dLatitude  = LOC_LATITUDE;
    udtLocation.dLongitude = LOC_LONGITUDE;
    QDateTime udtTime = udtTime_beg;
    udtTime.setTime(QTime(16,0,0)); //Ищем после 16:00
    udtTime.setTime(udtTime.time().addSecs( - 60 * 60 * nTimeHourLoc2UT));
    cSunCoordinates udtSunCoordinates;
    int nStepMinut = 10;
    bool bOK = false;

    for (int i=0; i < 12*60 / nStepMinut; i++){
      udtTime.setTime(udtTime.time().addSecs(60 * nStepMinut));
//      qDebug() << udtTime << udtSunCoordinates.m_dZenithAngle;
      sunpos(udtTime, udtLocation, &udtSunCoordinates);
      if (udtSunCoordinates.m_dZenithAngle >= 90.0)
      {
          bOK = true;
          break;
      }
    }
    if (bOK)
    {
      udtTime.setTime(udtTime.time().addSecs( + 60 * 60 * nTimeHourLoc2UT));
      return udtTime;
    }
    udtTime.setTime(QTime(12,0,0));
    return udtTime;

}

