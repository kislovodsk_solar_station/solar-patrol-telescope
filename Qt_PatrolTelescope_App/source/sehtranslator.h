// ----------------------------------------------------------------------------
// http://alexander-stoyan.blogspot.ru/2011/05/using-seh-with-c-exceptions.html


#ifndef SEHTRANSLATOR_H
#define SEHTRANSLATOR_H

#include <eh.h>
#include <windows.h>
#include <winternl.h>
#include <comdef.h>

class SehTranslator
{
public:
    explicit SehTranslator(_se_translator_function translator = NULL);
    ~SehTranslator();

 private:
    _se_translator_function m_newTranslator;
    _se_translator_function m_oldTranslator;

    typedef ULONG (NTAPI* statusToDosError)(NTSTATUS Status);
    static FARPROC p_statusToDosError;

    static void DefaultSeh2Cpp(unsigned int u, EXCEPTION_POINTERS* excp);
};

#endif // SEHTRANSLATOR_H
