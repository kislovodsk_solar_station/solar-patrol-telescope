#include <QFile>
#include <QMessageBox>
#include <QTimer>
#include <QtCore/qmath.h>
#include <QMutex>
#include <QDebug>

#include "Dlg_Coelostat.h"
#include "ui_Dlg_Coelostat.h"
#include "Dlg_Coelostat_Alpha_GoTo.h"
#include "Dlg_Coelostat_Delta_GoTo.h"
#include "Dlg_Coelostat_Scans.h"
#include "guide.h"
#include "scanner.h"
#include "QSM_InitControllers.h"
#include "QSM_CNLR_Ethernet.h"


//Период таймера вывода и отслеживания положений
#define VIEW_TIMER_MSEC 100
//Задержка таймера вывода
#define VIEW_TIMER_PLUS_MSEC 10
//#define THIS_WIDGET parentWidget()

//Параметры окна диалога (уменьшенный и увеличенный)
#define WIN_SIZE_HIGHT 570
#define WIN_SIZE_WIDTH_MAX 455
#define WIN_SIZE_WIDTH_MIN 265
#define SIZE_COMBO2WIN 55

Dlg_Coelostat::Dlg_Coelostat(CInitControllers *controllers, QString configDir, QWidget *parent) :
    m_pAll_CNLRs_SM(controllers),
    QWidget(parent),
    ui(new Ui::Dlg_Coelostat)
{
    setObjectName("Dlg_Coelostat");
    ui->setupUi(this);
    this->setWindowTitle("Целостат");
    config_coelostat = configDir + QString("SM_Coelostat.cfg");

//    MyInitDialog();
    QTimer::singleShot(50, this, SLOT(MyInitDialog()));     //Таймер запускается один раз для инициализации диалога
}

void Dlg_Coelostat::MyInitDialog()
{
    //CControllerEthernet * pCNLR_Alpha = m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA]];

    QString sTmp;;
    int	i, n, a[N_MAX_AXIS + 2];

//    m_PosWin = m_PosWin = THIS_WIDGET->pos();
//    qDebug() << "Dlg_Coelostat::POS = " << m_PosWin;

    //Для альфы
    BYTE nAxis_Alpha = m_pAll_CNLRs_SM->m_anFunAxis[FUN_ALPHA];

    //Часовая (солнечная) скорость ШД в шагах в секунду. Инициализировать часовую скорость и ее отоброжение в диалоге
    On_Dlg_Set_Speed_Alpha_Hours(SM_ALPHA_SPEED_SUN_INIT);

    m_Alpha_SysTime_Pos_0 = QDateTime::currentDateTime();
    m_fAlpha_Steps_From_Pos_0 = 0;           //Шагов двигателя от 0 установки времени
    m_bool_Get_Daily_Sun_Alpha_On = false;   //Режим определения суточной по изображению на гиде
    m_bool_On_Correction_Alpha = false;      //Режим коррекции положения по гиду Alpha
    m_fAngleDaily_Sun_Alpha = 0;             //Наклон суточной на изображении гида. По альфе.

    //Время захода Солнца для одного положения по дате и времени начала и разнице UT
    m_tDataTime_SunSet = funGetTime_SunSet(m_Alpha_SysTime_Pos_0, TIME_HOUR_LOC2UT).addSecs(-60 * MINUT_MINUS_SUNSET_OFF_TELESCOPE);
    //Отслеживать положение захода Солнца
    m_bool_ParkSunSet = true;

    //Для работы целостата в режиме управления двумя осями V=1 и V=1/2
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA_2_HALF_V] != NO_CNLR)
    {
        ui->Btn_Alpha_FORV_CORR_ONE_AXIS->setEnabled(true);
        ui->Btn_Alpha_REV_CORR_ONE_AXIS ->setEnabled(true);
        connect(ui->Btn_Alpha_FORV_CORR_ONE_AXIS, SIGNAL(clicked(bool)), this, SLOT(on_Btn_Alpha_FORV_REV_CORR_ONE_AXIS_clicked()));//Для остановки коррекции используем обну функцию
        connect(ui->Btn_Alpha_REV_CORR_ONE_AXIS , SIGNAL(clicked(bool)), this, SLOT(on_Btn_Alpha_FORV_REV_CORR_ONE_AXIS_clicked()));
    }

    //Для дельты
    BYTE nAxis_Delta = m_pAll_CNLRs_SM->m_anFunAxis[FUN_DELTA];
    m_fDelta_SM_Steps_1deg = SM_DELTA_STEP_DEGREE_INIT;             //Кол-во шагов ШД для перемещения на 1 градус (шаг/грд)
    m_fDelta_AnglMinut_Min2Max = SM_DELTA_ANGLE_MINUT_MIN2MAX;      //Для дельты. Сколько угловых минут от концевика MIN до MAX

    cSunCoordinates sunPos=funSunPos_curLoc_tSys(m_Alpha_SysTime_Pos_0, TIME_HOUR_LOC2UT);
    m_fDelta_Angle_Pos_0 = sunPos.m_dDeclination;   //.m_dZenithAngle *pi / 180.0;
    m_fDelta_Steps_From_Pos_0 = 0;         //Шагов двигателя от 0 установки времени
    m_bool_Delta_Centering = false;        //Работаем в режиме поиска центрирования дельты от концевиков
    m_bool_Delta_Centering_On = false;     //Для установки в конце центрирования значения текущей координаты Солнца
    m_bool_Get_Daily_Sun_Delta_On = false; //Режим определения суточной на изображении гида по делте
    m_bool_On_Correction_Delta = false;    //Работаем в режиме коррекции положения по гиду Delta
    m_fAngleDaily_Sun_Delta = 0;           //Наклон суточной на изображении гида. По дельте.

    //Для скана на целостате
    BYTE nAxis_Scan_С = m_pAll_CNLRs_SM->m_anFunAxis[FUN_SCAN_COELOSTAT];
    m_fScan_Angle_Minut_Pos_0   = 0;                                 //Угол - текущее положение 0 в радианах
    m_fScan_Steps_From_Pos_0    = 0;                                 //Шагов двигателя от 0
    m_fScan_SM_Steps_1deg       = SM_SCAN_STEP_DEGREE_INIT;          //Кол-во шагов ШД для перемещения на 1 градус (шаг/грд)
    m_fScan_Angle_Minut_Pos_Beg = SM_SCAN_POS_BEG_MINUT_INIT;        //Угол - положения начала сканирования в минутах дуги
    m_fScan_Angle_Minut_Pos_End = SM_SCAN_POS_END_MINUT_INIT;        //Угол - положения конца сканирования в  минутах дуги
    m_fScan_Speed_MinutOfMinut  = SM_SCAN_SPEED_MINUT_OF_MINUT_INIT; //Скорость сканирования (градусов в минуту)
    m_bScan_On_Delta_Correction = true;                              //Работаем в режиме сканирования Солнца с коррекцией положения по дельте
    m_bScan_On_Scan_back        = false;                             //Работаем в режиме сканирования Солнца в обоих направлениях


    m_pTimer_View = NULL;
    m_pCNLR_view_info = NULL;
//    m_pTimer_CNTL = NULL;

//    Sleep(100);
//    qDebug() << "Есть 1";

    //Загружаем настройки
    if (Load_Coelostat_CFG() == -1)
        QMessageBox::warning(this, "Coelostat", "Конфигурационный файл не найден!");


    //Инициализировать часовую скорость и ее отображение в диалоге
    On_Dlg_Set_Speed_Alpha_Hours(m_nSM_Speed_Alpha_Sun_StepPerSec);

    i = -1; n = 0;
    while (m_pAll_CNLRs_SM->m_asAll_CNLR_IP_Name_cur[n].length() > 4)
    {
        ui->Cmb_CNT_Name->addItem(m_pAll_CNLRs_SM->m_asAll_CNLR_IP_Name_cur[n]);
        if (m_pAll_CNLRs_SM->m_asAll_CNLR_IP_Name_cur[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA]] ==
                m_pAll_CNLRs_SM->m_asAll_CNLR_IP_Name_cur[n]) i = n;
        n++;
    }
    if (i >= 0)
    {
        ui->Cmb_CNT_Name->setCurrentIndex(i);
        m_pCNLR_view_info = m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA]];
    }
    else
    {
        //Выход!\n" + strLog);
        QMessageBox::information(this, tr("Непредвиденная ошибка"),tr("Контроллер по умолчанию не найден. Подключен другой найденный контроллер!"));
        ui->Cmb_CNT_Name->setCurrentIndex(0);

    }
    //m_pAll_CNLRs_SM->m_CNLRs.CControllerEthernet::GetLenStatusPacket();//Процедура определения длины статусного пакета (зависит от контроллера)

    if (m_pAll_CNLRs_SM->isInitSuccess())
    {
        m_pTimer_View = new QTimer(this);   //Таймер для просмотра диалога
        connect(m_pTimer_View, &QTimer::timeout, this, &Dlg_Coelostat::on_Timer_View);
        m_pTimer_View->start(VIEW_TIMER_MSEC);
    }

    //m_pTimer_CNTL = new QTimer(this);//Таймер для поддержания работы контроллера ШД
    //connect(m_pTimer_CNTL, SIGNAL(timeout()), this, SLOT(on_Timer_CNTL()));
    //m_pTimer_CNTL->start(CNTL_TIMER_MSEC);

    connect(ui->Cmb_CNT_Name, SIGNAL(currentIndexChanged(int)), this, SLOT(on_Cmb_CNT_Name_currentIndexChanged_my(int)));

    //Оси контроллера
    a[AXIS_ALPHA] = -1;
    a[AXIS_DELTA] = -1;
    a[AXIS_SCAN_COELOSTAT] = -1;

    for (i=1; i <= N_MAX_AXIS; i++){
        ui->Cmb_Alpha_Axis->addItem(QString::number(i));
        ui->Cmb_Delta_Axis->addItem(QString::number(i));
        ui->Cmb_Scans_Axis->addItem(QString::number(i));

        if (nAxis_Alpha  == BYTE(i)) a[AXIS_ALPHA] = i;
        if (nAxis_Delta  == BYTE(i)) a[AXIS_DELTA] = i;
        if (nAxis_Scan_С == BYTE(i)) a[AXIS_SCAN_COELOSTAT] = i;

    }
//    qDebug() << int(nAxis_Alpha) << a[AXIS_ALPHA];
    if (a[AXIS_ALPHA] >= 0)
        ui->Cmb_Alpha_Axis->setCurrentIndex(a[AXIS_ALPHA] - 1);
    else
        ui->Cmb_Alpha_Axis->setCurrentIndex(0);

    if (a[AXIS_DELTA] >= 0)
        ui->Cmb_Delta_Axis->setCurrentIndex(a[AXIS_DELTA] - 1);
    else
        ui->Cmb_Delta_Axis->setCurrentIndex(0);

    if (a[AXIS_SCAN_COELOSTAT] >= 0)
        ui->Cmb_Scans_Axis->setCurrentIndex(a[AXIS_SCAN_COELOSTAT] - 1);
    else
        ui->Cmb_Scans_Axis ->setCurrentIndex(0);

    connect(ui->Cmb_Alpha_Axis, SIGNAL(currentIndexChanged(int)), this, SLOT(on_Cmb_Alpha_Axis_currentIndexChanged_my(int)));
    connect(ui->Cmb_Delta_Axis, SIGNAL(currentIndexChanged(int)), this, SLOT(on_Cmb_Delta_Axis_currentIndexChanged_my(int)));
    connect(ui->Cmb_Scans_Axis, SIGNAL(currentIndexChanged(int)), this, SLOT(on_Cmb_Scans_Axis_currentIndexChanged_my(int)));

    //m_anCur_SM_speed[AXIS_ALPHA] = SM_STOP;
    //m_afSM_KSpeed_cur[AXIS_ALPHA] = 0;
    for (i = 0; i < N_MAX_AXIS; i++)
    {
        m_afSM_KSpeed_cur[i] = 0.0;     //Множитель для скорости данного режима от максимальной скорости ШД
//        m_afSM_KSpeed_old[i] = 0.0;     //Предыдущий множитель для скорости данного режима от максимальной скорости ШД
        m_anCur_SM_SpeedMode[i] = SM_STOP;
    }

    sTmp=QString("SM(%1)->%2").arg(0).arg(m_afSM_KSpeed_cur[AXIS_ALPHA]);
    ui->Labl_Alpha_Info->setText(sTmp);

    m_bool_Alpha_On_Go = false;          //Работаем не в режиме поиска положения Alpha
    m_bool_Alpha_On_Go_Cur_Time = false; //Работаем не в режиме поиска положения по текущему времени Alpha
    ui->Btn_GetDailyLineSun_Delta->setText(QString("Sd= %1").arg(m_fAngleDaily_Sun_Delta*180.0/pi_my));

    connect(ui->Btn_Alpha_START,     SIGNAL(pressed()), this, SLOT(on_Btn_Alpha_START_pressed()));
    connect(ui->Btn_Alpha_STOP,      SIGNAL(pressed()), this, SLOT(on_Btn_Alpha_STOP_pressed()));
    connect(ui->Btn_Alpha_FORV_SLOW, SIGNAL(clicked(bool)), this, SLOT(OnBtnClickedForvardSlow_Alpha()));
    connect(ui->Btn_Alpha_REV_SLOW,  SIGNAL(clicked(bool)), this, SLOT(OnBtnClickedReverseSlow_Alpha()));
    //connect(ui->Btn_Alpha_FORV_FAST, SIGNAL(clicked(bool)), this, SLOT(OnBtnClickedForvardFast_Alpha()));
    //connect(ui->Btn_Alpha_REV_FAST,  SIGNAL(clicked(bool)), this, SLOT(OnBtnClickedReverseFast_Alpha()));

    m_bool_Delta_On_Go = false;          //Работаем не в режиме поиска положения Delta
    m_bool_Delta_On_Go_Cur_Time = false; //Работаем не в режиме поиска положения по текущему времени Delta

    connect(ui->Btn_Delta_STOP,      SIGNAL(clicked(bool)), this, SLOT(OnBtnClickedStop_Delta()));
    connect(ui->Btn_Delta_FORV_SLOW, SIGNAL(clicked(bool)), this, SLOT(OnBtnClickedForvardSlow_Delta()));
    connect(ui->Btn_Delta_FORV_FAST, SIGNAL(clicked(bool)), this, SLOT(OnBtnClickedForvardFast_Delta()));
    connect(ui->Btn_Delta_REV_SLOW,  SIGNAL(clicked(bool)), this, SLOT(OnBtnClickedReverseSlow_Delta()));
    connect(ui->Btn_Delta_REV_FAST,  SIGNAL(clicked(bool)), this, SLOT(OnBtnClickedReverseFast_Delta()));

    m_bool_Scan_On_Go = false;       //Работаем в режиме поиска положения для сканировния на целостате

    //Определяем положение окна
    //qDebug() << "POS = " << m_PosWin;
//    THIS_WIDGET->move(m_PosWin);
//    parentWidget()->setWindowState(Qt::WindowMinimized);
//    On_Dlg_WinSizeMax(); //Диалог с полным размером
    setWindowFlags(Qt::WindowTitleHint | Qt::WindowMinimizeButtonHint | Qt::WindowCloseButtonHint);
    OnReset_Button_Info_Alpha();//Изменение отображения кнопок от нажатия и соответствующего режима
    On_Dlg_WinSizeMin(); //Сокращенный диалог

    emit sigCoelostatInit(true);
    if (!m_pAll_CNLRs_SM->isInitSuccess())
        qDebug()<<"Целостат: контроллеры не инициализированы!";
}

Dlg_Coelostat::~Dlg_Coelostat()
{
    qDebug() << "Dlg_Coelostat::~Dlg_Coelostat 0";
    if (m_pAll_CNLRs_SM->isInitSuccess())
    {
        on_Btn_Alpha_STOP_pressed();
        OnBtnClickedStop_Delta();
        Sleep(SLEEP_THREAD*40);
    }

    //Если есть рабочий контроллер
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA] != NO_CNLR
        || m_pAll_CNLRs_SM->m_anFunCNLR[FUN_DELTA] != NO_CNLR)
        Save_Coelostat_CFG(FALSE);//Сохранение файла конфигурации
    delete ui;
    qDebug() << "Dlg_Coelostat::~Dlg_Coelostat 1";
}


void Dlg_Coelostat::on_Cmb_CNT_Name_currentIndexChanged_my(int index)
{
    if (m_pAll_CNLRs_SM->m_apCNLRs[index])
        m_pCNLR_view_info = m_pAll_CNLRs_SM->m_apCNLRs[index];
    else
        // Определить номер контроллера в списке m_asAll_CNLR_IP_Name_cur
        ui->Cmb_CNT_Name->setCurrentIndex(m_pAll_CNLRs_SM->GetN_CNLR(m_pCNLR_view_info));

//    m_pAll_CNLRs_SM->m_CNLRs.m_sCNLR_IP  =m_pAll_CNLRs_SM->funGet_IP_CNRL(m_pAll_CNLRs_SM->m_asAll_CNLR_IP_Name_cur[index]); //[ui->Cmb_CNT_Name->currentIndex()];
//    m_pAll_CNLRs_SM->m_CNLRs.m_sCNLR_Name=m_pAll_CNLRs_SM->funGet_Name_CNRL(m_pAll_CNLRs_SM->m_asAll_CNLR_IP_Name_cur[index]);
//    m_pAll_CNLRs_SM->m_CNLRs.Terminate_Port();
//    QString strLog;
//    if (! (m_pAll_CNLRs_SM->m_CNLRs.Initialize_Ethernet(strLog))) // инициализация Ethernet порта
//    {   QMessageBox::information(this, tr("Непредвиденная ошибка"),tr("Нет прав администратора. Выход!")); //Выход!\n" + strLog);
//        exit(0);
    //    }
}

//Для Aльфы - инициализировать часовую скорость и ее отображение в диалоге
void Dlg_Coelostat::On_Dlg_Set_Speed_Alpha_Hours(long nSpeed_Alpha_Hours)
{
//    CControllerEthernet * pCNLR_Alpha = m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA]];
//    BYTE nAxis_Alpha = m_pAll_CNLRs_SM->m_anFunAxis[FUN_ALPHA];

    //Часовая (солнечная) скорость ШД в шагах в секунду
    m_nSM_Speed_Alpha_Sun_StepPerSec = nSpeed_Alpha_Hours ;

    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA] != NO_CNLR)
    {
        //Множитель для часовой скорости от максимальной скорости ШД
        m_fSM_KSpeed_Alpha_Sun= (double) m_nSM_Speed_Alpha_Sun_StepPerSec / m_pAll_CNLRs_SM->m_anFunMaxSpeed[FUN_ALPHA];
        //qDebug() << m_pAll_CNLRs_SM->m_anFunMaxSpeed[FUN_ALPHA];
    }

    ui->spinBox_VHours->setValue(m_nSM_Speed_Alpha_Sun_StepPerSec);
    //qDebug() << m_nSM_Speed_Alpha_Sun_StepPerSec;

    if ( m_anCur_SM_SpeedMode[AXIS_ALPHA] == SM_FORV_HOUR)
    {
        qDebug() << "On_Dlg_Set_Speed_Alpha_Hours: on_Btn_Alpha_START_pressed";
        on_Btn_Alpha_START_pressed();
    }
}

//Для Aльфы - подобрать часовую скорость
void Dlg_Coelostat::on_spinBox_VHours_valueChanged(int arg1)
{
    On_Dlg_Set_Speed_Alpha_Hours(long(arg1));
}


void Dlg_Coelostat::on_Cmb_Alpha_Axis_currentIndexChanged_my(int index)
{
    BYTE nAxis_Alpha = m_pAll_CNLRs_SM->m_anFunAxis[FUN_ALPHA];
    m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA]]->Send_Command_Ethernet_Stop(FALSE, nAxis_Alpha);
    nAxis_Alpha = index + 1;
    m_pAll_CNLRs_SM->m_anFunAxis[FUN_ALPHA] = nAxis_Alpha;
}

void Dlg_Coelostat::on_Cmb_Delta_Axis_currentIndexChanged_my(int index)
{
    BYTE nAxis_Delta = m_pAll_CNLRs_SM->m_anFunAxis[FUN_DELTA];
    m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_DELTA]]->Send_Command_Ethernet_Stop(FALSE, nAxis_Delta);
    nAxis_Delta = index + 1;
    m_pAll_CNLRs_SM->m_anFunAxis[FUN_DELTA] = nAxis_Delta;
}

void Dlg_Coelostat::on_Cmb_Scans_Axis_currentIndexChanged_my(int index)
{
    BYTE nAxis_Scan = m_pAll_CNLRs_SM->m_anFunAxis[FUN_SCAN_COELOSTAT];
    m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_SCAN_COELOSTAT]]->Send_Command_Ethernet_Stop(FALSE, nAxis_Scan);
    nAxis_Scan = index + 1;
    m_pAll_CNLRs_SM->m_anFunAxis[FUN_SCAN_COELOSTAT] = nAxis_Scan;
}

//Для альфы - изменение отображения кнопок от нажатия и соответствующего режима
void Dlg_Coelostat::OnReset_Button_Info_Alpha()
{
    QFont myFont_Off = ui->Btn_Alpha_Set_Time->font();
    QFont myFont_On  = myFont_Off;
    //int nFontSize= myFont_Off.pixelSize();
    float kFontSize_On = 17.0;
    myFont_On.setPixelSize(kFontSize_On);
    //qDebug() << nFontSize << kFontSize_On;
    myFont_On.setBold(true); //myFont_On.setUnderline(true);

    ui->Btn_Alpha_STOP      -> setFont(myFont_Off);
    ui->Btn_Alpha_START     -> setFont(myFont_Off);
    ui->Btn_Alpha_FORV_FAST -> setFont(myFont_Off);
    ui->Btn_Alpha_FORV_SLOW -> setFont(myFont_Off);
    ui->Btn_Alpha_REV_SLOW  -> setFont(myFont_Off);
    ui->Btn_Alpha_REV_FAST  -> setFont(myFont_Off);
    ui->Btn_Alpha_GoTo      -> setFont(myFont_Off);

    switch(m_anCur_SM_SpeedMode[AXIS_ALPHA])
    {
        case SM_STOP:
        {  ui->Btn_Alpha_STOP-> setFont(myFont_On);
           break;
        }
        case SM_FORV_HOUR:
        {   ui->Btn_Alpha_START-> setFont(myFont_On);
            break;
        }
        case SM_FORV_FAST:
        {   ui->Btn_Alpha_FORV_FAST-> setFont(myFont_On);
            break;
        }
        case SM_FORV_SLOW:
        {   ui->Btn_Alpha_FORV_SLOW-> setFont(myFont_On);
            break;
        }
        case SM_REVR_SLOW:
        {   ui->Btn_Alpha_REV_SLOW-> setFont(myFont_On);
            break;
        }
        case SM_REVR_FAST:
        {   ui->Btn_Alpha_REV_FAST-> setFont(myFont_On);
            break;
        }
    }
    if (m_bool_Alpha_On_Go)
        ui->Btn_Alpha_GoTo->setFont(myFont_On);
}


//Для скана на целостате - изменение отображения кнопок от нажатия и соответствующего режима
void Dlg_Coelostat::OnReset_Button_Info_Scan()
{
    QFont myFont_Off = ui->Btn_Scan_Set_0->font();
    QFont myFont_On  = myFont_Off;

    //int nFontSize= myFont_Off.pixelSize();
    float kFontSize_On = 17.0;
    myFont_On.setPixelSize(kFontSize_On);
    //qDebug() << nFontSize << kFontSize_On;
    myFont_On.setBold(true); //myFont_On.setUnderline(true);

    ui->Btn_Scan_START      -> setFont(myFont_Off);
    ui->Btn_Scan_STOP       -> setFont(myFont_Off);
    ui->Btn_Scan_NULL       -> setFont(myFont_Off);
    ui->Btn_Scan_FORV_FAST  -> setFont(myFont_Off);
    ui->Btn_Scan_REV_FAST   -> setFont(myFont_Off);

    switch(m_anCur_SM_SpeedMode[AXIS_SCAN_COELOSTAT])
    {
        case SM_STOP:
        {  ui->Btn_Scan_STOP-> setFont(myFont_On);
           break;
        }
        case SM_SCAN_START:
        {   ui->Btn_Scan_START-> setFont(myFont_On);
            if (m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT] == - SM_K_SPEED_FAST)
                ui->Btn_Scan_REV_FAST-> setFont(myFont_On); //К началу сканирования
            break;
        }
        case SM_FORV_FAST:
        {   ui->Btn_Scan_FORV_FAST-> setFont(myFont_On);
            break;
        }
        case SM_FORV_SLOW:
        {   ui->Btn_Scan_NULL-> setFont(myFont_On);
            break;
        }
        case SM_REVR_SLOW:
        {   ui->Btn_Scan_NULL-> setFont(myFont_On);
            break;
        }
        case SM_REVR_FAST:
        {   ui->Btn_Scan_REV_FAST-> setFont(myFont_On);
            break;
        }
    }
}


// Включаем часовое ведение
void Dlg_Coelostat::on_Btn_Alpha_START_pressed()
{
    // TODO: Add your control notification handler code here
    //if (m_anCur_SM_speed[AXIS_ALPHA] == SM_FORV_HOUR) return;
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA] == NO_CNLR) return;
    QString sTextRead;
    BYTE nAxis_Alpha = m_pAll_CNLRs_SM->m_anFunAxis[FUN_ALPHA];
    m_afSM_KSpeed_cur[AXIS_ALPHA]=m_fSM_KSpeed_Alpha_Sun;
    m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA]]
            ->SM_Forvard(sTextRead, m_afSM_KSpeed_cur[AXIS_ALPHA], nAxis_Alpha);
    //Для работы целостата в режиме управления двумя осями V=1 и V=1/2
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA_2_HALF_V] != NO_CNLR)
    {
        BYTE nAxis_Alpha_two = m_pAll_CNLRs_SM->m_anFunAxis[FUN_ALPHA_2_HALF_V];
        m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA_2_HALF_V]]
                ->SM_Forvard(sTextRead, m_afSM_KSpeed_cur[AXIS_ALPHA]*SM_ALPHA_K_SPEED_TWO_HALF_V, nAxis_Alpha_two);
        //qDebug() << "nAxis_Alpha =" << nAxis_Alpha << " m_afSM_KSpeed_cur[AXIS_ALPHA] = " << m_afSM_KSpeed_cur[AXIS_ALPHA];
        //qDebug() << "nAxis_Alpha_two =" << nAxis_Alpha_two << " m_afSM_KSpeed_cur[AXIS_ALPHA_2] = " << m_afSM_KSpeed_cur[AXIS_ALPHA]*SM_ALPHA_K_SPEED_TWO_HALF_V;
    }
    m_anCur_SM_SpeedMode[AXIS_ALPHA]=SM_FORV_HOUR;
    m_bool_Alpha_On_Go = FALSE;          //Работаем в режиме поиска положения
    m_bool_Alpha_On_Go_Cur_Time = FALSE; //Работаем в режиме поиска положения по текущему времени
    OnReset_Button_Info_Alpha();//Изменение отображения кнопок от нажатия и соответствующего режима


}


//Работаем по нажатию кнопки. Отпускание - остановка.
void Dlg_Coelostat::on_Btn_Alpha_FORV_SLOW_pressed()
{
    // TODO: Add your control notification handler code here
    //if (m_anCur_SM_speed[AXIS_ALPHA] == SM_FORV_SLOW) return;
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA] == NO_CNLR) return;
    QString sTextRead;
    BYTE nAxis_Alpha = m_pAll_CNLRs_SM->m_anFunAxis[FUN_ALPHA];
    m_afSM_KSpeed_cur[AXIS_ALPHA]=SM_K_SPEED_SLOW;
    m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA]]
            ->SM_Forvard(sTextRead, m_afSM_KSpeed_cur[AXIS_ALPHA], nAxis_Alpha);
    //Для работы целостата в режиме управления двумя осями V=1 и V=1/2
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA_2_HALF_V] != NO_CNLR)
    {
        BYTE nAxis_Alpha_two = m_pAll_CNLRs_SM->m_anFunAxis[FUN_ALPHA_2_HALF_V];
        m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA_2_HALF_V]]
                ->SM_Forvard(sTextRead, m_afSM_KSpeed_cur[AXIS_ALPHA]*SM_ALPHA_K_SPEED_TWO_HALF_V, nAxis_Alpha_two);
    }
    m_anCur_SM_SpeedMode_pre[AXIS_ALPHA] = m_anCur_SM_SpeedMode[AXIS_ALPHA];
    m_anCur_SM_SpeedMode[AXIS_ALPHA]=SM_FORV_SLOW;
    m_bool_Alpha_On_Go = FALSE;       //Работаем в режиме поиска положения
    m_bool_Alpha_On_Go_Cur_Time = FALSE; //Работаем в режиме поиска положения по текущему времени
    OnReset_Button_Info_Alpha();//Изменение отображения кнопок от нажатия и соответствующего режима
}

void Dlg_Coelostat::OnBtnClickedForvardSlow_Alpha()
{
    if (m_anCur_SM_SpeedMode_pre[AXIS_ALPHA] == SM_FORV_HOUR)
         on_Btn_Alpha_START_pressed();
    else
        on_Btn_Alpha_STOP_pressed();

    OnReset_Button_Info_Alpha();//Изменение отображения кнопок от нажатия и соответствующего режима
}

void Dlg_Coelostat::on_Btn_Alpha_FORV_FAST_pressed()
{
    // TODO: Add your control notification handler code here
    //if (m_anCur_SM_speed[AXIS_ALPHA] == SM_FORV_FAST) return;
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA] == NO_CNLR) return;
    QString sTextRead;
    BYTE nAxis_Alpha = m_pAll_CNLRs_SM->m_anFunAxis[FUN_ALPHA];
    m_afSM_KSpeed_cur[AXIS_ALPHA]=SM_K_SPEED_FAST;
    m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA]]
            ->SM_Forvard(sTextRead, m_afSM_KSpeed_cur[AXIS_ALPHA], nAxis_Alpha);
    //Для работы целостата в режиме управления двумя осями V=1 и V=1/2
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA_2_HALF_V] != NO_CNLR)
    {
        BYTE nAxis_Alpha_two = m_pAll_CNLRs_SM->m_anFunAxis[FUN_ALPHA_2_HALF_V];
        m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA_2_HALF_V]]
                ->SM_Forvard(sTextRead, m_afSM_KSpeed_cur[AXIS_ALPHA]*SM_ALPHA_K_SPEED_TWO_HALF_V, nAxis_Alpha_two);
    }
    m_anCur_SM_SpeedMode[AXIS_ALPHA]=SM_FORV_FAST;
    m_bool_Alpha_On_Go = FALSE;       //Работаем в режиме поиска положения
    m_bool_Alpha_On_Go_Cur_Time = FALSE; //Работаем в режиме поиска положения по текущему времени
    OnReset_Button_Info_Alpha();//Изменение отображения кнопок от нажатия и соответствующего режима
}

void Dlg_Coelostat::on_Btn_Alpha_STOP_pressed()
{
    // TODO: Add your control notification handler code here
    //if (m_anCur_SM_speed[AXIS_ALPHA] == SM_STOP) return;
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA] == NO_CNLR) return;
    QString sTextRead;
    BYTE nAxis_Alpha = m_pAll_CNLRs_SM->m_anFunAxis[FUN_ALPHA];
    m_afSM_KSpeed_cur[AXIS_ALPHA] = 0;
    m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA]]
            ->SM_Stop(sTextRead, nAxis_Alpha);
    //Для работы целостата в режиме управления двумя осями V=1 и V=1/2
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA_2_HALF_V] != NO_CNLR)
    {
        BYTE nAxis_Alpha_two = m_pAll_CNLRs_SM->m_anFunAxis[FUN_ALPHA_2_HALF_V];
        m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA_2_HALF_V]]
                ->SM_Stop(sTextRead, nAxis_Alpha_two);
    }
    m_anCur_SM_SpeedMode[AXIS_ALPHA]=SM_STOP;
    m_bool_Alpha_On_Go = FALSE;       //Работаем в режиме поиска положения
    m_bool_Alpha_On_Go_Cur_Time = FALSE; //Работаем в режиме поиска положения по текущему времени
    OnReset_Button_Info_Alpha();//Изменение отображения кнопок от нажатия и соответствующего режима
}

void Dlg_Coelostat::on_Btn_Alpha_REV_SLOW_pressed()
{
    // TODO: Add your control notification handler code here
    //if (m_anCur_SM_speed[AXIS_ALPHA] == SM_REVR_SLOW) return;
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA] == NO_CNLR) return;
    QString sTextRead;
    BYTE nAxis_Alpha = m_pAll_CNLRs_SM->m_anFunAxis[FUN_ALPHA];
    m_afSM_KSpeed_cur[AXIS_ALPHA] = -SM_K_SPEED_SLOW;
    m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA]]
            ->SM_Reverse(sTextRead, m_afSM_KSpeed_cur[AXIS_ALPHA], nAxis_Alpha);
    //Для работы целостата в режиме управления двумя осями V=1 и V=1/2
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA_2_HALF_V] != NO_CNLR)
    {
        BYTE nAxis_Alpha_two = m_pAll_CNLRs_SM->m_anFunAxis[FUN_ALPHA_2_HALF_V];
        m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA_2_HALF_V]]
                ->SM_Forvard(sTextRead, m_afSM_KSpeed_cur[AXIS_ALPHA]*SM_ALPHA_K_SPEED_TWO_HALF_V, nAxis_Alpha_two);
    }
    m_anCur_SM_SpeedMode_pre[AXIS_ALPHA] = m_anCur_SM_SpeedMode[AXIS_ALPHA];
    m_anCur_SM_SpeedMode[AXIS_ALPHA]=SM_REVR_SLOW;
    m_bool_Alpha_On_Go = FALSE;       //Работаем в режиме поиска положения
    m_bool_Alpha_On_Go_Cur_Time = FALSE; //Работаем в режиме поиска положения по текущему времени
    OnReset_Button_Info_Alpha();//Изменение отображения кнопок от нажатия и соответствующего режима
}

//Работаем по нажатию кнопки. Отпускание - остановка.
void Dlg_Coelostat::OnBtnClickedReverseSlow_Alpha()
{
    if (m_anCur_SM_SpeedMode_pre[AXIS_ALPHA] == SM_FORV_HOUR)
         on_Btn_Alpha_START_pressed();
    else
        on_Btn_Alpha_STOP_pressed();

    OnReset_Button_Info_Alpha();//Изменение отображения кнопок от нажатия и соответствующего режима
}

void Dlg_Coelostat::on_Btn_Alpha_REV_FAST_pressed()
{
    // TODO: Add your control notification handler code here
    //if (m_anCur_SM_speed[AXIS_ALPHA] == SM_REVR_FAST) return;
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA] == NO_CNLR) return;
    QString sTextRead;
    BYTE nAxis_Alpha = m_pAll_CNLRs_SM->m_anFunAxis[FUN_ALPHA];
    m_afSM_KSpeed_cur[AXIS_ALPHA] = - SM_K_SPEED_FAST;
    m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA]]
            ->SM_Reverse(sTextRead, m_afSM_KSpeed_cur[AXIS_ALPHA], nAxis_Alpha);
    //Для работы целостата в режиме управления двумя осями V=1 и V=1/2
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA_2_HALF_V] != NO_CNLR)
    {
        BYTE nAxis_Alpha_two = m_pAll_CNLRs_SM->m_anFunAxis[FUN_ALPHA_2_HALF_V];
        m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA_2_HALF_V]]
                ->SM_Forvard(sTextRead, m_afSM_KSpeed_cur[AXIS_ALPHA]*SM_ALPHA_K_SPEED_TWO_HALF_V, nAxis_Alpha_two);
    }
    m_anCur_SM_SpeedMode[AXIS_ALPHA]=SM_REVR_FAST;
    m_bool_Alpha_On_Go = FALSE;       //Работаем в режиме поиска положения
    m_bool_Alpha_On_Go_Cur_Time = FALSE; //Работаем в режиме поиска положения по текущему времени
    OnReset_Button_Info_Alpha();
}

void Dlg_Coelostat::on_Btn_Alpha_FORV_CORR_ONE_AXIS_pressed()
{
    // Для работы целостата в режиме управления двумя осями V=1 и V=1/2.
    // Для коррекции заполнения зеркал двигаем только основную ось, вторая на стоит на месте.

    //Какую ось подводим: FUN_ALPHA_2_HALF_V - верхнее зеркало; FUN_ALPHA - нижнее зеркало;
    int nCurFonCorr = FUN_ALPHA_2_HALF_V;

    if (m_pAll_CNLRs_SM->m_anFunCNLR[nCurFonCorr] == NO_CNLR)
        return;
    QString sTextRead;
    BYTE nAxis_Alpha = m_pAll_CNLRs_SM->m_anFunAxis[nCurFonCorr];
    m_afSM_KSpeed_cur[nCurFonCorr] = SM_K_SPEED_FAST;
    m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[nCurFonCorr]]
            ->SM_Reverse(sTextRead, m_afSM_KSpeed_cur[nCurFonCorr], nAxis_Alpha);
}

void Dlg_Coelostat::on_Btn_Alpha_REV_CORR_ONE_AXIS_pressed()
{
    // Для работы целостата в режиме управления двумя осями V=1 и V=1/2.
    // Для коррекции заполнения зеркал двигаем только основную ось, вторая на мтоит на месте.

    //Какую ось подвоим: FUN_ALPHA_2_HALF_V - верхнее зеркало; FUN_ALPHA - нижнее зеркало;
    int nCurFonCorr = FUN_ALPHA_2_HALF_V;

    if (m_pAll_CNLRs_SM->m_anFunCNLR[nCurFonCorr] == NO_CNLR)
        return;
    QString sTextRead;
    BYTE nAxis_Alpha = m_pAll_CNLRs_SM->m_anFunAxis[nCurFonCorr];
    m_afSM_KSpeed_cur[nCurFonCorr] = - SM_K_SPEED_FAST;
    m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[nCurFonCorr]]
            ->SM_Reverse(sTextRead, m_afSM_KSpeed_cur[nCurFonCorr], nAxis_Alpha);
}

//Работаем по нажатию кнопки. Отпускание - остановка.
void Dlg_Coelostat::on_Btn_Alpha_FORV_REV_CORR_ONE_AXIS_clicked()
{
    // Для работы целостата в режиме управления двумя осями V=1 и V=1/2.
    // Для коррекции заполнения зеркал двигаем только основную ось, вторая на стоит на месте.
    if (m_anCur_SM_SpeedMode_pre[AXIS_ALPHA] == SM_FORV_HOUR)
         on_Btn_Alpha_START_pressed();
    else
        on_Btn_Alpha_STOP_pressed();

    OnReset_Button_Info_Alpha();    //Изменение отображения кнопок от нажатия и соответствующего режима
}


void Dlg_Coelostat::on_Btn_Delta_FORV_FAST_pressed()
{
    // TODO: Add your control notification handler code here
    //if (m_anCur_SM_speed[AXIS_DELTA] == SM_FORV_FAST) return;
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_DELTA] == NO_CNLR) return;
    if ( m_anCur_SM_InPortMode[AXIS_DELTA] == SM_PORT_ON_MAX) return;
    QString sTextRead;
    BYTE nAxis_Delta = m_pAll_CNLRs_SM->m_anFunAxis[FUN_DELTA];
    m_afSM_KSpeed_cur[AXIS_DELTA]=SM_K_SPEED_FAST;
    m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_DELTA]]
            ->SM_Forvard(sTextRead, m_afSM_KSpeed_cur[AXIS_DELTA], nAxis_Delta);
    m_anCur_SM_SpeedMode[AXIS_DELTA]=SM_FORV_FAST;
    m_bool_Delta_On_Go = FALSE;       //Работаем в режиме поиска положения
    m_bool_Delta_On_Go_Cur_Time = FALSE; //Работаем в режиме поиска положения по текущему времени
}


//Работаем по нажатию кнопки. Отпускание - остановка.
void Dlg_Coelostat::OnBtnClickedForvardFast_Delta()
{
   OnBtnClickedStop_Delta();
}

void Dlg_Coelostat::on_Btn_Delta_FORV_SLOW_pressed()
{
    // TODO: Add your control notification handler code here
    //if (m_anCur_SM_speed[AXIS_DELTA] == SM_FORV_SLOW) return;
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_DELTA] == NO_CNLR) return;
    if ( m_anCur_SM_InPortMode[AXIS_DELTA] == SM_PORT_ON_MAX) return;
    QString sTextRead;
    BYTE nAxis_Delta = m_pAll_CNLRs_SM->m_anFunAxis[FUN_DELTA];
    m_afSM_KSpeed_cur[AXIS_DELTA]=SM_K_SPEED_SLOW;
    m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_DELTA]]
            ->SM_Forvard(sTextRead, m_afSM_KSpeed_cur[AXIS_DELTA], nAxis_Delta);
    m_anCur_SM_SpeedMode[AXIS_DELTA]=SM_FORV_SLOW;
    m_bool_Delta_On_Go = FALSE;       //Работаем в режиме поиска положения
    m_bool_Delta_On_Go_Cur_Time = FALSE; //Работаем в режиме поиска положения по текущему времени

}


//Работаем по нажатию кнопки. Отпускание - остановка.
void Dlg_Coelostat::OnBtnClickedForvardSlow_Delta()
{
    OnBtnClickedStop_Delta();
}


void Dlg_Coelostat::OnBtnClickedStop_Delta()
{
    // TODO: Add your control notification handler code here
    //if (m_anCur_SM_speed[AXIS_DELTA] == SM_STOP) return;
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_DELTA] == NO_CNLR) return;
    QString sTextRead;
    BYTE nAxis_Delta = m_pAll_CNLRs_SM->m_anFunAxis[FUN_DELTA];
    m_afSM_KSpeed_cur[AXIS_DELTA] = 0;
    m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_DELTA]]
            ->SM_Stop(sTextRead, nAxis_Delta);
    m_anCur_SM_SpeedMode[AXIS_DELTA]=SM_STOP;
    m_bool_Delta_On_Go = FALSE;       //Работаем в режиме поиска положения
    m_bool_Delta_On_Go_Cur_Time = FALSE; //Работаем в режиме поиска положения по текущему времени
 //   qDebug() << "on_stop";
}

//Работаем по нажатию кнопки. Отпускание - остановка.
void Dlg_Coelostat::on_Btn_Delta_REV_SLOW_pressed()
{
    // TODO: Add your control notification handler code here
    //if (m_anCur_SM_speed[AXIS_ALPHA] == SM_REVR_SLOW) return;
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_DELTA] == NO_CNLR) return;
    if (m_anCur_SM_InPortMode[AXIS_DELTA] == SM_PORT_ON_MIN) return;
    QString sTextRead;
    BYTE nAxis_Delta = m_pAll_CNLRs_SM->m_anFunAxis[FUN_DELTA];
    m_afSM_KSpeed_cur[AXIS_DELTA] = -SM_K_SPEED_SLOW;
    m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_DELTA]]
            ->SM_Reverse(sTextRead, m_afSM_KSpeed_cur[AXIS_DELTA], nAxis_Delta);
    m_anCur_SM_SpeedMode[AXIS_DELTA]=SM_REVR_SLOW;
    m_bool_Delta_On_Go = FALSE;       //Работаем в режиме поиска положения
    m_bool_Delta_On_Go_Cur_Time = FALSE; //Работаем в режиме поиска положения по текущему времени
}

void Dlg_Coelostat::OnBtnClickedReverseSlow_Delta()
{
    OnBtnClickedStop_Delta();
}

//Работаем по нажатию кнопки. Отпускание - остановка.
void Dlg_Coelostat::on_Btn_Delta_REV_FAST_pressed()
{
    // TODO: Add your control notification handler code here
    //if (m_anCur_SM_speed[AXIS_DELTA] == SM_REVR_FAST) return;
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_DELTA] == NO_CNLR) return;
    if (m_anCur_SM_InPortMode[AXIS_DELTA] == SM_PORT_ON_MIN) return;
    QString sTextRead;
    BYTE nAxis_Delta = m_pAll_CNLRs_SM->m_anFunAxis[FUN_DELTA];
    m_afSM_KSpeed_cur[AXIS_DELTA] = - SM_K_SPEED_FAST;
    m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_DELTA]]
            ->SM_Reverse(sTextRead, m_afSM_KSpeed_cur[AXIS_DELTA], nAxis_Delta);
    m_anCur_SM_SpeedMode[AXIS_DELTA]=SM_REVR_FAST;
    m_bool_Delta_On_Go = FALSE;       //Работаем в режиме поиска положения
    m_bool_Delta_On_Go_Cur_Time = FALSE; //Работаем в режиме поиска положения по текущему времени
}

void Dlg_Coelostat::OnBtnClickedReverseFast_Delta()
{
    OnBtnClickedStop_Delta();
}


//Для скана на целостате
void Dlg_Coelostat::on_Btn_Scan_START_pressed()
{
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_SCAN_COELOSTAT] == NO_CNLR)
        return;
    QString sTextRead;
    BYTE nAxis_Scan = m_pAll_CNLRs_SM->m_anFunAxis[FUN_SCAN_COELOSTAT];
    m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT] = - SM_K_SPEED_FAST;

    m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_SCAN_COELOSTAT]]
            ->SM_Reverse(sTextRead, m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT], nAxis_Scan);

    m_anCur_SM_SpeedMode[AXIS_SCAN_COELOSTAT] = SM_SCAN_START;
    //Угол - положение сканировния куда идем  в минутах дуги
    m_fAngle_Minut_Scan_Go = m_fScan_Angle_Minut_Pos_Beg;
    //Работаем в режиме поиска положения
    m_bool_Scan_On_Go = TRUE;

    //При сканировании: Изменение скорости по дельте при изменении скорости по альфе
    On_Scan_Correction_Delta(m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT] * m_pAll_CNLRs_SM->m_anFunMaxSpeed[FUN_SCAN_COELOSTAT]);

    //Для скана на целостате - изменение отображения кнопок от нажатия и соответствующего режима
    OnReset_Button_Info_Scan();
}

void Dlg_Coelostat::on_Btn_Scan_STOP_pressed()
{
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_SCAN_COELOSTAT] == NO_CNLR)
        return;
    QString sTextRead;
    BYTE nAxis_Scan = m_pAll_CNLRs_SM->m_anFunAxis[FUN_SCAN_COELOSTAT];
    m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT] = 0;

    m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_SCAN_COELOSTAT]]
            ->SM_Stop(sTextRead, nAxis_Scan);

    m_anCur_SM_SpeedMode[AXIS_SCAN_COELOSTAT]=SM_STOP;
    //Работаем в режиме поиска положения
    m_bool_Scan_On_Go = FALSE;

    //При сканировании: Изменение скорости по дельте при изменении скорости по альфе
    On_Scan_Correction_Delta(m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT] * m_pAll_CNLRs_SM->m_anFunMaxSpeed[FUN_SCAN_COELOSTAT]);

    //Для скана на целостате - изменение отображения кнопок от нажатия и соответствующего режима
    OnReset_Button_Info_Scan();
}

void Dlg_Coelostat::on_Btn_Scan_NULL_pressed()
{
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_SCAN_COELOSTAT] == NO_CNLR)
        return;
    QString sTextRead;
    BYTE nAxis_Scan = m_pAll_CNLRs_SM->m_anFunAxis[FUN_SCAN_COELOSTAT];

    //Ищем "0" если поз. < 0, то вперед, иначе назад
    if (SM_Scan_StepPos2Angle_Minut() < double(0.0))
    {
        m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT] = SM_K_SPEED_SLOW;
        m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_SCAN_COELOSTAT]]
                ->SM_Forvard(sTextRead, m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT], nAxis_Scan);
        m_anCur_SM_SpeedMode[AXIS_SCAN_COELOSTAT] = SM_FORV_SLOW;
    }
    else
    {
        m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT] = -SM_K_SPEED_SLOW;
        m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_SCAN_COELOSTAT]]
                ->SM_Forvard(sTextRead, m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT], nAxis_Scan);
        m_anCur_SM_SpeedMode[AXIS_SCAN_COELOSTAT] = SM_REVR_SLOW;
    }

    m_fAngle_Minut_Scan_Go = 0.0;   //Угол - положение сканировния куда идем  в минутах дуги
    m_bool_Scan_On_Go = TRUE;       //Работаем в режиме поиска положения

    //При сканировании: Изменение скорости по дельте при изменении скорости по альфе
    On_Scan_Correction_Delta(m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT] * m_pAll_CNLRs_SM->m_anFunMaxSpeed[FUN_SCAN_COELOSTAT]);

    //Для скана на целостате - изменение отображения кнопок от нажатия и соответствующего режима
    OnReset_Button_Info_Scan();
}

void Dlg_Coelostat::on_Btn_Scan_FORV_FAST_pressed()
{
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_SCAN_COELOSTAT] == NO_CNLR) return;
    QString sTextRead;
    BYTE nAxis_Scan = m_pAll_CNLRs_SM->m_anFunAxis[FUN_SCAN_COELOSTAT];
    m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT] = SM_K_SPEED_FAST;
    m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_SCAN_COELOSTAT]]
            ->SM_Forvard(sTextRead, m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT], nAxis_Scan);
    m_anCur_SM_SpeedMode[AXIS_SCAN_COELOSTAT] = SM_FORV_FAST;
    m_fAngle_Minut_Scan_Go = m_fScan_Angle_Minut_Pos_End; //Угол - положение сканировния куда идем  в минутах дуги
    m_bool_Scan_On_Go = TRUE;       //Работаем в режиме поиска положения
    //qDebug() << m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT] * m_pAll_CNLRs_SM->m_anFunMaxSpeed[FUN_SCAN_COELOSTAT];
    On_Scan_Correction_Delta(m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT] * m_pAll_CNLRs_SM->m_anFunMaxSpeed[FUN_SCAN_COELOSTAT]); //При сканировании: Изменение скорости по дельте при изменении скорости по альфе
    OnReset_Button_Info_Scan(); //Для скана на целостате - изменение отображения кнопок от нажатия и соответствующего режима
}

void Dlg_Coelostat::on_Btn_Scan_REV_FAST_pressed()
{
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_SCAN_COELOSTAT] == NO_CNLR) return;
    QString sTextRead;
    BYTE nAxis_Scan = m_pAll_CNLRs_SM->m_anFunAxis[FUN_SCAN_COELOSTAT];
    m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT] = - SM_K_SPEED_FAST;
    m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_SCAN_COELOSTAT]]
            ->SM_Reverse(sTextRead, m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT], nAxis_Scan);
    m_anCur_SM_SpeedMode[AXIS_SCAN_COELOSTAT] = SM_REVR_FAST;
    m_fAngle_Minut_Scan_Go = m_fScan_Angle_Minut_Pos_Beg; //Угол - положение сканировния куда идем  в минутах дуги
    m_bool_Scan_On_Go = TRUE;       //Работаем в режиме поиска положения
    On_Scan_Correction_Delta(m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT] * m_pAll_CNLRs_SM->m_anFunMaxSpeed[FUN_SCAN_COELOSTAT]); //При сканировании: Изменение скорости по дельте при изменении скорости по альфе
    OnReset_Button_Info_Scan(); //Для скана на целостате - изменение отображения кнопок от нажатия и соответствующего режима
}

//При сканировании: Изменение скорости по дельте при изменении скорости сканирования по альфе
void Dlg_Coelostat::On_Scan_Correction_Delta(double dScanAlpha_VStepsOfSec)
{
    if (! m_bScan_On_Delta_Correction) return;//Режим коррекции не работает
    if ( m_bool_Delta_Centering) return;; //Работаем в режиме поиска центрирования дельты от концевиков
    if (dScanAlpha_VStepsOfSec == 0.0) { OnBtnClickedStop_Delta();  return;}

    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_DELTA] == NO_CNLR) return;
    if (m_anCur_SM_InPortMode[AXIS_DELTA] == SM_PORT_ON_MIN) return;
    if (m_anCur_SM_InPortMode[AXIS_DELTA] == SM_PORT_ON_MAX) return;

    double dDaily = m_fAngleDaily_Sun_Delta;// dDaily = pi_my / 4.0;

    double dScanAlpha_SpeedArcsecOfSec = (double) dScanAlpha_VStepsOfSec * (60.0 * 60.0) / m_fScan_SM_Steps_1deg; //Скорость на сканировании по альфе (секунд в секунду)
    double dScanDelta_Corr_SpeedArcsecOfSec = - dScanAlpha_SpeedArcsecOfSec * tan(dDaily); //Скорость на сканировании по делте (секунд в секунду)
    //qDebug() << dScanAlpha_SpeedArcsecOfSec << dScanDelta_Corr_SpeedArcsecOfSec;

    double dScanDelta_VStepsOfSec = dScanDelta_Corr_SpeedArcsecOfSec * m_fDelta_SM_Steps_1deg / (60.0 * 60.0);
    double KSpeed_delta = dScanDelta_VStepsOfSec / m_pAll_CNLRs_SM->m_anFunMaxSpeed[FUN_DELTA];


    QString sTextRead;
    BYTE nAxis_Delta = m_pAll_CNLRs_SM->m_anFunAxis[FUN_DELTA];
    m_afSM_KSpeed_cur[AXIS_DELTA] = KSpeed_delta;
    m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_DELTA]]
            ->SM_Reverse(sTextRead, m_afSM_KSpeed_cur[AXIS_DELTA], nAxis_Delta);
    if (KSpeed_delta > 0) m_anCur_SM_SpeedMode[AXIS_DELTA]=SM_FORV_SLOW;
    else m_anCur_SM_SpeedMode[AXIS_DELTA]=SM_REVR_SLOW;
    //qDebug()<<"Delta speed corr. = " << KSpeed_delta;
    m_bool_Delta_On_Go = FALSE;       //Работаем в режиме поиска положения
    m_bool_Delta_On_Go_Cur_Time = FALSE; //Работаем в режиме поиска положения по текущему времени
}


//Таймер отслеживания изменений положений и состояния контроллера
void Dlg_Coelostat::on_Timer_View()
{
    if (m_bWinMax && m_pCNLR_view_info != NULL){
        QString sStatus=m_pCNLR_view_info->GetStrStatus();
        ui->textEdit_Info->setText(sStatus);
    }
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA] != NO_CNLR)
        //Для Aльфа - таймер отслеживания изменений положений и контроллера (запускается в on_Timer_View())
        on_Timer_Alpha();
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_DELTA] != NO_CNLR)
        //Для Дельты - таймер отслеживания изменений положений и контроллера (запускается в on_Timer_View())
        on_Timer_Delta();
    if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_SCAN_COELOSTAT] != NO_CNLR)
        //Для Сканирования на целостате - таймер отслеживания изменений положений и контроллера (запускается в on_Timer_View())
        on_Timer_Scan();

}

//void Dlg_Coelostat::on_Timer_CNTL()
//{
//     m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA]]
//             ->Send_Command_Ethernet_ON_Port(); // Отправить команду на Ethernet-порт для инициализации портов и поддержания работы контроллера (1 раз в 5сек)
//}


//Для Aльфа - таймер отслеживания изменений положений и контроллера (запускается в on_Timer_View())
void Dlg_Coelostat::on_Timer_Alpha()
{
    CControllerEthernet * pCNLR_Alpha = m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA]];
    BYTE nAxis_Alpha = m_pAll_CNLRs_SM->m_anFunAxis[FUN_ALPHA];
    QString sText, sPlus;
    QDateTime tSys_pos, tSys_cur = QDateTime::currentDateTime();
    double speed_sun = m_pAll_CNLRs_SM->m_anFunMaxSpeed[FUN_ALPHA] * m_fSM_KSpeed_Alpha_Sun;
    double speed_cur = 1000.0 * ((double) pCNLR_Alpha->GetPosAxes(nAxis_Alpha) - m_fAlpha_Steps_From_Pos_0) / double (m_tDataTime_Timer_View_pre.msecsTo(tSys_cur));
    m_fAlpha_Steps_From_Pos_0 = (double) pCNLR_Alpha->GetPosAxes(nAxis_Alpha);

    tSys_pos=SM_Alpha_StepPos2SysTime();
    //qDebug() << tSys_pos.time();

    //для просмотра в QString
    sText=tSys_pos.toString("hh:mm:ss");
    ui->Labl_Alpha_Time->setText(sText);
    double fCurSpeed = m_pAll_CNLRs_SM->m_anFunMaxSpeed[FUN_ALPHA] * m_afSM_KSpeed_cur[AXIS_ALPHA];
    //qDebug() << pCNLR_Alpha->m_anSM_Speed_max[AXIS_ALPHA] << m_fSM_KSpeed_Alpha_Sun << m_afSM_KSpeed_cur[AXIS_ALPHA];
    sText = QString("SM(%1->%2) ms(%3)")
//            .arg(int (fCurSpeed))
            .arg(int (speed_cur))
            .arg(int (1.0005 * fCurSpeed / speed_sun))
            .arg(m_tDataTime_Timer_View_pre.msecsTo(tSys_cur));
    //qDebug() << tSys_cur.msecsTo(m_tDataTime_Timer_View_pre);
    sPlus = QString("Pos(%1) ").arg(int(m_fAlpha_Steps_From_Pos_0));
    sText = sPlus + sText;
    ui->Labl_Alpha_Info->setText(sText);

    //для Функции определения суточной на изображении гида по альфе
    if (m_bool_Get_Daily_Sun_Alpha_On)
    {
//        qDebug() << tSys_cur.
        int nPlusAlpha_Sec = ALPHA_TIME_SECONDS_FOR_DAYLY_SUN;
//        m_tDataTime_Alpha_Go = m_Alpha_SysTime_Pos_0.addSecs(nPlusAlpha_Sec); //Время как положение куда идем в QTime
//        qDebug() << m_Alpha_SysTime_Pos_0.secsTo(tSys_cur);
        ui->Btn_GetDailyLineSun_Alpha->setText(QString("Sa(%1)").arg(nPlusAlpha_Sec - m_Alpha_SysTime_Pos_0.secsTo(tSys_cur)));
        if (m_Alpha_SysTime_Pos_0.secsTo(tSys_cur) >= nPlusAlpha_Sec) //Ждем заданные секунды
        {
            m_fAngleDaily_Sun_Alpha=Get_Daily_Sun_Alpha();//Функция определения суточной по изображению Солнца на гиде по альфе
        }
    }



    //Работаем в режиме отслеживания положение захода Солнца
    if (m_bool_ParkSunSet)
    {
        if (tSys_pos > m_tDataTime_SunSet)
        {
            if (m_pSolarScanner->scanSettings.scanMode != SCANNER_STOP) {
                qDebug() << "Dlg_Coelostat: sigScanStop!";
                emit sigScanStop();
            }
            if ( m_pGuide->isGuiding()) {
                qDebug() << "Dlg_Coelostat: sigGuideStop!";
                emit sigGuideStop(false);
            }


            int hh = PARKING_HOUR, mm = 0, ss = 0;
            m_tDataTime_Alpha_Go = tSys_pos;
            m_tDataTime_Alpha_Go.setTime(QTime(hh, mm, ss));
            if (m_tDataTime_Alpha_Go > tSys_pos)
            {
                 on_Btn_Alpha_FORV_FAST_pressed();
            }
            else
            {
                 on_Btn_Alpha_REV_FAST_pressed();
            }
            m_bool_Alpha_On_Go = true; //Работаем в режиме поиска положения по текущему времени
            m_bool_Alpha_On_Go_Cur_Time = FALSE;
        }
    }

    //Работаем в режиме коррекции положения по гиду Alpha
    if (m_bool_On_Correction_Alpha)
    {
       if (m_dCorrection_Alpha_Seconds < 0) //Если отрицательное значение, то была остановка, и ждём эти секунды;
         if (m_Alpha_SysTime_Pos_0.secsTo(tSys_cur) >= - (qint64) m_dCorrection_Alpha_Seconds) // Если дождались, то включаем часовое ведение
         {
//             qDebug() << "On_Timer_Alpha: m_Alpha_SysTime_Pos_0, m_dCorrection_Alpha_Seconds = " << m_Alpha_SysTime_Pos_0.secsTo(tSys_cur) << m_dCorrection_Alpha_Seconds;
             m_bool_On_Correction_Alpha=false;
             on_Btn_Alpha_Set_Time_clicked();
             on_Btn_Alpha_START_pressed();
//             qDebug() << "On_Timer_alpha: Start_Pressed по результатам проверки смещения";
         }
    }

//    m_tDataTime_Timer_View_pre = tSys_cur; //Возможно не надо. Нужна в последнем обработчике таймера (сейчас в скане на целостате)

    //Количество шагов до остановки двигателя (с учетом скорости, ускорения и времени сработки таймера);
    double delta_step_stop;

    //Работаем в режиме поиска положения
    if (m_bool_Alpha_On_Go)
    {
        if (m_bool_Alpha_On_Go_Cur_Time) //Работаем в режиме поиска положения по текущему времени
        {
//            qDebug() << "   Alpha_On_Go_Cur_Time = " << m_bool_Alpha_On_Go_Cur_Time;
            m_tDataTime_Alpha_Go = tSys_cur;

            delta_step_stop = pCNLR_Alpha-> SM_StepToNextSpeed(fCurSpeed, speed_sun , m_pAll_CNLRs_SM->m_anFunAcceler[FUN_ALPHA], VIEW_TIMER_MSEC + VIEW_TIMER_PLUS_MSEC);
//            qDebug() << tSys_pos.time();
        }
        else
            delta_step_stop = pCNLR_Alpha-> SM_StepToNextSpeed(fCurSpeed, 0.0 , m_pAll_CNLRs_SM->m_anFunAcceler[FUN_ALPHA], VIEW_TIMER_MSEC + VIEW_TIMER_PLUS_MSEC);


        qint64 nMSec_delta = tSys_pos.msecsTo(m_tDataTime_Alpha_Go);

        double dSpeedSunStep_ms = (double) speed_sun / 1000.0;//Солнечная скорость (steps/msec)
        double delta_msec_stop = delta_step_stop / dSpeedSunStep_ms;
        //qDebug() << tSys_pos.time() << m_tDataTime_Alpha_Go.time() << nMSec_delta << delta_msec;
        if  (( m_afSM_KSpeed_cur[AXIS_ALPHA] > 0 && nMSec_delta <  + qint64 (delta_msec_stop)) ||
             ( m_afSM_KSpeed_cur[AXIS_ALPHA] < 0 && nMSec_delta >  - qint64 (delta_msec_stop)))
        {
            if (m_bool_Alpha_On_Go_Cur_Time)
            {
                on_Btn_Alpha_START_pressed();
//                qDebug() << "on_Timer_Alpha: start_pressed!";
            }
            else
            {
//                qDebug() << "on_Timer_Alpha: stop_pressed!";
                on_Btn_Alpha_STOP_pressed();
            }

            //Процедура коррекции положения по гиду
            if (m_bool_On_Correction_Alpha)
            {
                m_bool_On_Correction_Alpha=false;
                on_Btn_Alpha_Set_Time_clicked();
                on_Btn_Alpha_START_pressed();
//                qDebug() << "on_Timer_Alpha: start_pressed!";
            }
            m_bool_Alpha_On_Go = FALSE;
            m_bool_Alpha_On_Go_Cur_Time = FALSE;
        }
    }
}

//Для Дельты - таймер отслеживания изменений положений и контроллера (запускается в on_Timer_View())
void Dlg_Coelostat::on_Timer_Delta()
{
    CControllerEthernet * pCNLR_Delta = m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_DELTA]];
    BYTE nAxis_Delta = m_pAll_CNLRs_SM->m_anFunAxis[FUN_DELTA];
    QString sText, sPlus;

    QDateTime tSys_cur = QDateTime::currentDateTime();
    double speed_cur = 1000.0 * (pCNLR_Delta->GetPosAxes(nAxis_Delta) - m_fDelta_Steps_From_Pos_0) / double (m_tDataTime_Timer_View_pre.msecsTo(tSys_cur));
    m_fDelta_Steps_From_Pos_0 = pCNLR_Delta->GetPosAxes(nAxis_Delta);

    double dDelta_Angle_pos  = SM_Delta_StepPos2Angle();

    ui->Labl_Delta_Angle->setText(funRAngle2String(dDelta_Angle_pos));
    double fCurSpeed = m_pAll_CNLRs_SM->m_anFunMaxSpeed[FUN_DELTA] * m_afSM_KSpeed_cur[AXIS_DELTA];
    //qDebug() << m_pAll_CNLRs_SM->m_anFunMaxSpeed[FUN_DELTA] << m_afSM_KSpeed_cur[AXIS_DELTA];
    sText = QString("SM(%1->%2 \'/s)")
            //.arg(int (fCurSpeed))
            .arg(int (speed_cur))
            .arg(int (60.000005 * fCurSpeed / m_fDelta_SM_Steps_1deg));

    sPlus = QString("Pos(%1) ").arg(int(m_fDelta_Steps_From_Pos_0));
    sText = sPlus + sText;
    ui->Labl_Delta_Info->setText(sText);

//    m_tDataTime_Timer_View_pre = tSys_cur; //Возможно не надо. Нужна в последнем обработчике таймера (сейчас в скане на целостате)

//    for (int i=10; i<16; i++) qDebug() << i << "->" << (pCNLR_Delta->GetInPorts() & (1 << i));//Вывод состояний портов
    //для концевиков - минимального положения
    ui->Labl_Delta_Error->setText(""); //("OK");
    m_anCur_SM_InPortMode[AXIS_DELTA] = SM_PORT_OFF;
    if (m_pAll_CNLRs_SM->Get_On_Port_MAX_MIN(FUN_DELTA, PORT_ON_MIN)) //для концевиков определение максимального или минимального положения
    {
        if (m_afSM_KSpeed_cur[AXIS_DELTA] < 0.0)
            OnBtnClickedStop_Delta();
        m_anCur_SM_InPortMode[AXIS_DELTA] = SM_PORT_ON_MIN;
        ui->Labl_Delta_Error->setText("<FONT COLOR=#990000>It's MIN!</FONT>");
    }
    //для концевиков - максимального положения
    if (m_pAll_CNLRs_SM->Get_On_Port_MAX_MIN(FUN_DELTA, PORT_ON_MAX)) //для концевиков определение максимального или минимального положения
    {
        if (m_afSM_KSpeed_cur[AXIS_DELTA] > 0.0)
            OnBtnClickedStop_Delta();
        m_anCur_SM_InPortMode[AXIS_DELTA] = SM_PORT_ON_MAX;
        ui->Labl_Delta_Error->setText("<FONT COLOR=#990000>It's MAX!</FONT>");
    }

    if (m_anCur_SM_InPortMode[AXIS_DELTA] != SM_PORT_ON_MIN     //Для установки в конце центрирования значения текущей координаты Солнца
        && m_anCur_SM_InPortMode[AXIS_DELTA] != SM_PORT_ON_MAX
        && m_bool_Delta_Centering_On && speed_cur == 0)
    {
        m_bool_Delta_Centering_On = false;
        m_bool_Delta_Centering = false;
        on_Btn_Delta_Set_Delta_clicked();
    }

    if (m_bool_Delta_Centering && m_anCur_SM_InPortMode[AXIS_DELTA] == SM_PORT_ON_MAX)
    {
        //Работаем в режиме поиска центра от концевиков MIN и MAX
        double dAnglDelCentr = ((m_fDelta_AnglMinut_Min2Max / 2.0)/ 60.0) * pi_my / 180.0;
        m_fAngle_Delta_Go = dDelta_Angle_pos - dAnglDelCentr;
        qDebug() << dDelta_Angle_pos * 180.0 / pi_my << m_fAngle_Delta_Go * 180.0 / pi_my;
        m_bool_Delta_Centering = false;
        on_Btn_Delta_REV_FAST_pressed();
        m_bool_Delta_On_Go = true;
    }

    //qDebug() << pCNLR_Delta->GetInPorts() << int(pCNLR_Delta->GetInPorts() & (1 << (m_pAll_CNLRs_SM->m_anFunNumInPorts_MAX[FUN_DELTA] - 1)));
    if (m_bool_Delta_On_Go)
    {
        //Работаем в режиме поиска положения
        if (m_bool_Delta_On_Go_Cur_Time)
        {
           //Работаем в режиме поиска положения по текущему времени
           cSunCoordinates sunPos=funSunPos_curLoc_tSys(tSys_cur, TIME_HOUR_LOC2UT);
           m_fAngle_Delta_Go = sunPos.m_dDeclination;       //.m_dZenithAngle *pi / 180.0;
        }

        //Количество шагов до остановки двигателя (с учетом скорости, ускорения и времени сработки таймера);
        double delta_stop_step  = 400 + pCNLR_Delta-> SM_StepToNextSpeed(fCurSpeed, 0.0, m_pAll_CNLRs_SM->m_anFunAcceler[FUN_DELTA], VIEW_TIMER_MSEC + VIEW_TIMER_PLUS_MSEC);
        double delta_stop_Angle = (double) (pi_my /180.0) * delta_stop_step / m_fDelta_SM_Steps_1deg;
//        qDebug() << "delta_stop_step=" << delta_stop_step << " m_fDelta_SM_Steps_1deg=" << m_fDelta_SM_Steps_1deg;
//        qDebug() << "m_fAngle_Delta_Go - dDelta_Angle_pos=" << (60.0 * 60.0 * 180.0 / pi_my) * (m_fAngle_Delta_Go - dDelta_Angle_pos) << "delta_stop_Angle" << (60.0 * 60.0 * 180.0 / pi_my) * delta_stop_Angle;

        if  (( m_afSM_KSpeed_cur[AXIS_DELTA] > 0.0 && m_fAngle_Delta_Go - dDelta_Angle_pos <  + delta_stop_Angle)
         ||  ( m_afSM_KSpeed_cur[AXIS_DELTA] < 0.0 && m_fAngle_Delta_Go - dDelta_Angle_pos >  - delta_stop_Angle))
        {
            OnBtnClickedStop_Delta();
            if (m_bool_Get_Daily_Sun_Delta_On)  //Режим определения суточной на изображении гида. По дельте
               m_fAngleDaily_Sun_Delta = Get_Daily_Sun_Delta();//Функция определения суточной по изображению Солнца на гиде.

            if (m_bool_On_Correction_Delta)//Процедура коррекции положения по гиду
            {
                m_bool_On_Correction_Delta=false;
                on_Btn_Delta_Set_Delta_clicked();
            }
        }
    }
}

//Для Скана на целостате - таймер отслеживания изменений положений и контроллера (запускается в on_Timer_View())
void Dlg_Coelostat::on_Timer_Scan()
{
    CControllerEthernet * pCNLR_Scan = m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_SCAN_COELOSTAT]];
    BYTE nAxis_Scan = m_pAll_CNLRs_SM->m_anFunAxis[FUN_SCAN_COELOSTAT];
    QString sText, sPlus;

    QDateTime tSys_cur = QDateTime::currentDateTime();
    double speed_cur = 1000.0 * (pCNLR_Scan->GetPosAxes(nAxis_Scan) - m_fScan_Steps_From_Pos_0) / double (m_tDataTime_Timer_View_pre.msecsTo(tSys_cur));
    m_fScan_Steps_From_Pos_0 = pCNLR_Scan->GetPosAxes(nAxis_Scan);

    double dScan_Angle_Minut_pos  = SM_Scan_StepPos2Angle_Minut();

    ui->Labl_Scan_Pos->setText(funRAngle2String_Minut((pi_my / 180.0) * dScan_Angle_Minut_pos / double(60)));
    double fCurSpeed = m_pAll_CNLRs_SM->m_anFunMaxSpeed[FUN_SCAN_COELOSTAT] * m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT];
    //qDebug() << m_pAll_CNLRs_SM->m_anFunMaxSpeed[FUN_SCAN_COELOSTAT] << m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT];
    sText = QString("SM(%1->%2 \"/s)")
            //.arg(int (fCurSpeed))
            .arg(int (speed_cur))
            .arg(int (60.00000005 * 60.0 * fCurSpeed / m_fScan_SM_Steps_1deg));

    sPlus = QString("Pos(%1) ").arg(int(m_fScan_Steps_From_Pos_0));
    sText = sPlus + sText;
    ui->Labl_Scan_Info->setText(sText);
    m_tDataTime_Timer_View_pre = tSys_cur; //Возможно не надо. Нужна в последнем обработчике таймера (сейчас в скане на целостате)

    if (m_bool_Scan_On_Go)//Работаем в режиме поиска положения
    {
        //Количество шагов до остановки двигателя (с учетом скорости, ускорения и времени сработки таймера);
        //double delta_stop_step  = 400 + pCNLR_Scan-> SM_StepToNextSpeed(fCurSpeed, 0.0, m_pAll_CNLRs_SM->m_anFunAcceler[FUN_SCAN_COELOSTAT], VIEW_TIMER_MSEC + VIEW_TIMER_PLUS_MSEC);
        double delta_stop_step  = 200 + pCNLR_Scan-> SM_StepToNextSpeed(fCurSpeed, 0.0, m_pAll_CNLRs_SM->m_anFunAcceler[FUN_SCAN_COELOSTAT], VIEW_TIMER_MSEC + VIEW_TIMER_PLUS_MSEC);
        double delta_stop_Angle_Minut = (double) 60.0 * delta_stop_step / m_fScan_SM_Steps_1deg;
//        qDebug() << delta_stop_step;
//        qDebug() << m_fAngle_Minut_Scan_Go << dScan_Angle_Minut_pos <<  delta_stop_Angle_Minut <<  m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT];
        if  (( m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT] > 0.0 && m_fAngle_Minut_Scan_Go - dScan_Angle_Minut_pos <  + delta_stop_Angle_Minut)
            ||  ( m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT] < 0.0 && m_fAngle_Minut_Scan_Go - dScan_Angle_Minut_pos >  - delta_stop_Angle_Minut))
        {
            double speed_scan_step_sec = m_fScan_SM_Steps_1deg * (m_fScan_Speed_MinutOfMinut / (60.0 * 60.0));     //Скорость сканирования (шагов в секунду)
            if (m_anCur_SM_SpeedMode[AXIS_SCAN_COELOSTAT] == SM_SCAN_START)  //Включен режим сканирования
            {
                if (m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT] == - SM_K_SPEED_FAST)   //Пришли к началу сканирования
                {
//                    qDebug() << "Дошли" << m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT] << bool(m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT] == - SM_K_SPEED_FAST);
                    QString sTextRead;
                    m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT] = speed_scan_step_sec / m_pAll_CNLRs_SM->m_anFunMaxSpeed[FUN_SCAN_COELOSTAT];
                    m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_SCAN_COELOSTAT]]
                            ->SM_Forvard(sTextRead, m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT], nAxis_Scan);
                    m_fAngle_Minut_Scan_Go = m_fScan_Angle_Minut_Pos_End; //Угол - положение сканировния куда идем  в минутах дуги
                    On_Scan_Correction_Delta(m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT] * m_pAll_CNLRs_SM->m_anFunMaxSpeed[FUN_SCAN_COELOSTAT]); //При сканировании: Изменение скорости по дельте при изменении скорости по альфе
                    OnReset_Button_Info_Scan(); //Для скана на целостате - изменение отображения кнопок от нажатия и соответствующего режима
                    m_pSolarScanner->StartSaveCamera();//Метод для запуска сохранения кадров с камеры
                    return ;
                }
                if (m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT] == speed_scan_step_sec / m_pAll_CNLRs_SM->m_anFunMaxSpeed[FUN_SCAN_COELOSTAT]) //Прошло сканирование
                {
                    QString sTextRead;
                    m_pSolarScanner->StopSaveCamera(); // Метод для остановки сохранения кадров с камеры
                    m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT] = - SM_K_SPEED_FAST;
                    m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_SCAN_COELOSTAT]]
                            ->SM_Reverse(sTextRead, m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT], nAxis_Scan);
                    m_fAngle_Minut_Scan_Go = m_fScan_Angle_Minut_Pos_Beg; //Угол - положение сканировния куда идем  в минутах дуги
                    On_Scan_Correction_Delta(m_afSM_KSpeed_cur[AXIS_SCAN_COELOSTAT] * m_pAll_CNLRs_SM->m_anFunMaxSpeed[FUN_SCAN_COELOSTAT]); //При сканировании: Изменение скорости по дельте при изменении скорости по альфе
                    OnReset_Button_Info_Scan(); //Для скана на целостате - изменение отображения кнопок от нажатия и соответствующего режима

                    return ;
                }
            }
            else
            {
                on_Btn_Scan_STOP_pressed();
            }
        }
    }
}


void Dlg_Coelostat::on_Btn_Alpha_GoTo_clicked()
{
    Dlg_Coelostat_Alpha_GoTo* pdlg = new Dlg_Coelostat_Alpha_GoTo();

    m_PosWin = parentWidget()->pos();
    //qDebug() << "POS = " << m_PosWin;

    pdlg->move(m_PosWin.x() + 300, m_PosWin.y() + 70);
    QDateTime tPos_cur = SM_Alpha_StepPos2SysTime();
    pdlg->m_tDataTime_old = tPos_cur;
    pdlg->m_dlg_tDataTime_SunSet = m_tDataTime_SunSet;          //Время заката Солнца
    pdlg->m_dlg_bool_ParkSunSet  = m_bool_ParkSunSet;           //Отслеживать положение захода Солнца
    if (pdlg->exec())
    {   //OK//
        m_tDataTime_Alpha_Go= pdlg->m_tDataTime_cur;
//        QString sTextRead;
        if (m_tDataTime_Alpha_Go > tPos_cur)
            on_Btn_Alpha_FORV_FAST_pressed();
        else
            on_Btn_Alpha_REV_FAST_pressed();

        m_bool_Alpha_On_Go = true;                         //Работаем в режиме поиска положения
        m_bool_Alpha_On_Go_Cur_Time = pdlg->m_On_Cur_Time; //Работаем в режиме поиска положения по текущему времени
        m_bool_ParkSunSet = pdlg->m_dlg_bool_ParkSunSet;
//        m_tDataTime_Timer_View_pre = QDateTime::currentDateTime();

    }
    else
    { //Cancel
    }

    OnReset_Button_Info_Alpha();//Изменение отображения кнопок от нажатия и соответствующего режима
    delete pdlg;
}

void Dlg_Coelostat::on_Btn_Delta_GoTo_clicked()
{
    Dlg_Coelostat_Delta_GoTo* pdlg = new Dlg_Coelostat_Delta_GoTo();
    m_PosWin = parentWidget()->pos();
    pdlg->move(m_PosWin.x() + 300, m_PosWin.y() + 280);
    double dDelta_Angle_pos  = SM_Delta_StepPos2Angle();
    pdlg->m_fAngle_Delta_Go_cur = pdlg->m_fAngle_Delta_Go_old = dDelta_Angle_pos;
    pdlg->m_bool_Delta_On_Go_Cur_Time = false;
    if (pdlg->exec())
    {//OK//
        m_bool_Delta_Centering    = pdlg->m_bool_Delta_Centering; //Работаем в режиме поиска центрирования дельты от концевиков
        m_bool_Delta_Centering_On = pdlg->m_bool_Delta_Centering; //Для установки в конце центрирования значения текущей координаты Солнца
        m_fAngle_Delta_Go = pdlg->m_fAngle_Delta_Go_cur;  //Угол - положение дельты куда идем в радианах
        if (m_bool_Delta_Centering)
        {
            double dAnglDelCentr = ((m_fDelta_AnglMinut_Min2Max / 2.0)/ 60.0) * pi_my / 180.0;
            if (m_anCur_SM_InPortMode[AXIS_DELTA] == SM_PORT_ON_MIN)
            {   m_fAngle_Delta_Go = dDelta_Angle_pos + dAnglDelCentr;
                m_bool_Delta_Centering = false;
                on_Btn_Delta_FORV_FAST_pressed();

            } else  if (m_anCur_SM_InPortMode[AXIS_DELTA] == SM_PORT_ON_MAX)
              { m_fAngle_Delta_Go = dDelta_Angle_pos - dAnglDelCentr;
                m_bool_Delta_Centering = false;
                on_Btn_Delta_REV_FAST_pressed();
              }
                else { m_fAngle_Delta_Go = dDelta_Angle_pos + 3 * dAnglDelCentr;
                     on_Btn_Delta_FORV_FAST_pressed();
                }
           // qDebug() << m_bool_Delta_Centering << m_fAngle_Delta_Go;
        }
        else {
            if (m_fAngle_Delta_Go > dDelta_Angle_pos)
                 on_Btn_Delta_FORV_FAST_pressed();
            else on_Btn_Delta_REV_FAST_pressed();
        }
        m_bool_Delta_On_Go = true;
        m_bool_Delta_On_Go_Cur_Time = pdlg->m_bool_Delta_On_Go_Cur_Time;
    } else { //Cancel

    }

    delete pdlg;
}

void Dlg_Coelostat::on_Btn_Scan_Init_clicked()
{
    Dlg_Coelostat_Scans* pdlg = new Dlg_Coelostat_Scans();
    m_PosWin = parentWidget()->pos();
    pdlg->move(m_PosWin.x() + 300, m_PosWin.y() + 400);

    pdlg->m_dlg_fScan_Angle_Minut_Pos_Beg = m_fScan_Angle_Minut_Pos_Beg;    //Угол - положения начала сканирования в минутах дуги
    pdlg->m_dlg_fScan_Angle_Minut_Pos_End = m_fScan_Angle_Minut_Pos_End;    //Угол - положения конца  сканирования в минутах дуги
    pdlg->m_dlg_fScan_Speed_MinutOfMinut  = m_fScan_Speed_MinutOfMinut;     //Скорость сканирования (минут дуги в минуту)
    pdlg->m_dlg_bScan_On_Scan_back = m_bScan_On_Scan_back;
    pdlg->m_dlg_bScan_On_Delta_Correction = m_bScan_On_Delta_Correction;
//    qDebug() << "m_bScan_On_Delta_Correction=" << m_bScan_On_Delta_Correction;
//    qDebug() << pdlg->m_dlg_fScan_Angle_Minut_Pos_Beg << pdlg->m_dlg_fScan_Angle_Minut_Pos_End;
    if (pdlg->exec())
    {//OK//
        m_fScan_Angle_Minut_Pos_Beg = pdlg->m_dlg_fScan_Angle_Minut_Pos_Beg;
        m_fScan_Angle_Minut_Pos_End = pdlg->m_dlg_fScan_Angle_Minut_Pos_End;
        m_fScan_Speed_MinutOfMinut  = pdlg->m_dlg_fScan_Speed_MinutOfMinut;
        m_bScan_On_Scan_back = pdlg->m_dlg_bScan_On_Scan_back;
        m_bScan_On_Delta_Correction = pdlg->m_dlg_bScan_On_Delta_Correction;
    }
}

void Dlg_Coelostat::on_Btn_Alpha_Set_Time_clicked()
{
    QDateTime tSys_cur = QDateTime::currentDateTime();
    CControllerEthernet * pCNLR = m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA]];
    BYTE nAxis_Alpha = m_pAll_CNLRs_SM->m_anFunAxis[FUN_ALPHA];
    m_Alpha_SysTime_Pos_0 = tSys_cur;
    //qDebug() << m_Alpha_SysTime_Pos_0;
    m_fAlpha_Steps_From_Pos_0 = 0;
    // Отправить команду на Ethernet-порт для установки текущих координат определенной оси
    pCNLR->Send_Command_Ethernet_SetPosAxes(0, nAxis_Alpha);
}

void Dlg_Coelostat::on_Btn_Delta_Set_Delta_clicked()
{
    QDateTime tSys_cur = QDateTime::currentDateTime();
    CControllerEthernet * pCNLR = m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_DELTA]];
    BYTE nAxis_Delta = m_pAll_CNLRs_SM->m_anFunAxis[FUN_DELTA];
    cSunCoordinates sunPos=funSunPos_curLoc_tSys(tSys_cur, TIME_HOUR_LOC2UT);
    m_fDelta_Angle_Pos_0 = sunPos.m_dDeclination;//.m_dZenithAngle *pi / 180.0;
    //qDebug()  << m_fDelta_Angle_Pos_0 * 180.0 / pi_my;
    m_fDelta_Steps_From_Pos_0 = 0;  //Шагов двигателя от 0 установки времени
    // Отправить команду на Ethernet-порт для установки текущих координат определенной оси
    pCNLR->Send_Command_Ethernet_SetPosAxes(0, nAxis_Delta);
}

void Dlg_Coelostat::on_Btn_Scan_Set_0_pressed()
{
    CControllerEthernet * pCNLR = m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_SCAN_COELOSTAT]];
    BYTE nAxis_Scan_С = m_pAll_CNLRs_SM->m_anFunAxis[FUN_SCAN_COELOSTAT];
    m_fScan_Angle_Minut_Pos_0 = 0;
    m_fScan_Steps_From_Pos_0 = 0;
    // Отправить команду на Ethernet-порт для установки текущих координат определенной оси
    pCNLR->Send_Command_Ethernet_SetPosAxes(0, nAxis_Scan_С);

}

//Инициализация телескопа, поиск Солнца по альфе, дельта на центр, сканирование в "0"
void Dlg_Coelostat::on_Btn_InitTelescope_pressed()
{
    //Ищем положение Солнца по альфе
    QDateTime tSys_cur, tPos_cur;

    tSys_cur =QDateTime::currentDateTime();
    tPos_cur=SM_Alpha_StepPos2SysTime();
    m_tDataTime_Alpha_Go = tSys_cur;

    qDebug() << m_tDataTime_Alpha_Go.toString();

    if (m_tDataTime_Alpha_Go > tPos_cur)
    {
        on_Btn_Alpha_FORV_FAST_pressed();
    }
    else
    {
        on_Btn_Alpha_REV_FAST_pressed();
    }

    m_bool_Alpha_On_Go = true;          //Работаем в режиме поиска положения
    m_bool_Alpha_On_Go_Cur_Time = true; //Работаем в режиме поиска положения по текущему времени

    //Ищем центральное положение подвижки по делте
    double dDelta_Angle_pos  = SM_Delta_StepPos2Angle();
    double dAnglDelCentr = ((m_fDelta_AnglMinut_Min2Max / 2.0)/ 60.0) * pi_my / 180.0;
    m_bool_Delta_Centering    = true; //Работаем в режиме поиска центрирования дельты от концевиков
    m_bool_Delta_Centering_On = true; //Для установки в конце центрирования значения текущей координаты Солнца

    if (m_anCur_SM_InPortMode[AXIS_DELTA] == SM_PORT_ON_MIN)
    {
        m_fAngle_Delta_Go = dDelta_Angle_pos + dAnglDelCentr;
        m_bool_Delta_Centering = false;
        on_Btn_Delta_FORV_FAST_pressed();
    }
    else  if (m_anCur_SM_InPortMode[AXIS_DELTA] == SM_PORT_ON_MAX)
    {
        m_fAngle_Delta_Go = dDelta_Angle_pos - dAnglDelCentr;
        m_bool_Delta_Centering = false;
        on_Btn_Delta_REV_FAST_pressed();
    }
    else
    {
        m_fAngle_Delta_Go = dDelta_Angle_pos + 3 * dAnglDelCentr;
        on_Btn_Delta_FORV_FAST_pressed();
    }
    m_bool_Delta_On_Go = true;

    //Нулевое положение на подвижке сканирования
    on_Btn_Scan_NULL_pressed();

}


//Создание или чтение файла конфигурации
int Dlg_Coelostat::Load_Coelostat_CFG()
{
 //	_wsetlocale(LC_CTYPE , _T("rus"));//Для записи русских букв в файл

    CControllerEthernet * pCNLR = m_pAll_CNLRs_SM->m_apCNLRs[m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA]];
    QString sMsg, sFN_CFG = config_coelostat;
    QMessageBox msgBox;
    QFile fileCFG(sFN_CFG);
    if (!fileCFG.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        int ret_msgBox = TRUE;
        sMsg = QString("No CFG file %1. \nCreate new?").arg(sFN_CFG);
        msgBox.setText("Error..");
        msgBox.setInformativeText(sMsg);
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Save);
        ret_msgBox = msgBox.exec();

        if (ret_msgBox == TRUE || ret_msgBox == QMessageBox::Save)
        {
            //Сохранение файла конфигурации c сообщениями
            if (Save_Coelostat_CFG(TRUE) == 0)
                return -1;
            else
                return 1;
        }
        else
            return -1;
//            exit(0);//Без файла конфигурации выход из программы
    }
    else
    {
        const int nMaxLen=300; char bufStr[nMaxLen];
        fileCFG.readLine(bufStr, nMaxLen); //пропускаем заголовок
        fileCFG.readLine(bufStr, nMaxLen); //пропускаем заголовок
        fileCFG.readLine(bufStr, nMaxLen); //пропускаем заголовок альфа

        fileCFG.readLine(bufStr, nMaxLen); sMsg = QString(bufStr);  //Часовая (солнечная) скорость ШД в шагах в секунду
        sMsg=sMsg.right(sMsg.length() - sMsg.indexOf(':') - 2);
        m_nSM_Speed_Alpha_Sun_StepPerSec =  sMsg.toLong();

        //qDebug() << "Есть 2";
        //qDebug() << pCNLR->m_anSM_Speed_max[AXIS_ALPHA];
        if (m_pAll_CNLRs_SM->m_anFunCNLR[FUN_ALPHA] != NO_CNLR)

            m_fSM_KSpeed_Alpha_Sun = 0;
            //Множитель для часовой скорости от максимальной скорости ШД
            if (m_pAll_CNLRs_SM->isInitSuccess())
                m_fSM_KSpeed_Alpha_Sun = (double) m_nSM_Speed_Alpha_Sun_StepPerSec / pCNLR->m_anSM_Speed_max[AXIS_ALPHA];

        int wHour, wMinute, wSecond, wMilliseconds;
        fileCFG.readLine(bufStr, nMaxLen); sMsg= QString(bufStr); //Координата(время) остановки ШД
        sMsg=sMsg.right(sMsg.length()-sMsg.indexOf(':')-2); 	wHour=          sMsg.left(2).toInt();
        sMsg=sMsg.right(sMsg.length()-sMsg.indexOf(':')-1);		wMinute=        sMsg.left(2).toInt();
        sMsg=sMsg.right(sMsg.length()-sMsg.indexOf(':')-1);		wSecond=        sMsg.left(2).toInt();
        sMsg=sMsg.right(sMsg.length()-sMsg.indexOf('.')-1);		wMilliseconds=  sMsg.left(3).toInt();
        m_Alpha_SysTime_Pos_0.currentDateTime();
        //qDebug() << m_Alpha_SysTime_Pos_0;
        m_Alpha_SysTime_Pos_0.setTime(QTime(wHour, wMinute, wSecond, wMilliseconds));

        fileCFG.readLine(bufStr, nMaxLen); //пропускаем заголовок Дельта
        fileCFG.readLine(bufStr, nMaxLen); //пропускаем заголовок Дельта
        fileCFG.readLine(bufStr, nMaxLen); sMsg = QString(bufStr);  //Кол-во шагов ШД для перемещения на 1 градус (шаг/грд)
        sMsg=sMsg.right(sMsg.length()-sMsg.indexOf(':')-1);
        m_fDelta_SM_Steps_1deg =  sMsg.toDouble();

        fileCFG.readLine(bufStr, nMaxLen); sMsg = QString(bufStr);  //Для дельты. Сколько угловых минут от концевика MIN до MAX
        sMsg=sMsg.right(sMsg.length()-sMsg.indexOf(':')-1);
        m_fDelta_AnglMinut_Min2Max =  sMsg.toDouble();

        cAngle Angle;
        fileCFG.readLine(bufStr, nMaxLen); sMsg= QString(bufStr); //Координата(угол Дельты) остановки ШД
        sMsg=sMsg.right(sMsg.length()-sMsg.indexOf(':')-2); 	Angle.cZnak=    sMsg.at(0).toLatin1();
        Angle.iDeg=     sMsg.left(3).toInt();
        //sMsg=sMsg.right(sMsg.length()-sMsg.indexOf('°')-1);		Angle.iMinut=   sMsg.left(2).toInt();
        sMsg=sMsg.right(sMsg.length() - 4);                    	Angle.iMinut=   sMsg.left(2).toInt();
        sMsg=sMsg.right(sMsg.length()-sMsg.indexOf('\'')-1);	Angle.fSec=     sMsg.left(4).toFloat();

        m_fDelta_Angle_Pos_0 = funAngle2Radian(Angle);//Перевод угла(грд, мин, сек) в радианы

        fileCFG.readLine(bufStr, nMaxLen); //пропускаем заголовок для Скана
        fileCFG.readLine(bufStr, nMaxLen); //пропускаем заголовок для Скана
        fileCFG.readLine(bufStr, nMaxLen); sMsg = QString(bufStr);  //Кол-во шагов ШД для перемещения на 1 градус (шаг/грд)
        sMsg=sMsg.right(sMsg.length()-sMsg.indexOf(':')-1);
        m_fScan_SM_Steps_1deg =  sMsg.toDouble();

        fileCFG.readLine(bufStr, nMaxLen); sMsg= QString(bufStr); //Координата(угол для Скана) остановки ШД
        sMsg=sMsg.right(sMsg.length()-sMsg.indexOf(':')-2); 	Angle.cZnak=    sMsg.at(0).toLatin1();
        Angle.iDeg=   0;
        sMsg=sMsg.right(sMsg.length() - 1);                    	Angle.iMinut=   sMsg.left(2).toInt();
        sMsg=sMsg.right(sMsg.length()-sMsg.indexOf('\'')-1);	Angle.fSec=     sMsg.left(4).toFloat();

        m_fScan_Angle_Minut_Pos_0 = funAngle2Radian(Angle) * 180.0 * 60.0 / pi_my;//Перевод угола(грд, мин, сек) в радианы, потом в минуты дуги (')

        fileCFG.readLine(bufStr, nMaxLen); sMsg= QString(bufStr); //Угол начала сканирования
        sMsg=sMsg.right(sMsg.length()-sMsg.indexOf(':')-2); 	Angle.cZnak=    sMsg.at(0).toLatin1();
        Angle.iDeg =0;
        sMsg=sMsg.right(sMsg.length() - 1);                    	Angle.iMinut=   sMsg.left(2).toInt();
        //qDebug() << sMsg;
        sMsg=sMsg.right(sMsg.length()-sMsg.indexOf('\'')-1);	Angle.fSec=     sMsg.left(4).toFloat();

        m_fScan_Angle_Minut_Pos_Beg = funAngle2Radian(Angle) * 180.0 * 60.0 / pi_my;//Перевод угола(грд, мин, сек) в радианы, потом в минуты дуги (')
        //qDebug() << Angle.cZnak << Angle.iMinut;

        fileCFG.readLine(bufStr, nMaxLen); sMsg= QString(bufStr); //Угол конца сканирования
        sMsg=sMsg.right(sMsg.length()-sMsg.indexOf(':')-2); 	Angle.cZnak=    sMsg.at(0).toLatin1();
        Angle.iDeg =0;
        sMsg=sMsg.right(sMsg.length() - 1);                    	Angle.iMinut=   sMsg.left(2).toInt();
        sMsg=sMsg.right(sMsg.length()-sMsg.indexOf('\'')-1);	Angle.fSec=     sMsg.left(4).toFloat();

        m_fScan_Angle_Minut_Pos_End = funAngle2Radian(Angle) * 180.0 * 60.0 / pi_my;//Перевод угола(грд, мин, сек) в радианы, потом в минуты дуги (')

        fileCFG.readLine(bufStr, nMaxLen); sMsg = QString(bufStr);  //Скорость сканирования ('/минуту)
        sMsg=sMsg.right(sMsg.length()-sMsg.indexOf(':')-1);
        m_fScan_Speed_MinutOfMinut =  sMsg.toDouble();

        fileCFG.readLine(bufStr, nMaxLen); sMsg = QString(bufStr);  //Cканирование с коррекцией положения по дельте
        sMsg=sMsg.right(sMsg.length()-sMsg.indexOf(':')-1);

        if (sMsg.contains("Да"))
            m_bScan_On_Delta_Correction = true;
        else
            m_bScan_On_Delta_Correction = false;
        //qDebug() << sMsg << m_bScan_On_Delta_Correction;

        fileCFG.readLine(bufStr, nMaxLen); //пропускаем заголовок Гидирование
        fileCFG.readLine(bufStr, nMaxLen); //пропускаем заголовок Гидирование

        fileCFG.readLine(bufStr, nMaxLen); sMsg = QString(bufStr);  //Наклон суточной на изображении гида. По дельте.
        sMsg=sMsg.right(sMsg.length()-sMsg.indexOf(':')-1);
        m_fAngleDaily_Sun_Delta =  sMsg.toDouble() * pi_my / 180.0;

//        fileCFG.readLine(bufStr, nMaxLen); //пропускаем заголовок Настройки системы
//        fileCFG.readLine(bufStr, nMaxLen); //пропускаем заголовок Настройки системы
//        fileCFG.readLine(bufStr, nMaxLen); sMsg= QString(bufStr); //Положение окна X
//        sMsg=sMsg.right(sMsg.length()-sMsg.indexOf(':') - 2);
//        m_PosWin.setX(sMsg.toInt());
//        fileCFG.readLine(bufStr, nMaxLen); sMsg= QString(bufStr); //Положение окна Y
//        sMsg=sMsg.right(sMsg.length()-sMsg.indexOf(':') - 2);
//        m_PosWin.setY(sMsg.toInt());
     //   qDebug() << pCNLR->m_PosWin[0];

        fileCFG.close();

    }
    return 1;
}
//Сохранение файла конфигурации
int Dlg_Coelostat::Save_Coelostat_CFG(bool bMessageOn)
{
    qDebug() << "SaveCoelostat_CFG 0";
//	_wsetlocale(LC_CTYPE , _T("rus"));//Для записи русских букв в файл
    QString sMsg, sTmp, sFN_CFG = config_coelostat;
    QFile fileCFG(sFN_CFG);

    if (fileCFG.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        sMsg=QString("Конфигурация программы управления целостатом - %1\n\n").arg(sFN_CFG);
        fileCFG.write(sMsg.toUtf8());

        sMsg=QString("Ось альфа:\n");
        fileCFG.write(sMsg.toUtf8());

        sMsg=QString("Часовая скорость ШД (ш/сек): %1\n").arg(m_nSM_Speed_Alpha_Sun_StepPerSec); //Часовая (солнечная) скорость ШД в шагах в секунду
        fileCFG.write(sMsg.toUtf8());

        QDateTime tSys;
        tSys=SM_Alpha_StepPos2SysTime();
        sMsg=tSys.toString("Координата(время) остановки ШД: hh:mm:ss.zzz\n"); //для просмотра в QString
        fileCFG.write(sMsg.toUtf8());

        sMsg=QString("\nОсь дельта:\n");
        fileCFG.write(sMsg.toUtf8());

        sMsg=QString("Кол-во шагов ШД для перемещения на 1 градус (шаг/грд): %1\n").arg(m_fDelta_SM_Steps_1deg); //Кол-во шагов ШД для перемещения на 1 градус (шаг/грд)
        fileCFG.write(sMsg.toUtf8());

        sMsg=QString("Угловых минут от концевика MIN до MAX: %1\n").arg(m_fDelta_AnglMinut_Min2Max); //Для дельты. Сколько угловых минут от концевика MIN до MAX
        fileCFG.write(sMsg.toUtf8());

        sMsg=QString("Координата (угол) остановки ШД: ") + funRAngle2String(SM_Delta_StepPos2Angle()) + "\n";
        fileCFG.write(sMsg.toUtf8());

        sMsg=QString("\nОсь сканирования на целостате:\n");
        fileCFG.write(sMsg.toUtf8());

        sMsg=QString("Кол-во шагов ШД для перемещения на 1 градус (шаг/грд): %1\n").arg(m_fScan_SM_Steps_1deg); //Кол-во шагов ШД для перемещения на 1 градус (шаг/грд)
        fileCFG.write(sMsg.toUtf8());

        sMsg=QString("Координата (угол) остановки ШД: ") + funRAngle2String_Minut(SM_Scan_StepPos2Angle_Minut() * pi_my / (180.0 *60.0)) + "\n";
        fileCFG.write(sMsg.toUtf8());

        sMsg=QString("Угол начала сканирования: ") + funRAngle2String_Minut(m_fScan_Angle_Minut_Pos_Beg * pi_my / (180.0 *60.0)) + "\n";
        fileCFG.write(sMsg.toUtf8());

        sMsg=QString("Угол конца  сканирования: ") + funRAngle2String_Minut(m_fScan_Angle_Minut_Pos_End * pi_my / (180.0 *60.0)) + "\n";
        fileCFG.write(sMsg.toUtf8());

        sMsg=QString("Скорость сканирования ('/минуту): %1\n").arg(m_fScan_Speed_MinutOfMinut);
        fileCFG.write(sMsg.toUtf8());

        if (m_bScan_On_Delta_Correction)
            sTmp = "Да";
        else
            sTmp = "Нет";

        sMsg=QString("Cканирование с коррекцией положения по дельте: %1\n").arg(sTmp);
        fileCFG.write(sMsg.toUtf8());

        fileCFG.write("\nГидирование:\n");
        sMsg=QString("Угол суточной по дельте(грд.): %1\n").arg(m_fAngleDaily_Sun_Delta*180.0/pi_my);
        fileCFG.write(sMsg.toUtf8());

//        fileCFG.write("\nНастройки системы:\n");
//        sMsg=QString("Положение окна X: %1\n").arg(m_PosWin.x());
//        fileCFG.write(sMsg.toUtf8());
//        sMsg=QString("Положение окна Y: %1\n").arg(m_PosWin.y());
//        fileCFG.write(sMsg.toUtf8());

        if(bMessageOn)
            QMessageBox::information(this, "Coelostat", "Файл конфигурации " + sFN_CFG + "  создан!");
        fileCFG.close();
    }
    else
    {
        QMessageBox::information(this, "Ошибка!", "Файл конфигурации создать не удалось, проверьте наличие директорий!!!");
        return 0;
//        exit(0);
    }

    qDebug() << "SaveCoelostat_CFG 1";
    return 1;
}

//Для Aльфа - перевод шагов двигателя от 0 к времени SYSTEMTIME положения Солнца
QDateTime Dlg_Coelostat::SM_Alpha_StepPos2SysTime()
{
    QDateTime tSys=m_Alpha_SysTime_Pos_0;
    double dStep=m_fAlpha_Steps_From_Pos_0;
    //tSys=QTime(23,59,59);   tSys=tSys.addSecs(10);   QString as=tSys.toString("hh:mm:ss.zzz");

    double dSpeedSunStep_ms= (double)  m_pAll_CNLRs_SM->m_anFunMaxSpeed[FUN_ALPHA]*m_fSM_KSpeed_Alpha_Sun / 1000.0;//Солнечная скорость (steps/msec)
    double dmSec= dStep/dSpeedSunStep_ms;
    tSys=tSys.addMSecs(int(dmSec));
    return tSys;
}

//Для Aльфа - перевод времени SYSTEMTIME в положение шагов двигателя от 0
int Dlg_Coelostat::SM_Alpha_SysTime2StepPos(QDateTime tSys)
{
    int nDeltaT = tSys.msecsTo(m_Alpha_SysTime_Pos_0);
    int nDeltaSteps = nDeltaT * int( m_pAll_CNLRs_SM->m_anFunMaxSpeed[FUN_ALPHA]) * m_fSM_KSpeed_Alpha_Sun / 1000;
    return nDeltaSteps;
}

//Для Дельты - перевод шагов двигателя от 0 к углу (радианы) положения Солнца
double Dlg_Coelostat::SM_Delta_StepPos2Angle()
{
    double dSteps = m_fDelta_Steps_From_Pos_0;
    double fSteps_1rad= m_fDelta_SM_Steps_1deg * double(180) / double (pi_my);   //Кол-во шагов ШД для перемещения на 1 радиану (шаг/рд)
    double dAngle = m_fDelta_Angle_Pos_0 + dSteps / fSteps_1rad;
    return dAngle;
}

//Для Дельты - перевод угла(радианы) положения Солнца в положение шагов двигателя от 0
int Dlg_Coelostat::SM_Delta_Angle2StepPos(double dAngle)
{
    double fSteps_1rad= m_fDelta_SM_Steps_1deg * double(180) / double (pi_my);   //Кол-во шагов ШД для перемещения на 1 радиану (шаг/рд)
    int nDeltaSteps = int((dAngle - m_fDelta_Angle_Pos_0) / fSteps_1rad);
    return nDeltaSteps;
}

//Для скана на целостате - перевод шагов двигателя от 0 к углу (радианы) положения
double Dlg_Coelostat::SM_Scan_StepPos2Angle_Minut()
{
    double fSteps_1Minut= m_fScan_SM_Steps_1deg / double(60);   //Кол-во шагов ШД для перемещения на 1 радиану (шаг/рд)
    double dAngle = m_fScan_Angle_Minut_Pos_0 + m_fScan_Steps_From_Pos_0 / fSteps_1Minut;
    return dAngle;
}

//Для скана на целостате - перевод угла(радианы) положения в положение шагов двигателя от 0
int Dlg_Coelostat::SM_Scan_Angle_Minut2StepPos(double dAngle)
{
    double fSteps_1Minut= m_fScan_SM_Steps_1deg / double(60);   //Кол-во шагов ШД для перемещения на 1 радиану (шаг/рд)
    int nDeltaSteps = int((dAngle - m_fScan_Angle_Minut_Pos_0) / fSteps_1Minut);
    return nDeltaSteps;
}


void Dlg_Coelostat::on_Btn_WinSizeMaxMin_clicked()
{

    if (m_bWinMax)
        On_Dlg_WinSizeMin();
    else
        On_Dlg_WinSizeMax();
}

//Диалог с полным размером
void Dlg_Coelostat::On_Dlg_WinSizeMax()
{
   m_bWinMax = true;
   ui->Btn_WinSizeMaxMin->setText(tr("Hide"));

   //Изменяем размер диалога
   QRect rectWin = parentWidget()->geometry();
   rectWin.setWidth(WIN_SIZE_WIDTH_MAX);
   this->setMaximumSize(WIN_SIZE_WIDTH_MAX, WIN_SIZE_HIGHT);
   this->setMinimumSize(WIN_SIZE_WIDTH_MAX, WIN_SIZE_HIGHT);
   parentWidget()->setGeometry(rectWin);
   //Изменяем размер Combo выбора контроллера
   //rectWin=ui->Cmb_CNT_Name->geometry();
   //rectWin.setRight(WIN_SIZE_WIDTH_MAX - SIZE_COMBO2WIN);
   //ui->Cmb_CNT_Name->setGeometry(rectWin);
   //Изменяем положение кнопки
   //rectWin=ui->Btn_WinSizeMaxMin->geometry();
   //rectWin.setX(WIN_SIZE_WIDTH_MAX - SIZE_COMBO2WIN + 5);
   //ui->Btn_WinSizeMaxMin->setGeometry(rectWin);

}

//Сокращенный диалог
void Dlg_Coelostat::On_Dlg_WinSizeMin()
{
   m_bWinMax = false;
   ui->Btn_WinSizeMaxMin->setText(tr("Info"));
   //Изменяем размер диалога
   QRect rectWin = parentWidget()->geometry();
//   qDebug() << rectWin;
   rectWin.setWidth(WIN_SIZE_WIDTH_MIN);
   this->setMaximumSize(WIN_SIZE_WIDTH_MIN, WIN_SIZE_HIGHT);
   this->setMinimumSize(WIN_SIZE_WIDTH_MIN, WIN_SIZE_HIGHT);
   parentWidget()->setGeometry(rectWin);
   //Изменяем размер Combo выбора контроллера
   //rectWin=ui->Cmb_CNT_Name->geometry();
   //rectWin.setRight(WIN_SIZE_WIDTH_MIN - SIZE_COMBO2WIN);
   //ui->Cmb_CNT_Name->setGeometry(rectWin);
   //Изменяем положение кнопки
   //rectWin=ui->Btn_WinSizeMaxMin->geometry();
   //rectWin.setX(WIN_SIZE_WIDTH_MIN - SIZE_COMBO2WIN + 5);
   //ui->Btn_WinSizeMaxMin->setGeometry(rectWin);
}

//для Функции определения суточной на изображении гида по альфе(нажатие и запуск поиска)
//void Dlg_Coelostat::on_Btn_GetDailyLineSun_Alpha_pressed()
//{
////    int nPlusAlpha_Sec = ALPHA_TIME_SECONDS_FOR_DAYLY_SUN;
////    m_afXC_YC_R_Alpha_pre[0] = 0;  m_afXC_YC_R_Alpha_pre[1] = 0;
//    m_afXC_YC_R_Alpha_pre[0]=((Guide *) m_pGuide)->GetMeanOffsetX();//Получаем данные с камеры гида
//    m_afXC_YC_R_Alpha_pre[1]=((Guide *) m_pGuide)->GetMeanOffsetY();//Получаем данные с камеры гида
//    on_Btn_Alpha_Set_Time_clicked(); //Считаем что идем по текущему времени. Сбрасываем шаги берем текущее время
//    on_Btn_Alpha_STOP_pressed(); //Останавлваем по альфе
//    m_bool_Get_Daily_Sun_Alpha_On = true; //Режим определения суточной по изображению на гиде
//}

//Функция определения суточной по изображению Солнца на гиде по альфе
double Dlg_Coelostat::Get_Daily_Sun_Alpha()
{
    //Режим определения суточной по изображению на гиде
    m_bool_Get_Daily_Sun_Alpha_On = false;
    double afXC_YC_R[3] , fRetAngleDaily_Sun;
//    afXC_YC_R[0] = -10;  afXC_YC_R[1] = 10;

    afXC_YC_R[0]= m_pGuide->GetOffsetX();//Получаем данные с камеры гида
    afXC_YC_R[1]= m_pGuide->GetOffsetY();//Получаем данные с камеры гида
//    qDebug() << afXC_YC_R[0] << afXC_YC_R[1] << m_afXC_YC_R_pre[0] << m_afXC_YC_R_pre[1];

    if (afXC_YC_R[1] != m_afXC_YC_R_Delta_pre[1])
        fRetAngleDaily_Sun = -qAtan((afXC_YC_R[0] - m_afXC_YC_R_Delta_pre[0])/(afXC_YC_R[1] - m_afXC_YC_R_Delta_pre[1]));

    else if (afXC_YC_R[0] > m_afXC_YC_R_Delta_pre[1])
        fRetAngleDaily_Sun = 0;

    else
        fRetAngleDaily_Sun = pi_my; //M_PI

    if (afXC_YC_R[1] < m_afXC_YC_R_Delta_pre[1])
        fRetAngleDaily_Sun = pi_my + fRetAngleDaily_Sun;

    fRetAngleDaily_Sun = fRetAngleDaily_Sun - pi_my / 2.0;

    on_Btn_Alpha_FORV_FAST_pressed();//Возвращаемся по дельте
    m_bool_Alpha_On_Go = true;
    m_bool_Alpha_On_Go_Cur_Time = true;
    m_bool_Get_Daily_Sun_Alpha_On = false; //Режим суточной на по альфе
    ui->Btn_GetDailyLineSun_Alpha->setText(QString("Sa= %1").arg(fRetAngleDaily_Sun*180.0/pi_my));

    return fRetAngleDaily_Sun;
}


// для Функции определения суточной на изображении гида по дельте (нажатие и запуск поиска)
void Dlg_Coelostat::on_Btn_GetDailyLineSun_Delta_pressed()
{
    double dAnglPlusDelta_minut = DELTA_ANGLE_MINUT_FOR_DAYLY_SUN;

    m_afXC_YC_R_Delta_pre[0]=m_pGuide->GetMeanOffsetX();    //Получаем данные с камеры гида
    m_afXC_YC_R_Delta_pre[1]=m_pGuide->GetMeanOffsetY();    //Получаем данные с камеры гида
    m_fDelta_Angle_Pos_Pre  = SM_Delta_StepPos2Angle();                 //Угол - положение дельты куда вернуться в радианах. Для определения наклона суточной

    double dAnglDelt_rad = (dAnglPlusDelta_minut / 60.0) * pi_my / 180.0;

    m_fAngle_Delta_Go = m_fDelta_Angle_Pos_Pre + dAnglDelt_rad;
    //qDebug() << "m_fAngle_Delta_Go=" << m_fAngle_Delta_Go * 180.0 / pi_my <<  " m_fDelta_Angle_Pos_Pre=" << m_fDelta_Angle_Pos_Pre* 180.0 / pi_my;
    //on_Btn_Delta_FORV_SLOW_pressed();//Отступаем по дельте


    on_Btn_Delta_FORV_FAST_pressed();                                   //Отступаем по дельте
    m_bool_Delta_On_Go = true;
    m_bool_Get_Daily_Sun_Delta_On = true;                               //Режим определения суточной по изображению на гиде
}

//Функция определения суточной по изображению Солнца на гиде по дельте
double Dlg_Coelostat::Get_Daily_Sun_Delta()
{
    m_bool_Get_Daily_Sun_Delta_On = false; //Режим определения суточной по изображению на гиде    
    double afXC_YC_R[3], fRetAngleDaily_Sun;

    afXC_YC_R[0]= m_pGuide->GetOffsetX();//Получаем данные с камеры гида
    afXC_YC_R[1]= m_pGuide->GetOffsetY();//Получаем данные с камеры гида
    qDebug() << "Get_Daily_Sun_Delta: afXC_YC_R[0], afXC_YC_R[1], afXC_YC_R_pre[0], afXC_YC_R_pre[1] = "
             << afXC_YC_R[0] << afXC_YC_R[1] << m_afXC_YC_R_Delta_pre[0] << m_afXC_YC_R_Delta_pre[1];

    if (afXC_YC_R[1] != m_afXC_YC_R_Delta_pre[1])
        fRetAngleDaily_Sun = -qAtan((afXC_YC_R[0] - m_afXC_YC_R_Delta_pre[0])/(afXC_YC_R[1] - m_afXC_YC_R_Delta_pre[1]));

    else if (afXC_YC_R[0] > m_afXC_YC_R_Delta_pre[1])
        fRetAngleDaily_Sun = 0;

    else
        fRetAngleDaily_Sun = pi_my; //M_PI

    if (afXC_YC_R[1] < m_afXC_YC_R_Delta_pre[1])
        fRetAngleDaily_Sun = pi_my + fRetAngleDaily_Sun;

    qDebug() << "Get_Daily_Sun_Delta: DailyAngle = " << fRetAngleDaily_Sun;

    //Угол - положение дельты - куда вернуться в радианах. Для определения наклона суточной
    m_fAngle_Delta_Go = m_fDelta_Angle_Pos_Pre;

    //Возвращаемся по дельте
    on_Btn_Delta_REV_SLOW_pressed();

    m_bool_Delta_On_Go = true;
    ui->Btn_GetDailyLineSun_Delta->setText(QString("Sd= %1").arg(fRetAngleDaily_Sun*180.0/pi_my));

    return fRetAngleDaily_Sun;
}



//Процедура коррекции положения по гиду
void Dlg_Coelostat::On_Correction_AlphaDelta(double fArcsecCorrAlpha, double fArcsecCorrDelta)
{
    if (!m_pAll_CNLRs_SM->isInitSuccess())
        return;

//        QMutexLocker locker(&guideMutex);

        //Работаем в режиме коррекции положения по гиду Alpha (проверяется в on_Timer_Alpha())
        m_bool_On_Correction_Alpha = true;

        //За одну секунду времени Солнце проходит 15 arcsec
        m_dCorrection_Alpha_Seconds = fArcsecCorrAlpha / 15.0;

        // Устанавливаем будущую координату "альфа"
        m_tDataTime_Alpha_Go = SM_Alpha_StepPos2SysTime().addMSecs(m_dCorrection_Alpha_Seconds * 1000);

//        qDebug() << SM_Alpha_StepPos2SysTime().time() << m_tDataTime_Alpha_Go.time();

        if (fArcsecCorrAlpha > 0)
        {
            on_Btn_Alpha_FORV_SLOW_pressed(); //Вперед
            //Для Aльфы - инициализировать часовую скорость (++) и ее отображение в диалоге
            On_Dlg_Set_Speed_Alpha_Hours(++m_nSM_Speed_Alpha_Sun_StepPerSec);
        }
        else
        {
//            qDebug() << "On_Correction_AlphaDelta: Стоим ждем!";
            on_Btn_Alpha_STOP_pressed(); //Стоим ждем

            //Для Aльфы - инициализировать часовую скорость (--) и ее отображение в диалоге
            On_Dlg_Set_Speed_Alpha_Hours(--m_nSM_Speed_Alpha_Sun_StepPerSec);
        }
        m_bool_Alpha_On_Go = true;
//    }

    // По Дельта
//    if (abs(fArcsecCorrDelta) > arcsecMaxErr)
//    {
        //Работаем в режиме коррекции положения по гиду Delta (проверяется в on_Timer_Delta())
        m_bool_On_Correction_Delta = true;

        double dAnglDelta_rad = (fArcsecCorrDelta / (60.0 * 60.0)) * pi_my / 180.0;

        // Устанавливаем будущую координату "дельта"
        m_fAngle_Delta_Go = SM_Delta_StepPos2Angle() + dAnglDelta_rad;

        if (fArcsecCorrDelta > 0)
            on_Btn_Delta_FORV_SLOW_pressed(); //Вперед
        else
            on_Btn_Delta_REV_SLOW_pressed(); //Назад

//        qDebug() << "On_Corr_AlphaDelta: delta " << dAnglDelta_rad * 180/pi_my << m_fAngle_Delta_Go * 180/pi_my  << SM_Delta_StepPos2Angle()* 180/pi_my;
        m_bool_Delta_On_Go = true;
        //    }
}

void Dlg_Coelostat::StopTimers()
{
    if(m_pTimer_View)
        m_pTimer_View->stop();
    qDebug() << "Dlg_Coelostat: StopTimers()";
}
