#include "observation_log.h"
#include "ui_observation_log.h"
#include <QDebug>
#include <QMessageBox>
#include <QTextCodec>
#include <QDate>
#include <QFile>
#include <QByteArray>
#include <iostream>
#include <fstream>

ObservationLog::ObservationLog(QString confDir, QString logPath, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ObservationLog)
{
//    qDebug() << "observation log created!";
    ui->setupUi(this);
    configDirectory = confDir;
    log_addres = logPath.toStdString();
    observersList = new QStringList;
    finishTime = "no observations";

    out = new ofstream(log_addres, std::ios_base::app);
    if (!out->is_open())
    {
        qDebug() << "Log wasn't' open!";
        QMessageBox::warning(this, "Observation Log",
                             "Не удалось открыть журнал!");
        this->setDisabled(true);
    }
    else
    {
        if (!LoadConfig())
        {
            this->setDisabled(true);
            QMessageBox::warning(this, "Observation Log",
                                 "Конфигурационный файл журнала не найден!");
        }
        else
        {
            // Add observers list to combo box
            ui->observer_comboBox->addItems(*observersList);
            // Открыть/скрыть меню настроек
            connect(ui->btn_settings, &QPushButton::clicked,
                    this, &ObservationLog::ShowSettings);
            connect(ui->btn_save, &QDialogButtonBox::accepted,
                    this, &ObservationLog::AddLog);
            connect(ui->btn_save, &QDialogButtonBox::rejected,
                    this, &ObservationLog::close);
        }
    }
}

ObservationLog::~ObservationLog()
{
    qDebug() << "~ObservationLog";
    if (finishTime == "no observations")
    {
        qDebug() << finishTime;
        *out << finishTime.toStdString() << endl;
    }
    out->close();
    delete out;
    delete observersList;
    delete ui;
}

//-----------------------------------------------------------------------------
// Метод для отображения/сокрытия меню настроек
//
void ObservationLog::ShowSettings(bool b)
{
//    if ( b ){
//        qDebug() << width() << ui->groupBox_settings->width();
//        ui->groupBox_settings->show();
//        setGeometry( x(), y(), width() + ui->groupBox_settings->width() + 200, height());
//        updateGeometry();
//        qDebug() << width() << ui->groupBox_settings->width();
//    }
//    else
//    {
//        qDebug() << width() << ui->groupBox_settings->width();
//        ui->groupBox_settings->hide();
//        setGeometry( x(), y(), width() - ui->groupBox_settings->width() + 50, height());
//        updateGeometry();
//        qDebug() << width() << ui->groupBox_settings->width();
    //    }
}

void ObservationLog::AddLog()
{
    AddDate();
    AddObserver();
}

void ObservationLog::AddDate()
{
    *out << endl << QDate::currentDate().toString("dd.MM.yyyy").toStdString() << endl;
}

void ObservationLog::AddObserver()
{
    QString observer = ui->observer_comboBox->currentText();
    QTextCodec *codec = QTextCodec::codecForLocale();
    QByteArray byte_name = codec->fromUnicode(observer);
    char *name = byte_name.data();
//    std::cout << name;
    qDebug() << observer.toUtf8().data();
    *out << "Observer:  " << name << endl;
    QMessageBox::information(this, "Patrol Telescope Log", "Запись добавлена в журнал!");
}


void ObservationLog::AddStartTime(QString time)
{
    if (!out->is_open())
        out->open(log_addres, std::ios_base::app);

    *out << "observation time: " << time.toStdString() << " - ";
    out->close();
}

void ObservationLog::GetFinishTime(QString time)
{
    finishTime = time;
}

void ObservationLog::AddFinishTime()
{
    out->open(log_addres, std::ios_base::app);
    if (!out->is_open())
        qDebug() << "Can't open log!";

    *out << finishTime.toStdString() << endl;
    out->close();
    finishTime = "no observations";
}

bool ObservationLog::LoadConfig()
{
    QString observers = configDirectory + QString("observers.ini");
    QFile config(observers);

    if (!config.exists())
        return false;
    else
    {
        // Загрузка параметров
        if (config.open(QIODevice::ReadOnly))
        {
            while (!config.atEnd())
            {
                char buf[50];
                memset(buf, 0, sizeof(buf));
                short nBytes = config.readLine(buf, sizeof(buf));
                if (nBytes == -1)
                {
                    qDebug() << "Load observers.ini: can't read line!";
                    continue;
                }
                short len = strlen(buf);
                while ((buf[len-1] == '\r') || (buf[len-1] == '\n'))
                        --len;
                QString name = QString::fromLocal8Bit(buf, len);
                observersList->append(name);
            }
            config.close();
        }
        return true;
    }
}

