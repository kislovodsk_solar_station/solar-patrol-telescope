#ifndef CONTROLLERS_MACROS_H
#define CONTROLLERS_MACROS_H

//Максимальное число осей
#define N_MAX_AXIS 4

//Длина для буферов
#define N_BYTE_BUFFER  128

//Время в миллисекундах для sleep в потоках
#define SLEEP_THREAD 10

// Порт, на котором будем работать
#define MY_ETHERNET_PORT 5000

//Время в миллисекундах, через которое отправляется пакет для поддержания работы контроллера (отправлять нужно не менее 1 раз в 5 сек)
#define CNTL_ON_MSEC 1200

//Число портов выхода контроллера
#define N_MAX_PORTS 3

//Длина статусного пакета, приходящего от контроллера имеет теперь длину 78(90 байт = 88 байт данных + 2 контрольной суммы) - 12 байт не.
#define N_STATUS_PACKET_LEN_PLC230  0x004E
#define N_STATUS_PACKET_LEN_PLCM_E3 0x005A

#endif // CONTROLLERS_MACROS_H

