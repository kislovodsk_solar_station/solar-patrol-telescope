//Работа с Ethernet портом в QT C++
//Обычно порт открывается как обычный файл. Как только порт открыт, то в него можно записывать и считывать как из обычного файла. Конечно же придётся добавить различные проверки байтов при чтении и записи.

#include "QSM_InitControllers.h"
#include "QSM_CNLR_Ethernet.h"

#include <QFile>
#include <QMessageBox>
#include <QDebug>


// Взять имена всех доступных контрллеров
CInitControllers::CInitControllers(QString configPath) : initSuccess(false)
{
    config_init_controllers = configPath + QString("SM_Init_Controllers.cfg");
    int i;
    terminated = false;
    m_asAll_CNLR_IP_Name_cur [0] = "";
    for (i = 1; i <  N_MAX_CNLR; i++)
        m_apCNLRs[i] = NULL;
    for (i = 0; i < FUN_NUMBER_ALL; i++)
    {
        m_anFunCNLR[i] = NO_CNLR;
        if (i < N_MAX_AXIS)
            m_anFunAxis[i] = i;
        else
            m_anFunAxis[i] = i - N_MAX_AXIS;

        m_anFunMaxSpeed [i] = DEFAULT_FUN_MAX_SPEED;
        m_anFunAcceler  [i] = DEFAULT_FUN_ACCELER;
        m_anFunMicrostep[i] = DEFAULT_FUN_MICROSTEP;
        m_anFunNumInPorts_MIN[i] = NO_PORT;
        m_anFunNumInPorts_MAX[i] = NO_PORT;
    }
    m_anFunCNLR[FUN_ALPHA] = 0;             // Для альфы первый контроллер
    m_anFunMaxSpeed [FUN_ALPHA] = 60000;    // Для альфы максимальная скорость небольшая
    m_anFunAcceler  [FUN_ALPHA] = 30000;    // Для альфы ускорение
    m_anFunMicrostep[FUN_ALPHA] = 64;       // Для альфы микрошаг
    m_anFunCNLR[FUN_DELTA] = 1;             // Для дельты второй контроллер (пока)
    m_anFunMaxSpeed [FUN_DELTA] = 30000;    // Для дельты максимальная скорость небольшая
    m_anFunAcceler  [FUN_DELTA] = 3000000;  // Для дельты ускорение
    m_anFunMicrostep[FUN_DELTA] = 64;       // Для дельты микрошаг
    m_anFunNumInPorts_MIN[FUN_DELTA] = 11;  // Для дельты номер порта для концевиков - минимального положения (11 и 15 для PLCM-E3)
    m_anFunNumInPorts_MAX[FUN_DELTA] = 15;  // Для дельты номер порта для концевиков - максимального положения
    m_anFunCNLR [FUN_SCAN_SPECTR] = 0;      // Для сканирования внутри спектрографа первый контроллер
    if(GetAll_CNLR_Names() == true)         // Имена всех доступных контрллеров
    {
        if(Load_Controllers_CFG(TRUE) == 1) // Создание или чтение файла конфигурации
        {
            //                emit sigSetCNLRs(this);
            if (!Init_All_CNLRs())           // Инициализация всех задействованых контроллеров
                qDebug() << "CInitControllers: Ошибка! Не удалось инициализировать все контроллеры!";
            else
                initSuccess = true;
        }
        else
            qDebug() << "No config file! ";
        //                emit sigSetCNLRs(0);
    }
    else
        qDebug() << "GetAll_CNLR_Names() = false";
    //            emit sigSetCNLRs(0);
}

CInitControllers::~CInitControllers()
{
    if(terminated)
        return;
    qDebug() << "~CInitControllers()";
     for (int i = 0; i < N_MAX_CNLR; i++)
        if (m_apCNLRs[i]) {
            if (initSuccess)
            {
                m_apCNLRs[i]->Terminate_Port();
                delete m_apCNLRs[i];
            }
            terminated = true;
            qDebug() << "terminate" << i;
        }
}

// Была ли инициализация проведена успешно
bool CInitControllers::isInitSuccess() { return initSuccess; }

bool CInitControllers::GetAll_CNLR_Names()
{
    WSADATA wsaData;
    SOCKET ConnectSocket;
    sockaddr_in RecvAddr;
    int iResult;
    QString sLog;
    WSAStartup(MAKEWORD(2,2), &wsaData);   // Initialize Winsock
    ConnectSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP); // Create a socket for sending data
    RecvAddr.sin_family = AF_INET;
    RecvAddr.sin_port = htons(5001);
    RecvAddr.sin_addr.s_addr = inet_addr("192.168.10.255");
    // Send a datagram to the receiver
    int nLen=4;
    char c_data[]={0x35, 0x74, 0x57, 0x35};
    sendto(ConnectSocket, c_data, nLen, 0, (SOCKADDR *) &RecvAddr, sizeof(RecvAddr));

    iResult = shutdown(ConnectSocket, SD_SEND);
    if (iResult == SOCKET_ERROR) {
        sLog.sprintf("shutdown failed: %d\n", WSAGetLastError());
        qDebug() << "GetAll_CNLR_Names: iResult == SOCKET_ERROR";
        closesocket(ConnectSocket);
        WSACleanup();
        return FALSE;
    }
    //msleep(SLEEP_THREAD*2);
    QString sTmp, sAll_CNLR_IP, sAll_CNLR_Name;
    ULONG j, n=0;
    int nByteEho_min = 42; //Длина пакета отзыва названия контроллера
    int nByteEho_max = 48; //Длина пакета отзыва названия контроллера
    int nDeltaByteName = 16; //Отступ для названия контроллера
    CHAR pcTmp[100];
    CHAR sBuffer[50];//Получаем входящие данные
    BOOL bTimedOut;
    for (j=0; j < N_MAX_CNLR*2 ; j++) {
        nLen=0;
        //nLen=m_CNLRs.RecvWithTimeout(ConnectSocket, sBuffer, sizeof(sBuffer), 50, &bTimedOut);
        nLen=m_apCNLRs[0]->RecvWithTimeout(ConnectSocket, sBuffer, sizeof(sBuffer), 50, &bTimedOut);

        if (!bTimedOut && nLen >= nByteEho_min && nLen <= nByteEho_max) {
            //sTmp.sprintf("%hhu", (BYTE)sBuffer[0]);
            sTmp.sprintf("%hhu.%hhu.%hhu.%hhu", (BYTE)sBuffer[0], (BYTE)sBuffer[1], (BYTE)sBuffer[2], (BYTE)sBuffer[3]);
            sAll_CNLR_IP = sTmp;

            memcpy(pcTmp, sBuffer + nDeltaByteName, nLen - nDeltaByteName);
            pcTmp[nLen - nDeltaByteName]='\0';
            sAll_CNLR_Name.sprintf("%s", pcTmp);
            m_asAll_CNLR_IP_Name_cur[n] = sAll_CNLR_IP + '(' + sAll_CNLR_Name +')';
            n++;
            QThread::msleep(SLEEP_THREAD);
        }
    }
    //qDebug() << m_asAll_CNLR_IP_Name_cur[1];
    m_asAll_CNLR_IP_Name_cur[n]="";
    closesocket(ConnectSocket);
    WSACleanup();

    //Нет ни одного контроллера
    if (n == 0)
    {
        QMessageBox msgBox;
        msgBox.setText("Error..");
        msgBox.setInformativeText("Not initialized controllers. Quit!");
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.exec();
//    exit(0);
        return FALSE;
    }
    return TRUE;
}

// Инициализация всех задействованых контроллеров
bool CInitControllers::Init_All_CNLRs()
{
    int i, n;
    //qDebug() << m_asAll_CNLR_IP_Name_cur[1];
    //for (i = 0; i < FUN_NUMBER_ALL; i++) qDebug() << m_anFunCNLR[i];
    for (n=0; m_asAll_CNLR_IP_Name_cur[n].length() > 7 && n < 3; n++)
      for (i = 0; i < FUN_NUMBER_ALL; i++)
          if ( m_anFunCNLR[i] == n)
          {
              CControllerEthernet* result = Init_One_CNLR(m_asAll_CNLR_IP_Name_cur[n]);  // Инициализация одного контроллера
              if (result == nullptr)
              {
                  qDebug() << "InitControllers: не удалось подключиться к контроллеру" << m_asAll_CNLR_IP_Name_cur[n]
                              << "i = " << i << "ось = " << m_anFunCNLR[i];
                  return false;
              }

              m_apCNLRs[n] = result;
              break;
          }
    return true;
}

// Инициализация одного конкретного контроллера
CControllerEthernet *CInitControllers::Init_One_CNLR(QString sIP_Name_CNLR)
{
    int i;
    QString strLog;
    QMessageBox msgBox;
    CControllerEthernet *pNew_CNLR = new CControllerEthernet();
    pNew_CNLR->Initialize_StepMotor();//Инициализация контроллера ШД
    for (i = 0; i < FUN_NUMBER_ALL; i++)
    {   if (m_anFunCNLR[i] !=  NO_CNLR)
            if( m_asAll_CNLR_IP_Name_cur[m_anFunCNLR[i]] == sIP_Name_CNLR)
            {
                pNew_CNLR->m_anSM_Speed_max  [m_anFunAxis[i] - 1] = m_anFunMaxSpeed [i];//Макимальная скорость ШД  (steps/sec)
                pNew_CNLR->m_anSM_Acceler    [m_anFunAxis[i] - 1] = m_anFunAcceler  [i];//Ускорение ШД  (steps/sec^2)
                pNew_CNLR->m_anSM_MicroStep  [m_anFunAxis[i] - 1] = m_anFunMicrostep[i];//Установка делителя (микрошага)
                //qDebug() << m_anFunMaxSpeed [i];
            }
    }
    //qDebug() << pNew_CNLR->m_anSM_Speed_max[0] << pNew_CNLR->m_anSM_Speed_max[1];
    pNew_CNLR->m_sCNLR_IP  =funGet_IP_CNRL  (sIP_Name_CNLR); //[ui->Cmb_CNT_Name->currentIndex()];
    pNew_CNLR->m_sCNLR_Name=funGet_Name_CNRL(sIP_Name_CNLR);
    pNew_CNLR->GetLenStatusPacket();//Процедура определения длины статусного пакета (зависит от контроллера)

    //qDebug() << pNew_CNLR->m_sCNLR_IP << pNew_CNLR->m_sCNLR_Name;

    if (!pNew_CNLR->Initialize_Ethernet()) // инициализация Ethernet порта
    {   msgBox.setText("Error..");
        msgBox.setInformativeText("Not initialized controllers. Quit!");
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.exec();
        return 0;
        //exit(0);
    }
    LONG allCurPos[]={0,0,0,0};
    pNew_CNLR->Send_Command_Ethernet_SetPos(allCurPos);// Отправить команду на Ethernet-порт для установки текущих координат

    return pNew_CNLR;
}


//Создание или чтение файла конфигурации
int CInitControllers::Load_Controllers_CFG(bool bMessageOn)
{
    int i, n, n_cur;
    QMessageBox msgBox;
    QString sMsg, sFN_CFG = config_init_controllers;
    QFile fileCFG(sFN_CFG);
    if (!fileCFG.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        int ret_msgBox = TRUE;
        if (bMessageOn)
        {
            sMsg = QString("No CFG file %1. \nCreate new?").arg(sFN_CFG);
            msgBox.setText("Error..");
            msgBox.setInformativeText(sMsg);
            msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel);
            msgBox.setDefaultButton(QMessageBox::Save);
            ret_msgBox = msgBox.exec();
       }
       if (ret_msgBox == QMessageBox::Save)
           Save_Controllers_CFG(TRUE); //Сохранение файла конфигурации c сообщениями
       else
           return 0;
//           //Без файла конфигурации выход из программы
//           exit(0);
    }
    else
    {
        const int nMaxLen=300; char bufStr[nMaxLen];
        fileCFG.readLine(bufStr, nMaxLen); //пропускаем заголовок
        fileCFG.readLine(bufStr, nMaxLen); //пропускаем заголовок
        n_cur = 0;
        for (i = 0; i < FUN_NUMBER_ALL; i++)
        {
            fileCFG.readLine(bufStr, nMaxLen); //пропускаем заголовок
            fileCFG.readLine(bufStr, nMaxLen); sMsg= QString(bufStr);//Имя контроллера
            sMsg=sMsg.right(sMsg.length()-sMsg.indexOf(":") - 2);
            //sMsg.remove(' ');
            sMsg.remove('\n');
            //qDebug() << m_asAll_CNLR_IP_Name_cur[0];
            m_anFunCNLR[i] = NO_CNLR;
            if (sMsg.length() > 8 ) //Если определен контроллер
            {
              //qDebug() << sMsg;
              for (n = 0; m_asAll_CNLR_IP_Name_cur[n].length() > 7 && n < N_MAX_CNLR; n++)
                if (m_asAll_CNLR_IP_Name_cur[n] == sMsg) //Если есть сейчас контроллер

              m_anFunCNLR[i] = n;
            }

            fileCFG.readLine(bufStr, nMaxLen); sMsg= QString(bufStr); //Номер оси
            sMsg=sMsg.right(sMsg.length() - sMsg.indexOf(':') - 2);
            m_anFunAxis[i] =  BYTE(sMsg.toInt()) - 0;


            fileCFG.readLine(bufStr, nMaxLen); sMsg= QString(bufStr); //Максимальная скорость
            sMsg=sMsg.right(sMsg.length() - sMsg.indexOf(':') - 2);
            m_anFunMaxSpeed[i] =  long(sMsg.toLong());

            fileCFG.readLine(bufStr, nMaxLen); sMsg= QString(bufStr); //Ускорение
            sMsg=sMsg.right(sMsg.length() - sMsg.indexOf(':') - 2);
            m_anFunAcceler[i] =  long(sMsg.toLong());

            fileCFG.readLine(bufStr, nMaxLen); sMsg= QString(bufStr); //Микрошаг
            sMsg=sMsg.right(sMsg.length() - sMsg.indexOf(':') - 2);
            m_anFunMicrostep[i] =  BYTE(sMsg.toInt());

            fileCFG.readLine(bufStr, nMaxLen); sMsg= QString(bufStr); // Номер порта для концевиков - минимального положения
            sMsg=sMsg.right(sMsg.length() - sMsg.indexOf(':') - 2);
            if (sMsg.indexOf(QString(S_NO_PORT)) == -1)
                  m_anFunNumInPorts_MIN[i] = sMsg.toInt();
             else m_anFunNumInPorts_MIN[i] = NO_PORT;

            fileCFG.readLine(bufStr, nMaxLen); sMsg= QString(bufStr); // Номер порта для концевиков - максимального положения
            sMsg=sMsg.right(sMsg.length() - sMsg.indexOf(':') - 2);
            if (sMsg.indexOf(QString(S_NO_PORT)) == -1)
                  m_anFunNumInPorts_MAX[i] = sMsg.toInt();
             else m_anFunNumInPorts_MAX[i] = NO_PORT;

//            qDebug() << i << m_anFunCNLR[i] << m_anFunAxis[i] << m_anFunMaxSpeed[i] << m_anFunAcceler[i] << m_anFunMicrostep[i] << m_anFunNumInPorts_MIN[i] << m_anFunNumInPorts_MAX[i];
            fileCFG.readLine(bufStr, nMaxLen); //пропускаем разделитель

        }
        fileCFG.close();
    }
    return 1;
}
//Сохранение файла конфигурации
int CInitControllers::Save_Controllers_CFG(bool bMessageOn)
{
    int i=0;
    QMessageBox msgBox;
    QString sMsg, sFN_CFG = config_init_controllers;
    QFile fileCFG(sFN_CFG);

    if (fileCFG.open(QIODevice::WriteOnly | QIODevice::Text)){

        sMsg=QString("Конфигурация контроллеров ШД - %1\n\n").arg(sFN_CFG);
        fileCFG.write(sMsg.toUtf8());
//        QTextStream out(&fileCFG);
//        out.setCodec("UTF-8");
//        out << sMsg;

        for (i = 0; i < FUN_NUMBER_ALL; i++)
        {
            sMsg = g_asFunControllers[i] + ":\n";
            fileCFG.write(sMsg.toUtf8());

            //Имя контроллера. Если = S_NO_CNLR функция отключена
            sMsg = QString("  " + g_asFun_Field_CNLR[0]) + ": ";

            if (m_anFunCNLR[i] != NO_CNLR)
                  sMsg = sMsg + m_asAll_CNLR_IP_Name_cur[m_anFunCNLR[i]] + "\n";
            else
                sMsg = sMsg + QString(S_NO_CNLR) + "\n";
            //qDebug() << sMsg;
            fileCFG.write(sMsg.toUtf8());

            sMsg = QString("  " + g_asFun_Field_CNLR[1] + ": %1\n" ).arg(UINT(m_anFunAxis[i]) + 1); //В файле конфигурации ось
            fileCFG.write(sMsg.toUtf8());

            sMsg = QString("  " + g_asFun_Field_CNLR[2] + ": %1\n" ).arg(m_anFunMaxSpeed[i]); // Максимальная скорость
            fileCFG.write(sMsg.toUtf8());

            sMsg = QString("  " + g_asFun_Field_CNLR[3] + ": %1\n" ).arg(m_anFunAcceler[i]);  // Ускорение
            fileCFG.write(sMsg.toUtf8());

            sMsg = QString("  " + g_asFun_Field_CNLR[4] + ": %1\n" ).arg(m_anFunMicrostep[i]);// Микрошаг
            fileCFG.write(sMsg.toUtf8());

            sMsg = QString("  " + g_asFun_Field_CNLR[5]) + ": "; // Номер порта для концевиков - минимального положения
            if (m_anFunNumInPorts_MIN[i] != NO_PORT)
                  sMsg = sMsg + QString::number(m_anFunNumInPorts_MIN[i]) + "\n";
             else sMsg = sMsg + QString(S_NO_PORT) + "\n";
            fileCFG.write(sMsg.toUtf8());

            sMsg = QString("  " + g_asFun_Field_CNLR[6]) + ": "; // Номер порта для концевиков - максиинимального положения
            if (m_anFunNumInPorts_MAX[i] != NO_PORT)
                sMsg = sMsg + QString::number(m_anFunNumInPorts_MAX[i]) + "\n";
            else
                sMsg = sMsg + QString(S_NO_PORT) + "\n";

            fileCFG.write(sMsg.toUtf8());

            fileCFG.write("\n");
        }
        if (bMessageOn)
        {
            msgBox.setText("Файл конфигурации создан!");
            msgBox.exec();
        }
    }
    else
    {
        msgBox.setText("Файл конфигурации создать не удалось, проверьте наличие директорий!!!");
        msgBox.exec();
        exit(0);
    }

    fileCFG.close();
    return 1;
}

//символное IP  CNRL из IP+имя контроллера (IP + '(' + Name +')')
QString CInitControllers::funGet_IP_CNRL(QString sIP_Name_CNRL)
{
    QString sIP_CNRL;
    sIP_CNRL  = sIP_Name_CNRL.left(sIP_Name_CNRL.indexOf('('));
    return sIP_CNRL;
}

//символное имя CNRL из IP+имя контроллера
QString CInitControllers::funGet_Name_CNRL (QString sIP_Name_CNRL)
{
    QString s_Name_CNRL;
    s_Name_CNRL = sIP_Name_CNRL.right(sIP_Name_CNRL.length() - sIP_Name_CNRL.indexOf('(')-1);
    s_Name_CNRL = s_Name_CNRL.left(s_Name_CNRL.length() - 1);
    return s_Name_CNRL;
}

// Определить номер контроллера в списке m_asAll_CNLR_IP_Name_cur
int CInitControllers::GetN_CNLR(CControllerEthernet * pCNLR)
{
    int  n = 0;
    while (m_asAll_CNLR_IP_Name_cur[n].length() > 4) {
        if (funGet_IP_CNRL(m_asAll_CNLR_IP_Name_cur[n]) == pCNLR->m_sCNLR_IP )
          return n;
        n++;
    }
    return -1;
}

//для концевиков определение максимального или минимального положения
bool CInitControllers::Get_On_Port_MAX_MIN(int nFun, int nMAX_MIN)
{
    CControllerEthernet * pCNLR = m_apCNLRs[m_anFunCNLR[nFun]];
    bool bRet;
    if (nMAX_MIN == PORT_ON_MIN)
        bRet = (!(pCNLR->GetInPorts() & (1 << (m_anFunNumInPorts_MIN[nFun] - 1))));           // Определение состояния входных портов
    if (nMAX_MIN == PORT_ON_MAX)
        bRet = (!(pCNLR->GetInPorts() & (1 << (m_anFunNumInPorts_MAX[nFun] - 1))));           // Определение состояния входных портов
    //for (int i=10; i<16; i++) qDebug() << i << " -> " << (pCNLR->GetInPorts() & (1 << (i-1)));//Вывод состояний портов
    if (pCNLR->m_nLenStatusPacket == N_STATUS_PACKET_LEN_PLCM_E3)  bRet= !bRet;
    return bRet;
}
