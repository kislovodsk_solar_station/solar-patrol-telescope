#ifndef OBSERVATION_LOG_H
#define OBSERVATION_LOG_H

#include <QDialog>

using std::ofstream;
using std::endl;

namespace Ui {
class ObservationLog;
}

class ObservationLog : public QDialog
{
    Q_OBJECT

public:
    explicit ObservationLog(QString confDir, QString logPath, QWidget *parent = 0);
    ~ObservationLog();

public slots:
    void AddStartTime(QString time);
    void GetFinishTime(QString time);
    void AddFinishTime();

private slots:
    bool LoadConfig();
    void ShowSettings(bool b);
    void AddLog();
    void AddDate();
    void AddObserver();

private:
    Ui::ObservationLog *ui;
    QString configDirectory;
    std::string log_addres;
    QStringList *observersList;
    ofstream *out;
    QString finishTime;

signals:
//    void sigSuccessCreated();
};

#endif // OBSERVATION_LOG_H
