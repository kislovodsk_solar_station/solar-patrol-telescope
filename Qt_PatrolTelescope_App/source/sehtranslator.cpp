//#include <stdio.h>
#include "sehtranslator.h"

SehTranslator::SehTranslator(_se_translator_function translator) :
        m_newTranslator(translator != NULL ? translator : DefaultSeh2Cpp)
{
    m_oldTranslator = _set_se_translator(m_newTranslator);
}

SehTranslator::~SehTranslator()
{
    _set_se_translator(m_oldTranslator);
}

void SehTranslator::DefaultSeh2Cpp(unsigned int u, EXCEPTION_POINTERS *excp)
{
    if (p_statusToDosError != NULL)
    {
        const ULONG win32code = reinterpret_cast<statusToDosError>
                (p_statusToDosError)(excp->ExceptionRecord->ExceptionCode);
        _com_issue_error(HRESULT_FROM_WIN32(win32code));
    }
    else
        _com_issue_error(E_UNEXPECTED);
}

FARPROC SehTranslator::p_statusToDosError = GetProcAddress(
            GetModuleHandle(L"ntdll.dll"),
            "StatusToDosError");




