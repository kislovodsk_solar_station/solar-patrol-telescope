#include "dlg_scanner.h"
#include "ui_dlg_scanner.h"
#include "scanner.h"

#include <QFile>
#include <QSettings>
#include <QMessageBox>
#include <QThread>
#include <QDebug>

Dlg_Scanner::Dlg_Scanner(QString configDir, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Dlg_Scanner)
{
    setObjectName("Dlg_Scanner");
    config_scanner = configDir + QString("scanner.ini");
    ui->setupUi(this);

    scanner = new Scanner();
    pScannerThread = new QThread;
    scanner->moveToThread(pScannerThread);
    pScannerThread->start();

    // Соединения для кнопок
    connect(ui->btn_scan, SIGNAL(clicked(bool)),
            scanner, SLOT(Start()));
    connect(ui->btn_stop, SIGNAL(clicked(bool)),
            scanner, SLOT(Stop()));
    connect(ui->checkBox_assembling, SIGNAL(toggled(bool)),
            scanner, SLOT(SetAutoCreateImage(bool)));
    connect(ui->checkBox_convert, SIGNAL(toggled(bool)),
            scanner, SLOT(SetConvertToJpeg(bool)));
    connect(ui->checkBox_doppler, SIGNAL(toggled(bool)),
            scanner, SLOT(SetDopplerMode(bool)));
    connect(ui->checkBox_upload, SIGNAL(toggled(bool)),
            scanner, SLOT(SetUploadMode(bool)));

    connect(ui->testSolarImageButton, SIGNAL(clicked(bool)),
            this, SLOT(on_testSolarImageButton_clicked(bool)));
    connect(this, SIGNAL(sigStartCreateImageTest(QString,QString)),
            scanner, SLOT(StartCreateImageProgram(QString,QString)));
    connect(this, SIGNAL(sigStartUploadTest(QString)),
            scanner, SLOT(StartUploadProgram(QString)));

    // Соединения для других элементов
    connect(scanner, SIGNAL(sigProgressBarCounter(int)),
            ui->progressBar, SLOT(setValue(int)));
    connect(ui->spinBox_scanTime, SIGNAL(editingFinished()),
            this, SLOT(SetScanTime()));
    connect(ui->spinBox_scanRadius, SIGNAL(editingFinished()),
            this, SLOT(SetScanRadius()));

    // Вывод сообщений
    connect(scanner, SIGNAL(sigStatusMessage(QString)),
            this, SLOT(ShowStatusMessage(QString)));
    connect(scanner, SIGNAL(sigStatusMessage(int)),
            this, SLOT(ShowStatusMessage(int)));
    connect(scanner, SIGNAL(sigWarningMessage(QString)),
            this, SLOT(ShowWarningMessage(QString)));

    // Открыть/скрыть меню настроек
    connect(ui->btn_settings, SIGNAL(clicked(bool)),
            this, SLOT(ShowSettings(bool)));
    // Удаление scanner
    connect(this, &Dlg_Scanner::sigDeleteScanner,
            scanner, &Scanner::DeleteScanner);

    if (!LoadConfig())
    {
        ShowWarningMessage("Конфигурационный файл сканера отсутствует, либо не заданы параметры!");
        this->setDisabled(true);
    }
    ui->groupBox_settings->hide();

}

Dlg_Scanner::~Dlg_Scanner()
{
    qDebug() << "~SolarScannerWdgt 0";
    SaveConfig();

    emit sigDeleteScanner();
    pScannerThread->quit();
    pScannerThread->wait();
    delete pScannerThread;
    delete ui;
    qDebug() << "~SolarScannerWdgt 1";
}

//-----------------------------------------------------------------------------
// Установка пути для сохранения сканов
//
void Dlg_Scanner::SetSavePath(QString path)
{
    scanner->scanSettings.savePath = path;
}

//-----------------------------------------------------------------------------
// Вывод сообщений
//
void Dlg_Scanner::ShowStatusMessage(QString strMsg)
{
    ui->label_status->setText(strMsg);
}

void Dlg_Scanner::ShowStatusMessage(int number)
{
    ui->label_counter->setText(QString().setNum(number));
}

//-----------------------------------------------------------------------------
//  Вывод сообщения об ошибке
//
void Dlg_Scanner::ShowWarningMessage(QString strMsg)
{
    QMessageBox::warning( this, "Scanner", strMsg );
}

//-----------------------------------------------------------------------------
// Сохранение настроек в конфигурационный файл
//
void Dlg_Scanner::SaveConfig()
{
    QFile config(config_scanner);
    if(config.exists())
        config.remove();
    QSettings settings(config_scanner, QSettings::IniFormat);
    settings.setValue("Type", "Configuration file");
    settings.setValue("Module", "Scan system");
    settings.beginGroup("Parameters");
    settings.setValue("Scan_time", ui->spinBox_scanTime->value());
    settings.setValue("Scan_radius", ui->spinBox_scanRadius->value());
    settings.setValue("Auto_create_image", scanner->scanSettings.autoCreateImage);
    settings.setValue("Convert_to_Jpeg", scanner->scanSettings.convertToJpeg);
    settings.setValue("Path_to_scan", scanner->scanSettings.scanPath);
    settings.setValue("Path_to_save",  scanner->scanSettings.savePath);
    settings.setValue("Path_to_create_image_program",  scanner->scanSettings.programCreateImagePath);
    settings.setValue("Path_to_upload_program",  scanner->scanSettings.programUploadImagePath);
    settings.setValue("Doppler_mode", scanner->scanSettings.dopplerMode);
    settings.setValue("Auto_Upload", scanner->scanSettings.autoUpload);
    settings.endGroup();
    config.close();
}

//-----------------------------------------------------------------------------
// Загрузка настроек из конфигурационного файла
//
bool Dlg_Scanner::LoadConfig()
{
    QFile config(config_scanner);
    if(config.exists())
    {
        config.open(QIODevice::ReadOnly);
        QSettings settings(config_scanner, QSettings::IniFormat);
        settings.beginGroup("Parameters");

        scanner->scanSettings.msecToScan = settings.value("Scan_time").toUInt() * 1000;
        scanner->scanSettings.arcsecScanRadius = settings.value("Scan_radius").toInt();
        scanner->scanSettings.scanPath = settings.value("Path_to_scan").toString();
        scanner->scanSettings.savePath = settings.value("Path_to_save").toString();
        scanner->scanSettings.programCreateImagePath = settings.value("Path_to_create_image_program").toString();
        scanner->scanSettings.programUploadImagePath = settings.value("Path_to_upload_program").toString();

        ui->spinBox_scanRadius->setValue(scanner->scanSettings.arcsecScanRadius);
        ui->spinBox_scanTime->setValue(scanner->scanSettings.msecToScan / 1000);
        ui->checkBox_assembling->setChecked(settings.value("Auto_create_image").toBool());
        ui->checkBox_convert->setChecked(settings.value("Convert_to_Jpeg").toBool());
        ui->checkBox_doppler->setChecked(settings.value("Doppler_mode").toBool());
        ui->checkBox_upload->setChecked(settings.value("Auto_Upload").toBool());

        if(scanner->scanSettings.scanPath == "")
        {
            scanner->scanSettings.scanPath = SCANNER_SCAN_DIRECTORY;
            qDebug() << "SolarScannerWdgt::Не задан путь для сканирования!";
            ui->label_status->setText("Не задан путь для сканирования!");
            return false;
        }
        if(scanner->scanSettings.savePath == "")
        {
            scanner->scanSettings.savePath = SCANNER_SAVE_DIRECTORY;
            qDebug() << "SolarScannerWdgt::Не задан путь размещения файлов!";
            ui->label_status->setText("Не задан путь размещения файлов!");
            return false;
        }
        if(scanner->scanSettings.programCreateImagePath == "")
        {
            qDebug() << "SolarScannerWdgt::Не задан путь к программе сборки!";
            ui->label_status->setText("Не задан путь к программе сборки!");
            return false;
        }
        if(scanner->scanSettings.programUploadImagePath == "")
        {
            qDebug() << "SolarScannerWdgt::Не задан путь к программе загрузки!";
            ui->label_status->setText("Не задан путь к программе загрузки!");
            return false;
        }
//        qDebug() << scanner->scanSettings.programUploadImagePath;
        settings.endGroup();
        config.close();
        return true;
    }
    else
        return false;
}

//-----------------------------------------------------------------------------
// Метод для отображения/сокрытия меню настроек
//
void Dlg_Scanner::ShowSettings(bool b)
{
    if ( b ){
        ui->groupBox_settings->show();
//        qDebug() << parentWidget()->width() << width() << ui->groupBox_settings->width();
        this->parentWidget()->setGeometry( parentWidget()->x(),
                                           parentWidget()->y(),
                                           parentWidget()->width() + ui->groupBox_settings->width() + 200,
                                           parentWidget()->height());
        this->updateGeometry();
//        qDebug() << parentWidget()->width() << width();
    }
    else
    {
        ui->groupBox_settings->hide();
//        qDebug() << parentWidget()->width() << width();
        this->parentWidget()->setGeometry( parentWidget()->x(),
                                           parentWidget()->y(),
                                           parentWidget()->width() - ui->groupBox_settings->width() - 50,
                                           parentWidget()->height());
        this->updateGeometry();
//        qDebug() << parentWidget()->width() << width();
    }
}

//-----------------------------------------------------------------------------
// Установка времени сканирования
//
void Dlg_Scanner::SetScanTime()
{
    scanner->scanSettings.msecToScan = ui->spinBox_scanTime->value() * 1000.0;
}

//-----------------------------------------------------------------------------
// Установка диапазона сканирования
//
void Dlg_Scanner::SetScanRadius()
{
    scanner->scanSettings.arcsecScanRadius = ui->spinBox_scanRadius->value();
}

void Dlg_Scanner::on_testSolarImageButton_clicked(bool)
{
//    QString pathCreate = "D:/2017/test/scan/";
//    QString pathSave = "D:/2017/test/save/";
//    qDebug() << "pathCreate = " << pathCreate;
//    qDebug() << "pathSave = " << pathSave;

//    scanner->scanSettings.scanMode = 3;

//    emit sigStartCreateImageTest(pathCreate, pathSave);

//    scanner->scanSettings.scanMode = 5;
//    emit sigStartUploadTest("something");

    return;
}
