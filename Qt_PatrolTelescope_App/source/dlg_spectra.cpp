#include "dlg_spectra.h"
#include "ui_dlg_spectra.h"
#include "camera_avt.h"
#include "camera_jai.h"

#include <QTimer>
#include <QMessageBox>
#include <QSettings>
#include <QFile>
#include <QDebug>
#include <QThread>

#define TIMER_SHOW_MSEC 200
#define TIMER_SAVE_IMAGE 60000

bool Dlg_Spectra::isExists = false;
int Dlg_Spectra::nCameras = 0;

Dlg_Spectra::Dlg_Spectra(QString configDir, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Dlg_Spectra),
    pTimer_ShowImage(nullptr)
{
    config_spectral_cam = configDir + QString("spectral_camera.ini");
    config_spectral_cam_2 = configDir + QString("spectral_camera2.ini");
    setObjectName("Dlg_Spectra");
    ui->setupUi(this);
    ui->groupBox->hide();
    saveMode = false;
    pSpectralCamera = SelectCamera();
    if (!pSpectralCamera)
    {
        this->setDisabled(true);
        return;
    }

    p_cameraThread = new QThread(this);
    p_cameraThread->setObjectName("SpectralCamera thread");
    //-----------------------------------------------------------------------------------
    // Соединения для обеспечения работы камеры
    //-----------------------------------------------------------------------------------
    // Инициализация камеры
    connect(p_cameraThread, SIGNAL(started()),
            pSpectralCamera, SLOT(Open()));
    // Загрузка параметров из конфигурационного файла
    connect (pSpectralCamera, SIGNAL(sigCameraOpened()),
            this, SLOT(LoadConfig()));
    // Установка параметров в камеру
    connect(this, SIGNAL(sigSetSettings()),
            pSpectralCamera, SLOT(SetParams()));
    // Получение текущей экспозиции при запуске
    connect (pSpectralCamera, SIGNAL(sigCameraStarted()),
            this, SLOT(GetExposure()));
    // Запуск таймеров после старта камеры
    connect(pSpectralCamera, SIGNAL(sigCameraStarted()),
            this, SLOT(StartTimers()));

    //-----------------------------------------------------------------------------------
    // Кнопки и галочки
    //-----------------------------------------------------------------------------------
    // Открыть камеру
    connect(ui->btn_Open, &QPushButton::pressed,
            pSpectralCamera, &Camera::Open);

    // Закрыть камеру
    connect(ui->btn_Close, &QPushButton::pressed,
            pSpectralCamera, &Camera::Stop);
    connect(ui->btn_Close, &QPushButton::pressed,
            pSpectralCamera, &Camera::Close);

    // Перезагрузить камеру
//    connect(ui->btn_Close, &QPushButton::pressed,
//            this, &Dlg_Spectra::StopTimers);
    connect(ui->btn_Reset, &QPushButton::pressed,
            pSpectralCamera, &Camera::Reset);

    // Открыть/скрыть меню настроек
    connect(ui->btn_Settings, SIGNAL(clicked(bool)),
            this, SLOT(ShowSettingsPanel(bool)));
    // Отобразить значения настроек по умолчанию
    connect(ui->btn_defaultSettings, SIGNAL(clicked(bool)),
                this, SLOT(DefaultSettings()));
    // Установить настройки
    connect(ui->btn_setSettings, SIGNAL(clicked(bool)),
            this, SLOT(SetSettings()));
    // Установка режима сохранения кадров
    connect (ui->checkBox_saveMode, SIGNAL(toggled(bool)),
             this, SLOT(SetSaveMode(bool)));
    //-----------------------------------------------------------------------------------
    // Связи графических элементов
    //-----------------------------------------------------------------------------------
    // Установка экспозиции
    connect(ui->exp_spinBox, SIGNAL(valueChanged(int)),
            pSpectralCamera, SLOT(SetExposure(int)));
    // Соединие ползунка и числового поля между собой
    connect(ui->exposureSlider, SIGNAL(valueChanged(int)),
            ui->exp_spinBox, SLOT(setValue(int)));
    connect(ui->exp_spinBox, SIGNAL(valueChanged(int)),
            ui->exposureSlider, SLOT(setValue(int)));
    // Установка режима автоматической экспозиции
    connect(ui->autoExp_radioBtn, SIGNAL(toggled(bool)),
            this, SLOT(ChangeExposureMode(bool)));
    // Скрытие элементов ручного режима при включении автоматического режима экспозиции
    connect(ui->autoExp_radioBtn, SIGNAL(toggled(bool)),
            ui->exposureSlider, SLOT(setDisabled(bool)));
    connect(ui->autoExp_radioBtn, SIGNAL(toggled(bool)),
            ui->exp_spinBox, SLOT(setDisabled(bool)));
    //-----------------------------------------------------------------------------------
    // Сообщения
    //-----------------------------------------------------------------------------------
    connect(pSpectralCamera, SIGNAL(sigStatusMessage(QString)),
            this, SLOT(ShowStatusMessage(QString)));
    connect(pSpectralCamera, SIGNAL(sigWarningMessage(QString)),
            this, SLOT(ShowWarningMessage(QString)));

    // Запуск потока камеры
    pSpectralCamera->moveToThread(p_cameraThread);
    p_cameraThread->start();

    // Таймер для вывода изображения
    pTimer_ShowImage = new QTimer(this);
    pTimer_ShowImage->setObjectName("TimerShowSpectralImage");

    connect(pTimer_ShowImage, SIGNAL(timeout()), this, SLOT(On_Timer_ShowImage()));
    // Остановка таймеров при перезапуске камеры
    connect(pSpectralCamera, &Camera::sigCameraReset,
            this, &Dlg_Spectra::StopTimers);

    ui->imageViewLabel->setObjectName("SpectraLabel");
}

Dlg_Spectra::~Dlg_Spectra()
{
    qDebug() << "~SolarSpectraWdgt 0";
    SaveConfig();
    StopTimers();

    if (pSpectralCamera)
    {
        delete pSpectralCamera;
        StopThreads();
    }
    delete ui;
    qDebug() << "~SolarSpectraWdgt 1";
}

void Dlg_Spectra::StopThreads()
{
    p_cameraThread->quit();
    p_cameraThread->wait();
}

//-----------------------------------------------------------------------------
// Выбор камеры из конфигурацонного файла
//
Camera* Dlg_Spectra::SelectCamera()
{
    nCameras++;
    if(nCameras == 1)
        configName = config_spectral_cam;
    if(nCameras == 2)
        configName = config_spectral_cam_2;

    QFile config(configName);
    Camera *camera;
    if(config.exists())
    {
//        qDebug() << "select camera: config found!";
        config.open(QIODevice::ReadOnly);
        QSettings settings(configName, QSettings::IniFormat);
        cameraClass = settings.value("Camera").toString();
        if(cameraClass == "JAI")
            camera = new Camera_JAI();
        else if(cameraClass == "AVT")
        {
            camera = new Camera_AVT();
//            qDebug() << "Select AVT camera!";
        }
        else
        {
            ui->label_status->setText("Камера не задана!");
            config.close();
            return nullptr;
        }
        camera->SetCameraModel(settings.value("Model").toString());
        config.close();
    }
    else
    {
//        qDebug() << "select camera: config not found!";
        ui->label_status->setText("Конфигурационный файл не найден!");
        ShowWarningMessage("Конфигурационный файл камеры спектра не найден!");
        return nullptr;
    }
    return camera;
}

//-----------------------------------------------------------------------------
// Загрузка настроек из конфигурационного файла
//
void Dlg_Spectra::LoadConfig()
{
    QFile config(configName);
    if(config.exists())
    {
        config.open(QIODevice::ReadOnly);
        LoadSettings();
        config.close();
    }
    else
    {
        DefaultSettings();
    }
    SetSettings();
//    qDebug() << "LoadConfig";
//    emit sigSetSettings();
    return;
}

//-----------------------------------------------------------------------------
// Загрузка значений из конфигурации в окно
//
void Dlg_Spectra::LoadSettings()
{
    QSettings settings(configName, QSettings::IniFormat);
    settings.beginGroup("Frame_settings");
    ui->FPS_SpinBox->setValue(settings.value(AVT_FPS).toDouble());
    ui->width_spinBox->setValue(settings.value(AVT_WIDTH).toInt());
    ui->height_spinBox->setValue(settings.value(AVT_HEIGHT).toInt());
    ui->xbin_spinBox->setValue(settings.value(AVT_BIN_X).toInt());
    ui->ybin_spinBox->setValue(settings.value(AVT_BIN_Y).toInt());
    ui->xpos_spinBox->setValue(settings.value(AVT_OFFSET_X).toInt());
    ui->ypos_spinBox->setValue(settings.value(AVT_OFFSET_Y).toInt());
    ui->exp_spinBox->setValue(settings.value(AVT_EXPOSURE).toInt());
    ui->gain_spinBox->setValue(settings.value(AVT_GAIN).toDouble());
    if(settings.value("Auto_exposure_mode").toBool() == true)
    {
        ui->autoExp_radioBtn->setChecked(true);
        pSpectralCamera->SetAutoExposure(true);
        ui->exposureSlider->setDisabled(true);
        ui->exp_spinBox->setDisabled(true);
    }
    else
    {
        ui->manualExp_radioBtn->setChecked(true);
        pSpectralCamera->SetAutoExposure(false);
    }
    settings.endGroup();
}

//-----------------------------------------------------------------------------
// Применить настройки
//
void Dlg_Spectra::SetSettings()
{
    pSpectralCamera->parameters.fps = ui->FPS_SpinBox->value();
    pSpectralCamera->parameters.width = ui->width_spinBox->value();
    pSpectralCamera->parameters.height = ui->height_spinBox->value();
    pSpectralCamera->parameters.binningX = ui->xbin_spinBox->value();
    pSpectralCamera->parameters.binningY = ui->ybin_spinBox->value();
    pSpectralCamera->parameters.offsetX = ui->xpos_spinBox->value();
    pSpectralCamera->parameters.offsetY = ui->ypos_spinBox->value();
    pSpectralCamera->parameters.gain = ui->gain_spinBox->value();
    pSpectralCamera->parameters.exposure = ui->exp_spinBox->value();
    emit sigSetSettings();
    SaveConfig();
}

//-----------------------------------------------------------------------------
// Вывод настроек по умолчанию
//
void Dlg_Spectra::DefaultSettings()
{
    ui->FPS_SpinBox->setValue(AVT_VAL_FPS);
    ui->xbin_spinBox->setValue(AVT_VAL_BINNING_X);
    ui->ybin_spinBox->setValue(AVT_VAL_BINNING_Y);
    ui->ypos_spinBox->setValue(AVT_VAL_OFFSET_Y);
    ui->xpos_spinBox->setValue(AVT_VAL_OFFSET_X);
    ui->width_spinBox->setValue(AVT_VAL_WIDTH);
    ui->height_spinBox->setValue(AVT_VAL_HEIGHT);
    ui->gain_spinBox->setValue(AVT_VAL_GAIN);
    ui->exp_spinBox->setValue(AVT_VAL_EXP);
}

//-----------------------------------------------------------------------------
// Вывод изображения с камеры по таймеру
//
void Dlg_Spectra::On_Timer_ShowImage()
{
    QPixmap pix = QPixmap::fromImage(pSpectralCamera->GetImage());
    ui->imageViewLabel->setScaledContents(true);
    ui->imageViewLabel->setPixmap(pix);
}

//-----------------------------------------------------------------------------
// Сохранение изображения с камеры по таймеру
//
void Dlg_Spectra::On_Timer_SaveImage()
{
//    if(pSpectralCamera->isStarted() == true && saveMode == true)
//    {
//        // Задание имени для папки на диске
//        QString dirName = QDateTime::currentDateTimeUtc().toString(savePath);

//        // Создание папки
//        QDir dir;
//        if(dir.exists(dirName) == false)
//            if(dir.mkpath(dirName))
//                if(dir.exists(dirName))
//                    qDebug() << "Папка" << dirName << "создана";

//        // Задание имени для сохраняемого изображения
//        QString nameFormat = "HHmmss.jpg";
//        QString fileName = QDateTime::currentDateTimeUtc().toString(nameFormat);

//        // Сохранение изображения
//        //p_guideCamera->GetImage().save(dirName.append(fileName),"JPG");
//        if(pSpectralCamera->GetImageBuf() != NULL)
//        {
//            QImage img(pSpectralCamera->GetImageBuf(),pSpectralCamera->GetFrameWidth(),pSpectralCamera->GetFrameHeight(),
//                               QImage::Format_Grayscale8,0,0);
//            img.save(dirName.append(fileName),"JPG");
//        }
//        else
//            ui->label_status->setText("Изображение не сохранено");
//    }
}

//-----------------------------------------------------------------------------
// Сохранение настроек в конфигурационный файл
//
void Dlg_Spectra::SaveConfig()
{
    if (!pSpectralCamera)
        return;

    QFile config(configName);
    if(config.exists())
        config.remove();
    QSettings settings(configName, QSettings::IniFormat);
    settings.setValue("Type", "Configuration file");
    settings.setValue("Module", "Spectral Camera");
    settings.setValue("Camera", cameraClass);
    settings.setValue("Model", pSpectralCamera->parameters.cameraName);
    settings.beginGroup("Frame_settings");
    settings.setValue("Auto_exposure_mode", ui->autoExp_radioBtn->isChecked());
    settings.setValue(AVT_EXPOSURE, ui->exp_spinBox->value());
    settings.setValue(AVT_FPS, ui->FPS_SpinBox->value());
    settings.setValue(AVT_WIDTH, ui->width_spinBox->value());
    settings.setValue(AVT_HEIGHT, ui->height_spinBox->value());
    settings.setValue(AVT_BIN_X, ui->xbin_spinBox->value());
    settings.setValue(AVT_BIN_Y, ui->ybin_spinBox->value());
    settings.setValue(AVT_OFFSET_X, ui->xpos_spinBox->value());
    settings.setValue(AVT_OFFSET_Y, ui->ypos_spinBox->value());
    settings.setValue(AVT_GAIN, ui->gain_spinBox->value());
    settings.endGroup();
    config.close();
}

//-----------------------------------------------------------------------------
// Метод для отображения меню настроек
//
void Dlg_Spectra::ShowSettingsPanel(bool b)
{
    if ( b )
    {
        ui->groupBox->show();
        this->parentWidget()->setGeometry( parentWidget()->x(),
                                           parentWidget()->y(),
                                           parentWidget()->width(),
                                           parentWidget()->height() + 107);
    }
    else
    {
        ui->groupBox->hide();
        this->parentWidget()->setGeometry( parentWidget()->x(),
                                           parentWidget()->y(),
                                           parentWidget()->width(),
                                           parentWidget()->height() - 107);
    }
}

//-----------------------------------------------------------------------------
// Получение текущей экспозиции
//
void Dlg_Spectra::GetExposure()
{
    ui->exp_spinBox->setValue(pSpectralCamera->GetExposure());
}

//-----------------------------------------------------------------------------
// Запуск таймеров
//
void Dlg_Spectra::StartTimers()
{
    // Таймер для вывода изображения
    if(!pTimer_ShowImage->isActive())
        pTimer_ShowImage->start(TIMER_SHOW_MSEC);

    // Таймер для сохранения изображения
//    if(!pTimer_SaveImage->isActive())
    //        pTimer_SaveImage->start(TIMER_SAVE_IMAGE);
}

//-----------------------------------------------------------------------------
// Остановка таймеров
//
void Dlg_Spectra::StopTimers()
{
    if(pTimer_ShowImage)
        pTimer_ShowImage->stop();
    qDebug() << "Dlg_Spectra: StopTimers()";
}

//-----------------------------------------------------------------------------
// Установка автоматического либо ручного режима экспозиции
//
void Dlg_Spectra::ChangeExposureMode(bool autoMode)
{
    pSpectralCamera->SetAutoExposure(autoMode);
    if(!autoMode)
        ui->exp_spinBox->setValue(pSpectralCamera->GetExposure());
}

//-------------------------------------------------------------------------------------------------
// Включение режима сохранения изображений
//
void Dlg_Spectra::SetSaveMode(bool b)
{
    saveMode = b;
    On_Timer_SaveImage();
}

//-----------------------------------------------------------------------------
// Вывод сообщения с камеры
//
void Dlg_Spectra::ShowStatusMessage(QString strMsg)
{
    ui->label_status->setText(strMsg);
}

//-----------------------------------------------------------------------------
//  Вывод сообщения об ошибке
//
void Dlg_Spectra::ShowWarningMessage(QString strMsg)
{
    QMessageBox::warning( this, "Spectral camera", strMsg );
    qDebug() << "Dlg_Spectra_WarningMessage";
}
