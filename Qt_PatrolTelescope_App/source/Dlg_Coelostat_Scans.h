#ifndef Dlg_Coelostat_SCANS_H
#define Dlg_Coelostat_SCANS_H

#include <QDialog>
#include <QDateTime>

namespace Ui {
class Dlg_Coelostat_Scans;
}

class Dlg_Coelostat_Scans : public QDialog
{
    Q_OBJECT

public:
    double m_dlg_fScan_Angle_Minut_Pos_Beg;    //Угол - положения начала сканирования в минутах дуги
    double m_dlg_fScan_Angle_Minut_Pos_End;    //Угол - положения конца  сканирования в минутах дуги
    double m_dlg_fScan_Speed_MinutOfMinut;     //Скорость сканирования (минут дуги в минуту)
    bool m_dlg_bScan_On_Scan_back;             //Работаем в режиме сканирования Солнца в обоих направлениях
    bool m_dlg_bScan_On_Delta_Correction;      //Работаем в режиме сканирования Солнца с коррекцией положения по дельте
    bool m_OnStartDialog;                      //Запустили полностью диалог (прошли все инициализации)

public:
    explicit Dlg_Coelostat_Scans(QWidget *parent = 0);
    ~Dlg_Coelostat_Scans();
    void SetDlgCurItem();                           //Установка элементов в диалоге по значениям

private slots:
    void slotDelayedInit();
    
    void on_checkBox_ScanBack_clicked(bool checked);

    void on_cmb_start_Znak_currentIndexChanged(int index);
    void on_spinBox_start_mm_valueChanged(int arg1);
    void on_spinBox_start_ss_valueChanged(int arg1);

    void on_cmb_end_Znak_currentIndexChanged(int index);
    void on_spinBox_end_mm_valueChanged(int arg1);
    void on_spinBox_end_ss_valueChanged(int arg1);

    void on_doubleSpinBox_speed_valueChanged(double arg1);

    void on_checkBox_OnCorrDelta_clicked(bool checked);

private:
    Ui::Dlg_Coelostat_Scans *ui;
};

#endif // Dlg_Coelostat_SCANS_H
