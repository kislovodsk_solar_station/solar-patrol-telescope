#include <QTimer>
#include "Dlg_Coelostat.h"
#include "Dlg_Coelostat_Delta_GoTo.h"
#include "ui_Dlg_Coelostat_Delta_GoTo.h"

Dlg_Coelostat_Delta_GoTo::Dlg_Coelostat_Delta_GoTo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dlg_Coelostat_Delta_GoTo)
{
    setObjectName("Dlg_Coelostat_DeltaGoTo");
    m_OnStartDialog = false;
    ui->setupUi(this);
    QTimer::singleShot(0, this, SLOT(slotDelayedInit()));
//    connect(ui->spinBox_hh, SIGNAL(valueChanged(int)), ui->spinBox_hh, SLOT(Value(int)));

}
void Dlg_Coelostat_Delta_GoTo::slotDelayedInit()
{
    m_bool_Delta_On_Go_Cur_Time = false;
    m_bool_Delta_Centering = false;
    ui->cmbZnak->addItem(QString('-'));
    ui->cmbZnak->addItem(QString('+'));
    m_fAngle_Delta_Go_cur=m_fAngle_Delta_Go_old;
    SetDlgAngle();//Установка в диалоге по m_fAngle_Delta_Go_cur
    m_OnStartDialog = true;
}

Dlg_Coelostat_Delta_GoTo::~Dlg_Coelostat_Delta_GoTo()
{
    delete ui;
}

//Установка времени в диалоге по m_tSysPos
void Dlg_Coelostat_Delta_GoTo::SetDlgAngle()
{
    QString sMsg;
    cAngle Angle=funRadian2Angle(m_fAngle_Delta_Go_cur);//Перевод радиан в угол(грд, мин, сек)
    int n;
    //sTmp=m_tDataTime_cur.toString("hh:mm:ss"); //для просмотра в QString
    if( Angle.cZnak == '-') ui->cmbZnak->setCurrentIndex(0);
                       else ui->cmbZnak->setCurrentIndex(1);
    n = abs(Angle.iDeg);
    if (n < 10 ) ui->spinBox_hh->setPrefix("0"); else ui->spinBox_hh->setPrefix("");
    ui->spinBox_hh->setValue(n);

    n = Angle.iMinut;
    if (n < 10) ui->spinBox_mm->setPrefix("0"); else ui->spinBox_mm->setPrefix("");
    ui->spinBox_mm->setValue(n);

    n = int(Angle.fSec);
    if (n < 10) ui->spinBox_ss->setPrefix("0"); else ui->spinBox_ss->setPrefix("");
    ui->spinBox_ss->setValue(n);

    m_bool_Delta_Centering = false;
    //qDebug()<<"m_fAngle_Delta_Go_cur=" << m_fAngle_Delta_Go_cur;
}

void Dlg_Coelostat_Delta_GoTo::on_Btn_Cur_Time_clicked()
{
    if (!m_OnStartDialog) return;
    QDateTime tSys_cur = QDateTime::currentDateTime();
    cSunCoordinates sunPos=funSunPos_curLoc_tSys(tSys_cur, TIME_HOUR_LOC2UT);
    m_fAngle_Delta_Go_cur = sunPos.m_dDeclination;//.m_dZenithAngle *pi / 180.0;
    m_bool_Delta_On_Go_Cur_Time = true;
    SetDlgAngle();//Установка в диалоге по m_fAngle_Delta_Go_cur

}

void Dlg_Coelostat_Delta_GoTo::on_Btn_Cur_Pos_clicked()
{
    if (!m_OnStartDialog) return;
    m_fAngle_Delta_Go_cur = m_fAngle_Delta_Go_old;
    m_bool_Delta_On_Go_Cur_Time = false;
    SetDlgAngle();//Установка в диалоге по m_fAngle_Delta_Go_cur
}

void Dlg_Coelostat_Delta_GoTo::on_Btn_Centering_clicked()
{

    if (!m_OnStartDialog) return;
    m_fAngle_Delta_Go_cur = 0;
    SetDlgAngle();//Установка в диалоге по m_fAngle_Delta_Go_cur
    m_bool_Delta_Centering = true;
    m_bool_Delta_On_Go_Cur_Time = false;
}


void Dlg_Coelostat_Delta_GoTo::on_spinBox_hh_valueChanged(int arg1)
{
    if (!m_OnStartDialog) return;
    cAngle Angle=funRadian2Angle(m_fAngle_Delta_Go_cur);//Перевод радиан в угол(грд, мин, сек)
    Angle.iDeg=arg1;
    if (Angle.cZnak == '-') Angle.iDeg=-Angle.iDeg;//в значении градусов угла хранится знак
    m_fAngle_Delta_Go_cur = funAngle2Radian(Angle);
    //qDebug()<<"m_fAngle_Delta_Go_cur=" << m_fAngle_Delta_Go_cur;
    SetDlgAngle();//Установка в диалоге по m_fAngle_Delta_Go_cur

}

void Dlg_Coelostat_Delta_GoTo::on_spinBox_mm_valueChanged(int arg1)
{
    if (!m_OnStartDialog) return;
    cAngle Angle=funRadian2Angle(m_fAngle_Delta_Go_cur);//Перевод радиан в угол(грд, мин, сек)
    Angle.iMinut=arg1;
    m_fAngle_Delta_Go_cur = funAngle2Radian(Angle);
    SetDlgAngle();//Установка в диалоге по m_fAngle_Delta_Go_cur
}

void Dlg_Coelostat_Delta_GoTo::on_spinBox_ss_valueChanged(int arg1)
{
    if (!m_OnStartDialog) return;
    cAngle Angle=funRadian2Angle(m_fAngle_Delta_Go_cur);//Перевод радиан в угол(грд, мин, сек)
    Angle.fSec=arg1;
    m_fAngle_Delta_Go_cur = funAngle2Radian(Angle);
    SetDlgAngle();//Установка в диалоге по m_fAngle_Delta_Go_cur
}

void Dlg_Coelostat_Delta_GoTo::on_cmbZnak_currentIndexChanged(int index)
{
    if (!m_OnStartDialog) return;
    if (index == 0 )m_fAngle_Delta_Go_cur = -abs(m_fAngle_Delta_Go_cur);
               else m_fAngle_Delta_Go_cur = +abs(m_fAngle_Delta_Go_cur);
    SetDlgAngle();//Установка в диалоге по m_fAngle_Delta_Go_cur
}

