//Работа с Ethernet портом в QT C++
//Обычно порт открывается как обычный файл. Как только порт открыт, то в него можно записывать и считывать как из обычного файла.
//Конечно же придётся добавить различные проверки байтов при чтении и записи.

#include <QDebug>
#include "QSM_CNLR_Ethernet.h"

//Для CRC16
//#define P_CCITT 0x1021
//#define P_CCITT 0x1189

USHORT fast_crc_blk  ( const BYTE *blk_ptr ,  USHORT blk_len);
//USHORT CalculateCRC16( char *c_ptr, USHORT len);
//static void init_crcccitt_tab( void );


//конструктор CControllerEthernet
CControllerEthernet::CControllerEthernet()
{
    setObjectName("CNRL_Ethernet");
    m_ConnectSocket = -1;
    m_bOnThread = true;
    m_pCurInfoCntlr_PLC230  = 0;
    m_pCurInfoCntlr_PLCM_E3 = 0;
    m_anSM_Ports_Out[0]=0;
    m_anSM_Ports_Out[1]=0;
    m_anSM_Ports_Out[2]=0;
}

//деструктор CControllerEthernet
CControllerEthernet::~CControllerEthernet()
{
    Terminate_Port();   // освобождаем порт
}

// Поток в котором осуществляется сканирование cостояния контроллера
//#pragma optimize ("", off)
void CControllerEthernet::run() //Код, исполняемый в потоке
{
   msleep(SLEEP_THREAD*2);//CControllerEthernet::
   QString strLog = "";
   BOOL bGetInf=FALSE;
   int nCNTL_on_max = CNTL_ON_MSEC /SLEEP_THREAD, nCNTL_on = nCNTL_on_max; //Отсчеты для отправки пакета поддержания работы контроллера (отправлять нужно не менее 1 раз в 5 сек)

   //InfoCntlr *pInf;//Последнее сообщение контроллера
   //int nSize=sizeof(InfoCntlr);
   while(m_bOnThread)
   {
        m_bPortReady  = FALSE;
        bGetInf=Get_Data_Ethernet();
        m_bPortReady  = TRUE;// FALSE;

        if (!(--nCNTL_on))
        {
            nCNTL_on = nCNTL_on_max;
            Send_Command_Ethernet_ON_Port(); // Отправить команду на Ethernet-порт для инициализации портов и поддержания работы контроллера
        }
        else
            msleep(SLEEP_THREAD);

//        qDebug() << bGetInf << "  -  " << m_bPortReady;
//        if (bGetInf) qDebug() << m_pCurInfoCntlr_PLCM_E3->Position_0;
   }
  //Вышли
}

//#pragma optimize ("", on)

//Recv с таймаутом
int CControllerEthernet::RecvWithTimeout( int Socket, char *Buffer, int Len, long nuTimeout, int *bTimedOut )
{

    fd_set ReadSet ;
    int n ;
    struct timeval Time ;
    FD_ZERO(&ReadSet) ;
    FD_SET(Socket,&ReadSet) ;
    Time.tv_sec = 0 ;
    Time.tv_usec = nuTimeout ;
    *bTimedOut = FALSE ;
    n = select(Socket+1,&ReadSet,NULL,NULL,&Time) ;
    if (n > 0) { /* got some data */
        return recv(Socket,Buffer,Len,0);
    }
    if (n == 0) { /* timeout */
        *bTimedOut = TRUE ;
    }
    return(n) ; /* trouble */
}


// инициализация Ethernet порта
BOOL CControllerEthernet::Initialize_Ethernet()
{
    WSADATA wsaData;
    int n;
    WSAStartup(MAKEWORD(2,2), &wsaData);   // Initialize Winsock
    m_ConnectSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP); // Create a socket for sending data
    //qDebug() << "Сокет " << m_ConnectSocket;
    m_RecvAddr.sin_family = AF_INET;
    m_RecvAddr.sin_port = htons(MY_ETHERNET_PORT);

    m_RecvAddr.sin_addr.s_addr = inet_addr( m_sCNLR_IP.toUtf8().data() );
		
    m_bPortReady = TRUE;// FALSE;
    LONG anMaxSpeed[N_MAX_AXIS], anAcceler[N_MAX_AXIS];
    for (n=0; n < N_MAX_AXIS; n++) {
        anMaxSpeed[n] = m_anSM_Speed_max[n] * 2;
        anAcceler [n] = m_anSM_Acceler  [n];
    }
    Send_Command_Ethernet_Reset(anMaxSpeed, anAcceler);   // Отправить команду на Ethernet-порт Reset );

    start();
    msleep(SLEEP_THREAD*4);

    Send_Command_Ethernet_HardInit();  //Отправить команду на Ethernet-порт инициализации железа контроллера
    msleep(SLEEP_THREAD*4);
    Send_Command_Ethernet_InitPorts(); //Отправить команду на Ethernet-порт инициализации железа контроллера - настройка портов ввода-вывода
    msleep(SLEEP_THREAD*4);
    Send_Command_Ethernet_HardInit();  //Отправить команду на Ethernet-порт инициализации железа контроллера
    msleep(SLEEP_THREAD*4);

//    strLog="Ok";
    qDebug() << "CControllerEthernet::Initialize_Ethernet() -> " << m_sCNLR_IP + "(" + m_sCNLR_Name + ")";
    return TRUE;
        
} // end CControllerEthernet::Initialize


// Считать данные с Ethernet-порта
BOOL CControllerEthernet::Get_Data_Ethernet() //(CString &strLog, char *outBuffer)
{
    if (m_sCNLR_IP.length() < 4 ) return FALSE;

    BOOL bTimedOut;
    //g_NSizeByteP = -1;

    m_NSizeByteP=RecvWithTimeout(m_ConnectSocket, m_btBuffer_In, sizeof(m_btBuffer_In), 20, &bTimedOut);

//    qDebug() << m_NSizeByteP;

    if ( m_NSizeByteP)
    {
        if (m_NSizeByteP == m_nLenStatusPacket && *(USHORT*)m_btBuffer_In == m_nLenStatusPacket){ //Получили статусный пакет
            if (m_nLenStatusPacket == N_STATUS_PACKET_LEN_PLC230)
                m_pCurInfoCntlr_PLC230 = (InfoCntlr_PLC230*)m_btBuffer_In;

            if (m_nLenStatusPacket == N_STATUS_PACKET_LEN_PLCM_E3)
                m_pCurInfoCntlr_PLCM_E3 = (InfoCntlr_PLCM_E3*)m_btBuffer_In;
        }
    }

    if(!bTimedOut && m_NSizeByteP) return TRUE;
    else {
      /*Terminate();
      msleep(SLEEP_THREAD*2);
      CString strLog;
      Initialize_Ethernet(strLog);
      msleep(SLEEP_THREAD*2);
      */
      return FALSE;
  }
}

//Процедура определения длины статусного пакета (зависит от контроллера)
void  CControllerEthernet::GetLenStatusPacket()
{   //По имени контроллера
    if (m_sCNLR_Name.contains("PLC230", Qt::CaseInsensitive))
        m_nLenStatusPacket = N_STATUS_PACKET_LEN_PLC230;

    if (m_sCNLR_Name.contains("PLCM-E3", Qt::CaseInsensitive))
        m_nLenStatusPacket = N_STATUS_PACKET_LEN_PLCM_E3;
}

//Cтатусный пакет получен хотябы один раз
BOOL CControllerEthernet::StatusPakOn()
{
    BOOL ret;
    if (m_nLenStatusPacket == N_STATUS_PACKET_LEN_PLCM_E3)
       ret = m_pCurInfoCntlr_PLCM_E3 > 0;

    if (m_nLenStatusPacket == N_STATUS_PACKET_LEN_PLC230)
       ret = m_pCurInfoCntlr_PLC230 > 0;
    return ret;
}

//Последнее сообщение контроллера PLCM_E3
InfoCntlr_PLCM_E3 * CControllerEthernet::GetCurInfoCntlr_PLCM_E3()
{
    return m_pCurInfoCntlr_PLCM_E3;
}


//Последнее сообщение контроллера PLC230
InfoCntlr_PLC230 * CControllerEthernet::GetCurInfoCntlr_PLC230()
{
    return m_pCurInfoCntlr_PLC230;
}

//Определение позиции в шагах ШД для оси nAxis
long CControllerEthernet::GetPosAxes(int nAxis)
{
    //m_bPortReady  = FALSE;  bGetInf=Get_Data_Ethernet();  m_bPortReady  = TRUE;
    long nPosStep;
    if (m_nLenStatusPacket == N_STATUS_PACKET_LEN_PLCM_E3)
       nPosStep = double(*( &m_pCurInfoCntlr_PLCM_E3->Position_0 + nAxis - 1));

    if (m_nLenStatusPacket == N_STATUS_PACKET_LEN_PLC230)
       nPosStep = double(*( &m_pCurInfoCntlr_PLC230->Position_0 + nAxis - 1));

    return nPosStep;
}
// Определение состояния входных портов
long CControllerEthernet::GetInPorts()
{
    //m_bPortReady  = FALSE;  bGetInf=Get_Data_Ethernet();  m_bPortReady  = TRUE;
    long nPort_in;
    if (m_nLenStatusPacket == N_STATUS_PACKET_LEN_PLCM_E3)
       nPort_in = m_pCurInfoCntlr_PLCM_E3->Port_1;

    if (m_nLenStatusPacket == N_STATUS_PACKET_LEN_PLC230)
       nPort_in = m_pCurInfoCntlr_PLC230->Port_2;

    return nPort_in;

}

//Определение номера пакета который прошел в контроллере(из статусного пакета)
BYTE CControllerEthernet::GetCurNComm()
{
    //m_bPortReady  = FALSE;  bGetInf=Get_Data_Ethernet();  m_bPortReady  = TRUE;
    BYTE nComm;
    if (m_nLenStatusPacket == N_STATUS_PACKET_LEN_PLCM_E3)
       nComm = m_pCurInfoCntlr_PLCM_E3->Ncmd;

    if (m_nLenStatusPacket == N_STATUS_PACKET_LEN_PLC230)
       nComm = m_pCurInfoCntlr_PLC230->Ncmd;

    return nComm;
}


// Проверяем свободен ли порт Ethernet для отправки
BOOL CControllerEthernet::Ethernet_ON()
{
    int i, Nloop=20;
    for (i=0; i < Nloop; i++)
        if (StatusPakOn() && m_bPortReady) return TRUE;
          else msleep(SLEEP_THREAD /2);//Ждем пока освободиться порт Ethernet
    return FALSE;
}

// Отправить команду на Ethernet-порт
BOOL CControllerEthernet::Send_Command_Ethernet(int nLen)
{
  //long aaa=*(LONG*)outBuffer;
  *(SHORT*)m_btBuffer_Out=SHORT(nLen);
  if (*(LONG*)m_btBuffer_Out != 0x0101004B){ //Если не иницализация
    //if (!g_bPortReady) return FALSE;//Ждем пока освободиться порт Ethernet
     //while(!g_bPortReady) msleep(SLEEP_THREAD);//Ждем пока освободиться порт Ethernet
     *(UCHAR*)(m_btBuffer_Out+3) = GetCurNComm() + 1; //Номер пакета
    }
  USHORT nCRC1 =  fast_crc_blk ((BYTE*) m_btBuffer_Out, nLen-2);
  *(USHORT*)(m_btBuffer_Out + nLen - 2) = nCRC1;
  int i, Nloop=8;
  BOOL bOnSend = FALSE;
  for (i=0; i < Nloop && !bOnSend; i++){//Повторяем пока не выполнится в контроллере
     bOnSend = sendto(m_ConnectSocket, m_btBuffer_Out, nLen, 0, (SOCKADDR *) &m_RecvAddr, sizeof(m_RecvAddr));
     msleep(SLEEP_THREAD*4);
     Get_Data_Ethernet();// Считать данные статусного пакета с Ethernet-порта
     if (*(LONG*)m_btBuffer_Out != 0x0101004B) //Если не иницализация
         bOnSend = bOnSend && (*(UCHAR*)(m_btBuffer_Out+3) == GetCurNComm());//Проверяем перещелкнулся ли номер команды
     else bOnSend = bOnSend && (*(UCHAR*)(m_btBuffer_Out+3) == 1);//Проверяем перещелкнулся ли номер команды

  }

  /*
    UCHAR sBuffer_U[200];
    memcpy(sBuffer_U, sBuffer, nLen);
    CString sstt1[200];
    for (i=0; i < nLen; i++) sstt1[i].Format("%2.2X", sBuffer_U[i]);
    //char mas[]={'A','B','C'}; //char mas[]={0x41,0x42,0x43}; //init_crcccitt_tab();
    //USHORT nCRC1 =  fast_crc_blk ((BYTE*) mas, 3); //nCRC2 = CalculateCRC16((char*) mas, 3); //62728 //nCRC3 =  crc16_ccitt     ((BYTE*) mas, 3); //nCRC4 =  crc16_x25_ccitt ((BYTE*) mas, 3);

    nLen=0x1e;
    //char c_data[]={0x4b, 0x00, 0x01, 0x79, 0x88, 0x13, 0x00, 0x00, 0xe0, 0x2e, 0x00, 0x00, 0x28, 0x23, 0x00, 0x00, 0xe0, 0x2e, 0x00, 0x00, 0xe0, 0x2e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1e, 0x41, 0x00, 0x00, 0xa4, 0x47, 0x00, 0x00, 0x1e, 0x41, 0x00, 0x00, 0x1e, 0x41, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x14, 0x2d, 0xdd};
    char c_data[]={0x1e, 0x00, 0x09, 0x07, 0x78, 0xd9, 0x07, 0x00, 0x91, 0x47, 0x06, 0x00, 0xa0, 0x7e, 0x05, 0x00, 0xba, 0xec, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x88, 0x9c};
    *(UCHAR*)(c_data+3)=m_pCurInfoCntlr->Ncmd + 1; //Номер пакета
    nCRC1 =  fast_crc_blk ((BYTE*) c_data, nLen - 2);
    *(USHORT*)(c_data + nLen - 2)=nCRC1;
    sendto(SendSocket, c_data, nLen, 0, (SOCKADDR *) &RecvAddr, sizeof(RecvAddr));
  closesocket(SendSocket);
  WSACleanup(); */
  return bOnSend;
}

//Отправить команду на Ethernet-порт инициализации железа контроллера
BOOL CControllerEthernet::Send_Command_Ethernet_HardInit()
{
  if (!Ethernet_ON()) return FALSE; // Проверяем свободен ли порт Ethernet для отправки
  
  m_bPortReady  = FALSE;
  memset(m_btBuffer_Out, 0x00, N_BYTE_BUFFER);
  
  int nLen=0x13;
  int i;

  BYTE VREF[] =     {1,1,1,1};	  //{15,15,15,15}-значения рабочих токов для каналов 1-4 (влияет на ток в катушках шагового двигателя, (НЕ ТОЧНО! НАЙДЕНО В ИНТЕРНЕТЕ))
  BYTE VPFD[] =     {1,1,1,1};	  //{10,10,10,10}-значения подстройки FineTune каналов 1-4 (влияет на временные характеристики переходных процессов в катушках.)
  BYTE * dividers = m_anSM_MicroStep; //установка делителя (микрошага) (Делители похоже на деление шага(микрошаги))

  m_btBuffer_Out[2]=19; //Номер команды
  for (i = 0; i < 4; i++)
  {
    m_btBuffer_Out[4 + i]  = (byte)VREF[i];
    m_btBuffer_Out[8 + i]  = (byte)VPFD[i];
    m_btBuffer_Out[12 + i] = (byte) dividers[i];
  }
  m_btBuffer_Out[16] = (byte) 0xF;//0xFF; //half_curr_mask;
  
  Send_Command_Ethernet(nLen);// Отправить команду на Ethernet-порт
  
  m_bPortReady  = TRUE;// FALSE;

  //UCHAR c_data[]={0x13, 0x00, 0x13, 0x01, 0x0e, 0x0f, 0x0f, 0x0f, 0x0a, 0x0a, 0x0a, 0x0a, 0x04, 0x04, 0x04, 0x04, 0x0f, 0xb1, 0xa3};
  //ULONG aaa[5]; memcpy(aaa, c_data, 5*4);

  return TRUE;
}

 //Отправить команду на Ethernet-порт инициализации железа контроллера - настройка портов ввода-вывода
BOOL CControllerEthernet::Send_Command_Ethernet_InitPorts()
{
  if (!Ethernet_ON()) return FALSE; // Проверяем свободен ли порт Ethernet для отправки
  
  m_bPortReady  = FALSE;
  memset(m_btBuffer_Out, 0x00, N_BYTE_BUFFER);

  bool bOnSend;
  int nLen=0x36;

  m_bPortReady  = TRUE;
  msleep(SLEEP_THREAD*4);
  m_bPortReady  = FALSE;
  
  UCHAR c_data_PLCM_E3[]={0x36, 0x00, 0x0c, 0x6f, 0x2f, 0x09, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x8a, 0x8b, 0x8c, 0x8e, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x01, 0x02, 0x03, 0x04, 0xff, 0xff, 0x85, 0x86, 0x87, 0x88, 0xff, 0xff, 0xff, 0xff, 0x50, 0xbd};
  UCHAR c_data_PLC230[] ={0x36, 0x00, 0x0c, 0x25, 0x2f, 0x89, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x8a, 0x8b, 0x8c, 0x8e, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x01, 0x02, 0x03, 0x04, 0xff, 0xff, 0x85, 0x86, 0x87, 0x88, 0xff, 0xff, 0xff, 0xff, 0x62, 0x08};

  if (m_nLenStatusPacket == N_STATUS_PACKET_LEN_PLCM_E3)
     memcpy(m_btBuffer_Out, c_data_PLCM_E3, nLen);

  if (m_nLenStatusPacket == N_STATUS_PACKET_LEN_PLC230)
     memcpy(m_btBuffer_Out, c_data_PLC230, nLen);

  bOnSend=Send_Command_Ethernet(nLen);// Отправить команду на Ethernet-порт
    
  m_bPortReady  = TRUE;
  msleep(SLEEP_THREAD*4);
  m_bPortReady  = FALSE;


  nLen=0x07;
  UCHAR c_data_3[]={0x07, 0x00, 0x12, 0x2e, 0x64, 0x78, 0xac};
  memcpy(m_btBuffer_Out, c_data_3, nLen);
  bOnSend = bOnSend && Send_Command_Ethernet(nLen);// Отправить команду на Ethernet-порт
  
  m_bPortReady  = TRUE;
  msleep(SLEEP_THREAD*4);
  m_bPortReady  = FALSE;

  nLen=0x1e;
  UCHAR c_data_4[]={0x1e, 0x00, 0x11, 0x2f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xac, 0x45};
  memcpy(m_btBuffer_Out, c_data_4, nLen);
  bOnSend = bOnSend && Send_Command_Ethernet(nLen);// Отправить команду на Ethernet-порт
  
  m_bPortReady  = TRUE;
  msleep(SLEEP_THREAD*4);
  m_bPortReady  = FALSE;

  nLen=0x22;
  UCHAR c_data_5[]={0x22, 0x00, 0x14, 0x3a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x97, 0xe3};
  memcpy(m_btBuffer_Out, c_data_5, nLen);
  bOnSend = bOnSend && Send_Command_Ethernet(nLen);// Отправить команду на Ethernet-порт
  m_bPortReady  = TRUE;
  msleep(SLEEP_THREAD*4);
  m_bPortReady  = FALSE;

  nLen=0x0f;
  UCHAR c_data_6[]={0x0f, 0x00, 0x0a, 0x03, 0x00, 0x00, 0x00, 0x00, 0xc8, 0x20, 0x03, 0x00, 0x00, 0xbe, 0x17};
  memcpy(m_btBuffer_Out, c_data_6, nLen);
  bOnSend = bOnSend && Send_Command_Ethernet(nLen);// Отправить команду на Ethernet-порт

  nLen=0x12;
  UCHAR c_data_2[]={0x12, 0x00, 0x10, 0x2d, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xe9, 0xb8};
  memcpy(m_btBuffer_Out, c_data_2, nLen);
  bOnSend = bOnSend && Send_Command_Ethernet(nLen);// Отправить команду на Ethernet-порт


  m_bPortReady  = TRUE;
  msleep(SLEEP_THREAD*4);
  m_bPortReady  = FALSE;

  m_bPortReady  = TRUE;
  msleep(SLEEP_THREAD*4);
  m_bPortReady  = FALSE;

  nLen=0x07;
  UCHAR c_data_7[]={0x07, 0x00, 0x0b, 0x09, 0x00, 0xea, 0xfe};
  memcpy(m_btBuffer_Out, c_data_7, nLen);
  bOnSend = bOnSend && Send_Command_Ethernet(nLen);// Отправить команду на Ethernet-порт


  m_bPortReady  = TRUE;// FALSE;
  return bOnSend;
}

// Отправить команду на Ethernet-порт инициализации контроллера и сброса параметров
BOOL CControllerEthernet::Send_Command_Ethernet_Reset(LONG * pnMaxSpeed, LONG * pnAcceler)
{
  //if (!Ethernet_ON()) return FALSE; // Проверяем свободен ли порт Ethernet для отправки
  m_bPortReady  = FALSE;

  memset(m_btBuffer_Out, 0x00, N_BYTE_BUFFER);

  bool bOnSend;
  int nLen=0x4B;
  int i;

  m_btBuffer_Out[2]=1; //Номер команды
  m_btBuffer_Out[3]=1; //Начальный номер пакета

  *(ULONG*)(m_btBuffer_Out+4)=5000; //Discretization time длительность исполенения каждого элемента траектории в микросекундах. Минимальная длительность 2000мкс, типовое значение 5000-10000мкс;
  //ULONG a = *(ULONG*)(m_btBuffer_Out+4);

  //qDebug() << pnMaxSpeed[0] << pnAcceler[0];
  for (i= 0; i < N_MAX_AXIS; i++){
      *(ULONG*)(m_btBuffer_Out +  8 + i*4) = pnAcceler[i]; //Ускорения
      *(ULONG*)(m_btBuffer_Out + 32 + i*4) = pnMaxSpeed[i];//Скорости
  }
  for (i=56; i < 68; i=i+4) *(ULONG*)(m_btBuffer_Out+i)=0x0; //значение выходов по-умолчанию для каждого порта. Автоматически выставляется на портах при переходе в состояние аварии;

  *(ULONG*) (m_btBuffer_Out+68)=5000; //SpindleAccel значение ускорения шпинделя;
  *(USHORT*)(m_btBuffer_Out+72)=0;//100;  //MaxCorrAcc коэффициент разрешенного превышения заданного ускорения, заданный в %. Применяется, например, при выборке люфтов.

  //UCHAR c_data_0[]={0x4b, 0x00, 0x01, 0x01, 0x88, 0x13, 0x00, 0x00, 0x04, 0xa6, 0x00, 0x00, 0xe0, 0x2e, 0x00, 0x00, 0xe0, 0x2e, 0x00, 0x00, 0xe0, 0x2e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x4c, 0x01, 0x00, 0x1e, 0x41, 0x00, 0x00, 0x1e, 0x41, 0x00, 0x00, 0x1e, 0x41, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x14, 0x37, 0x9};
  //memcpy(m_btBuffer_Out, c_data_0, nLen);
  bOnSend = Send_Command_Ethernet(nLen);// Отправить команду на Ethernet-порт
  m_bPortReady  = TRUE;// FALSE;
  //char c_data[]={0x4b, 0x00, 0x01, 0x50, 0x88, 0x13, 0x00, 0x00, 0xe0, 0x2e, 0x00, 0x00, 0xe0, 0x2e, 0x00, 0x00, 0xe0, 0x2e, 0x00, 0x00, 0xe0, 0x2e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1e, 0x41, 0x00, 0x00, 0x1e, 0x41, 0x00, 0x00, 0x1e, 0x41, 0x00, 0x00, 0x1e, 0x41, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x14, 0x88, 0x93};
  //ULONG aaa[17]; memcpy(aaa, c_data, 17*4);

  return bOnSend;
}

// Отправить команду на Ethernet-порт для установки текущих координат
BOOL CControllerEthernet::Send_Command_Ethernet_SetPos(LONG * aCurPos)
{
  if (!Ethernet_ON()) return FALSE; // Проверяем свободен ли порт Ethernet для отправки
  m_bPortReady  = FALSE;
  memset(m_btBuffer_Out, 0x00, N_BYTE_BUFFER);
  
  bool bOnSend;
  int nLen=0x1E;
  
  *(UCHAR*)(m_btBuffer_Out+2) = 9; //Номер команды
  
  for (int i = 0; i < 4; i++)
    *(LONG*) (m_btBuffer_Out + 4 + i*4)  = (LONG) aCurPos[i];
	  
  bOnSend = Send_Command_Ethernet(nLen);// Отправить команду на Ethernet-порт
  m_bPortReady  = TRUE;// FALSE;
  return bOnSend;;
}

// Отправить команду на Ethernet-порт для установки текущих координат определенной оси
BOOL CControllerEthernet::Send_Command_Ethernet_SetPosAxes(LONG nCurPos, int nAxes)
{
    if (!Ethernet_ON()) return FALSE; // Проверяем свободен ли порт Ethernet для отправки
    m_bPortReady  = FALSE;
    memset(m_btBuffer_Out, 0x00, N_BYTE_BUFFER);

    bool bOnSend;
    int i, nLen=0x1E;
    LONG *alCurPos;

    *(UCHAR*)(m_btBuffer_Out+2) = 9; //Номер команды

    if (m_nLenStatusPacket == N_STATUS_PACKET_LEN_PLC230)
        alCurPos = (LONG *) & m_pCurInfoCntlr_PLC230->Position_0;

    if (m_nLenStatusPacket == N_STATUS_PACKET_LEN_PLCM_E3)
        alCurPos = (LONG *) & m_pCurInfoCntlr_PLCM_E3->Position_0;
//    qDebug() << alCurPos;
    alCurPos[nAxes - 1] = nCurPos;
    for (i = 0; i < 4; i++)
      *(LONG*) (m_btBuffer_Out + 4 + i*4)  = (LONG) alCurPos[i];

    bOnSend = Send_Command_Ethernet(nLen);// Отправить команду на Ethernet-порт
    m_bPortReady  = TRUE;// FALSE;
    return bOnSend;
}

// Отправить команду на Ethernet-порт для запуска двигателей ручного управления int nSpeed - скорость перемещения (шаг/с), int nAxes - номер оси
BOOL CControllerEthernet::Send_Command_Ethernet_Jogging(int nSpeed, int nAxes)
{
  //if (g_NSizeByteP != N_BYTE_INFO_CRL) return FALSE;
  //while (g_NSizeByteP != N_BYTE_INFO_CRL) ;

  // Проверяем свободен ли порт Ethernet для отправки
  if (!Ethernet_ON())
      return FALSE;

  m_bPortReady  = FALSE;
  memset(m_btBuffer_Out, 0x00, N_BYTE_BUFFER);
  
  bool bOnSend;
  int nLen=0x0B;
  
  //qDebug() << nSpeed << nAxes;
  *(UCHAR*)(m_btBuffer_Out+2) = 3; //Номер команды
  //*(UCHAR*)(m_btBuffer_Out+3) = m_pCurInfoCntlr->Ncmd + 1; //Номер пакета отправляется в функции отправки
  *(ULONG*) (m_btBuffer_Out+4) = ULONG(nSpeed) ; //Частота 8000;//
  //ULONG a = *(ULONG*)(m_btBuffer_Out+4);
  *(SHORT*)(m_btBuffer_Out+8) = 1 << (nAxes -1); //0x1;//0xF;//Маска осей
  
  bOnSend = Send_Command_Ethernet(nLen);// Отправить команду на Ethernet-порт
  m_bPortReady  = TRUE;// FALSE;
  return bOnSend;;
}

// Отправить команду на Ethernet-порт для Поиск базы (homing) int nSpeed - скорость перемещения (шаг/с), int nPos - искомая позиция, int nAxes - номер оси
BOOL CControllerEthernet::Send_Command_Ethernet_Homing(int nSpeed, int nPos, int nAxes)
{//Работает не верно
  if (!Ethernet_ON()) return FALSE; // Проверяем свободен ли порт Ethernet для отправки
  m_bPortReady  = FALSE;

  memset(m_btBuffer_Out, 0x00, N_BYTE_BUFFER);
  
  bool bOnSend;
  int nLen=0x0F;
  
  *(UCHAR*)(m_btBuffer_Out+2) = 4; //Номер команды
  //*(UCHAR*)(m_btBuffer_Out+3) = m_pCurInfoCntlr->Ncmd + 1; //Номер пакета отправляется в функции отправки
  *(LONG*) (m_btBuffer_Out+4) = LONG(nSpeed) ; //Частота 8000;//
  *(LONG*) (m_btBuffer_Out+8) = LONG(nPos) ;    //Искомая позиция

  *(SHORT*)(m_btBuffer_Out+12) = 1 << (nAxes -1); //0x1;//0xF;//Маска осей
  
  bOnSend = Send_Command_Ethernet(nLen);// Отправить команду на Ethernet-порт
//  qDebug() << nSpeed << nPos << nAxes << bOnSend;
  m_bPortReady  = TRUE;// FALSE;
  return bOnSend;;
}

// Отправить команду на Ethernet-порт траекторные данные (Traj) int nSpeed - скорость перемещения (шаг/с), int nPos - искомая позиция, int nAxes - номер оси
BOOL CControllerEthernet::Send_Command_Ethernet_Traj(int nAxes)
{//Работает не верно
  if (!Ethernet_ON()) return FALSE; // Проверяем свободен ли порт Ethernet для отправки
  m_bPortReady  = FALSE;

  memset(m_btBuffer_Out, 0x00, N_BYTE_BUFFER);

  bool bOnSend;
  int n, a, N_traj = 5;
  int nLen = 0x0A + N_traj *6 *4;
  *(UCHAR*)(m_btBuffer_Out+2) = N_traj; //N_traj
  *(LONG*) (m_btBuffer_Out+5) = 0xFFF ; //Identier
  for (n=0; n<N_traj ; n++)
    for (a=1; a<7 ; a++)
    {
      if (a == nAxes)
      *(LONG*) (m_btBuffer_Out + 8 + a*4 + n*6*4) =  50 * 0x100000; //Шагов умноженое на 0x100000
      else *(LONG*) (m_btBuffer_Out + 8 + a*4 + n*6*4) = 0 ; //Частота 8000;//
  }
  bOnSend = Send_Command_Ethernet(nLen);// Отправить команду на Ethernet-порт
  //qDebug() << nSpeed << nPos << nAxes << bOnSend;
  m_bPortReady  = TRUE;// FALSE;
  return bOnSend;;
}



// Отправить команду на Ethernet-порт Stop; Если nAxes < 1 выключаем все оси
BOOL CControllerEthernet::Send_Command_Ethernet_Stop(bool bFullStop, int nAxes)
{
  if (!Ethernet_ON()) return FALSE; // Проверяем свободен ли порт Ethernet для отправки
  m_bPortReady  = FALSE;
  
  memset(m_btBuffer_Out, 0x00, N_BYTE_BUFFER);
  
  bool bOnSend;
  int nLen;
  if (bFullStop) {
    nLen=0x06;
    *(UCHAR*)(m_btBuffer_Out+2) = 6; //Номер команды
  } else {
    nLen=0x07;
    *(UCHAR*)(m_btBuffer_Out+2) = 5; //Номер команды
  }
  //*(UCHAR*)(m_btBuffer_Out+3) = m_pCurInfoCntlr->Ncmd + 1; //Номер пакета
  if (nAxes > 0)
        *(SHORT*)(m_btBuffer_Out+4) = 1 << (nAxes -1); //0xF; Маска осей
  else  *(SHORT*)(m_btBuffer_Out+4) = 0xF; //0xF; Маска осей
  
  bOnSend = Send_Command_Ethernet(nLen);// Отправить команду на Ethernet-порт
  m_bPortReady  = TRUE;// FALSE;

  return bOnSend;;
}

// Отправить команду на Ethernet-порт инициализации портов и поддержания работы контроллера (1 раз в 5сек)
BOOL CControllerEthernet::Send_Command_Ethernet_ON_Port()
{
  if (!Ethernet_ON()) return FALSE; // Проверяем свободен ли порт Ethernet для отправки
  m_bPortReady  = FALSE;
   
  memset(m_btBuffer_Out, 0x00, N_BYTE_BUFFER);
  
  bool bOnSend;
  int nLen=0x17;

  *(UCHAR*)(m_btBuffer_Out+ 2) = 0; //Номер команды
  *(LONG*) (m_btBuffer_Out+ 4) = m_anSM_Ports_Out[0]; //Port 1
  *(LONG*) (m_btBuffer_Out+ 8) = m_anSM_Ports_Out[1]; //Port 2
  *(LONG*) (m_btBuffer_Out+12) = m_anSM_Ports_Out[2]; //Port 3
  *(LONG*) (m_btBuffer_Out+16) = 0;  //SpindleVelocity скорость вращения вала шпинделя, выраженная в шагах/с;
  *(SHORT*)(m_btBuffer_Out+20) = 10; //PWM необходимая скважность импульсов на выходе ШИМ в процентах.

  bOnSend = Send_Command_Ethernet(nLen);// Отправить команду на Ethernet-порт
  m_bPortReady  = TRUE;// FALSE;
  return bOnSend;;
}

//Инициализация конроллера ШД
BOOL CControllerEthernet::Initialize_StepMotor()
{	//Настройки по умолчанию
  for (int i = 0; i < N_MAX_AXIS; i++){
    m_anSM_Speed_start[i] = 1000;   //Начальная скорость ШД
    m_anSM_Speed_max[i]   = 15000;  //Макимальная скорость ШД
    m_anSM_Acceler[i]     = 15000;  //Ускорение ШД
  }

  return TRUE;

}

// Выводим статус пакет в текст
QString CControllerEthernet::GetStrStatus()
{
    QString * sFieldInf;
    QString sFieldInf_PLCM_E3[] =
    {"NByte",	// 00-длина пакета в байтах.78(0x004e)--было 86(0x0056)
    "NCmd" ,	// 02-номер последнего выполненного пакета;
    "Job"  ,	// 03-текущее состояние контроллера:0 готов к работе;1 идет выполнение траекторных данных;2 состояни аварии;3 выполнение траектории приостановлено.
    "Flags",	// 04-некоторые битовые флаги: Err признак ошибки; Act X единица, если контроллер в состоянии Idle и ось с данным номером находится в движении.
    "ErrCN",	// 05-если был установлен бит ошибки в поле Flags, данное число указывает на номер команды, вызвавшей ошибку;
    "ErrCo",	// 06-код ошибки если был установлен бит ошибки в поле Flags:0 неизвестная команда;1 выполнение команды невозможно в данном состоянии;2 траекторный буфер переполнен;3 неверный параметр команды;4 достигнуты программные пределы перемещений;5 достигнуты аппаратные пределы перемещений (сработал концевой датчик) ;6 сработал внешний датчик аварии (Estop) ;7 ошибка связи. Обычно возникает при длительном отсутсвии пакетов от ПО;8 внутренняя авария;9 переход в аварию в связи с получением соответствующей команды от ПО.
    "Refer",	// 07-битовое поле, где каждый бит сообщает о том, что по соответсвующей оси завершен поиск базы;
    "AxisJ",	// 08-младшие 18 бит слова представляют собой состояние каждой оси (по три бита на ось, младшая тройка ось 0) . Так же как и для битов Act поля Flags, данные поля имеют значение только при состоянии контроллера Idle. Может принимать одно из значений: 0 ось не перемещается; 1 выполняется ручное перемещение (jogging) ; 2 выполняется поиск базы (homing) ; 3 выполняется поиск материала (probing) ; Старшие 14 битов не используются.
    "Pos_0",	// 12-текущее положение каждой оси 0 в шагах;
    "Pos_1",	// 16-текущее положение каждой оси 1 в шагах;
    "Pos_2",	// 20-текущее положение каждой оси 2 в шагах;
    "Pos_3",	// 24-текущее положение каждой оси 3 в шагах;
    "Pos_4",	// 28-текущее положение каждой оси 4 в шагах;
    "Pos_5",	// 32-текущее положение каждой оси 5 в шагах;
    "CurTr",	// 36-ID элемента траектории, выполняемого в данный момент
    "Tr_cn",    // 40-количество элементов траектории в буфере контроллера;
    "Sys_t",	// 44-время в миллисекундах, прошедшее с момента подачи питания на контроллер;
    "Firmw",    // 48-версия прошивки;
    "Port1",	// 52-текущее состояние входов/выходов соответствующего порта 1;
    "Port2",	// 56-текущее состояние входов/выходов соответствующего порта 2;
    "Port3",	// 60-текущее состояние входов/выходов соответствующего порта 3;
    "THC_c",	// 64-значение на которое скорректировано положение оси 2 в данный момент, т. е. текущее отклонение оси 2 от заданной величины;
    "THC_V",    // 68-текущее напряжение в дуге плазмореза, полученное от устройства контроля высоты резака; Из UInt16 стал UInt32
    "THC_R",    // 72-2 байта (72-73) - THC RawData. UInt16
    "THC_F",	// 74-2 байта (74-75) - THC Flags. UInt16
    "ADC"  ,	// 12-битовое значение измеренное на входе АЦП PLCM-E3; UInt32
    "MPG_P",	// текущее положение энкодера, подключенного к соответствующему входу PLCM-E3; UInt32
    "MPG_V",	// 76-текущая скорость энкодера, подключенного к соответствующему входу PLCM-E3;
    "CRC16"     // 80-2 байта контрольной суммы.
};
    QString sFieldInf_PLC230[] =
    {"NByte",	// 00-длина пакета в байтах.78(0x004e)--было 86(0x0056)
    "NCmd" ,	// 02-номер последнего выполненного пакета;
    "Job"  ,	// 03-текущее состояние контроллера:0 готов к работе;1 идет выполнение траекторных данных;2 состояни аварии;3 выполнение траектории приостановлено.
    "Flags",	// 04-некоторые битовые флаги: Err признак ошибки; Act X единица, если контроллер в состоянии Idle и ось с данным номером находится в движении.
    "ErrCN",	// 05-если был установлен бит ошибки в поле Flags, данное число указывает на номер команды, вызвавшей ошибку;
    "ErrCo",	// 06-код ошибки если был установлен бит ошибки в поле Flags:0 неизвестная команда;1 выполнение команды невозможно в данном состоянии;2 траекторный буфер переполнен;3 неверный параметр команды;4 достигнуты программные пределы перемещений;5 достигнуты аппаратные пределы перемещений (сработал концевой датчик) ;6 сработал внешний датчик аварии (Estop) ;7 ошибка связи. Обычно возникает при длительном отсутсвии пакетов от ПО;8 внутренняя авария;9 переход в аварию в связи с получением соответствующей команды от ПО.
    "Refer",	// 07-битовое поле, где каждый бит сообщает о том, что по соответсвующей оси завершен поиск базы;
    "AxisJ",	// 08-младшие 18 бит слова представляют собой состояние каждой оси (по три бита на ось, младшая тройка ось 0) . Так же как и для битов Act поля Flags, данные поля имеют значение только при состоянии контроллера Idle. Может принимать одно из значений: 0 ось не перемещается; 1 выполняется ручное перемещение (jogging) ; 2 выполняется поиск базы (homing) ; 3 выполняется поиск материала (probing) ; Старшие 14 битов не используются.
    "Pos_0",	// 12-текущее положение каждой оси 0 в шагах;
    "Pos_1",	// 16-текущее положение каждой оси 1 в шагах;
    "Pos_2",	// 20-текущее положение каждой оси 2 в шагах;
    "Pos_3",	// 24-текущее положение каждой оси 3 в шагах;
    "Pos_4",	// 28-текущее положение каждой оси 4 в шагах;
    "Pos_5",	// 32-текущее положение каждой оси 5 в шагах;
    "CurTr",	// 36-ID элемента траектории, выполняемого в данный момент
    "Tr_cn",    // 40-количество элементов траектории в буфере контроллера;
    "Sys_t",	// 44-время в миллисекундах, прошедшее с момента подачи питания на контроллер;
    "Firmw",    // 48-версия прошивки;
    "Port1",	// 52-текущее состояние входов/выходов соответствующего порта 1;
    "Port2",	// 56-текущее состояние входов/выходов соответствующего порта 2;
    "Port3",	// 60-текущее состояние входов/выходов соответствующего порта 3;
    "THC_c",	// 64-значение на которое скорректировано положение оси 2 в данный момент, т. е. текущее отклонение оси 2 от заданной величины;
    "THC_V",    // 68-текущее напряжение в дуге плазмореза, полученное от устройства контроля высоты резака; Из UInt16 стал UInt32
    "THC_R",    // 72-2 байта (72-73) - THC RawData. UInt16
    "THC_F",	// 74-2 байта (74-75) - THC Flags. UInt16
//    "ADC"  ,	// 12-битовое значение измеренное на входе АЦП PLCM-E3; UInt32
//    "MPG_P",	// текущее положение энкодера, подключенного к соответствующему входу PLCM-E3; UInt32
//    "MPG_V",	// 76-текущая скорость энкодера, подключенного к соответствующему входу PLCM-E3;
    "CRC16"     // 80-2 байта контрольной суммы.
};
    int n_FieldInf, i;
    QString sText="", sTmp = "%1 \r\n";
    if (m_nLenStatusPacket == N_STATUS_PACKET_LEN_PLCM_E3)
    {   n_FieldInf = 30;
        sFieldInf = sFieldInf_PLCM_E3;
        for (i=1; i < n_FieldInf; i++) sText=sText + sFieldInf[i-1] + "=%" + sTmp.arg(i);
        sTmp=sText.arg(m_pCurInfoCntlr_PLCM_E3->inf_NBYTE)
             .arg(m_pCurInfoCntlr_PLCM_E3->Ncmd)
             .arg(m_pCurInfoCntlr_PLCM_E3->Job)
             .arg(m_pCurInfoCntlr_PLCM_E3->Flags)
             .arg(m_pCurInfoCntlr_PLCM_E3->ErrCmdNum)
             .arg(m_pCurInfoCntlr_PLCM_E3->ErrCode)
             .arg(m_pCurInfoCntlr_PLCM_E3->Referenced)
             .arg(m_pCurInfoCntlr_PLCM_E3->AxisJobs)
             .arg(m_pCurInfoCntlr_PLCM_E3->Position_0)
             .arg(m_pCurInfoCntlr_PLCM_E3->Position_1)
             .arg(m_pCurInfoCntlr_PLCM_E3->Position_2)
             .arg(m_pCurInfoCntlr_PLCM_E3->Position_3)
             .arg(m_pCurInfoCntlr_PLCM_E3->Position_4)
             .arg(m_pCurInfoCntlr_PLCM_E3->Position_5)
             .arg(m_pCurInfoCntlr_PLCM_E3->Cur_traject)
             .arg(m_pCurInfoCntlr_PLCM_E3->Traject_coun)
             .arg(m_pCurInfoCntlr_PLCM_E3->System_time)
             .arg(m_pCurInfoCntlr_PLCM_E3->Firmware_ver)
             .arg(m_pCurInfoCntlr_PLCM_E3->Port_1)
             .arg(m_pCurInfoCntlr_PLCM_E3->Port_2)
             .arg(m_pCurInfoCntlr_PLCM_E3->Port_3)
             .arg(m_pCurInfoCntlr_PLCM_E3->THC_correct)
             .arg(m_pCurInfoCntlr_PLCM_E3->THC_Voltage)
             .arg(m_pCurInfoCntlr_PLCM_E3->THC_RawData)
             .arg(m_pCurInfoCntlr_PLCM_E3->THC_Flags)
             .arg(m_pCurInfoCntlr_PLCM_E3->ADCVal)
             .arg(m_pCurInfoCntlr_PLCM_E3->MPGPos)
             .arg(m_pCurInfoCntlr_PLCM_E3->MPGVel)
             .arg(m_pCurInfoCntlr_PLCM_E3->CRC_16);
    }
    if (m_nLenStatusPacket == N_STATUS_PACKET_LEN_PLC230 )
    {   n_FieldInf = 27;
        sFieldInf=sFieldInf_PLC230;
        for (i=1; i < n_FieldInf; i++) sText=sText + sFieldInf[i-1] + "=%" + sTmp.arg(i);
        sTmp=sText.arg(m_pCurInfoCntlr_PLC230->inf_NBYTE)
             .arg(m_pCurInfoCntlr_PLC230->Ncmd)
             .arg(m_pCurInfoCntlr_PLC230->Job)
             .arg(m_pCurInfoCntlr_PLC230->Flags)
             .arg(m_pCurInfoCntlr_PLC230->ErrCmdNum)
             .arg(m_pCurInfoCntlr_PLC230->ErrCode)
             .arg(m_pCurInfoCntlr_PLC230->Referenced)
             .arg(m_pCurInfoCntlr_PLC230->AxisJobs)
             .arg(m_pCurInfoCntlr_PLC230->Position_0)
             .arg(m_pCurInfoCntlr_PLC230->Position_1)
             .arg(m_pCurInfoCntlr_PLC230->Position_2)
             .arg(m_pCurInfoCntlr_PLC230->Position_3)
             .arg(m_pCurInfoCntlr_PLC230->Position_4)
             .arg(m_pCurInfoCntlr_PLC230->Position_5)
             .arg(m_pCurInfoCntlr_PLC230->Cur_traject)
             .arg(m_pCurInfoCntlr_PLC230->Traject_coun)
             .arg(m_pCurInfoCntlr_PLC230->System_time)
             .arg(m_pCurInfoCntlr_PLC230->Firmware_ver)
             .arg(m_pCurInfoCntlr_PLC230->Port_1)
             .arg(m_pCurInfoCntlr_PLC230->Port_2)
             .arg(m_pCurInfoCntlr_PLC230->Port_3)
             .arg(m_pCurInfoCntlr_PLC230->THC_correct)
             .arg(m_pCurInfoCntlr_PLC230->THC_Voltage)
             .arg(m_pCurInfoCntlr_PLC230->THC_RawData)
             .arg(m_pCurInfoCntlr_PLC230->THC_Flags)
//             .arg(m_pCurInfoCntlr_PLC230->ADCVal)
//             .arg(m_pCurInfoCntlr_PLC230->MPGPos)
//             .arg(m_pCurInfoCntlr_PLC230->MPGVel)
             .arg(m_pCurInfoCntlr_PLC230->CRC_16);
    }
//    qDebug() << m_nLenStatusPacket << sTmp;
    return sTmp;
}

// освобождаем порт
void CControllerEthernet::Terminate_Port()
{
// qDebug() << "CControllerEthernet::Terminate_Port() -> " << m_ConnectSocket;
    if (m_ConnectSocket >= 0) {
        Send_Command_Ethernet_Stop(TRUE, 0); //Если nAxes < 1 выключаем все оси
//        Send_Command_Ethernet_Stop(TRUE, 2);
//        Send_Command_Ethernet_Stop(TRUE, 3);
//        Send_Command_Ethernet_Stop(TRUE, 4);
        m_bOnThread = false;
        //terminate();
        qDebug() << "CControllerEthernet::Terminate_Port() -> " << m_sCNLR_IP + "(" + m_sCNLR_Name + ")";
        //if(g_hThread_InfoCntlr) { TerminateThread(g_hThread_InfoCntlr,0); CloseHandle(g_hThread_InfoCntlr);}
        closesocket(m_ConnectSocket );
//        qDebug() << "CControllerEthernet: test!";
        WSACleanup();
        m_ConnectSocket = -1;
        //quit();             // Завершение потока
        terminate();        //
        wait(250);          // Ожидание корректного завершения
   }
}

BOOL CControllerEthernet::SM_Forvard(QString &sTextRead_out, float fKSpeed, int nAxis)
{
    int iResult;
    //qDebug() << m_sCNLR_IP << m_sCNLR_Name << fKSpeed << nAxis << int(m_anSM_Speed_max[nAxis - 1]);

    // Отправить команду на Ethernet-порт для запуска двигателей ручного управления
    iResult=Send_Command_Ethernet_Jogging(int(m_anSM_Speed_max[nAxis - 1] * fKSpeed), nAxis);
    sTextRead_out=GetStrStatus();// Выводим статус пакет в текст
    //m_fSM_KSpeed_old = m_fSM_KSpeed_cur;
    return iResult;
}

BOOL CControllerEthernet::SM_Reverse(QString &sTextRead_out, float fKSpeed, int nAxis)
{
    int iResult;
    //CString aCommand[20];
    iResult=Send_Command_Ethernet_Jogging(int(m_anSM_Speed_max[nAxis - 1] * fKSpeed), nAxis); // Отправить команду на Ethernet-порт для запуска двигателей ручного управления
    //iResult=Send_Command_Ethernet_Homing(int(m_anSM_Speed_max[nAxis - 1] * -fKSpeed), 240000, nAxis);
    //iResult=Send_Command_Ethernet_Traj(int(m_anSM_Speed_max[nAxis - 1] * fKSpeed), int(m_anSM_Speed_max[nAxis - 1] * fKSpeed), 1000, nAxis);
    sTextRead_out=GetStrStatus();// Выводим статус пакет в текст
    //m_fSM_KSpeed_old = m_fSM_KSpeed_cur;
        return iResult;
}

BOOL CControllerEthernet::SM_Stop(QString & sTextRead_out, int nAxis)
{
    int iResult;
    iResult=Send_Command_Ethernet_Stop(FALSE, nAxis); // Отправить команду на Ethernet-порт Stop
    sTextRead_out=GetStrStatus();// Выводим статус пакет в текст
    return iResult;

}

//  Количество шагов до остановки двигателя (с учетом начальной скорости, целевой скорости, ускорения и времени сработки таймера)
double CControllerEthernet::SM_StepToNextSpeed(double speed_beg, double speed_next, double acceler, double timer_ms)
{
    // Расчет времени до остановки
    double t2next = (speed_beg - speed_next) / acceler;
    // Расчет шагов до остановки
    double step2next = acceler * t2next * t2next / 2.0;
    // Добавочное значение ошибки шагов, из-за задержки времени таймера
    double err_time_step = timer_ms * abs(speed_beg - speed_next) / (1000.0 * 0.88);//* 2)* 0.26);//0.88);
//    qDebug() << speed_beg <<speed_next << acceler << timer_ms;
//    qDebug()<<"err_time_step = " << err_time_step << "step2next = " << step2next;
    return step2next + err_time_step;
}


////****************************************************************************************
////CRC Tables
////****************************************************************************************
////****************************************************************************************
////****************************************************************************************
////****************************************************************************************
////CRC Tables
//BYTE auchCRCHi_exp[] = {
//   0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81,
//   0x40, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0,
//   0x80, 0x41, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x00, 0xc1, 0x81, 0x40, 0x01,
//   0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x01, 0xc0, 0x80, 0x41,
//   0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x00, 0xc1, 0x81,
//   0x40, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x01, 0xc0,
//   0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x01,
//   0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81, 0x40,
//   0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81,
//   0x40, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0,
//   0x80, 0x41, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x00, 0xc1, 0x81, 0x40, 0x01,
//   0xc0, 0x80, 0x41, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41,
//   0x00, 0xc1, 0x81, 0x40, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81,
//   0x40, 0x01, 0xc0, 0x80, 0x41, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0,
//   0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x01,
//   0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81, 0x40, 0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41,
//   0x00, 0xc1, 0x81, 0x40, 0x01, 0xc0, 0x80, 0x41, 0x01, 0xc0, 0x80, 0x41, 0x00, 0xc1, 0x81,
//   0x40
//} ;

//BYTE auchCRCLo_exp[] = {
//   0x00, 0xc0, 0xc1, 0x01, 0xc3, 0x03, 0x02, 0xc2, 0xc6, 0x06, 0x07, 0xc7, 0x05, 0xc5, 0xc4,
//   0x04, 0xcc, 0x0c, 0x0d, 0xcd, 0x0f, 0xcf, 0xce, 0x0e, 0x0a, 0xca, 0xcb, 0x0b, 0xc9, 0x09,
//   0x08, 0xc8, 0xd8, 0x18, 0x19, 0xd9, 0x1b, 0xdb, 0xda, 0x1a, 0x1e, 0xde, 0xdf, 0x1f, 0xdd,
//   0x1d, 0x1c, 0xdc, 0x14, 0xd4, 0xd5, 0x15, 0xd7, 0x17, 0x16, 0xd6, 0xd2, 0x12, 0x13, 0xd3,
//   0x11, 0xd1, 0xd0, 0x10, 0xf0, 0x30, 0x31, 0xf1, 0x33, 0xf3, 0xf2, 0x32, 0x36, 0xf6, 0xf7,
//   0x37, 0xf5, 0x35, 0x34, 0xf4, 0x3c, 0xfc, 0xfd, 0x3d, 0xff, 0x3f, 0x3e, 0xfe, 0xfa, 0x3a,
//   0x3b, 0xfb, 0x39, 0xf9, 0xf8, 0x38, 0x28, 0xe8, 0xe9, 0x29, 0xeb, 0x2b, 0x2a, 0xea, 0xee,
//   0x2e, 0x2f, 0xef, 0x2d, 0xed, 0xec, 0x2c, 0xe4, 0x24, 0x25, 0xe5, 0x27, 0xe7, 0xe6, 0x26,
//   0x22, 0xe2, 0xe3, 0x23, 0xe1, 0x21, 0x20, 0xe0, 0xa0, 0x60, 0x61, 0xa1, 0x63, 0xa3, 0xa2,
//   0x62, 0x66, 0xa6, 0xa7, 0x67, 0xa5, 0x65, 0x64, 0xa4, 0x6c, 0xac, 0xad, 0x6d, 0xaf, 0x6f,
//   0x6e, 0xae, 0xaa, 0x6a, 0x6b, 0xab, 0x69, 0xa9, 0xa8, 0x68, 0x78, 0xb8, 0xb9, 0x79, 0xbb,
//   0x7b, 0x7a, 0xba, 0xbe, 0x7e, 0x7f, 0xbf, 0x7d, 0xbd, 0xbc, 0x7c, 0xb4, 0x74, 0x75, 0xb5,
//   0x77, 0xb7, 0xb6, 0x76, 0x72, 0xb2, 0xb3, 0x73, 0xb1, 0x71, 0x70, 0xb0, 0x50, 0x90, 0x91,
//   0x51, 0x93, 0x53, 0x52, 0x92, 0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54, 0x9c, 0x5c,
//   0x5d, 0x9d, 0x5f, 0x9f, 0x9e, 0x5e, 0x5a, 0x9a, 0x9b, 0x5b, 0x99, 0x59, 0x58, 0x98, 0x88,
//   0x48, 0x49, 0x89, 0x4b, 0x8b, 0x8a, 0x4a, 0x4e, 0x8e, 0x8f, 0x4f, 0x8d, 0x4d, 0x4c, 0x8c,
//   0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 0x43, 0x83, 0x41, 0x81, 0x80,
//   0x40
//} ;
////---------------------------------------------------------------------------
//    union CRC

//    {
//        WORD  CRC_W;
//        BYTE  CRC_B[2];
//    };
///*============================================================================*/

//// Procedure to check the checksum of the message
//WORD CRC16 ( BYTE *chkbuf, BYTE len )
//{

//   BYTE    uchCRCHi = 0xff ;
//   BYTE    uchCRCLo = 0xff ;
//   WORD  uIndex;
//   WORD  temp_code;
//   while ( len )
//      {
//      uIndex = (unsigned short) ( uchCRCHi ^ *chkbuf++ ) ;

//      uchCRCHi = (BYTE) ( uchCRCLo ^ auchCRCHi_exp[uIndex] ) ;
//      uchCRCLo = auchCRCLo_exp[uIndex];
//      len-- ;
//      }
//   temp_code = (unsigned  short) uchCRCHi;
//   temp_code = (unsigned short) (temp_code << 8);
//   temp_code |=uchCRCLo;
//   return temp_code;
//}

unsigned short crc16_table[256] = {
    0x0000, 0x1189, 0x2312, 0x329b, 0x4624, 0x57ad, 0x6536, 0x74bf,
    0x8c48, 0x9dc1, 0xaf5a, 0xbed3, 0xca6c, 0xdbe5, 0xe97e, 0xf8f7,
    0x1081, 0x0108, 0x3393, 0x221a, 0x56a5, 0x472c, 0x75b7, 0x643e,
    0x9cc9, 0x8d40, 0xbfdb, 0xae52, 0xdaed, 0xcb64, 0xf9ff, 0xe876,
    0x2102, 0x308b, 0x0210, 0x1399, 0x6726, 0x76af, 0x4434, 0x55bd,
    0xad4a, 0xbcc3, 0x8e58, 0x9fd1, 0xeb6e, 0xfae7, 0xc87c, 0xd9f5,
    0x3183, 0x200a, 0x1291, 0x0318, 0x77a7, 0x662e, 0x54b5, 0x453c,
    0xbdcb, 0xac42, 0x9ed9, 0x8f50, 0xfbef, 0xea66, 0xd8fd, 0xc974,
    0x4204, 0x538d, 0x6116, 0x709f, 0x0420, 0x15a9, 0x2732, 0x36bb,
    0xce4c, 0xdfc5, 0xed5e, 0xfcd7, 0x8868, 0x99e1, 0xab7a, 0xbaf3,
    0x5285, 0x430c, 0x7197, 0x601e, 0x14a1, 0x0528, 0x37b3, 0x263a,
    0xdecd, 0xcf44, 0xfddf, 0xec56, 0x98e9, 0x8960, 0xbbfb, 0xaa72,
    0x6306, 0x728f, 0x4014, 0x519d, 0x2522, 0x34ab, 0x0630, 0x17b9,
    0xef4e, 0xfec7, 0xcc5c, 0xddd5, 0xa96a, 0xb8e3, 0x8a78, 0x9bf1,
    0x7387, 0x620e, 0x5095, 0x411c, 0x35a3, 0x242a, 0x16b1, 0x0738,
    0xffcf, 0xee46, 0xdcdd, 0xcd54, 0xb9eb, 0xa862, 0x9af9, 0x8b70,
    0x8408, 0x9581, 0xa71a, 0xb693, 0xc22c, 0xd3a5, 0xe13e, 0xf0b7,
    0x0840, 0x19c9, 0x2b52, 0x3adb, 0x4e64, 0x5fed, 0x6d76, 0x7cff,
    0x9489, 0x8500, 0xb79b, 0xa612, 0xd2ad, 0xc324, 0xf1bf, 0xe036,
    0x18c1, 0x0948, 0x3bd3, 0x2a5a, 0x5ee5, 0x4f6c, 0x7df7, 0x6c7e,
    0xa50a, 0xb483, 0x8618, 0x9791, 0xe32e, 0xf2a7, 0xc03c, 0xd1b5,
    0x2942, 0x38cb, 0x0a50, 0x1bd9, 0x6f66, 0x7eef, 0x4c74, 0x5dfd,
    0xb58b, 0xa402, 0x9699, 0x8710, 0xf3af, 0xe226, 0xd0bd, 0xc134,
    0x39c3, 0x284a, 0x1ad1, 0x0b58, 0x7fe7, 0x6e6e, 0x5cf5, 0x4d7c,
    0xc60c, 0xd785, 0xe51e, 0xf497, 0x8028, 0x91a1, 0xa33a, 0xb2b3,
    0x4a44, 0x5bcd, 0x6956, 0x78df, 0x0c60, 0x1de9, 0x2f72, 0x3efb,
    0xd68d, 0xc704, 0xf59f, 0xe416, 0x90a9, 0x8120, 0xb3bb, 0xa232,
    0x5ac5, 0x4b4c, 0x79d7, 0x685e, 0x1ce1, 0x0d68, 0x3ff3, 0x2e7a,
    0xe70e, 0xf687, 0xc41c, 0xd595, 0xa12a, 0xb0a3, 0x8238, 0x93b1,
    0x6b46, 0x7acf, 0x4854, 0x59dd, 0x2d62, 0x3ceb, 0x0e70, 0x1ff9,
    0xf78f, 0xe606, 0xd49d, 0xc514, 0xb1ab, 0xa022, 0x92b9, 0x8330,
    0x7bc7, 0x6a4e, 0x58d5, 0x495c, 0x3de3, 0x2c6a, 0x1ef1, 0x0f78
};


USHORT fast_crc_blk ( const BYTE *blk_ptr ,  USHORT blk_len)
{
    USHORT crc = 0xFFFF;
    while ( blk_len --)
        crc = crc16_table[(crc ^ *blk_ptr++) & 0xFFL]^(crc >> 8);
    return crc;
}
      
//USHORT CalculateCRC16( char *c_ptr, USHORT len)
//{
//    const char *c = c_ptr;
//    char ch;
//    unsigned short crc = 0xFFFF;
//    unsigned short tmp, short_c;
//    while (len--){
//        ch=*c++;
//        short_c  = 0x00ff & (unsigned short) ch;
//        tmp = (crc >> 8) ^ short_c;
//        crc = (crc << 8) ^ crc16_table[tmp];

//    }
//    return crc;
//}


//static void init_crcccitt_tab( void ) {

//    int i, j;
//    unsigned short crc, c;
//    for (i=0; i<256; i++) {
//        crc = 0;
//        c   = ((unsigned short) i) << 8;
//        for (j=0; j<8; j++) {
//           if ( (crc ^ c) & 0x8000 ) crc = ( crc << 1 ) ^ P_CCITT;
//            else                      crc =   crc << 1;
//             c = c << 1;
//        }
//        crc16_table[i] = crc;

//    }
///*	CStdioFile fileCFG;
//    CString sMsg,sFN_CFG="CRC16_my.txt";
//    if (fileCFG.Open(sFN_CFG,CFile::modeCreate|CFile::modeWrite|CFile::modeRead) )
//    {
//     for (i=0; i<32; i++) {
//        for (j=0; j<8; j++) {
//         sMsg.Format("0x%4.4X, ", crc16_table[i*8 + j]);
//         fileCFG.WriteString(sMsg);
//        }
//        fileCFG.WriteString("\n");
//     }
//     fileCFG.Close();
//    }
//*/
//}  /* init_crcccitt_tab */
