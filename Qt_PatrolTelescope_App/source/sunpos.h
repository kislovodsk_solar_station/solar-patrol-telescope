// This file is available in electronic form at http://www.psa.es/sdg/sunpos.htm

#ifndef __SUNPOS_H
#define __SUNPOS_H

#include <QDateTime>

// Declaration of some constants 
#define pi_my    3.14159265358979323846
#define twopi (2*pi_my)
#define rad   (pi_my/180)
#define dEarthMeanRadius     6371.01	// In km
#define dAstronomicalUnit    149597890	// In km

//Ширта наблюдения (N+)
#define LOC_LATITUDE  43.7413556

//Долгота наблюдения (E+)
#define LOC_LONGITUDE 42.662725

struct cTime
{
	int iYear;
	int iMonth;
	int iDay;
	double dHours;
	double dMinutes;
	double dSeconds;
};

struct cAngle
{
	char cZnak;
	int iDeg;
	int iMinut;
	float fSec;
};


struct cLocation
{
	double dLongitude;
	double dLatitude;
};

struct cSunCoordinates
{
	double m_dZenithAngle;
	double m_dAzimuth;
	double m_dHourAngle;
	double m_dDeclination;

};

void sunpos(QDateTime udtTime, cLocation udtLocation, cSunCoordinates *udtSunCoordinates);
cSunCoordinates funSunPos_curLoc_tSys(QDateTime udtTime, int nTimeHourLoc2UT); //Координаты Солнца для одного положения по времени SYSTEMTIME и разнице UT

cAngle funRadian2Angle(double dRadian);//Перевод радиан в угол(грд, мин, сек)
double funAngle2Radian(cAngle Angle); //Перевод угола(грд, мин, сек) в радианы

QString funRAngle2String(double dAngle_radian); //Вывод радианы в символьный вид (грд, мин, сек)

QString funRAngle2String_Minut(double dAngle_radian); //Вывод радианы в символьный вид (мин, сек). Без градусов - для малых углов.

QDateTime funGetTime_SunSet(QDateTime udtTimeBeg, int nTimeHourLoc2UT); //Время захода Солнца для одного положения по дате и времени начала и разнице UT

#endif
