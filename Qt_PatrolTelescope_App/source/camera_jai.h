//-------------------------------------------------------------------------------------------------
//  Класс для работы с ethernet-камерами (GigE interface) на основе библиотеки JAI SDK.
//  Разработка: Чернов Ярослав
//-------------------------------------------------------------------------------------------------
//  Алгоритм работы с данным классом камеры.
//-------------------------------------------------------------------------------------------------
//  1. Поиск и открытие камеры, используя метод Open().
//  2. Установка настроек камеры, используя метод SetParams().
//  3. Запуск камеры, метод Start(), в этом методе создается поток камеры
//          этот поток работает в методе StreamCBFunc(J_tIMAGE_INFO *pAqImageInfo).
//  4. В этом методе данные копируются в структуру rawImageInfo
//                                  и запускается метод CreateImageBuf().
//  5. В методе CreateImageBuf() сырые данные конвертируются в изображение
//                      и записываются в структуру convertImageInfo.
//  6. Указатель на буфер с изображением из этой структуры передается в указатель imageBuf,
//          подается сигнал с ссылкой на imageBuf, который можно принять.
//  7. Останавливается работа камеры вызовом метода Stop().
//  8. Полностью закрывается вызовом метода Close().
//-------------------------------------------------------------------------------------------------
#ifndef CAMERA_JAI_H
#define CAMERA_JAI_H
#include "camera.h"
#include "jai_factory.h"
#include <QVector>
//------------------------------------------------------------------
// Здесь задаются ключевые слова для изменения параметров камеры
//
#define NODE_NAME_WIDTH             "Width"
#define NODE_NAME_HEIGHT            "Height"
#define NODE_NAME_PIXELFORMAT       "PixelFormat"
#define NODE_NAME_GAIN              "Gain"
#define NODE_NAME_ACQSTART          "AcquisitionStart"
#define NODE_NAME_ACQSTOP           "AcquisitionStop"
#define NODE_NAME_EXPOSURE          "ExposureTime"
#define NODE_NAME_EXPOSURE_MODE     "ExposureMode"
#define NODE_NAME_EXPOSURE_AUTO     "ExposureAuto"
#define NODE_NAME_BINNING_X         "BinningHorizontal"
#define NODE_NAME_BINNING_Y         "BinningVertical"
#define NODE_NAME_OFFSET_X          "OffsetX"
#define NODE_NAME_OFFSET_Y          "OffsetY"
#define NODE_NAME_FPS               "AcquisitionFrameRate"
#define NODE_VALUE_OFF              "Off"
#define NODE_VALUE_CONTINUOUS       "Continuous"
#define NODE_VALUE_EXPOSURE_MODE    "Timed"
#define NODE_VALUE_MONO8            "Mono8"
#define NODE_VALUE_MONO12           "Mono12"
#define NODE_VALUE_MONO10           "Mono10"
#define NODE_VALUE_MONO12_PACKED    "Mono12Packed"
//------------------------------------------------------------------
// Здесь задаются значения параметров камеры по умолчанию
//
#define DEFAULT_WIDTH               2560
#define DEFAULT_HEIGHT              2048
#define DEFAULT_EXPOSURE            5000
#define DEFAULT_BINNING_X           2
#define DEFAULT_BINNING_Y           2
#define DEFAULT_OFFSET_X            0
#define DEFAULT_OFFSET_Y            0
#define DEFAULT_FPS                 4
#define DEFAULT_GAIN                1
#define DEFAULT_BITS_PER_PIXEL      16

class Camera_JAI : public Camera
{
    Q_OBJECT

public:
    explicit Camera_JAI(QObject* parent = 0);
    ~Camera_JAI();

    static FACTORY_HANDLE hFactory;     // Дескриптор библиотеки
    static int nOpenedCameras;          // Количество открытых камер JAI
    static bool8_t camerasFound;        // Флаг успешности получения списка камер
    static uint32_t nCameras;           // Количество обнаруженных камер
    static bool wait;                   // Флаг ожидания (для работы 2 камер)

public slots:
    bool Open();
    bool Start();
    bool Stop();
    bool Close();
    bool Reset();
    void Restart();
    int SaveImage();
    QImage GetImage();
    unsigned char* GetImageBuf();
    bool SetParams();
    void SetDefaultSettings();
    void SetSaveMode(bool saveMode);
    bool SetAutoExposure(bool autoMode);
    bool SetExposure(int val);
    int GetAverageIntensity();
    int GetExposure() const;
    int GetFrameWidth() const;
    int GetFrameHeight() const;
    int GetImageSize() const;

private:
    CAM_HANDLE hCamera;                         // Дескриптор камеры
    NODE_HANDLE     hNode;                      // Дескриптор списка параметров
    THRD_HANDLE     hThread;                    // Дескриптор потока
    J_tIMAGE_INFO returnImage_t;                // Структура с информацией о изображении с камеры
    J_tIMAGE_INFO* image_t;                     // Структура с информацией о изображении с камеры
    J_STATUS_TYPE   retval;                     // Возвращаемое значение
    SIZE            ViewSize;                   // Размеры кадра
    int8_t sCameraID[J_CAMERA_ID_SIZE];         // Идентификатор камеры
    int8_t sCameraInfo[J_CAMERA_INFO_SIZE];     // Производитель камеры
    uint32_t sizeID;                            // Размер идентификатора
    uint32_t sizeCameraInfo;                    // Размер производителя камеры
    bool bufReady;                              // Флаг готовности буфера с изображением с камеры
    bool isFirst;                               // Флаг первого полученного кадра с камеры
    QVector<int> meanIntensity;                 // Ряд средних интенсивностей по кадру
    ulong counter;
    char fileName[40];

    void StreamCBFunc(J_tIMAGE_INFO *pAqImageInfo);
    bool CreateImageBuf(J_tIMAGE_INFO *pAqImageInfo);
    void ConvertTo8Bit(J_tIMAGE_INFO *pAqImageInfo);
    bool SetParams(char* name, int value);
    bool SetParams(char* name, char *value);
    bool SetParams(char* name, double value);
    double GetParams(char* name) const;
    void SaveFitsImage();

signals:
    //void sigImage16BitGot(unsigned char* buf, int width, int height);
};

#endif // CAMERA_JAI_H
