#include "camera.h"
#include <QDebug>

Camera::Camera(QObject *parent) :
    QObject(parent),
    opened(false),
    started(false),
    saving(false),
    frameReady(false),
    scanWasStarted(false),
    dailyAngle(0)
{}

Camera::~Camera()
{
    qDebug() << "~Camera()";
}

void Camera::SetCameraModel(QString model)
{
    parameters.cameraName = model;
}

void Camera::isScanStart(bool b)
{
    scanWasStarted = b;
}

void Camera::SetDailyAngle(double angle)
{
    dailyAngle = angle;
}
