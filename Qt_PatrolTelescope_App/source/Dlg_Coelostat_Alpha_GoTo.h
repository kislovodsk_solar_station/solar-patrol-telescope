#ifndef Dlg_Coelostat_ALPHA_GOTO_H
#define Dlg_Coelostat_ALPHA_GOTO_H

#include <QDialog>
#include <QDateTime>

//Время парковки целостата
#define PARKING_HOUR 11


namespace Ui {
class Dlg_Coelostat_Alpha_GoTo;
}

class Dlg_Coelostat_Alpha_GoTo : public QDialog
{
    Q_OBJECT

public:
    QDateTime m_tDataTime_cur;
    QDateTime m_tDataTime_old;

    QDateTime m_dlg_tDataTime_SunSet;          //Время заката Солнца
    bool m_dlg_bool_ParkSunSet;                //Отслеживать положение захода Солнца

    bool m_On_Cur_Time; //Работаем в режиме поиска положения по текущему времени
    void SetDlgTime();  //Установка времени в диалоге по m_tSysPos

public:
    explicit Dlg_Coelostat_Alpha_GoTo(QWidget *parent = 0);
    ~Dlg_Coelostat_Alpha_GoTo();

private slots:
    void slotDelayedInit();
    void on_spinBox_hh_valueChanged(int arg1);
    void on_spinBox_mm_valueChanged(int arg1);
    void on_spinBox_ss_valueChanged(int arg1);
    void on_Btn_Cur_Pos_clicked();
    void on_Btn_Cur_Time_clicked();
    void on_Btn_Parking_clicked();

    void on_chkParkSunSet_clicked(bool checked);

private:
    Ui::Dlg_Coelostat_Alpha_GoTo *ui;
};

#endif // Dlg_Coelostat_ALPHA_GOTO_H
