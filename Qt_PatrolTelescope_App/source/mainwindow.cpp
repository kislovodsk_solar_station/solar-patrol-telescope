#include <QSettings>
#include <QLabel>
#include <QCloseEvent>
#include <QDialogButtonBox>
#include <QFormLayout>
#include <QMdiArea>
#include <QDir>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dlg_guide.h"
#include "guide.h"
#include "dlg_spectra.h"
#include "dlg_scanner.h"
#include "dlg_telescopemotors.h"
#include "Dlg_Coelostat.h"
#include "QSM_Dlg_InitControllers.h"
#include "xmlconfig.h"
#include "observation_log.h"


MainWindow::MainWindow(QString confDir, QString logPath, QWidget *parent) :
    configDirectory(confDir),
    observLogPath(logPath),
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    setObjectName("MainWindow");
    ui->setupUi(this);

    config_name = configDirectory + QString("main.ini");
    config_second_spectral_camera = configDirectory + QString("spectral_camera2.ini");

    // Создаем объекты
    all_Controllers     = new CInitControllers(configDirectory);
    dlg_Spectra         = new Dlg_Spectra(configDirectory);
    dlg_Coelostat       = new Dlg_Coelostat(all_Controllers, configDirectory);
    dlg_TelescopeMotors = new Dlg_TelescopeMotors(all_Controllers, configDirectory);
    dlg_Scanner         = new Dlg_Scanner(configDirectory);
    dlg_Guide           = new Dlg_Guide(dlg_Coelostat, configDirectory);
    Camera *spectralCamera2 = ConnectSecondSpectralCamera();

    // Журнал наблюдений
    CreateObservationLog();
    ConnectScannerAndLog();
    if (log->isEnabled())
        log->show();
    else
    {
        delete log;
        log = nullptr;
    }


    mdiArea = new QMdiArea;
    mdiArea->setObjectName("MDI Area");

    //  Добавляем окна в оконную область
    mdiArea->addSubWindow(dlg_TelescopeMotors);
    mdiArea->addSubWindow(dlg_Spectra);
    mdiArea->addSubWindow(dlg_Scanner);
    mdiArea->addSubWindow(dlg_Guide);
    mdiArea->addSubWindow(dlg_Coelostat);
    this->setCentralWidget(mdiArea);

    LoadConfig();

    if (!ConnectScannerAndSpectra(dlg_Scanner->scanner, dlg_Spectra->pSpectralCamera))
        qDebug() << "Не удалось соединить сканер и диалог спектра первой камеры!";

    if (!ConnectScannerAndSpectra(dlg_Scanner->scanner, spectralCamera2))
        qDebug() << "Не удалось соединить сканер и диалог спектра второй камеры!";

    if (!ConnectCoelostatAndGuide())
        qDebug() << "Не удалось соединить целостат и гид!";

    if (!ConnectCoelostatAndScanner())
        qDebug() << "Не удалось соединить целостат и сканер!";

    if (!ConnectScannerAndGuide())
        qDebug() << "Не удалось соединить сканер и гид!";

    if (!ConnectGuideAndSpectra(dlg_Spectra->pSpectralCamera))
        qDebug() << "Не удалось соединить гид и диалог спектра первой камеры!";

    if (!ConnectGuideAndSpectra(spectralCamera2))
        qDebug() << "Не удалось соединить гид и диалог спектра второй камеры!";

    if (all_Controllers->isInitSuccess())
        dlg_Scanner->scanner->SetMotorController(&dlg_Coelostat->m_pAll_CNLRs_SM[0]);
    else
    {
        dlg_Scanner->setDisabled(true);
        dlg_Coelostat->setDisabled(true);
    }


    // Соединения для отображения окон
    connect(ui->actionDlgSpectra, SIGNAL(triggered(bool)),
            dlg_Spectra, SLOT(show()));
    connect(ui->actionDlgGuide, SIGNAL(triggered(bool)),
            dlg_Guide, SLOT(show()));
    connect(ui->actionDlgScanner, SIGNAL(triggered(bool)),
            dlg_Scanner, SLOT(show()));
    connect(ui->actionDlgCoelostat, SIGNAL(triggered(bool)),
            dlg_Coelostat, SLOT(show()));
    connect(ui->actionDlgTelescopeMotors, SIGNAL(triggered(bool)),
            dlg_TelescopeMotors, SLOT(show()));
//    connect(ui->actionDlgInitControllers, SIGNAL(triggered(bool)),
//            dlg_InitControllers, SLOT(show()));


    // остановка таймера отображения в окне камеры спектра
    connect(this, &MainWindow::sigExit,
            dlg_Spectra, &Dlg_Spectra::StopTimers);

    // остановка таймера отображения в окне камеры гида
    connect(this, &MainWindow::sigExit,
            dlg_Guide, &Dlg_Guide::StopShowImage);

    // остановка таймера отображения целостата
    connect(this, &MainWindow::sigExit,
            dlg_Coelostat, &Dlg_Coelostat::StopTimers);

}


void MainWindow::CreateObservationLog()
{
    log = new ObservationLog(configDirectory, observLogPath);
    log->setObjectName("Observation Log");
    log->setWindowTitle("Журнал наблюдений");
    log->setWindowModality(Qt::ApplicationModal);   // Делаем окно модальным
}


bool MainWindow::ConnectCoelostatAndScanner()
{
    if (!dlg_Scanner->scanner || !dlg_Coelostat)
        return false;

    dlg_Coelostat->m_pSolarScanner = dlg_Scanner->scanner;

    // Остановка сканирования перед началом парковки целостата
    return connect(dlg_Coelostat, SIGNAL(sigScanStop()),
                   dlg_Scanner->scanner, SLOT(Stop()));
}


bool MainWindow::ConnectCoelostatAndGuide()
{
    if (!dlg_Guide->p_guide || !dlg_Coelostat)
        return false;

    dlg_Coelostat->m_pGuide = dlg_Guide->p_guide;

    bool result = connect(dlg_Guide->p_guide, SIGNAL(sigCorrectionAlphaDelta(double, double)),
                          dlg_Coelostat, SLOT(On_Correction_AlphaDelta(double,double)));
    // Остановка гидирования перед началом парковки целостата
    result = result && connect(dlg_Coelostat, SIGNAL(sigGuideStop(bool)),
                dlg_Guide->p_guide, SLOT(SetGuideMode(bool)));
    // Снятие галочки "Гидирование"
    result = result && connect(dlg_Coelostat, SIGNAL(sigGuideStop(bool)),
            dlg_Guide, SLOT(SetGuideCheckBox(bool)));
    return result;
}

bool MainWindow::ConnectScannerAndGuide()
{
    if (!dlg_Guide->p_guide || !dlg_Scanner->scanner)
        return false;

    dlg_Guide->p_guide->pScanner = dlg_Scanner->scanner;

    // Связь гида со сканированием
    bool result = connect(dlg_Guide->p_guide, SIGNAL(sigFindSunError(bool)),
                          dlg_Scanner->scanner, SLOT(SetNextScanPause(bool)));

    result = result && connect(dlg_Guide->p_guide, &Guide::sigScanBreak,
                               dlg_Scanner->scanner, &Scanner::SetScanPause);

    // Перезапуск таймера гида и обнуление счетчиков гидирования по сигналу из Scanner
    result = result && connect(dlg_Scanner->scanner, SIGNAL(sigGuideRestart()),
                               dlg_Guide, SLOT(GuideRestart()));
    return result;
}

bool MainWindow::ConnectGuideAndSpectra(Camera *camera)
{
    if (!dlg_Guide->p_guide || !camera)
        return false;

    // Передача суточного угла в камеру для записи в fit-файл
    return connect(dlg_Guide->p_guide, &Guide::sigDailyAngle,
                   camera, &Camera::SetDailyAngle);
}

void MainWindow::ConnectScannerAndLog()
{
    connect(dlg_Scanner->scanner, &Scanner::sigStartTime,
            log, &ObservationLog::AddStartTime);
    connect(dlg_Scanner->scanner, &Scanner::sigCurrentTime,
            log, &ObservationLog::GetFinishTime);
    connect(dlg_Scanner->scanner, &Scanner::sigFinishTime,
            log, &ObservationLog::AddFinishTime);
}

bool MainWindow::ConnectScannerAndSpectra(Scanner *scanner, Camera *camera)
{
    // Передаем указатели в объекты
    if (!scanner->SetCamera(camera))
        return false;

    // Соединяем сигналы и слоты
    bool result = connect(scanner, &Scanner::sigScanWasStarted,
                          camera, &Camera::isScanStart);

    result = result && connect(camera, &Camera::sigScanPause,
                               scanner, &Scanner::SetScanPause);

    result = result && connect(camera, &Camera::sigScanPause,
                               scanner, &Scanner::ClearDisk);

    result = result && connect(camera, &Camera::sigScanStart,
                               scanner, &Scanner::Start);

    result = result && connect(camera, &Camera::sigCameraReset,
                               scanner, &Scanner::Stop);
    return result;
}




MainWindow::~MainWindow()
{
    qDebug() << "~MainWindow 0";

//    mdiArea->removeSubWindow(dlg_TelescopeMotors);
//    mdiArea->removeSubWindow(dlg_Coelostat);
//    mdiArea->removeSubWindow(dlg_Scanner);
//    mdiArea->removeSubWindow(dlg_Guide);
//    mdiArea->removeSubWindow(dlg_Spectra);
//    delete dlg_TelescopeMotors;
//    delete dlg_Coelostat;
//    delete dlg_Scanner;
//    delete all_Controllers;
//    delete dlg_Guide;
//    delete dlg_Spectra;

//    delete mdiArea;
//    delete ui;
    qDebug() << "~MainWindow 1";
}

//-----------------------------------------------------------------------------
// Переопределение закрытия окна, вопрос подтверждения закрытия

void MainWindow::closeEvent(QCloseEvent *event)
{
    QDialog *dialog = new QDialog(this);
    dialog->setWindowTitle("Закрытие программы");

    QLabel label("Вы уверены?", dialog, 0);

    QDialogButtonBox buttons(QDialogButtonBox::Yes | QDialogButtonBox::No, Qt::Horizontal, dialog);
        connect(&buttons, SIGNAL(accepted()), dialog, SLOT(accept()));
        connect(&buttons, SIGNAL(rejected()), dialog, SLOT(reject()));

    QFormLayout *layout = new QFormLayout(dialog);
    layout->addWidget(&label);
    layout->addWidget(&buttons);
    if (dialog->exec() == QDialog::Accepted)
    {
        QApplication::processEvents(QEventLoop::AllEvents, 10);
        qDebug() << "closeEvents 1";
        this->hide();
        emit sigExit();
        SaveConfig();
        event->accept();
        mdiArea->removeSubWindow(dlg_TelescopeMotors);
        delete dlg_TelescopeMotors;
        mdiArea->removeSubWindow(dlg_Scanner);
        delete dlg_Scanner;
        mdiArea->removeSubWindow(dlg_Coelostat);
        delete dlg_Coelostat;
        delete all_Controllers;
        mdiArea->removeSubWindow(dlg_Spectra);
        delete dlg_Spectra;
        mdiArea->removeSubWindow(dlg_Guide);
        delete dlg_Guide;
        delete log;
        delete mdiArea;
        delete ui;
        qDebug() << "closeEvents 1 exit";
        exit(0);

    }
    else
    {
        qDebug() << "closeEvents 2";
        dialog->close();
        event->ignore();
    }
}


//-----------------------------------------------------------------------------
// Сохранение настроек в конфигурационный файл
//
void MainWindow::SaveConfig()
{
    QFile config(config_name);
    if(config.exists())
        config.remove();
    QSettings settings(config_name, QSettings::IniFormat);
    settings.setValue("Type", "Configuration file");
    settings.setValue("Module", "Main");

    settings.beginGroup("MainWindow");
        settings.setValue("MainWindow_pos_X", geometry().x());
        settings.setValue("MainWindow_pos_Y", geometry().y());
        settings.setValue("MainWindow_width", width());
        settings.setValue("MainWindow_height", height());
    settings.endGroup();

    settings.beginGroup("Windows");
    if(dlg_Guide)
    {
        settings.setValue("Guide_pos_X", dlg_Guide->parentWidget()->x());
        settings.setValue("Guide_pos_Y", dlg_Guide->parentWidget()->y());
    }
    if(dlg_Scanner)
    {
        settings.setValue("Scanner_pos_X", dlg_Scanner->parentWidget()->x());
        settings.setValue("Scanner_pos_Y", dlg_Scanner->parentWidget()->y());
    }
    if(dlg_Spectra)
    {
        settings.setValue("Spectra_pos_X", dlg_Spectra->parentWidget()->x());
        settings.setValue("Spectra_pos_Y", dlg_Spectra->parentWidget()->y());
    }
    if(dlg_Spectra2)
    {
        settings.setValue("Spectra2_pos_X", dlg_Spectra->parentWidget()->x());
        settings.setValue("Spectra2_pos_Y", dlg_Spectra->parentWidget()->y());
    }
    if(dlg_TelescopeMotors)
    {
        settings.setValue("TelescopeMotors_pos_X", dlg_TelescopeMotors->parentWidget()->x());
        settings.setValue("TelescopeMotors_pos_Y", dlg_TelescopeMotors->parentWidget()->y());
    }
    if(dlg_Coelostat)
    {
        settings.setValue("Coelostat_pos_X", dlg_Coelostat->parentWidget()->x());
        settings.setValue("Coelostat_pos_Y", dlg_Coelostat->parentWidget()->y());
    }
    settings.endGroup();
    config.close();
}

//-----------------------------------------------------------------------------
// Загрузка настроек из конфигурационного файла
//
void MainWindow::LoadConfig()
{
    QFile config(config_name);
    if(config.exists())
    {
        // Загрузка параметров
        config.open(QIODevice::ReadOnly);
        QSettings settings(config_name, QSettings::IniFormat);

        settings.beginGroup("MainWindow");
            QRect geometry(settings.value("MainWindow_pos_X").toInt(),
                           settings.value("MainWindow_pos_Y").toInt(),
                           settings.value("MainWindow_width").toInt(),
                           settings.value("MainWindow_height").toInt());
            setGeometry(geometry);
        settings.endGroup();

        settings.beginGroup("Windows");
        if(dlg_Guide != NULL)
        {
            dlg_Guide->parentWidget()->move(settings.value("Guide_pos_X").toInt(),
                                             settings.value("Guide_pos_Y").toInt());
        }
        if(dlg_Scanner != NULL)
        {
            dlg_Scanner->parentWidget()->move(settings.value("Scanner_pos_X").toInt(),
                                               settings.value("Scanner_pos_Y").toInt());
        }
        if(dlg_Spectra != NULL)
        {
            dlg_Spectra->parentWidget()->move(settings.value("Spectra_pos_X").toInt(),
                                               settings.value("Spectra_pos_Y").toInt());
        }
        if(dlg_Spectra2 != NULL)
        {
            dlg_Spectra->parentWidget()->move(settings.value("Spectra2_pos_X").toInt(),
                                               settings.value("Spectra2_pos_Y").toInt());
        }
        if(dlg_TelescopeMotors != NULL)
        {
            dlg_TelescopeMotors->parentWidget()->move(settings.value("TelescopeMotors_pos_X").toInt(),
                                               settings.value("TelescopeMotors_pos_Y").toInt());
        }
        if(dlg_Coelostat != NULL)
        {
            dlg_Coelostat->parentWidget()->move(settings.value("Coelostat_pos_X").toInt(),
                                               settings.value("Coelostat_pos_Y").toInt());
        }
        settings.endGroup();
        config.close();
    }
//    else
        // Добавить геометрию по умолчанию
    return;
}

//-----------------------------------------------------------------------------
// Проверка наличия 2-ой спектральной камеры и её подключение
//
Camera * MainWindow::ConnectSecondSpectralCamera()
{
    QFile config(config_second_spectral_camera);
    if(config.exists())
    {
        dlg_Spectra2 = new Dlg_Spectra(configDirectory, mdiArea);
        mdiArea->addSubWindow(dlg_Spectra2);

        connect(ui->actionDlgSpectra, SIGNAL(triggered(bool)),
                dlg_Spectra2, SLOT(show()));

        return dlg_Spectra2->pSpectralCamera;
    }
    else
        return nullptr;
}

//void MainWindow::setControllers(CInitControllers *p)
//{
//    all_Controllers = p;
//    qDebug() << "setControllers()!" << all_Controllers;
//}
