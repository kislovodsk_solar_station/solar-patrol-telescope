//---------------------------------------------------------------------------------------
//  SolarMotorsControl - класс графического интерфейса - окно управления подвижками
//               телескопа (дифракционная решетка, камера, щель и т.д.).
//  Разработка: Чернов Ярослав
//---------------------------------------------------------------------------------------

#ifndef DLG_TELESCOPEMOTORS_H
#define DLG_TELESCOPEMOTORS_H


#include <QWidget>
#include "init_controllers_macros.h"

#define MAX_POSITION 1                              // концевик максимум
#define MIN_POSITION -1                             // концевик минимум
#define TIMER_POSITION_MSEC 50                      // время отработки таймера при движении подвижек
#define STEPS_TO_MM 800.0                           // коэффициент перевода шагов в миллиметры
#define STEPS_TO_DEGREES 100.0                      // коэффициент перевода шагов в градусы
#define CENTER_SCANNER_POSITION 16                  // центр подвижки сканирования от начала (левого края)
#define MOTORS_NO_CALIBRATION 0                     // обычный режим, калибровка не производится
#define MOTORS_GO_TO_LIMIT_SWITCH 1                 // режим движения до концевика
#define MOTORS_GO_CALIBRATION_POSITION 2            // режим движения на позицию (калиброванную)

typedef unsigned char BYTE;
namespace Ui {
class Dlg_TelescopeMotors;
}

class CInitControllers;
class Dlg_TelescopeMotors : public QWidget
{
    Q_OBJECT

public:
    explicit Dlg_TelescopeMotors(CInitControllers *controllers, QString configDir, QWidget *parent = 0);
    ~Dlg_TelescopeMotors();

    CInitControllers *pMotorControllers;            // указатель на контроллеры
    static bool isControllersExists;

private:
    Ui::Dlg_TelescopeMotors *ui;                    // указатель на графические элементы формы
    QString config_motors_control;
    QTimer *pTimer_motorMoves;                      // таймер для обработки движения подвижек
    BYTE currentAxisFunction;                       // ось, работающая в данный момент
    int motorsMaxMinPositons[FUN_NUMBER_ALL];       // массив состояний концевиков подвижек
    float motorsPositions[FUN_NUMBER_ALL];          // массив текущих положений подвижек
    float motorsStartPositions[FUN_NUMBER_ALL];     // начальные позиции подвижек
    float motorsSpeeds[FUN_NUMBER_ALL];             // массив множителей скоростей подвижек
    BYTE calibrationMode;                           // режим калибровки ( 0 - нет калибровки,
                                                    //                    1 - движение до концевика,
                                                    //                    2 - движение на позицию )
    float calibrationPosition;                      // позиция для калибровки

    void MaxMinPositionCheck(int nFunction);
    void Calibrate(float position, long step2stop);
    void MoveRight(int nFunction);
    void MoveLeft(int nFunction);
    void MoveStop(int nFunction);
    void SaveConfig();
    bool LoadConfig();

private slots:
    void SetAxisFunction(int nFunction);
    void On_Timer_MotorsMoves();
    void on_btn_Right_pressed();
    void on_btn_Right_released();
    void on_btn_Left_pressed();
    void on_btn_Left_released();
    void on_btn_calibrate_clicked();
};

#endif // DLG_TELESCOPEMOTORS_H
