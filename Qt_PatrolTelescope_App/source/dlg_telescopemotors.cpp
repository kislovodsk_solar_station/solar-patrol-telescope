#include "dlg_telescopemotors.h"
#include "ui_dlg_telescopemotors.h"
#include "QSM_InitControllers.h"
#include "QSM_CNLR_Ethernet.h"

#include <QSettings>
#include <QFile>
#include <QTimer>
#include <QDebug>
#include <QMessageBox>

Dlg_TelescopeMotors::Dlg_TelescopeMotors(CInitControllers *controllers, QString configDir, QWidget *parent) :
    pMotorControllers(controllers),
    QWidget(parent),
    ui(new Ui::Dlg_TelescopeMotors),
    pTimer_motorMoves(nullptr)
{
    setObjectName("Dlg_TelescopeMotors");
    ui->setupUi(this);
    config_motors_control = configDir + QString("telescope_motors.ini");

    ui->comboBox->addItem("Управление сканером", Qt::DisplayRole);
    ui->comboBox->addItem("Фокус на щели", Qt::DisplayRole);
    ui->comboBox->addItem("Фокус камеры", Qt::DisplayRole);
    ui->comboBox->addItem("Управление решеткой", Qt::DisplayRole);
    ui->comboBox->addItem("Управление коллиматором", Qt::DisplayRole);

    if (pMotorControllers == nullptr)
        return;

    // Если инициализация контроллеров прошла успешно, выполняем необходимые действия
    if (pMotorControllers->isInitSuccess())
    {
        calibrationMode = MOTORS_NO_CALIBRATION;
        currentAxisFunction = FUN_SCAN_SPECTR;
        calibrationPosition = 0;
        memset(motorsPositions, 0, sizeof(float)*FUN_NUMBER_ALL);
        memset(motorsMaxMinPositons, 0, sizeof(int)*FUN_NUMBER_ALL);

        if (!LoadConfig())
            QMessageBox::warning( this, "Spectrograph motor controller",
                                  "Конфигурационный файл не найден - параметры по умолчанию!");

        // Таймер для вывода позиций подвижек
        pTimer_motorMoves = new QTimer(this);
        connect(pTimer_motorMoves, SIGNAL(timeout()), this, SLOT(On_Timer_MotorsMoves()));
        pTimer_motorMoves->start(TIMER_POSITION_MSEC);

        connect(ui->comboBox, SIGNAL(currentIndexChanged(int)),this, SLOT(SetAxisFunction(int)));
    }
    // В противном случае отключаем графические элементы
    else
    {
        ui->label_Position->setText("<FONT COLOR=#990000>Не удалось подключиться к контроллерам!</FONT>");
        ui->comboBox->setDisabled(true);
        ui->btn_calibrate->setDisabled(true);
        ui->btn_Left->setDisabled(true);
        ui->btn_Right->setDisabled(true);
    }

}

Dlg_TelescopeMotors::~Dlg_TelescopeMotors()
{
    qDebug() << "~SolarMotorsControl 0";
    if (pMotorControllers->isInitSuccess())
    {
        SaveConfig();
        if(pTimer_motorMoves)
            pTimer_motorMoves->stop();
    }
    delete ui;
    qDebug() << "~SolarMotorsControl 1";
}

//-----------------------------------------------------------------------------
// Сохранение настроек в конфигурационный файл
//
void Dlg_TelescopeMotors::SaveConfig()
{
    QFile config(config_motors_control);
    if(config.exists())
        config.remove();
    QSettings settings(config_motors_control, QSettings::IniFormat);
    settings.setValue("Type", "Configuration file");
    settings.setValue("Module", "Motor's control");
    settings.beginGroup("Motors_positions");
    settings.setValue("Spectral_camera_position", QString("%1").arg(motorsPositions[FUN_FOCUS_SPECTR]));
    settings.setValue("Slit_focus_position", QString("%1").arg(motorsPositions[FUN_FOCUS_SLIT]));
    settings.setValue("Grating_position", QString("%1").arg(motorsPositions[FUN_GRATING]));
    settings.setValue("Scanner_position", QString("%1").arg(motorsPositions[FUN_SCAN_SPECTR]));
    settings.setValue("Collimate_position", QString("%1").arg(motorsPositions[FUN_COLLIMATE]));
    settings.endGroup();
    settings.beginGroup("Motors_speed");
    settings.setValue("Spectral_camera_driver_speed", QString("%1").arg(motorsSpeeds[FUN_FOCUS_SPECTR]));
    settings.setValue("Slit_focus_driver_speed", QString("%1").arg(motorsSpeeds[FUN_FOCUS_SLIT]));
    settings.setValue("Grating_driver_speed", QString("%1").arg(motorsSpeeds[FUN_GRATING]));
    settings.setValue("Scanner_driver_speed", QString("%1").arg(motorsSpeeds[FUN_SCAN_SPECTR]));
    settings.setValue("Collimate_driver_speed", QString("%1").arg(motorsSpeeds[FUN_COLLIMATE]));
    settings.endGroup();
    config.close();
}

//-----------------------------------------------------------------------------
// Загрузка настроек из конфигурационного файла
//
bool Dlg_TelescopeMotors::LoadConfig()
{
    QFile config(config_motors_control);
    if(config.exists())
    {
        config.open(QIODevice::ReadOnly);
        QSettings settings(config_motors_control, QSettings::IniFormat);
        settings.beginGroup("Motors_positions");
        motorsStartPositions[FUN_FOCUS_SPECTR] = settings.value("Spectral_camera_position").toFloat();
		motorsPositions[FUN_FOCUS_SPECTR] = motorsStartPositions[FUN_FOCUS_SPECTR];
        motorsStartPositions[FUN_FOCUS_SLIT] = settings.value("Slit_focus_position").toFloat();
		motorsPositions[FUN_FOCUS_SLIT] = motorsStartPositions[FUN_FOCUS_SLIT];
        motorsStartPositions[FUN_GRATING] = settings.value("Grating_position").toFloat();
		motorsPositions[FUN_GRATING] = motorsStartPositions[FUN_GRATING];
        motorsStartPositions[FUN_SCAN_SPECTR] = settings.value("Scanner_position").toFloat();
		motorsPositions[FUN_SCAN_SPECTR] = motorsStartPositions[FUN_SCAN_SPECTR];
        motorsStartPositions[FUN_COLLIMATE] = settings.value("Collimate_position").toFloat();
		motorsPositions[FUN_COLLIMATE] = motorsStartPositions[FUN_COLLIMATE];
        settings.endGroup();
        settings.beginGroup("Motors_speed");
        motorsSpeeds[FUN_FOCUS_SPECTR] = settings.value("Spectral_camera_driver_speed").toFloat();
        motorsSpeeds[FUN_FOCUS_SLIT] = settings.value("Slit_focus_driver_speed").toFloat();
        motorsSpeeds[FUN_GRATING] = settings.value("Grating_driver_speed").toFloat();
        motorsSpeeds[FUN_SCAN_SPECTR] = settings.value("Scanner_driver_speed").toFloat();
        motorsSpeeds[FUN_COLLIMATE] = settings.value("Collimate_driver_speed").toFloat();
        settings.endGroup();
        ui->label_Position->setNum(motorsStartPositions[FUN_SCAN_SPECTR]);
        config.close();
        return true;
    }
    else
    {
        motorsStartPositions[FUN_FOCUS_SPECTR] = 0.0f;
        motorsStartPositions[FUN_FOCUS_SLIT] = 0.0f;
        motorsStartPositions[FUN_GRATING] = 0.0f;
        motorsStartPositions[FUN_SCAN_SPECTR] = 0.0f;
        motorsSpeeds[FUN_FOCUS_SPECTR] = 0.1f;
        motorsSpeeds[FUN_FOCUS_SLIT] = 0.1f;
        motorsSpeeds[FUN_GRATING] = 0.01f;
        motorsSpeeds[FUN_SCAN_SPECTR] = 0.25f;
        motorsSpeeds[FUN_COLLIMATE] = 0.1f;
        return false;
    }
}

//-----------------------------------------------------------------------------
// Выбор подвижки контроллера в окне диалога
//
void Dlg_TelescopeMotors::SetAxisFunction(int nFunction)
{
    switch(nFunction)
    {
        case 0:
        {
            currentAxisFunction = FUN_SCAN_SPECTR;
            break;
        }
        case 1:
        {
            currentAxisFunction = FUN_FOCUS_SLIT;
            break;
        }
        case 2:
        {
            currentAxisFunction = FUN_FOCUS_SPECTR;
            break;
        }
        case 3:
        {
            currentAxisFunction = FUN_GRATING;
            break;
        }
        case 4:
        {
            currentAxisFunction = FUN_COLLIMATE;
            break;
        }
        default:
        {
            break;
        }
    }
    ui->label_Position->setNum(motorsPositions[currentAxisFunction]);
}

//-----------------------------------------------------------------------------
// Вывод позиций подвижек телескопа, проверка на отработку концевиков
//
void Dlg_TelescopeMotors::On_Timer_MotorsMoves()
{
    // Ссылка на ось контроллера, работающую в данный момент
    CControllerEthernet* pController = pMotorControllers->m_apCNLRs[pMotorControllers->m_anFunCNLR[currentAxisFunction]];
    // Номер работающей оси контроллера
    BYTE nAxis = pMotorControllers->m_anFunAxis[currentAxisFunction];
    // Текущее дробление шага
    float microStep = pMotorControllers->m_anFunMicrostep[currentAxisFunction];
    // Текущая позиция оси
    long currentStepPosition = pController->GetPosAxes(nAxis);
    float currentPosition = 0;
    // Шаги до предварительной остановки
    long step2stop = pController->SM_StepToNextSpeed(motorsSpeeds[currentAxisFunction], 0,
                                                     pController->m_anSM_Acceler[currentAxisFunction],
                                                     TIMER_POSITION_MSEC);

    // В зависимости от работающей оси выводятся координаты и проверяются концевики
    switch (currentAxisFunction)
    {
        case FUN_GRATING:
        {
            currentPosition = currentStepPosition / (STEPS_TO_DEGREES * microStep);
            motorsPositions[FUN_GRATING] = motorsStartPositions[FUN_GRATING] + currentPosition;
            ui->label_Position->setText(QString("%1 deg").arg(motorsPositions[FUN_GRATING], 0, 'f', 2));
            calibrationMode = MOTORS_NO_CALIBRATION;
            break;
        }
        default:
        {
            currentPosition = currentStepPosition / (STEPS_TO_MM * microStep);
            motorsPositions[currentAxisFunction] =  motorsStartPositions[currentAxisFunction] + currentPosition;
            ui->label_Position->setText(QString("%1 mm").arg(motorsPositions[currentAxisFunction], 0, 'f', 2));
            MaxMinPositionCheck(currentAxisFunction);
            break;
        }
    }

   Calibrate(currentPosition, step2stop);
}

//-----------------------------------------------------------------------------
// Проверка на пороговые значения положения подвижки
//
void Dlg_TelescopeMotors::MaxMinPositionCheck(int nFunction)
{
    QString responseController;

    // Проверка на отсутствие отработки концевика
    if(pMotorControllers->Get_On_Port_MAX_MIN(nFunction, PORT_ON_MAX) == false &&
            pMotorControllers->Get_On_Port_MAX_MIN(nFunction, PORT_ON_MIN) == false)
        motorsMaxMinPositons[nFunction] = 0;

    // Проверка на максимальное значение (предел подвижки)
    else if(pMotorControllers->Get_On_Port_MAX_MIN(nFunction, PORT_ON_MAX))
    {
        if(motorsMaxMinPositons[nFunction] == 0)
            pMotorControllers->m_apCNLRs[pMotorControllers->m_anFunCNLR[nFunction]]->SM_Stop(responseController,
                                                                                    pMotorControllers->m_anFunAxis[nFunction]);
        motorsMaxMinPositons[nFunction] = MAX_POSITION;
        ui->label_Position->setText("<FONT COLOR=#990000>Максимум!</FONT>");
    }

    // Проверка на минимальное значение (предел подвижки)
    else if(pMotorControllers->Get_On_Port_MAX_MIN(nFunction, PORT_ON_MIN))
    {
        if(motorsMaxMinPositons[nFunction] == 0)
            pMotorControllers->m_apCNLRs[pMotorControllers->m_anFunCNLR[nFunction]]->SM_Stop(responseController,
                                                                                    pMotorControllers->m_anFunAxis[nFunction]);
        motorsMaxMinPositons[nFunction] = MIN_POSITION;
        ui->label_Position->setText("<FONT COLOR=#990000>Минимум!</FONT>");
    }
}

void Dlg_TelescopeMotors::Calibrate(float position, long step2stop)
{
    // Текущее дробление шага
    float microStep = pMotorControllers->m_anFunMicrostep[currentAxisFunction];

    // Движение от концевика на позицию
    if(calibrationMode == MOTORS_GO_CALIBRATION_POSITION)
    {
        double mm2stop = step2stop / STEPS_TO_MM * microStep;
        if(currentAxisFunction == FUN_SCAN_SPECTR && motorsPositions[currentAxisFunction] >= (CENTER_SCANNER_POSITION - mm2stop))
        {
            MoveStop(currentAxisFunction);
            calibrationMode = MOTORS_NO_CALIBRATION;
            ui->label_Position->setText("Отцентрировано!");
        }
        else if(currentAxisFunction != FUN_SCAN_SPECTR && position >= (calibrationPosition - step2stop) )
        {
            MoveStop(currentAxisFunction);
            calibrationMode = MOTORS_NO_CALIBRATION;
            ui->label_Position->setText("Отцентрировано!");
        }
        else
        {
            MoveRight(currentAxisFunction);
        }
    }

    // Движение до концевика
    if(calibrationMode == MOTORS_GO_TO_LIMIT_SWITCH)
    {
         if(motorsMaxMinPositons[currentAxisFunction] == MIN_POSITION)
         {
             motorsStartPositions[currentAxisFunction] = -position;
             motorsPositions[currentAxisFunction] = 0;
             calibrationMode = MOTORS_GO_CALIBRATION_POSITION;
         }
    }
}

//-----------------------------------------------------------------------------
// Методы нажатия на кнопки движения
//
void Dlg_TelescopeMotors::on_btn_Right_pressed()
{
    MoveRight(currentAxisFunction);
}

void Dlg_TelescopeMotors::on_btn_Right_released()
{
    MoveStop(currentAxisFunction);
}

void Dlg_TelescopeMotors::on_btn_Left_pressed()
{
    MoveLeft(currentAxisFunction);
}

void Dlg_TelescopeMotors::on_btn_Left_released()
{
    MoveStop(currentAxisFunction);
}

//-----------------------------------------------------------------------------
// Движение подвижки вправо (вперёд)
//
void Dlg_TelescopeMotors::MoveRight(int nFunction)
{
    QString responseController;
    if(motorsMaxMinPositons[nFunction] != MAX_POSITION)
    {
        pMotorControllers->m_apCNLRs[pMotorControllers->m_anFunCNLR[nFunction]]->SM_Forvard(responseController,
                                                                                       motorsSpeeds[nFunction],
                                                                                       pMotorControllers->m_anFunAxis[nFunction]);
        currentAxisFunction = nFunction;
    }
}

//-----------------------------------------------------------------------------
// Движение подвижки влево (назад)
//
void Dlg_TelescopeMotors::MoveLeft(int nFunction)
{
    QString responseController;
    if(motorsMaxMinPositons[nFunction] != MIN_POSITION)
    {
        pMotorControllers->m_apCNLRs[pMotorControllers->m_anFunCNLR[nFunction]]->SM_Forvard(responseController,
                                                                                       motorsSpeeds[nFunction]* (-1),
                                                                                       pMotorControllers->m_anFunAxis[nFunction]);
        currentAxisFunction = nFunction;
    }
}

//-----------------------------------------------------------------------------
//  Остановка движения подвижки
//
void Dlg_TelescopeMotors::MoveStop(int nFunction)
{
    QString responseController;
    pMotorControllers->m_apCNLRs[pMotorControllers->m_anFunCNLR[nFunction]]->SM_Stop(responseController,
                                                                                   pMotorControllers->m_anFunAxis[nFunction]);
}

//-----------------------------------------------------------------------------
//  Калибровка позиции подвижки
//
void Dlg_TelescopeMotors::on_btn_calibrate_clicked()
{
    BYTE nAxis = pMotorControllers->m_anFunAxis[currentAxisFunction];
    calibrationPosition = pMotorControllers->m_apCNLRs[pMotorControllers->m_anFunCNLR[currentAxisFunction]]->GetPosAxes(nAxis);
    calibrationMode = MOTORS_GO_TO_LIMIT_SWITCH;
    MoveLeft(currentAxisFunction);
}
