//-------------------------------------------------------------------------------------------------
//  SolarGuide - класс вычисления смещения диска Солнца и проведения необходимых коррекций
//  Разработка: Чернов Ярослав, Шрамко Андрей, Максим Стрелков
//-------------------------------------------------------------------------------------------------

#ifndef GUIDE_H
#define GUIDE_H

#include "Dlg_Coelostat.h"
#include "scanner.h"
#include "camera_jai.h"
#include "Point3.h"
#include <QObject>

#define CAMERA_ARCESC_ON_PIXEL              2.9 /*Гид до щели. Камера GE 5000M.*/
//#define CAMERA_ARCESC_ON_PIXEL              4.73  /*Гид на щели. Камера GE130M.*/
#define GUIDE_CAMERA_MIN_INTENSITY          20000
#define GUIDE_NUM_LAST_OFFSET               4
#define GUIDE_PIX_OFFSET_LIMIT              4.0
#define GUIDE_STABILITY_NUMBER              4
#define GUIDE_SUN_DIAMETER                  310
#define GUIDE_ARCSEC_OFFSET_ERROR           CAMERA_ARCESC_ON_PIXEL * GUIDE_PIX_OFFSET_LIMIT // 14.50 /*24.0*/

class QFileInfo;

class Guide : public QObject
{
    Q_OBJECT

public:
    explicit Guide(Dlg_Coelostat *coelostat, Camera_JAI *camera, QObject *parent = 0);
    ~Guide();

    Dlg_Coelostat* pDlg_Coelostat;
    Scanner* pScanner;
    Camera_JAI* pGuideCamera;

    int radius;
    bool findSunError;                     // Флаг, требуется ли остановка сканирования

    float aSunCirX[N_POINT_FIND_CIRC];  // Массив Х-координаты найденных точек на лимбе солнца
    float aSunCirY[N_POINT_FIND_CIRC];  // Массив Y-координаты найденных точек на лимбе солнца


    inline bool isGuiding(){return guiding;}
    inline int GetPosX() {return posX;}
    inline int GetPosY() {return posY;}
    inline int GetOffsetX() {return offsetX;}
    inline int GetOffsetY() {return offsetY;}
    inline int GetMeanOffsetX() {return meanOffsetX;}
    inline int GetMeanOffsetY() {return meanOffsetY;}
    inline int GetMeanRadius() {return meanRadiusSun;}
    inline bool GetInProcess() {return inProcess;}
    inline int GetArcsecMinOffsetError() {return arcsecMinOffsetError;}
    bool NeedCorrection();
    bool IsCorrection();
    void OnNullAll_OffsetСounter(); // Обнуляем счетчики и смещения


public slots:
    void FindCenter(unsigned char *imageBuf, int width, int height);
    void CalcSolarShift();
    void Guiding();
    void SetGuideMode(bool b);
    void SetShiftX(int x);
    void SetShiftY(int y);

private slots:
    void StartTimer();

private:
    QFileInfo* fileInfo;
    QDateTime* dTime;
    QString filename;
    QImage* image;
    QTimer* processTimer;
    float lastOffsets[GUIDE_NUM_LAST_OFFSET];   // Массив последних найденных радиусов
    int* intensity_X;                           // Массив интенсивностей по оси X
    int* intensity_Y;                           // Массив интенсивностей по оси Y
    int maxIntensity_X;                         // Максимальное значение интенсивности по оси X
    int maxIntensity_Y;                         // Максимальное значение интенсивности по оси Y
    int posX;                                   // мгновенное положение центра диска Солнца по X
    int posY;                                   // мгновенное положение центра диска Солнца по Y
    int startPosX;                              // координата центра кадра
    int startPosY;                              // координата центра кадра
    int shiftX;                                 // смещение по горизонтали
    int shiftY;                                 // смещение по вертикали
    int offsetX;                                // усредненное смещение диска Солнца по X
    int offsetY;                                // усредненное смещение диска Солнца по X
    bool guiding;                               // Флаг включено ли гидирование
    int sumOffsetX;
    int sumOffsetY;
    int counter;                            // Счетчик для усреднения смещений Солнца
    int nOffsets;                           // Счетчик для получения нескольких значений отклонения
    int stabilityCounter;                   // Счетчик для получения нескольких стабильных значений отклонения
    bool inProcess;                         // Флаг занятости класса
    int meanOffsetX;
    int meanOffsetY;
    int arcsecMinOffsetError;               // Пороговое значение смещения диска Солнца для запуска корректировки
    float sumRSun;                          // Радиус Солнца в пикселях (суммированный)
    int meanRadiusSun;                      // Радиус Солнца в пикселях (усредненный)
    double meanOffsetR;                     // Среднеквадратичное смещение
    int meanIntensitySun;                   // Усреднённая интенсивность диска Солнца
    int meanIntensityFrame;                 // Усреднённая интенсивность всего кадра
    int counterLastFrames;                  // Счетчик числа кадров для метода контроля облаков
    float summLastFramesIntensity;          // Сумма средних интенсивностей для метода контроля облаков
    bool StopScanning;                      // Требуется ли остановка текущего сканирования
    QVector<int> *last_intensities;         // Последние значения интенсивности
    bool isScanStopFromGuide;               // Было ли остановлено сканирование в методе контроля облаков

    void GuideCorrection();
    void FindMaxIntensity(unsigned char* imageBuf, int width, int height);
    bool CloudsControl(float R_sun, float offsetR);
    bool FindSunDeviation(float R_sun, float offsetR);
    void IntensityDeviation(float minIntensitySumm, int sec);   // sec - за сколько секунд подсчитывать яркость
    void isBreakScanning();


signals:
    //void sigCenter(int,int,int);
    //void sigShift(int,int);
    void sigFindCenterFinished(QImage image);
    void sigFindSunError(bool);
    void sigDailyAngle(double);
    void sigInProcess();
    void sigCorrectionAlphaDelta(double, double);
//    void sigScanStart();
    void sigScanBreak();
};

#endif // GUIDE_H
