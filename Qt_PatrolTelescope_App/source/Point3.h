﻿//---------------------------------------------------------------------------

#ifndef __PONT3_H
#define __PONT3_H

#define N_POINT_FIND_CIRC 21

  float Circle_Three_Points_Rand(int n,int iter,float x[],float y[],int *xc,int *yc);
  float Circle_Conjugate_Gradient(int n,float x[],float y[],int *xc,int *yc);
  //int CircleFit(int N, TPoint P[], double *pa, double *pb, double *pr);
  float Ellipse_Three_Points(int n,float x[],float y[],int *xc,int *yc,float *a,float *b);


  float GetR_XC_YC_3P(int xc_0, int yc_0, int nFindGrad, QImage *pImage, int *x_ret, int *y_ret, float *aSunCirX, float *aSunCirY);  //Поиск центра окружности и радиуса по удаленым точкам
  float GetR_XC_YC_3P(int xc_0, int yc_0, int nFindGrad, unsigned char* imageBuf, int width, int height, int *x_ret, int *y_ret, float *aSunCirX, float *aSunCirY);  //Поиск центра окружности и радиуса по удаленым точкам (буфер камеры)

  void FindCirclePoints(int *n_0, int xc_0, int yc_0, int nFindGrad, QImage *pImage, float *ax_ret, float *ay_ret);//  Метод нахождения точек на диске Солнца
  void FindCirclePoints(int *n_0, int xc_0, int yc_0, int nFindGrad, unsigned char* imageBuf, int width, int height, float *ax_ret, float *ay_ret);//  Метод нахождения точек на диске Солнца (буфер камеры)

  void PlotCirclePix(QImage *image, int xc, int yc, int r, uint rgb);  //Вывод окружности на image
  void PlotCross(QPainter *pPaint, int xc, int yc, int d, QColor color);  //Вывод крестика на QPainter



#endif
