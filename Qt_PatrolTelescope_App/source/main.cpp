/*
  \mainpage Попытка дать некое описание проекту: его структуре и принципам реализации.
  */

#include <QApplication>
#include <QPainter>
#include <QLabel>
#include <QFile>
#include <QDir>
#include <QSettings>
#include <QMessageBox>
#include <memory>
#include "mainwindow.h"
#include "sehtranslator.h"

/**
 * \brief Класс управления приложением Qt
 * \details Данный класс переопределяет метод notify(QObject *reciever, QEvent *event)
 * класса QApplication из библиотеки QtCore для обработки ошибки, когда не удается доставить событие получателю:
 * например, если объект получателя был удален.
 */
class MyApplication final: public QApplication {

public:
    MyApplication(int argc, char *argv[]) : QApplication(argc, argv) {} //, sehtr(SehTranslator()) {}

    /**
     * Оборачивает вызов метода notify в блок try catch.
     * За более подробной информацией обратитесь к документации Qt класса QCoreApplication
     * (базовый класс управления приложением)
     \param reciever Получатель события
     \param event Событие
     \return Информацию о том, было ли событие доставлено получателю (true или alse)
     */
    virtual bool notify(QObject *reciever, QEvent *event) override
    {
        try {
            return QApplication::notify(reciever, event);
        }
        catch(const _com_error &ex) {
            qDebug() << "QApp Win32 error: notify(" << reciever->objectName() <<"," << event->type() << ")";
            qDebug() <<"is a system event? "<< event->spontaneous();
            qDebug() <<"is event accepted? " << event->isAccepted();
        }
        catch (...) {
            qDebug() << "QApp error: notify(" << reciever->objectName() <<"," << event->type() << ")";
            qDebug() <<"is a system event? "<< event->spontaneous();
            qDebug() <<"is event accepted? " << event->isAccepted();
        }
        return false;
    }
};

//! \brief Переменные для хранения пути к конфигурационным файлам
static QString configDirectory;
static QString observLogPath;

/** \brief Загрузка имени директории с конфигурационными файлами и журналом наблюдений
 * \details Попытка загрузить конфигурационный файл по адресу ./path_to_configs.ini.
 * В случае успеха из него берутся пути к конфигурационным файлам других элементов приложения
 * \return True, если удалось прочитать конфигурационный файл, False - если не удалось.
*/
bool LoadConfig()
{
    QString pathConfig("./path_to_configs.ini");
    QFile config(pathConfig);
    if (!config.exists())
    {
        QString title("Patrol Telescope");
        QMessageBox::warning(nullptr, title,
                             "Не указан путь к конфигурационным файлам! \nОтсутствует файл path_to_config.ini");
        return false;
    }
    else
    {
        // Загрузка параметров
        if (config.open(QIODevice::ReadOnly))
        {
            QSettings settings(pathConfig, QSettings::IniFormat);
            configDirectory = settings.value("path").toString();
            observLogPath = settings.value("observation_log").toString();
            config.close();
            qDebug() << configDirectory << observLogPath;
            return true;
        }
        if (!QDir(configDirectory).exists())
        {
            qDebug() << "Main: config directory don't exist!";
            QString title("Patrol Telescope");
            QMessageBox::warning(nullptr, title,
                                 "Путь к конфигурационным файлам " + configDirectory +
                                 " \nне найден");
            return false;
        }        
    }
}


int main(int argc, char *argv[])
{
    // Транслируем исключения SEH в исключения С++ _com_error
    SehTranslator sehtr;

    // Объект приложения Qt с обработкой исключений
    // в методе notify(QObject *reciever, QEvent *event)
    MyApplication app(argc, argv);
    app.setObjectName("PatrolTelescopeApp");

    if (!LoadConfig())
    {
        qDebug() << "Ошибка пути к конфигурационным файлам! Выход!";
        return 0;
    }

    try {
        // Создаем объект главного окна (управление телескопом)
        MainWindow w(configDirectory, observLogPath);
        w.setLocale(QLocale::Russian);
        w.show();
        return app.exec();
    }
    catch(const _com_error &ex) {
        qDebug() << "main: Win32 error: code =" <<ex.Error();
        qDebug() << app.applicationState();
        return -10;
    }
    catch(...) {
        qDebug() << "C++ exception!";
        return -20;
    }

}
