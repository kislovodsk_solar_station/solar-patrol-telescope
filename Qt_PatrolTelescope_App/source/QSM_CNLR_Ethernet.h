//  CControllerEthernet--- QSM_CNLR_Ethernet.h
#ifndef QSM_CNLR_ETHERNET_H
#define QSM_CNLR_ETHERNET_H

#include "controllers_ethernet_macros.h"
//Глобальные для SM_COM
//WORD CRC16 ( BYTE *chkbuf, BYTE len );

//#define UCHAR  unsigned char
//#define USHORT unsigned short
//#define ULONG  unsigned long
//#define LONG   long
//#define BYTE   unsigned char
//#define DWORD  unsigned short
//#define BOOL   bool

#define FALSE  0
#define TRUE   1

#ifdef WIN32
#include <winsock2.h>
#else
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#endif

//#include <winsock2.h>
#include <stdio.h>
#include <stdint.h>
//#include <tchar.h>
#include <math.h>
#include <QString>
#include <QTime>
#include <QThread>
#include <QObject>


#pragma comment(lib, "ws2_32.lib")

//Структура состояния контроллера PLCM_E3. Пакет, посылаемый контроллером (90-байт)
struct InfoCntlr_PLCM_E3 {
    USHORT inf_NBYTE;	// 00-длина пакета в байтах - 90(0x005A)--было 86(0x0056)
	UCHAR  Ncmd;		// 02-номер последнего выполненного пакета;
	UCHAR  Job;			// 03-текущее состояние контроллера:0 готов к работе;1 идет выполнение траекторных данных;2 состояни аварии;3 выполнение траектории приостановлено.
	UCHAR  Flags;		// 04-некоторые битовые флаги: Err признак ошибки; Act X единица, если контроллер в состоянии Idle и ось с данным номером находится в движении.
	UCHAR  ErrCmdNum;	// 05-если был установлен бит ошибки в поле Flags, данное число указывает на номер команды, вызвавшей ошибку;
	UCHAR  ErrCode;		// 06-код ошибки если был установлен бит ошибки в поле Flags:0 неизвестная команда;1 выполнение команды невозможно в данном состоянии;2 траекторный буфер переполнен;3 неверный параметр команды;4 достигнуты программные пределы перемещений;5 достигнуты аппаратные пределы перемещений (сработал концевой датчик) ;6 сработал внешний датчик аварии (Estop) ;7 ошибка связи. Обычно возникает при длительном отсутсвии пакетов от ПО;8 внутренняя авария;9 переход в аварию в связи с получением соответствующей команды от ПО.
	UCHAR  Referenced;	// 07-битовое поле, где каждый бит сообщает о том, что по соответсвующей оси завершен поиск базы;
	ULONG  AxisJobs;	// 08-младшие 18 бит слова представляют собой состояние каждой оси (по три бита на ось, младшая тройка ось 0) . Так же как и для битов Act поля Flags, данные поля имеют значение только при состоянии контроллера Idle. Может принимать одно из значений: 0 ось не перемещается; 1 выполняется ручное перемещение (jogging) ; 2 выполняется поиск базы (homing) ; 3 выполняется поиск материала (probing) ; Старшие 14 битов не используются.
    LONG   Position_0;	// 12-текущее положение каждой оси 0 в шагах;
    LONG   Position_1;	// 16-текущее положение каждой оси 1 в шагах;
    LONG   Position_2;	// 20-текущее положение каждой оси 2 в шагах;
    LONG   Position_3;	// 24-текущее положение каждой оси 3 в шагах;
    LONG   Position_4;	// 28-текущее положение каждой оси 4 в шагах;
    LONG   Position_5;	// 32-текущее положение каждой оси 5 в шагах;
	ULONG  Cur_traject;	// 36-ID элемента траектории, выполняемого в данный момент
	ULONG  Traject_coun;// 40-количество элементов траектории в буфере контроллера;
	ULONG  System_time;	// 44-время в миллисекундах, прошедшее с момента подачи питания на контроллер;
	ULONG  Firmware_ver;// 48-версия прошивки;
	ULONG  Port_1;		// 52-текущее состояние входов/выходов соответствующего порта 1;
	ULONG  Port_2;		// 56-текущее состояние входов/выходов соответствующего порта 2;
	ULONG  Port_3;		// 60-текущее состояние входов/выходов соответствующего порта 3;
	ULONG  THC_correct;	// 64-значение на которое скорректировано положение оси 2 в данный момент, т. е. текущее отклонение оси 2 от заданной величины;
	ULONG  THC_Voltage; // 68-текущее напряжение в дуге плазмореза, полученное от устройства контроля высоты резака; Из UInt16 стал UInt32
	USHORT THC_RawData; // 72-2 байта (72-73) - THC RawData. UInt16
	USHORT THC_Flags;	// 74-2 байта (74-75) - THC Flags. UInt16
    ULONG  ADCVal;		// 12-битовое значение измеренное на входе АЦП PLCM-E3; UInt32
    ULONG  MPGPos;		// текущее положение энкодера, подключенного к соответствующему входу PLCM-E3; UInt32
    ULONG  MPGVel;		// 76-текущая скорость энкодера, подключенного к соответствующему входу PLCM-E3;
    USHORT CRC_16;		// 80-2 байта контрольной суммы.
};

//Структура состояния контроллера PLC230. Пакет, посылаемый контроллером (78-байт)
struct InfoCntlr_PLC230 {
    USHORT inf_NBYTE;	// 00-длина пакета в байтах - 78(0x004e)--было 86(0x0056)
    UCHAR  Ncmd;		// 02-номер последнего выполненного пакета;
    UCHAR  Job;			// 03-текущее состояние контроллера:0 готов к работе;1 идет выполнение траекторных данных;2 состояни аварии;3 выполнение траектории приостановлено.
    UCHAR  Flags;		// 04-некоторые битовые флаги: Err признак ошибки; Act X единица, если контроллер в состоянии Idle и ось с данным номером находится в движении.
    UCHAR  ErrCmdNum;	// 05-если был установлен бит ошибки в поле Flags, данное число указывает на номер команды, вызвавшей ошибку;
    UCHAR  ErrCode;		// 06-код ошибки если был установлен бит ошибки в поле Flags:0 неизвестная команда;1 выполнение команды невозможно в данном состоянии;2 траекторный буфер переполнен;3 неверный параметр команды;4 достигнуты программные пределы перемещений;5 достигнуты аппаратные пределы перемещений (сработал концевой датчик) ;6 сработал внешний датчик аварии (Estop) ;7 ошибка связи. Обычно возникает при длительном отсутсвии пакетов от ПО;8 внутренняя авария;9 переход в аварию в связи с получением соответствующей команды от ПО.
    UCHAR  Referenced;	// 07-битовое поле, где каждый бит сообщает о том, что по соответсвующей оси завершен поиск базы;
    ULONG  AxisJobs;	// 08-младшие 18 бит слова представляют собой состояние каждой оси (по три бита на ось, младшая тройка ось 0) . Так же как и для битов Act поля Flags, данные поля имеют значение только при состоянии контроллера Idle. Может принимать одно из значений: 0 ось не перемещается; 1 выполняется ручное перемещение (jogging) ; 2 выполняется поиск базы (homing) ; 3 выполняется поиск материала (probing) ; Старшие 14 битов не используются.
    LONG   Position_0;	// 12-текущее положение каждой оси 0 в шагах;
    LONG   Position_1;	// 16-текущее положение каждой оси 1 в шагах;
    LONG   Position_2;	// 20-текущее положение каждой оси 2 в шагах;
    LONG   Position_3;	// 24-текущее положение каждой оси 3 в шагах;
    LONG   Position_4;	// 28-текущее положение каждой оси 4 в шагах;
    LONG   Position_5;	// 32-текущее положение каждой оси 5 в шагах;
    ULONG  Cur_traject;	// 36-ID элемента траектории, выполняемого в данный момент
    ULONG  Traject_coun;// 40-количество элементов траектории в буфере контроллера;
    ULONG  System_time;	// 44-время в миллисекундах, прошедшее с момента подачи питания на контроллер;
    ULONG  Firmware_ver;// 48-версия прошивки;
    ULONG  Port_1;		// 52-текущее состояние входов/выходов соответствующего порта 1;
    ULONG  Port_2;		// 56-текущее состояние входов/выходов соответствующего порта 2;
    ULONG  Port_3;		// 60-текущее состояние входов/выходов соответствующего порта 3;
    ULONG  THC_correct;	// 64-значение на которое скорректировано положение оси 2 в данный момент, т. е. текущее отклонение оси 2 от заданной величины;
    ULONG  THC_Voltage; // 68-текущее напряжение в дуге плазмореза, полученное от устройства контроля высоты резака; Из UInt16 стал UInt32
    USHORT THC_RawData; // 72-2 байта (72-73) - THC RawData. UInt16
    USHORT THC_Flags;	// 74-2 байта (74-75) - THC Flags. UInt16
//    ULONG  ADCVal;		// 12-битовое значение измеренное на входе АЦП PLCM-E3; UInt32
//    ULONG  MPGPos;		// текущее положение энкодера, подключенного к соответствующему входу PLCM-E3; UInt32
//    ULONG  MPGVel;		// 76-текущая скорость энкодера, подключенного к соответствующему входу PLCM-E3;
    USHORT CRC_16;		// 80-2 байта контрольной суммы.
};


class CControllerEthernet: public QThread
{
public:
    explicit CControllerEthernet();
    ~CControllerEthernet();

    QString m_sCNLR_IP;        //IP контроллера
    QString m_sCNLR_Name;      //Имя контроллера
    int m_ConnectSocket;    //Cокет для отправки команд и получения статусного пакета, приходящего от контроллера
    sockaddr_in m_RecvAddr; //адрес контроллера
    BOOL m_bPortReady;      //Порт готов приему-передачи
    int  m_NSizeByteP;      //Размер полученого статусного пакета
    bool m_bOnThread;       //для бесконечного цикла в потоке

public:
    ULONG m_anSM_Speed_max  [N_MAX_AXIS];  //Макимальная скорость ШД  (steps/sec)
    ULONG m_anSM_Speed_start[N_MAX_AXIS];  //Начальная скорость ШД  (steps/sec) (не работает)
    ULONG m_anSM_Acceler    [N_MAX_AXIS];  //Ускорение ШД  (steps/sec^2)
    BYTE  m_anSM_MicroStep  [N_MAX_AXIS];  //Установка делителя (микрошага)
    ULONG m_anSM_Ports_Out  [N_MAX_PORTS]; //Выходные порты контроллера (битовое значение выходов для соответствующего порта. При этом биты, соответствующие входным пинам будут проигнорированы)
	

public:	
    InfoCntlr_PLC230*  GetCurInfoCntlr_PLC230() ;//Последнее сообщение контроллера PLC230
    InfoCntlr_PLCM_E3* GetCurInfoCntlr_PLCM_E3();//Последнее сообщение контроллера PLCM-E3
    int m_nLenStatusPacket;             //Длина статусного пакета зависит от контроллера
    char m_btBuffer_In [N_BYTE_BUFFER];	//Буфер размера статусного пакета, приходящего от контроллера
    char m_btBuffer_Out[N_BYTE_BUFFER];	//Буфер размера статусного пакета, для отправки контроллеру
	
public:
    BOOL Initialize_Ethernet();// инициализация Ethernet порта для получения статусных пакетов
	BOOL Initialize_StepMotor();//Инициализация конроллера ШД
	BOOL Get_Data_Ethernet(); // Считать данные с Ethernet-порта (CString &strLog, char *outBuffer)
    int RecvWithTimeout( int Socket, char *Buffer, int Len, long nuTimeout, int *bTimedOut );//Recv с таймаутом

    BOOL Send_Command_Ethernet (int nLen); // Отправить команду на Ethernet-порт Reset
	
	BOOL Send_Command_Ethernet_Reset(LONG * pnMaxSpeed, LONG * pnAcceler);   // Отправить команду на Ethernet-порт Reset 
    BOOL Send_Command_Ethernet_HardInit();                              // Отправить команду на Ethernet-порт инициализации железа контроллера
    BOOL Send_Command_Ethernet_InitPorts();                             // Отправить команду на Ethernet-порт инициализации железа контроллера - настройка портов ввода-вывода
    BOOL Send_Command_Ethernet_SetPos(LONG * aCurPos);                  // Отправить команду на Ethernet-порт для установки текущих координат
    BOOL Send_Command_Ethernet_SetPosAxes(LONG nCurPos, int nAxes);     // Отправить команду на Ethernet-порт для установки текущих координат определенной оси
    BOOL Send_Command_Ethernet_Jogging(int nSpeed, int nAxes);          // Отправить команду на Ethernet-порт для запуска двигателей ручного управления int nSpeed - скорость перемещения (шаг/с), int nAxes - номер оси
    BOOL Send_Command_Ethernet_Homing (int nSpeed, int nPos, int nAxes);// Отправить команду на Ethernet-порт для Поиск базы (homing) int nSpeed - скорость перемещения (шаг/с), int nPos - искомая позиция, int nAxes - номер оси
    BOOL Send_Command_Ethernet_Traj(int nAxes);// Отправить команду на Ethernet-порт траекторные данные (Traj) int nSpeed - скорость перемещения (шаг/с), int nPos - искомая позиция, int nAxes - номер оси
    BOOL Send_Command_Ethernet_Stop   (bool bFullStop, int nAxes);      // Отправить команду на Ethernet-порт Stop
    BOOL Send_Command_Ethernet_ON_Port();                               // Отправить команду на Ethernet-порт для инициализации портов и поддержания работы контроллера (1 раз в 5сек)

    BOOL Ethernet_ON();         // Проверяем свободен ли порт Ethernet для отправки
    QString GetStrStatus();     // Выводим статус пакет в текст
    BOOL StatusPakOn();         // Cтатусный пакет получен хотя бы один раз
    void GetLenStatusPacket();  // Процедура определения длины статусного пакета (зависит от контроллера)
    long GetPosAxes(int nAxis); // Определение позиции в шагах ШД для оси nAxis
    long GetInPorts();          // Определение состояния входных портов
    BYTE GetCurNComm();         // Определение номера пакета который прошел в контроллере(из статусного пакета)

    double SM_StepToNextSpeed(double speed_beg, double speed_next, double acceler, double timer_ms); //  Количество шагов до остановки двигателя (с учетом начальной скорости, целевой скорости, ускорения и времени сработки таймера)

    void Terminate_Port();

    BOOL SM_Forvard(QString & sTextRead_out, float fKSpeed, int nAxis);
    BOOL SM_Reverse(QString & sTextRead_out, float fKSpeed, int nAxis);
    BOOL SM_Stop   (QString & sTextRead_out, int nAxis);
	
protected:	
    InfoCntlr_PLC230  *m_pCurInfoCntlr_PLC230;		//Последнее сообщение контроллера PLC230
    InfoCntlr_PLCM_E3 *m_pCurInfoCntlr_PLCM_E3;		//Последнее сообщение контроллера PLCM-E3


protected:
    void run ();                    //Код, исполняемый в потоке
};


#endif // QSM_CNLR_ETHERNET_H
