//---------------------------------------------------------------------------------------
//  Camera_AVT.h заголовочный файл класса для работы с камерами
//  Alied Vision Technologies через GigE Interface
//  Разработка: Пащенко Михаил, Чернов Ярослав
//---------------------------------------------------------------------------------------

#ifndef CAMERA_AVT_H
#define CAMERA_AVT_H
#define PVDECL __stdcall // Какой-то флаг для библиотеки

#include "camera.h"
#include "PvApi.h"
#include <QObject>
#include <memory>

//-------------------------------------------------------------------------------------------------
// Настройки по-умолчанию
//-------------------------------------------------------------------------------------------------
#define AVT_TIME_TO_WAIT_SEC 10                  // Время ожидания подключения камеры
#define AVT_FILE_NAME_SIZE 40                   // Количество символов, выделяемых под имя файла
#define AVT_VAL_GAIN 10                         // Усиление сигнала
#define AVT_VAL_OFFSET_X 0                      // Позиция кадра по оси X
#define AVT_VAL_OFFSET_Y 1050                   // Позиция кадра по оси Y
#define AVT_VAL_BINNING_X 2                     // Бининг по оси X
#define AVT_VAL_BINNING_Y 1                     // Бининг по оси Y
#define AVT_VAL_HEIGHT 120                      // Высота кадра
#define AVT_VAL_WIDTH 1632                      // Ширина кадра
#define AVT_VAL_FPS 47                          // Количество кадров в секунду
#define AVT_VAL_EXP 5000                        // Значение экспозиции
#define AVT_VAL_PATH "F://"                     // Путь сохранения изображений
//-------------------------------------------------------------------------------------------------
// Названия параметров для настройки
//-------------------------------------------------------------------------------------------------
#define AVT_BIN_X "BinningX"                    // Масштабирование по оси X
#define AVT_BIN_Y "BinningY"                    // Масштабирование по оси Y
#define AVT_HEIGHT "Height"                     // Высота кадра
#define AVT_WIDTH "Width"                       // Ширина кадра
#define AVT_OFFSET_X "RegionX"                  // Смещение по оси X
#define AVT_OFFSET_Y "RegionY"                  // Смещение по оси Y
#define AVT_FPS "FrameRate"                     // Количество кадров в секунду
#define AVT_EXPOSURE "ExposureValue"            // Значение экспозиции
#define AVT_EXP_MODE "ExposureMode"             // Режим экспозиции
#define AVT_PIX_FORMAT "PixelFormat"            // Формат изображения
#define AVT_TRIGGER_MODE "FrameStartTriggerMode"// Режим кадров в секунду
#define AVT_ACQ_MODE "AcquisitionMode"          // Режим получения кадров
#define AVT_GAIN "GainValue"

struct tCamera
{
    unsigned long   UID;
    tPvHandle       Handle;
    tPvFrame        Frame;
    tPvUint32       Counter;
    char            Filename[AVT_FILE_NAME_SIZE];
};

class Camera_AVT : public Camera
{
    Q_OBJECT

public:
    explicit Camera_AVT(QObject *parent = 0);
    ~Camera_AVT();

public slots:
    bool Open();
    bool Start();
    bool Stop();
    bool Close();
    void Restart();
    bool Reset();
    bool SetParams();
    void SetDefaultSettings();
    int SaveImage();
    bool SetAutoExposure(bool autoMode);
    bool SetExposure(int val);
    int GetExposure() const;
    int GetFrameWidth() const;
    int GetFrameHeight() const;   
    void SetSaveMode(bool saveMode);
    unsigned char *GetImageBuf();
    QImage GetImage();
    int GetImageSize() const;

private:
    tCamera *_tCamera;
    char cameraInfo[32];
    unsigned long nCameras;
    unsigned long frameSize;
    tPvErr errCode;
    tPvCameraInfoEx* cameraList;    // Стуктуры с информацией по камерам
    bool closed;

    bool Initialize();
    bool Setup();
    void WorkLoop();
    tPvErr CreateImage(unsigned &i);
    bool SaveFitsImage();

signals:
    void sigSetSettings();
    void sigCameraStop();
    void sigCameraClose();



};

#endif // CAMERA_AVT_H
