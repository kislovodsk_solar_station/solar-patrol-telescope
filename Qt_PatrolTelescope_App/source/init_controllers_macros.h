#ifndef INIT_CONTROLLERS_MACROS_H
#define INIT_CONTROLLERS_MACROS_H

#include <QString>

//Максимальное число контроллеров
#define N_MAX_CNLR 3

//Функции, которые реализуют контроллеры ШД
#define FUN_NUMBER_ALL      10 /*количество функций*/
#define FUN_ALPHA           0
#define FUN_ALPHA_2_HALF_V  1
#define FUN_DELTA           2
#define FUN_SCAN_COELOSTAT  3
#define FUN_SCAN_SPECTR     4
#define FUN_FOCUS_SLIT      5
#define FUN_FOCUS_SPECTR    6
#define FUN_FOCUS_GUIDE     7
#define FUN_GRATING         8
#define FUN_COLLIMATE       9

//Контроллер не определен или не найден
#define NO_CNLR 255
#define S_NO_CNLR "NO_CNLR"

//Порт для концевиков не определен
#define NO_PORT 255
#define S_NO_PORT "NO_PORT"

#define DEFAULT_FUN_MAX_SPEED 10000
#define DEFAULT_FUN_ACCELER   10000
#define DEFAULT_FUN_MICROSTEP 16

#define PORT_ON_MIN 0
#define PORT_ON_MAX 1

#define ORGANIZATION_NAME   "Kislovodsk Mountain Station of the Pulkovo observatory RAS"
#define ORGANIZATION_DOMAIN "http://www.solarstation.ru/"
#define APPLICATION_NAME    "SM_InitControllers"

//#define config_init_controllers "f:/NEW TELESCOP/Soft/solar-patrol-telescope/config\\SM_Init_Controllers.cfg"

#endif // INIT_CONTROLLERS_MACROS_H

