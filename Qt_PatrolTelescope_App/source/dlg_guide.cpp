#include "dlg_guide.h"
#include "ui_dlg_guide.h"
#include "camera_avt.h"
#include "camera_jai.h"
#include "guide.h"

#include <QSettings>
#include <QImage>
#include <QPainter>
#include <QTimer>
#include <QThread>
#include <QDir>
#include <QDebug>
#include <QMessageBox>

// Поля настроек для работы с конфигурационным файлом
//#define guide_config_name                   "g:/NEW TELESCOP/Soft/solar-patrol-telescope/config/guide.ini"

#define GUIDE_CONFIG_CAM_SETTINGS           "Camera_settings"
#define GUIDE_CONFIG_SETTINGS               "Guide_settings"
#define GUIDE_CONFIG_SAVE_PATH              "Path_to_save"
#define GUIDE_CONFIG_SAVE_TIME              "Guide_save_image_period"
#define GUIDE_CONFIG_GUIDE_TIME             "Guide_time_period"
#define GUIDE_SHIFT_X                       "Shift_X"
#define GUIDE_SHIFT_Y                       "Shift_Y"
//#define GUIDE_CONFIG_ON_SLIT                "Guiding on slit"

// Настройки по-умолчанию (если не найден конфигурационный файл)
#define GUIDE_SAVE_DEFAULT_TIME             10000
#define GUIDE_DEFAULT_TIME                  1000
#define GUIDE_DEFAULT_SAVE_PATH             "'D:'/yyyy/MM/dd/'guide/'"

// Флаг, дающий понять создан ли объект класса
bool Dlg_Guide::isExists = false;

//-------------------------------------------------------------------------------------------------
Dlg_Guide::Dlg_Guide(Dlg_Coelostat *coelostat, QString configDir, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Dlg_Guide),
    p_guide(nullptr),
    p_guideCamera(nullptr),
    pTimer_Guide(nullptr),
    pTimer_SaveImage(nullptr)
{
    setObjectName("Dlg_Guide");
    guide_config_name = configDir + QString("guide.ini");
    ui->setupUi(this);
    ui->groupBox_camera->hide();
    ui->groupBox_guide->hide();
    ui->viewLabel->setObjectName("GuideLabel");

    msecToSave = 0;
    msecToGuide = 0;
    shiftX = 0;
    shiftY = 0;
    saveMode = false;
    guideMode = false;
    hideLines = false;
//    guideOnSlit = false;

    // Создание объектов и перенос их в потоки
    if (!SelectCamera())
    {
        this->setDisabled(true);
        return;
    }
    p_guide = new Guide(coelostat, p_guideCamera);
    guideThread = new QThread(this);
    cameraThread = new QThread(this);
    p_guideCamera->moveToThread(cameraThread);
    p_guide->moveToThread(guideThread);

    pTimer_Guide = new QTimer(this);
    pTimer_SaveImage = new QTimer(this);

    //-----------------------------------------------------------------------------------
    // Соединения для обеспечения работы камеры и гидирования
    //-----------------------------------------------------------------------------------
    // Запуск камеры
    connect(cameraThread, SIGNAL(started()), p_guideCamera, SLOT(Open()));
    // Загрузка параметров
    connect(p_guideCamera, SIGNAL(sigCameraOpened()), this, SLOT(LoadConfig()));
    // Таймер для подсчета смещения солнечного диска
    connect(pTimer_Guide, SIGNAL(timeout()), p_guide, SLOT(Guiding()));
    // Таймер для сохранения изображений
    connect(pTimer_SaveImage, SIGNAL(timeout()), this, SLOT(On_Timer_SaveImage()));
    // Соединение для нахождения координат солнечного диска
    connect(p_guideCamera, SIGNAL(sigImageGot(unsigned char*,int,int)),
                p_guide, SLOT(FindCenter(unsigned char*,int,int)));
    // Вывод изображения
    connect(p_guide, SIGNAL(sigFindCenterFinished(QImage)),
            this, SLOT(ShowImage(QImage)));
    // Установить значения настроек
    connect(this, SIGNAL(sigSetSettings()), p_guideCamera, SLOT(SetParams()));
    // Получение текущей экспозиции при запуске
    connect(p_guideCamera, SIGNAL(sigCameraStarted()),
            this, SLOT(GetExposure()));
    // Установить смещение:
    // в гиде
    connect(ui->spinBox_shiftX, SIGNAL(valueChanged(int)),
            p_guide, SLOT(SetShiftX(int)));
    connect(ui->spinBox_shiftY, SIGNAL(valueChanged(int)),
            p_guide, SLOT(SetShiftY(int)));
    // в диалоге
    connect(ui->spinBox_shiftX, SIGNAL(valueChanged(int)),
            this, SLOT(SetShiftX(int)));
    connect(ui->spinBox_shiftY, SIGNAL(valueChanged(int)),
            this, SLOT(SetShiftY(int)));

    //-----------------------------------------------------------------------------------
    // Кнопки и галочки
    //-----------------------------------------------------------------------------------
    // Кнопка закрытие камеры
    connect(ui->btn_Close, SIGNAL(clicked(bool)), p_guideCamera, SLOT(Close()));
    // Открыть/скрыть меню настроек
    connect(ui->btn_Settings, SIGNAL(clicked(bool)),
            this, SLOT(ShowSettingsPanel(bool)));
    // Отобразить значения настроек по умолчанию
    connect(ui->btn_defaultSettings, SIGNAL(clicked(bool)),
                this, SLOT(DefaultSettings()));
    // Задать значения настроек
    connect(ui->btn_setCamSettings, SIGNAL(clicked(bool)),
                this, SLOT(SetSettings()));
    connect(ui->btn_setGuideSettings, SIGNAL(clicked(bool)),
                this, SLOT(SetGuideSettings()));
    // Установка режима сохранения кадров
    connect (ui->checkBox_saveMode, SIGNAL(toggled(bool)),
             this, SLOT(SetSaveMode(bool)));
    // Включение/выключение режима гидирования
    connect(ui->checkBox_guideMode, SIGNAL(toggled(bool)),
            p_guide, SLOT(SetGuideMode(bool)));
    // Скрыть/показать линии гидирования
    connect(ui->checkBox_hideLines, SIGNAL(toggled(bool)),
            this, SLOT(HideLines(bool)));


    //-----------------------------------------------------------------------------------
    // Связи графических элементов
    //-----------------------------------------------------------------------------------
    // Установка экспозиции
    connect(ui->exp_spinBox, SIGNAL(valueChanged(int)),
            p_guideCamera, SLOT(SetExposure(int)));
    // Соединие ползунка и числового поля между собой
    connect(ui->exposureSlider, SIGNAL(valueChanged(int)),
            ui->exp_spinBox, SLOT(setValue(int)));
    connect(ui->exp_spinBox, SIGNAL(valueChanged(int)),
            ui->exposureSlider, SLOT(setValue(int)));
    // Установка режима автоматической экспозиции
    connect(ui->autoExp_radioBtn, SIGNAL(toggled(bool)),
            p_guideCamera, SLOT(SetAutoExposure(bool)));
    // Скрытие элементов ручного режима при включении автоматического режима экспозиции
    connect(ui->autoExp_radioBtn, SIGNAL(toggled(bool)),
            ui->exposureSlider, SLOT(setDisabled(bool)));
    connect(ui->autoExp_radioBtn, SIGNAL(toggled(bool)),
            ui->exp_spinBox, SLOT(setDisabled(bool)));

    // Запуск потоков и таймеров
    cameraThread->start();
    guideThread->start();
//    pTimer_SaveImage->start(msecToSave);
//        pTimer_Guide->start(msecToGuide);

    SetGuideSettings();

    isExists = true;
}

//-------------------------------------------------------------------------------------------------
Dlg_Guide::~Dlg_Guide()
{
    qDebug() << "~SolarGuideWdgt 0";
    SaveConfig();
    if(pTimer_SaveImage)
        pTimer_SaveImage->stop();

    if(pTimer_Guide)
        pTimer_Guide->stop();

    if (p_guideCamera)
    {
        p_guideCamera->Stop();
        cameraThread->quit();
        cameraThread->wait();
        guideThread->quit();
        guideThread->wait();
        delete p_guideCamera;
    }
    if (p_guide)
        delete p_guide;
    if (ui)
        delete ui;

    isExists = false;
    qDebug() << "~SolarGuideWdgt 1";
}

bool Dlg_Guide::StopShowImage()
{
    qDebug() << "Dlg_Guide: StopShowImage()";
    return disconnect(p_guide, &Guide::sigFindCenterFinished,
               this, &Dlg_Guide::ShowImage);
}

//-----------------------------------------------------------------------------
// Режим гидирования - на щели или нет
//
//bool Dlg_Guide::isGuideOnSlit() { return guideOnSlit; }

//-----------------------------------------------------------------------------
// Выбор камеры из конфигурационного файла
//
bool Dlg_Guide::SelectCamera()
{
    QFile config(guide_config_name);
    if(config.exists())
    {
        config.open(QIODevice::ReadOnly);
        QSettings settings(guide_config_name, QSettings::IniFormat);
        cameraClass = settings.value("Camera").toString();

        if(cameraClass == "JAI")
            p_guideCamera = new Camera_JAI();
        //else if(cameraClass == "AVT")
        //    p_guideCamera = new Camera_AVT();
        else
        {
            QMessageBox::warning(this, "Guide", "Модель камеры не задана!");
            ui->label_status->setText("Камера не задана!");
            return false;
        }
        config.close();
        p_guideCamera->SetCameraModel(settings.value("Model").toString());
    }
    else
    {
        QMessageBox::warning(this, "Guide", "Конфигурационный файл гида не найден!");
        ui->label_status->setText("Конфигурационный файл не найден!");
        return false;
    }
    return true;
}

//-------------------------------------------------------------------------------------------------
// Вывод изображения с камеры фотогида
//
void Dlg_Guide::ShowImage(QImage image)
{
    if(image.isNull())
        return;

    // Создание изображения для вывода
    QPixmap pixImage = QPixmap::fromImage(image);
    int width = pixImage.width();
    int height = pixImage.height();
    //qDebug() << "Dlg_Guide::ShowImage: width, heigth = " << width << ", " << height;

    if(!hideLines)
    {
        // Нанесение линий центра кадра
        QPainter paintInfo(&pixImage);
        QPen pen;
        pen.setColor(Qt::gray);
        pen.setWidth(4);
        paintInfo.setPen(pen);
        paintInfo.drawLine(width / 2 - shiftX, 0, width / 2 - shiftX, height);
        paintInfo.drawLine(0, height / 2 - shiftY, width, height / 2 - shiftY);

        if(!p_guide->findSunError)
        {
            // Нанесение линий найденного центра Солнца
            int x = p_guide->GetPosX();
            int y = p_guide->GetPosY();
            if(p_guide->IsCorrection())
                pen.setColor(Qt::red);
            else
                pen.setColor(Qt::green);
            paintInfo.setPen(pen);
            paintInfo.drawLine(x, 0, x, height);
            paintInfo.drawLine(0, height - y, width, height - y);

            // Вывод диска Солнца
            int R = p_guide->radius;
            paintInfo.drawEllipse(x - R, height - y - R, R * 2, R * 2);
            for (int i=0; i<N_POINT_FIND_CIRC; i++)
                if(p_guide->aSunCirX[i] > 0 && p_guide->aSunCirY[i] > 0)
                  PlotCross(&paintInfo, int(p_guide->aSunCirX[i]), int(p_guide->aSunCirY[i]), 10,  Qt::green);
        }
    }

    // Вывод изображения
//    QCoreApplication::processEvents( QEventLoop::AllEvents, 50 );
    ui->viewLabel->setScaledContents(true);
    ui->viewLabel->setPixmap(pixImage);

    // Вывод координат центра Солнца
    ShowCenterCoordinates();
    update();

    return;
}

//-------------------------------------------------------------------------------------------------
//  Метод отображения найденных координат диска Солнца и его смещения
//
void Dlg_Guide::ShowCenterCoordinates()
{
    //QString str = "Координаты: ";
    QString str;

    str = str + QString("XYc=(%1,%2); ")
            .arg(p_guide->GetPosX()).arg(p_guide->GetPosY());

    str = str + QString("R=%1; ").arg(p_guide->GetMeanRadius());

    //str.append(" Смещение : ");
    str = str + QString("dXYc=(%1,%2);")
            .arg(p_guide->GetMeanOffsetX()).arg(p_guide->GetMeanOffsetY());

    ui->label_status->setText(str);
}

//-------------------------------------------------------------------------------------------------
//  Сохранение изображений с камеры фотогида по таймеру
//
void Dlg_Guide::On_Timer_SaveImage()
{
    if(p_guideCamera->isStarted() == true && saveMode == true)
    {
        // Задание имени для папки на диске
        QString dirName = QDateTime::currentDateTimeUtc().toString(savePath);

        // Создание папки
        QDir dir;
        if(dir.exists(dirName) == false)
            if(dir.mkpath(dirName))
                if(dir.exists(dirName))
                    qDebug("Папка создана");

        // Задание имени для сохраняемого изображения
        QString nameFormat = "HHmmss.jpg";
        QString fileName = QDateTime::currentDateTimeUtc().toString(nameFormat);

        // Сохранение изображения
        //p_guideCamera->GetImage().save(dirName.append(fileName),"JPG");
        if(p_guideCamera->GetImageBuf() != NULL)
        {
            QImage img(p_guideCamera->GetImageBuf(),p_guideCamera->GetFrameWidth(),p_guideCamera->GetFrameHeight(),
                               QImage::Format_Grayscale8,0,0);
            img.save(dirName.append(fileName),"JPG");
        }
        else
            ui->label_status->setText("Изображение не получено");
    }
}

//-------------------------------------------------------------------------------------------------
// Отображение панели настроек
//
void Dlg_Guide::ShowSettingsPanel(bool b)
{
    // Отобразить настройки
    if ( b )
    {
        ui->groupBox_camera->show();
        ui->groupBox_guide->show();
        this->parentWidget()->setGeometry( parentWidget()->x(),
                                           parentWidget()->y(),
                                           parentWidget()->width() + 270,
                                           parentWidget()->height());
    }
    // Скрыть настройки
    else
    {
        ui->groupBox_camera->hide();
        ui->groupBox_guide->hide();
        this->parentWidget()->setGeometry( parentWidget()->x(),
                                           parentWidget()->y(),
                                           parentWidget()->width() - 270,
                                           parentWidget()->height());
    }
}

//-------------------------------------------------------------------------------------------------
// Получение текущей экспозиции
//
void Dlg_Guide::GetExposure()
{
    ui->exp_spinBox->setValue(p_guideCamera->GetExposure());
}

//-------------------------------------------------------------------------------------------------
// Загрузка настроек из конфигурационного файла
//
bool Dlg_Guide::LoadConfig()
{
    if (!p_guideCamera)
    {
        QMessageBox::warning(this, "Guide", "Камера не найдена!");
        return false;
    }
    if (!p_guideCamera->isOpened())
    {
        QMessageBox::warning(this, "Guide", "Камера не открыта!");
        return false;
    }

    QFile config(guide_config_name);
    if(config.exists())
    {
        config.open(QIODevice::ReadOnly);
        LoadSettings();
        SetSettings();
        config.close();
        return true;
    }
    else
    {
        msecToSave = GUIDE_SAVE_DEFAULT_TIME;
        msecToGuide = GUIDE_DEFAULT_TIME;
        ui->spinBox_timerSave->setValue(msecToSave);
        ui->spinBox_timerGuide->setValue(msecToGuide);
        DefaultSettings();
        SetSettings();
        QMessageBox::warning(this, "Guide", "Конфигурационный файл гида не найден - параметры по умолчанию!");
        return true;
    }
}

//-------------------------------------------------------------------------------------------------
// Сохранение настроек в конфигурационный файл
//
void Dlg_Guide::SaveConfig()
{
    if (!p_guideCamera)
        return;

    QFile config(guide_config_name);
    if(config.exists())
        config.remove();
    QSettings settings(guide_config_name, QSettings::IniFormat);
    settings.setValue("Type", "Configuration file");
    settings.setValue("Module", "Guide");
    settings.setValue("Camera", cameraClass);
    settings.setValue("Model", p_guideCamera->parameters.cameraName);

    settings.beginGroup(GUIDE_CONFIG_CAM_SETTINGS);
    settings.setValue(NODE_NAME_EXPOSURE_AUTO, ui->autoExp_radioBtn->isChecked());
    settings.setValue(NODE_NAME_EXPOSURE, ui->exp_spinBox->value());
    settings.setValue(NODE_NAME_FPS, ui->spinBox_FPS->value());
    settings.setValue(NODE_NAME_WIDTH, ui->spinBox_width->value());
    settings.setValue(NODE_NAME_HEIGHT, ui->spinBox_height->value());
    settings.setValue(NODE_NAME_BINNING_X, ui->spinBox_binning_x->value());
    settings.setValue(NODE_NAME_BINNING_Y, ui->spinBox_binning_y->value());
    settings.setValue(NODE_NAME_OFFSET_X, ui->spinBox_offset_x->value());
    settings.setValue(NODE_NAME_OFFSET_Y, ui->spinBox_offset_y->value());
    settings.setValue(NODE_NAME_GAIN, ui->spinBox_gain->value());
    settings.endGroup();

    settings.beginGroup(GUIDE_CONFIG_SETTINGS);
    //settings.setValue(GUIDE_CONFIG_ON_SLIT, guideOnSlit);
    settings.setValue(GUIDE_CONFIG_SAVE_TIME, ui->spinBox_timerSave->value());
    settings.setValue(GUIDE_CONFIG_GUIDE_TIME, ui->spinBox_timerGuide->value());
    if(savePath == "")
        savePath = GUIDE_DEFAULT_SAVE_PATH;
    settings.setValue(GUIDE_SHIFT_X, ui->spinBox_shiftX->value());
    settings.setValue(GUIDE_SHIFT_Y, ui->spinBox_shiftY->value());
    settings.setValue(GUIDE_CONFIG_SAVE_PATH, savePath);
    settings.endGroup();
    config.close();
}

//-------------------------------------------------------------------------------------------------
// Отображение настроек из конфигурационного файла в форму (панель настроек)
//
void Dlg_Guide::LoadSettings()
{
    QSettings settings(guide_config_name, QSettings::IniFormat);
    // Загрузка настроек камеры
    settings.beginGroup(GUIDE_CONFIG_CAM_SETTINGS);
    ui->spinBox_FPS->setValue(settings.value(NODE_NAME_FPS).toDouble());
    ui->spinBox_width->setValue(settings.value(NODE_NAME_WIDTH).toInt());
    ui->spinBox_height->setValue(settings.value(NODE_NAME_HEIGHT).toInt());
    ui->spinBox_binning_x->setValue(settings.value(NODE_NAME_BINNING_X).toInt());
    ui->spinBox_binning_y->setValue(settings.value(NODE_NAME_BINNING_Y).toInt());
    ui->spinBox_offset_x->setValue(settings.value(NODE_NAME_OFFSET_X).toInt());
    ui->spinBox_offset_y->setValue(settings.value(NODE_NAME_OFFSET_Y).toInt());
    ui->exp_spinBox->setValue(settings.value(NODE_NAME_EXPOSURE).toInt());
    ui->spinBox_gain->setValue(settings.value(NODE_NAME_GAIN).toDouble());
    if(settings.value(NODE_NAME_EXPOSURE_AUTO).toBool() == true){
        ui->autoExp_radioBtn->setChecked(true);
        p_guideCamera->SetAutoExposure(true);
    }
    else
    {
        ui->manualExp_radioBtn->setChecked(true);
        p_guideCamera->SetAutoExposure(false);
    }
    settings.endGroup();

    // Загрузка настроек гида
    settings.beginGroup(GUIDE_CONFIG_SETTINGS);

    //guideOnSlit = settings.value(GUIDE_CONFIG_ON_SLIT).toBool();
    msecToSave = settings.value(GUIDE_CONFIG_SAVE_TIME).toInt();
    if(msecToSave == 0)
        msecToSave = GUIDE_SAVE_DEFAULT_TIME;
    ui->spinBox_timerSave->setValue(msecToSave);

    msecToGuide = settings.value(GUIDE_CONFIG_GUIDE_TIME).toInt();
    if(msecToGuide == 0)
        msecToGuide = GUIDE_DEFAULT_TIME;
    ui->spinBox_timerGuide->setValue(msecToGuide);

    shiftX = settings.value(GUIDE_SHIFT_X).toInt();
    ui->spinBox_shiftX->setValue(shiftX);

    shiftY = settings.value(GUIDE_SHIFT_Y).toInt();
    ui->spinBox_shiftY->setValue(shiftY);

    savePath = settings.value(GUIDE_CONFIG_SAVE_PATH).toString();
    if(savePath == "")
        savePath = GUIDE_DEFAULT_SAVE_PATH;

    settings.endGroup();
}

//-------------------------------------------------------------------------------------------------
// Отображение настроек по-умолчанию
//
void Dlg_Guide::DefaultSettings()
{
    ui->spinBox_FPS->setValue(DEFAULT_FPS);
    ui->spinBox_binning_x->setValue(DEFAULT_BINNING_X);
    ui->spinBox_binning_y->setValue(DEFAULT_BINNING_Y);
    ui->spinBox_offset_y->setValue(DEFAULT_OFFSET_Y);
    ui->spinBox_offset_x->setValue(DEFAULT_OFFSET_X);
    ui->spinBox_width->setValue(DEFAULT_WIDTH);
    ui->spinBox_height->setValue(DEFAULT_HEIGHT);
}

//-------------------------------------------------------------------------------------------------
// Применить настройки камеры
//
void Dlg_Guide::SetSettings()
{
    p_guideCamera->parameters.fps = ui->spinBox_FPS->value();
    p_guideCamera->parameters.width = ui->spinBox_width->value();
    p_guideCamera->parameters.height = ui->spinBox_height->value();
    p_guideCamera->parameters.binningX = ui->spinBox_binning_x->value();
    p_guideCamera->parameters.binningY = ui->spinBox_binning_y->value();
    p_guideCamera->parameters.offsetX = ui->spinBox_offset_x->value();
    p_guideCamera->parameters.offsetY = ui->spinBox_offset_y->value();
    p_guideCamera->parameters.exposure = ui->exp_spinBox->value();
    p_guideCamera->parameters.gain = ui->spinBox_gain->value();
    emit sigSetSettings();
    SaveConfig();
}

//-------------------------------------------------------------------------------------------------
// Применить настройки гида
//
void Dlg_Guide::SetGuideSettings()
{
    pTimer_Guide->stop();
    pTimer_SaveImage->stop();
    msecToSave = ui->spinBox_timerSave->value();
    msecToGuide = ui->spinBox_timerGuide->value();
    pTimer_SaveImage->start(msecToSave);
    pTimer_Guide->start(msecToGuide);
    //qDebug() << "pTimer_Guide->start(msecToGuide)->msecToGuide=" << msecToGuide;
}

//-------------------------------------------------------------------------------------------------
// Включение режима сохранения изображений
//
void Dlg_Guide::SetSaveMode(bool b)
{
    saveMode = b;
    On_Timer_SaveImage();
}

//-------------------------------------------------------------------------------------------------
// Сокрытие/отображение линий гидирования
//
void Dlg_Guide::HideLines(bool b)
{
    hideLines = b;
}

//---------------------------------------------------------------------------------------
//  Метод перезапуска таймера гида с обнулением счетчиков и усредненных смещений
//
void Dlg_Guide::GuideRestart()
{
    if (p_guide->isGuiding())
    {
        qDebug() << "On Dlg_Guide::GuideRestart()";
        pTimer_Guide->stop();
        bool inProcess = 0;
        while (inProcess = p_guide->GetInProcess() == true)
        {
            QApplication::processEvents(QEventLoop::AllEvents, 100);
            qDebug() << "GuideRestart: GetInProcess = " << inProcess;
        };
//        p_guide->OnNullAll_OffsetСounter(); // Обнуляем счетчики и смещения
        //Останавливаем сканирование до определения ошибки положения Солнца по гиду
        emit p_guide->sigFindSunError(true);
        p_guide->CalcSolarShift();
        pTimer_Guide->start(msecToGuide);
    }

}

void Dlg_Guide::SetGuideCheckBox(bool b)
{
    ui->checkBox_guideMode->setChecked(b);
}

void Dlg_Guide::SetShiftX(int x) { shiftX = x; }

void Dlg_Guide::SetShiftY(int y) { shiftY = y; }
