#include <QTimer>
#include "Dlg_Coelostat_Alpha_GoTo.h"
#include "ui_Dlg_Coelostat_Alpha_GoTo.h"

Dlg_Coelostat_Alpha_GoTo::Dlg_Coelostat_Alpha_GoTo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dlg_Coelostat_Alpha_GoTo)
{
    setObjectName("Dlg_Coelostat_AlphaGoTo");
    ui->setupUi(this);

    QTimer::singleShot(0, this, SLOT(slotDelayedInit()));
//    connect(ui->spinBox_hh, SIGNAL(valueChanged(int)), ui->spinBox_hh, SLOT(Value(int)));

}
void Dlg_Coelostat_Alpha_GoTo::slotDelayedInit()
{
    m_tDataTime_cur = m_tDataTime_old;
    SetDlgTime();//Установка времени в диалоге по m_tDataTime_cur
    m_On_Cur_Time = false; //Работаем в режиме поиска положения по текущему времени
    ui->Lbl_time_sunset->setText(m_dlg_tDataTime_SunSet.toString("hh:mm"));//для просмотра в QString
    ui->chkParkSunSet->setChecked(m_dlg_bool_ParkSunSet);
}

//Установка времени в диалоге по m_tSysPos
void Dlg_Coelostat_Alpha_GoTo::SetDlgTime()
{
    int n;
    //sTmp=m_tDataTime_cur.toString("hh:mm:ss"); //для просмотра в QString
    n = m_tDataTime_cur.time().hour();
    if (n < 10) ui->spinBox_hh->setPrefix("0"); else ui->spinBox_hh->setPrefix("");
    ui->spinBox_hh->setValue(n);

    n = m_tDataTime_cur.time().minute();
    if (n < 10) ui->spinBox_mm->setPrefix("0"); else ui->spinBox_mm->setPrefix("");
    ui->spinBox_mm->setValue(n);

    n = m_tDataTime_cur.time().second();
    if (n < 10) ui->spinBox_ss->setPrefix("0"); else ui->spinBox_ss->setPrefix("");
    ui->spinBox_ss->setValue(n);
}


Dlg_Coelostat_Alpha_GoTo::~Dlg_Coelostat_Alpha_GoTo()
{
    delete ui;
}


void Dlg_Coelostat_Alpha_GoTo::on_spinBox_hh_valueChanged(int arg1)
{
//    int hh=m_tDataTime_cur.time().hour();
    int mm=m_tDataTime_cur.time().minute();
    int ss=m_tDataTime_cur.time().second();
    m_tDataTime_cur.setTime(QTime(arg1, mm, ss));
    SetDlgTime();//Установка времени в диалоге по m_tDataTime_cur
}

void Dlg_Coelostat_Alpha_GoTo::on_spinBox_mm_valueChanged(int arg1)
{
        int hh=m_tDataTime_cur.time().hour();
//        int mm=m_tDataTime_cur.time().minute();
        int ss=m_tDataTime_cur.time().second();
        m_tDataTime_cur.setTime(QTime(hh, arg1, ss));
        SetDlgTime();//Установка времени в диалоге по m_tDataTime_cur

}

void Dlg_Coelostat_Alpha_GoTo::on_spinBox_ss_valueChanged(int arg1)
{
//        int arg1=ui->spinBox_hh->value();
        int hh=m_tDataTime_cur.time().hour();
        int mm=m_tDataTime_cur.time().minute();
//        int ss=m_tDataTime_cur.time().second();
        m_tDataTime_cur.setTime(QTime(hh, mm, arg1));
        SetDlgTime();//Установка времени в диалоге по m_tDataTime_cur
}

void Dlg_Coelostat_Alpha_GoTo::on_Btn_Cur_Pos_clicked()
{
    m_tDataTime_cur = m_tDataTime_old;
    SetDlgTime();//Установка времени в диалоге по m_tDataTime_cur
    m_On_Cur_Time = false; //Работаем в режиме поиска положения по текущему времени
}

void Dlg_Coelostat_Alpha_GoTo::on_Btn_Cur_Time_clicked()
{
    m_tDataTime_cur = QDateTime::currentDateTime();
    SetDlgTime();//Установка времени в диалоге по m_tDataTime_cur
    m_On_Cur_Time = true; //Работаем в режиме поиска положения по текущему времени
}

void Dlg_Coelostat_Alpha_GoTo::on_Btn_Parking_clicked()
{
    int hh = PARKING_HOUR;
    int mm = 0;
    int ss = 0;
    m_tDataTime_cur.setTime(QTime(hh, mm, ss));
    SetDlgTime();//Установка времени в диалоге по m_tDataTime_cur
    m_On_Cur_Time = false; //Работаем в режиме поиска положения по текущему времени
}

void Dlg_Coelostat_Alpha_GoTo::on_chkParkSunSet_clicked(bool checked)
{
    m_dlg_bool_ParkSunSet = checked;
}
