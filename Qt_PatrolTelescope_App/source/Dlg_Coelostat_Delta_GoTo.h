#ifndef Dlg_Coelostat_DELTA_GOTO_H
#define Dlg_Coelostat_DELTA_GOTO_H

#include <QDialog>
#include <QDateTime>

namespace Ui {
class Dlg_Coelostat_Delta_GoTo;
}

class Dlg_Coelostat_Delta_GoTo : public QDialog
{
    Q_OBJECT

public:
    double m_fAngle_Delta_Go_cur;
    double m_fAngle_Delta_Go_old;
    bool m_bool_Delta_On_Go_Cur_Time; //Работаем в режиме поиска положения по текущему времени
    bool m_bool_Delta_Centering;      //Работаем в режиме поиска центрирования дельты от концевиков
    bool m_OnStartDialog;             //Запустили полностью диалог (прошли все инициализации)

public:
    void SetDlgAngle();  //Установка угла в диалоге по m_tSysPos

public:
    explicit Dlg_Coelostat_Delta_GoTo(QWidget *parent = 0);
    ~Dlg_Coelostat_Delta_GoTo();

private slots:
    void slotDelayedInit();
    void on_Btn_Cur_Time_clicked();
    void on_Btn_Cur_Pos_clicked();

    void on_spinBox_hh_valueChanged(int arg1);

    void on_spinBox_mm_valueChanged(int arg1);

    void on_spinBox_ss_valueChanged(int arg1);

    void on_cmbZnak_currentIndexChanged(int index);

    void on_Btn_Centering_clicked();

private:
    Ui::Dlg_Coelostat_Delta_GoTo *ui;
};

#endif // Dlg_Coelostat_DELTA_GOTO_H
