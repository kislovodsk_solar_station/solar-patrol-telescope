//---------------------------------------------------------------------------------------
//  SolarSpectraWdgt - класс графического интерфейса - окна вывода спектра,
//                  объединяющий классы и функции для работы с камерой
//  Разработка: Чернов Ярослав
//---------------------------------------------------------------------------------------

#ifndef DLG_SPECTRA_H
#define DLG_SPECTRA_H


#include <stdint.h>                         /* uint16_t */
#include <QWidget>
class Camera;

namespace Ui {
class Dlg_Spectra;
}

class Dlg_Spectra : public QWidget
{
    Q_OBJECT

public:
    explicit Dlg_Spectra(QString configDir, QWidget *parent = 0);
    ~Dlg_Spectra();
    void StopThreads();
    void StopTimers();

    Camera *pSpectralCamera;
    static bool isExists;

private:
    Ui::Dlg_Spectra *ui;
    QThread *p_cameraThread;
    QTimer *pTimer_ShowImage;
    bool saveMode;
    QString config_spectral_cam;
    QString config_spectral_cam_2;
    QString cameraClass;
    QString configName;
    static int nCameras;

private slots:
    Camera* SelectCamera();
    void LoadConfig();
    void SaveConfig();
    void LoadSettings();
    void SetSettings();
    void DefaultSettings();
    void ShowSettingsPanel(bool b);
    void GetExposure();
    void StartTimers();
    void ShowStatusMessage(QString strMsg);
    void ShowWarningMessage(QString strMsg);
    void On_Timer_ShowImage();
    void On_Timer_SaveImage();
    void ChangeExposureMode(bool autoMode);
    void SetSaveMode(bool b);

signals:
    void sigSetSettings();
};

#endif // DLG_SPECTRA_H
