  //-------------------------------------------------------------------------------------------------
//  Класс, реализующий сканирование
//          (движение контроллера и сохранение изображений с камеры)
//  Разработка: Шрамко Андрей, Чернов Ярослав
//-------------------------------------------------------------------------------------------------

#ifndef SCANNER_H
#define SCANNER_H
#define _WINSOCKAPI_

//#include "camera.h"
//#include "QSM_InitControllers.h"
#include <QObject>

class QTimer;
class QTime;
class QProcess;

class Camera;
class CInitControllers;
class CControllerEthernet;

#define SCANNER_SCAN_DIRECTORY              "F:/yyyyMMdd_HHmmss/"
#define SCANNER_SAVE_DIRECTORY              "D:/yyyy/MM/dd/yyyyMMdd_HHmmss/"
#define SCANNER_STOP            5            //-------------------------------------------------
#define SCANNER_TO_START        1            //
#define SCANNER_ON              2            //  Режимы сканирования
#define SCANNER_TO_FINISH       3            //
#define SCANNER_TO_NEXT         4            //-------------------------------------------------
#define SCANNER_TIMER_MSEC      30           //  Время проверки позиций контроллеров
#define SCANNER_K_MAX_SPEED     0.5f         //  Коэффициент для максимальной скорости
#define SCANNER_STEP_TO_ARCSEC  43.85        //  Коэффициент перевода шагов в угловые секунды
#define SCANNER_RADIUS_ARCSEC   1800         //  Радиус сканирования (в угловых секундах)
#define SCANNER_SCAN_TIME       40000        //  Время сканирования по-умолчанию (в миллисекундах)
#define SCANNER_FLIP_RENAMEFILE 0            /*  Переименование файлов в директории сканирования для зеркального отражения в изображении 1-да, 0-нет*/


//-------------------------------------------------------------------------------------------------
// Структура с данными о сканировании
//
struct ScanSettings_t
{
    int msecToScan;                 // Время движения сканирования
    double kArcsec2Steps;           // Коэффициент перевода шагов в угловые секунды
    int arcsecScanRadius;           // Радиус сканирования (в угловых секундах)
    char scanMode;                  // Режим сканирования
    QString scanPath;               // Путь сохранения файлов во время сканирования
    QString savePath;               // Путь сохранения файлов после обработки
    QString programCreateImagePath; // Путь к программе сборки изображения
    QString programUploadImagePath; // Путь к программе загрузки на сайт
    bool autoCreateImage;           // Автоматическая сборка
    bool convertToJpeg;             // Конвертация в JPEG
    QString dopplerMode;            // Режим сборки (простая или допплерограмма)
    bool autoUpload;                // Загрузка на FTP
    bool isAlreadyCreatedUploaded;  // Была ли вызвана сборка и загрузка для данного скана
    QString core_name_ftp;          // Имя файла в центре спектральной линии при загрузке на FTP
    QString doppler_name_ftp;       // Имя файла допплера при загрузке на FTP

    ScanSettings_t()
    {
        arcsecScanRadius = SCANNER_RADIUS_ARCSEC;
        kArcsec2Steps = SCANNER_STEP_TO_ARCSEC;
        msecToScan = SCANNER_SCAN_TIME;
        scanMode = SCANNER_STOP;
        autoCreateImage = false;
        convertToJpeg = false;
        autoUpload = false;
        isAlreadyCreatedUploaded = false;
        dopplerMode = "-c";
        core_name_ftp = "'hal4.jpg'";
        doppler_name_ftp = "'hal3.jpg'";
    }
};

//-------------------------------------------------------------------------------------------------
//  Класс, реализующий сканирование
//
class Scanner : public QObject
{
    Q_OBJECT

public:
    explicit Scanner(QObject *parent = 0);
    ~Scanner();

    ScanSettings_t scanSettings;

    bool isScanning();
    bool isSaving();
    bool isScanWasBreaked();
    bool SetCamera(Camera *camera);
    bool SetMotorController(CInitControllers *motorControllers);


public slots:
    void Start();
    void Stop();
    void SetAutoCreateImage(bool b);
    void SetConvertToJpeg(bool b);
    bool StartSaveCamera();
    void StopSaveCamera();
    void SetNextScanPause(bool needPause);
    void SetScanPause();
    void SetDopplerMode(bool doppler);
    void SetUploadMode(bool upload);
    void DeleteScanner();
    void StartCreateImageProgram(QString pathToScan, QString pathToSave);
    void StartUploadProgram(const QString &pathToImage);
    void DeleteScan();
    void ClearDisk();

private slots:
    void on_Timer_Scan();
    void on_Proc_Finished(int code);

private:
    Camera* pCamera;                        // Указатель на камеру
    Camera* pCamera2;                       // Указатель на вторую камеру
    bool isFirstCameraExist;                // Была ли попытка инициализации первой камеры
    CInitControllers *pInitControllers;     // Указатель на диалог для работы с контроллером
    CControllerEthernet *pMotorController;  // Указатель на контроллер
    QTimer *pScanTimer;                     // Таймер для проверки позиционирования сканирования
    QProcess* processSolarImage;            // Процесс для запуска программы сборки полнодискового изображения
    QProcess* processUpload;                // Процесс для загрузки на сайт
    unsigned char scanAxis;                 // Рабочая ось контроллера для внутренного сканирования
    unsigned long maxSpeed;                 // Максимальная скорость оси контроллера
    unsigned long accelerSpeed;             // Значение ускорения оси контроллера
    QString response;                       // Строка для хранения ответа
    int counter;                            // Счетчик прогресса сканирования
    bool cameraSaving;                      // Флаг на сохранение с камеры
    bool motorMax;                          // Флаг концевого датчика МАКСИМУМ
    bool motorMin;                          // Флаг концевого датчика МИНИМУМ
    bool scanning;                          // Флаг сканирования (запущено ли в данный момент)
    bool scanPause;                         // Флаг для приостановки сканирования
    bool scanWasBreaked;                    // Было ли прервано сканирование
    bool guideOnSlit;
    int nScans;                             // Количество циклов сканирования
    float currentKSpeed;                    // Значение коэфициента текущей скорости к максимальной
    float startPosition;                    // Положение сканера в начале сканирования
    QString currentScanPath;                // Путь для сохранения текущего скана
    QString currentSavePath;                // Путь для переноса скана после обработки
    QString previousScanPath;               // Путь сохранения предыдущего скана
    QString previousSavePath;               // Путь для переноса предыдущего скана
    bool procImageCreateFinished;           // Флаг завершения обработки
    unsigned debug_counter;                 // Счетчик для подсчета времени работы программы сборки
    unsigned timeForWaitSolarImageFinished; // Время ожидания завершения программы сборки, по истечении которого процесс убивается


    void GoToStart(double position);
    void GoScan(double position);
    bool GoToFinish(double position);
    void GoToStop(double position);
    double ErrorSteps(double speed);
    double CalcScanSpeed();
    void MaxMinCheck();
    QString CreateDir(QString path);
    bool ExistsScanPath();
    void CopyFiles(QString source, QString dest);
    void DeleteFiles(QString scanPath);
    bool ReadyToScan();
    void RenameFiles_OnFlip();                 //  Переименование файлов в директории сканирования для зеркального отражения в изображении


signals:
    void sigStartCameraScan(bool scanMode);
    void sigScanWasStarted(bool b);
    void sigStatusMessage(QString);
    void sigStatusMessage(int);
    void sigWarningMessage(QString);
    void sigProgressBarCounter(int);
    void sigGuideRestart();
    void sigStartTime(QString);
    void sigCurrentTime(QString);
    void sigFinishTime();
};

#endif // SCANNER_H
