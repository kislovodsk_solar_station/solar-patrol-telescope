//-------------------------------------------------------------------------------------------------
//  SolarGuideWdgt - класс графического интерфейса - окна фотогида,
//                  объединяющий классы и функции для работы камеры фотогида
//  Разработка: Чернов Ярослав, Максим Стрелков
//-------------------------------------------------------------------------------------------------

#ifndef DLG_GUIDE_H
#define DLG_GUIDE_H
#define _WINSOCKAPI_

#include <QWidget>
class Camera_AVT;
class Camera_JAI;
class Guide;
class Dlg_Coelostat;

namespace Ui {
class Dlg_Guide;
}

class Dlg_Guide : public QWidget
{
    Q_OBJECT

public:
    explicit Dlg_Guide(Dlg_Coelostat *coelostat, QString configDir, QWidget *parent = 0);
    ~Dlg_Guide();

    static bool isExists;
    Guide* p_guide;
    Camera_JAI* p_guideCamera;

    bool StopShowImage();
//    bool isGuideOnSlit();

private:
    Ui::Dlg_Guide *ui;
    QString guide_config_name;
    QThread *cameraThread;
    QThread* guideThread;
    QTimer* pTimer_SaveImage;
    QTimer* pTimer_Guide;
    QString savePath;
    int msecToSave;                                 // Время отработки таймера для сохранения изображения
    int msecToGuide;                                // Время отработки таймера для подсчета смещения
    int shiftX;                                     // Смещение центра Солнца по горизонтали
    int shiftY;                                     // Смещение центра Солнца по вертикали
    bool saveMode;                                  // Флаг активации режима сохранения кадров
    bool guideMode;                                 // Флаг активации режима гидирования
    bool hideLines;                                 // Флаг сокрытия/отображения линий гидирования
    QString cameraClass;
//    bool guideOnSlit;                              // Тип гида - на щели (true) или до щели (false)

    void SaveConfig();
    void LoadSettings();

private slots:
    bool SelectCamera();
    bool LoadConfig();
    void ShowImage(QImage image);
    void ShowCenterCoordinates();
    void On_Timer_SaveImage();
    void DefaultSettings();
    void SetSettings();
    void SetGuideSettings();
    void ShowSettingsPanel(bool b);
    void GetExposure(); 
    void SetSaveMode(bool b);
    void HideLines(bool b);
    void GuideRestart();   //  Метод перезапуска таймера гида с обнулением счетчиков и усредненных смещений
    void SetGuideCheckBox(bool b);
    void SetShiftX(int x);
    void SetShiftY(int y);

signals:
    void sigSetSettings();
    void sigStopCamera();
};

#endif // DLG_GUIDE_H
