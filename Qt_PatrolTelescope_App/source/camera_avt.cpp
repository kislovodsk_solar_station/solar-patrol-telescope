#include "camera_avt.h"
#include <memory>

#include <io.h>                             /* _access */
#include <fitsio.h>                         /* fits_create_file, fits_create_img, ... */
#include <stdint.h>                         /* uint16_t */
#include <time.h>                           /* time, difftime */
#include <QDebug>
#include <QCoreApplication>
#include <QDateTime>

#define AVT_MAX_CAMERA_COUNT 2

Camera_AVT::Camera_AVT(QObject *parent) :
    Camera(parent),
    frameSize(0),
    _tCamera(new tCamera),
    closed(false)
{
    setObjectName("AVT_Camera");
    connect(this, &Camera_AVT::sigSetSettings,
            this, &Camera_AVT::SetParams);
    connect(this, &Camera_AVT::sigCameraStop,
            this, &Camera_AVT::Stop);
    connect(this, &Camera_AVT::sigCameraClose,
            this, &Camera_AVT::Close);

}

//------------------------------------------------------------------------------------
// Деструктор. Очищается буфер с изображением.
//
Camera_AVT::~Camera_AVT()
{
    qDebug() << "Camera_AVT::~Camera_AVT()";
    closed = true;
    Stop();
    Close();
    if (_tCamera)
        delete _tCamera;
}

//-------------------------------------------------------------------------------------------------
// Запуск камеры
//
bool Camera_AVT::Open()
{
    qDebug() << "Open!";
    if(opened)
        return false;
    if(!Initialize())
    {
        qDebug() << "can't initialize camera!";
        return false;
    }
    if(!Setup())
    {
        qDebug() << "can't setup camera!";
        return false;
    }
    opened = true;
    emit sigCameraOpened();
//    qDebug() << "sigCameraOpened!";
    return true;
}

//-------------------------------------------------------------------------------------------------
// Запуск получения изображений с камеры
//
bool Camera_AVT::Start()
{
    if(!opened || started)
        return false;

//    qDebug() << "Start(): started =  " << started;
    buf8bit = new uchar[_tCamera->Frame.ImageBufferSize];

    // Старт захвата изображения
    if((errCode = PvCaptureStart(_tCamera->Handle)) != ePvErrSuccess)
    {
        PvCaptureEnd(_tCamera->Handle);
        delete [] buf8bit;
        qDebug() << "CameraStart: PvCaptureStart err:";
        emit sigStatusMessage("CameraStart: PvCaptureStart err:");
        return false;
    }

    // Создание очереди фреймов

    if((errCode = PvCaptureQueueFrame(_tCamera->Handle,&(_tCamera->Frame),NULL)) != ePvErrSuccess)
    {
        // Очистка очереди
        PvCaptureQueueClear(_tCamera->Handle);
        // Остановка захвата изображения
        PvCaptureEnd(_tCamera->Handle);
        delete [] buf8bit;
        qDebug() << "CameraStart: PvCaptureQueueFrame err:";
        emit sigStatusMessage("CameraStart: PvCaptureQueueFrame err:");
        return false;
    }
    started = true;
    emit sigCameraStarted();
//    qDebug() << "Start(): started = " << started;

    // Запуск цикла получения изображений
    WorkLoop();

    return true;
}

//-------------------------------------------------------------------------------------------------
// Рабочий цикл камеры (постоянное получение изображений)
//
void Camera_AVT::WorkLoop()
{
    // Запуск цикла получения изображений на камеру
    while(started && opened)
    {
        QCoreApplication::processEvents( QEventLoop::AllEvents, 10 );

        unsigned i = 0;
        tPvErr retCode = CreateImage(i);

        switch (retCode)
        {
            case ePvErrSuccess:
                break;
            case ePvErrUnplugged:
            {
                emit sigStatusMessage("Камера была отключена!");
                qDebug() << "AVT_Camera:: can't CreateImage: i = " << i;
                emit sigCameraReset();
                emit sigCameraStop();
                emit sigCameraClose();
                return;
            }
            case ePvErrBadHandle:
            {
                emit sigStatusMessage("Camera handle is invalid!");
                qDebug() << "Camera handle is invalid!";
                return;
            }
            case ePvErrCancelled:
            {
                emit sigStatusMessage("Камера была остановлена!");
                qDebug() << "Camera_AVT: Камера была остановлена!";
                return;
            }
            default:
            {
                qDebug() << "AVT_Camera:: can't CreateImage: camera restart! i = " << i << "retCode = " << retCode;
                emit sigCameraReset();
//                Restart();
                Reset();
                return;
            }
        }

        if (saving)
            //qDebug() << "create image " << started;
            if(SaveImage() == 2)
            {
                qDebug() << "AVT_Camera:: sigScanStop";
                emit sigCameraReset();
                Reset();
                return;
            }
    }
}

//-------------------------------------------------------------------------------------------------
// Остановка получения изображений с камеры
//
bool Camera_AVT::Stop()
{

    if(!started)
        return true;

    emit sigCameraReset();
    bool retCode = true;
    started = false;

    // Остановка съемки
    emit sigStatusMessage("Камера остановлена");
    delete [] buf8bit;

    if ((errCode = PvCommandRun(_tCamera->Handle, "AcquisitionStop")) != ePvErrSuccess)
    {
//        emit sigWarningMessage("AcquisitionStop command err: ");
        qDebug() << "AcquisitionStop command errCode = " << errCode;
        retCode *= false;
    }

    // Очистка очереди
    if ((errCode = PvCaptureQueueClear(_tCamera->Handle)) != ePvErrSuccess)
    {
//        emit sigWarningMessage("PvCaptureQueueClear err: ");
        qDebug() << "PvCaptureQueueClear err = " << errCode;
        retCode *= false;
    }

    // Остановка захвата кадров
    if ((errCode = PvCaptureEnd(_tCamera->Handle)) != ePvErrSuccess)
    {
//        emit sigWarningMessage("PvCaptureEnd err: ");
        qDebug() << "PvCaptureEnd err = " << errCode;
        retCode *= false;
    }
    return retCode;
}

//-------------------------------------------------------------------------------------------------
// Завершение работы камеры
//
bool Camera_AVT::Close()
{
    bool retCode = true;
    if(!opened)
        return true;

    // Завершение работы камеры
    if ((errCode = PvCameraClose(_tCamera->Handle)) != ePvErrSuccess)
    {
//        emit sigWarningMessage("CameraUnSetup: PvCameraClose err: ");
        qDebug() << "CameraUnSetup: PvCameraClose err = " << errCode;
        retCode = false;
    }

    // Очистка буфера
    delete[](char*)_tCamera->Frame.ImageBuffer;
    if(cameraList)
        delete[] cameraList;

    // Отмена инициализации
    PvUnInitialize();

    opened = false;

    emit sigStatusMessage((char*)"Камера закрыта");
    qDebug() << "Камера закрыта";
    return retCode;
}

void Camera_AVT::Restart()
{
    emit sigStatusMessage((char*)"Перезапуск камеры");
    saving = false;
    frameReady = true;
    emit sigSetSettings();
    if (scanWasStarted)
        emit sigScanStart();
}

bool Camera_AVT::Reset()
{
    emit sigStatusMessage((char*)"Перезагрузка камеры");
    saving = false;
    frameReady = true;
    Stop();
    Close();

    if (!Open())
    {
        qDebug() << "Camera_AVT: can't open camera! Scan stop!";
        return false;
    }
    if (scanWasStarted)
        emit sigScanStart();
//    qDebug() << "Reset(): emit sigScanStart";
    return true;
}

//-------------------------------------------------------------------------------------------------
//  Установка параметров камеры
//
bool Camera_AVT::SetParams()
{
    if(!opened)
        return false;
//    qDebug() << "SetParams: camera is opened";
    if(started)
        if (!Stop())
            return false;

    //	Установка параметров кадра камеры
    if (
            (PvAttrEnumSet(_tCamera->Handle, "PixelFormat", "Mono16") != ePvErrSuccess) ||
            (PvAttrUint32Set(_tCamera->Handle, "BinningX", parameters.binningX) != ePvErrSuccess) ||
            (PvAttrUint32Set(_tCamera->Handle, "BinningY", parameters.binningY) != ePvErrSuccess) ||
            (PvAttrUint32Set(_tCamera->Handle, "Height", parameters.height) != ePvErrSuccess) ||
            (PvAttrUint32Set(_tCamera->Handle, "Width", parameters.width) != ePvErrSuccess) ||
            (PvAttrUint32Set(_tCamera->Handle, "RegionY", parameters.offsetY) != ePvErrSuccess) ||
            (PvAttrUint32Set(_tCamera->Handle, "RegionX", parameters.offsetX) != ePvErrSuccess) ||
            (PvAttrUint32Set(_tCamera->Handle, AVT_GAIN, parameters.gain) != ePvErrSuccess)
        )
    {
        emit sigStatusMessage("Camera_AVT::SetParams Ошибка установки параметров кадра");
        return false;
    }

    // Установка атрибутов камеры
    if (
        (PvAttrEnumSet(_tCamera->Handle, "FrameStartTriggerMode", "FixedRate") != ePvErrSuccess) ||
        (PvAttrFloat32Set(_tCamera->Handle, "FrameRate", parameters.fps) != ePvErrSuccess) ||
        (PvAttrEnumSet(_tCamera->Handle, "AcquisitionMode", "Continuous") != ePvErrSuccess) ||
        (PvCommandRun(_tCamera->Handle, "AcquisitionStart") != ePvErrSuccess)
        )
    {
        emit sigStatusMessage("Camera_AVT::SetParams Ошибка установки атрибутов камеры");
        return false;
    }

    emit sigStatusMessage("Параметры установлены.");
    if(!started)
        Start();

    return true;
}

//-----------------------------------------------------------------------------
//  Установка параметров по-умолчанию
//
void Camera_AVT::SetDefaultSettings()
{
    parameters.binningX = AVT_VAL_BINNING_X;
    parameters.binningY = AVT_VAL_BINNING_Y;
    parameters.exposure = AVT_VAL_EXP;
    parameters.fps = AVT_VAL_FPS;
    parameters.gain = AVT_VAL_GAIN;
    parameters.height = AVT_VAL_HEIGHT;
    parameters.width = AVT_VAL_WIDTH;
    parameters.offsetX = AVT_VAL_OFFSET_X;
    parameters.offsetY = AVT_VAL_OFFSET_Y;
    parameters.savePath = AVT_VAL_PATH;
    //parameters.isLoad = true;
}

//-----------------------------------------------------------------------------
//  Сохранение изображения с камеры
//
int Camera_AVT::SaveImage()
{
    if(!started || !opened)
        return -1;
    if(parameters.savePath == "")
        parameters.savePath = AVT_VAL_PATH;

    // Конвертация пути в нужный формат
    std::string str = parameters.savePath.toStdString();
    // Запись имени файла в структуру камеры
    // 04 - означает заполнение нулями до 4-х значного числа, если само число меньше.
    // l - модификатор длинного числа (long)
    // u - беззнакового целого.
    //const char fname[] = "F://frames/%04lu.tif";
    str.append("%04lu.fit[compress]");
    sprintf_s(_tCamera->Filename, sizeof(_tCamera->Filename), str.c_str(), _tCamera->Counter++);

    // Проверка существования файла
    if (_access(str.c_str(),0) != -1)
        if (!remove(_tCamera->Filename))
        {
            qDebug() << "Camera_AVT::Не удаётся удалить файл";
//            emit sigWarningMessage("Camera_AVT::Не удаётся удалить файл");
            return 1;
        }

    // Сохранения файла
    if (!SaveFitsImage())
        return 3;

    // Проверка на целостность кадра
    if (_tCamera->Frame.Status == ePvErrDataMissing)
    {
        qDebug() << "Camera_AVT::Dropped packets. Possible improper network card settings:\n See GigE Installation Guide.";
//        emit sigWarningMessage("Camera_AVT::Dropped packets. Possible improper network card settings:\n See GigE Installation Guide.");

        return 2;
    }
    return 0;
}

//-----------------------------------------------------------------------------
//  Установка режима автоматической либо ручной экспозиции
//
bool Camera_AVT::SetAutoExposure(bool autoMode)
{
    if (autoMode == true)
    {
        if (PvAttrEnumSet(_tCamera->Handle, "ExposureMode", "Auto") != ePvErrSuccess)
        {
            emit sigWarningMessage("Camera: Ошибка установки экспозиции!");
            return false;
        }
        emit sigStatusMessage("Автоматический режим экспозиции");
    }
    else
    if (PvAttrEnumSet(_tCamera->Handle, "ExposureMode", "Manual") != ePvErrSuccess)
    {
        qDebug() << "Camera_AVT::Ошибка установки экспозиции!";
//        emit sigWarningMessage("Camera_AVT::Ошибка установки экспозиции!");
        return false;
    }
    else
        emit sigStatusMessage("Ручной режим экспозиции");
    return true;
}

//-----------------------------------------------------------------------------
//  Установка значения экспозиции
//
bool Camera_AVT::SetExposure(int val)
{
    if (PvAttrUint32Set(_tCamera->Handle, "ExposureValue", val) != ePvErrSuccess)
    {
        qDebug() << "Camera_AVT: Ошибка установки экспозиции";
//        emit sigWarningMessage("Camera_AVT: Ошибка установки экспозиции");
        return false;
    }
    emit sigStatusMessage("Camera_AVT: Экспозиция установлена");
    return true;
}

//-----------------------------------------------------------------------------
//  Получение текущего значения экспозиции
//
int Camera_AVT::GetExposure() const
{
    unsigned long exp;
    if(PvAttrUint32Get(_tCamera->Handle, "ExposureValue", &exp) != ePvErrSuccess)
    {
        qDebug("Camera_AVT::Ошибка получения экспозиции");
        return 0;
    }
    else
        return static_cast<int>(exp);
}

//-------------------------------------------------------------------------------------------------
// Получение буфера с изображением
//
unsigned char *Camera_AVT::GetImageBuf()
{
    if(frameReady && buf8bit)
        return buf8bit;
    else
        return NULL;
}

//-----------------------------------------------------------------------------
//  Получение текущей ширины кадра
//
int Camera_AVT::GetFrameWidth() const
{
    return static_cast<int>(_tCamera->Frame.Width);
}

//-----------------------------------------------------------------------------
//  Получение текущей высоты кадра
//
int Camera_AVT::GetFrameHeight() const
{
    return static_cast<int>(_tCamera->Frame.Height);
}

//-----------------------------------------------------------------------------
//  Установка режима сохранения изображений
//
void Camera_AVT::SetSaveMode(bool saveMode)
{
    saving = saveMode;
    if(saveMode == false)
        _tCamera->Counter = 0;
}

//-----------------------------------------------------------------------------
//  Получение изображения с камеры формата QImage
//
QImage Camera_AVT::GetImage()
{
    uint16_t * p16bit;

    if (!started)
        return QImage();

    // Ожидание кадра с камеры
    while (isReady())
    {
         QCoreApplication::processEvents( QEventLoop::AllEvents, 10 );
    }

    p16bit = (uint16_t*)_tCamera->Frame.ImageBuffer;

    // Каждые 2 байта записываются в 1 байт
    for ( unsigned int i = 0; i < _tCamera->Frame.ImageBufferSize / 2; i++ )
    {
        // Полученное число делится на 64(сдин на 6) для конвертации в 8 бит, т.к. данные в 14 битном виде
        buf8bit[i] = uchar (p16bit[i] >> 6);
    }

    emit sigImageGot(buf8bit,_tCamera->Frame.Width,
                     _tCamera->Frame.Height);

    return QImage(buf8bit,
                   _tCamera->Frame.Width,
                   _tCamera->Frame.Height,
                  QImage::Format_Grayscale8 ); //Формат изображения Монохромный, 8 бит
}

//-----------------------------------------------------------------------------
// Получить размер буфера изображения
//
int Camera_AVT::GetImageSize() const
{
    return static_cast<int>(_tCamera->Frame.ImageBufferSize);
}

//-----------------------------------------------------------------------------
// Функция инициализации камеры
//
bool Camera_AVT::Initialize()
{
    emit sigStatusMessage((char*)"Инициализация...");

    // Объявление переменных
    tPvUint32 count,connected;
    // Инициализация
    if((errCode = PvInitialize()) != ePvErrSuccess)
    {
        emit sigStatusMessage("Camera_AVT::Ошибка инициализации");
        qDebug() << "Camera_AVT: initialize() err = " << errCode;
        return false;
    }
    else
    {

        // Ожидание подключенной камеры, ограничено по времени
        emit sigStatusMessage((char*)"Ожидаем подключения камеры...");
        time_t timer_start, timer_end;
        time(&timer_start);
        while(!closed && PvCameraCount() == 0)
        {
           time(&timer_end);
           if( difftime(timer_end, timer_start) >= AVT_TIME_TO_WAIT_SEC)
           {
               emit sigStatusMessage("Camera_AVT::Подключенные камеры не найдены");
               qDebug() << "Camera_AVT::Подключенные камеры не найдены";
               PvUnInitialize();
               sigStatusMessage("Camera_AVT::Подключенные камеры не найдены");
               return false;
           }
        }

//        qDebug() << "Camera_AVT::InitCamera, memset";
        memset(_tCamera,0,sizeof(tCamera));

        nCameras = PvCameraCount();

        cameraList = new tPvCameraInfoEx[nCameras];

        // Если подключена 1 камера, присваеваем ей ID и выводим сообщение на экран
        // Если больше одной - ищем совпадение по имени
        count = PvCameraListEx(cameraList, nCameras, &connected, sizeof(tPvCameraInfoEx));
        if(count == 1)
        {
            _tCamera->UID = cameraList[0].UniqueId;
            emit sigStatusMessage(QString("Камера подключена, ID %1").arg(_tCamera->UID));
            qDebug() << "Camera_AVT::Имя камеры = " << (char*)cameraList[0].CameraName;
        }
        else if(count > 1)
        {
            for(unsigned int i = 0; i < count; i++)
            {
                //cameraInfo = cameraList->CameraName;
                qDebug() << "Camera_AVT::Имя камеры = " << (char*)cameraList[i].CameraName;
                if(cameraList[i].CameraName == "GX3300")
                {
                    _tCamera->UID = cameraList[i].UniqueId;
                    break;
                }
                else
                    continue;
            }
        }
        else
        {
            emit sigWarningMessage("Не удалось найти камеру");
            delete [] cameraList;
            return false;
        }
    }

    // Открытие камеры
    if ((errCode = PvCameraOpen(_tCamera->UID,ePvAccessMaster,&(_tCamera->Handle))) != ePvErrSuccess)
    {
        if (errCode == ePvErrAccessDenied)
        {
            delete [] cameraList;
            emit sigWarningMessage("Camera_AVT::Ошибка открытия камеры - нет доступа.");
            qDebug() << "Camera_AVT::::Ошибка открытия камеры - нет доступа";
            return false;
        }
        else
        {
            delete [] cameraList;
            emit sigWarningMessage("Camera_AVT::Ошибка открытия камеры ");
            qDebug() << "Camera_AVT::::Ошибка открытия камеры: err = " << errCode;
            return false;
        }
    }
    qDebug() << "Camera_AVT::Камера открыта";
    emit sigStatusMessage("Camera_AVT::Камера открыта");
    return true;
}

//-----------------------------------------------------------------------------
// Функция запуска камеры
//
bool Camera_AVT::Setup()
{
    // Определение размера буфера
    if ((errCode = PvAttrUint32Get(_tCamera->Handle, "TotalBytesPerFrame", &frameSize)) != ePvErrSuccess)
    {
        delete [] cameraList;
        qDebug() << "Camera_AVT:: Get TotalBytesPerFrame err = " << errCode;
        emit sigWarningMessage("CameraSetup: Get TotalBytesPerFrame err: ");
        return false;
    }

    // Выделение памяти под буфер
    _tCamera->Frame.ImageBuffer = new unsigned char[frameSize+10];

    if (!_tCamera->Frame.ImageBuffer)
    {
        delete [] cameraList;
        qDebug() << "Camera_AVT:: Ошибка выделения буфера.";
        emit sigWarningMessage("CameraSetup: Ошибка выделения буфера.");
        return false;
    }

    _tCamera->Frame.ImageBufferSize = frameSize;

    // Функция автоматического определения максимального размера пакета (не работает)

    //unsigned long MaxAllowablePacketSize;
    // NOTE: This call sets camera PacketSize to largest sized test packet, up to 8228, that doesn't fail
    // on network card. Some MS VISTA network card drivers become unresponsive if test packet fails.
    // Use PvAttrUint32Set(handle, "PacketSize", MaxAllowablePacketSize) instead. See network card properties
    // for max allowable PacketSize/MTU/JumboFrameSize.

    //errCode = PvAttrUint32Get(Camera->Handle, "PacketSize", &MaxAllowablePacketSize);
    //QMessageBox("PacketSize = " + MaxAllowablePacketSize);

    // Установка размера пакета 9014

    if ((errCode = PvCaptureAdjustPacketSize(_tCamera->Handle, 9014)) != ePvErrSuccess)
    {
        delete [] cameraList;
        delete [] _tCamera->Frame.ImageBuffer;
        qDebug() << "Camera_AVT:: Ошибка установки размера пакета = " << errCode;
        emit sigStatusMessage("Camera_AVT:: Ошибка установки размера пакета");
        return false;
    }
    emit sigStatusMessage("Буфер создан");
    return true;
}

//-----------------------------------------------------------------------------
// Функция вывода изображения на экран
//
tPvErr Camera_AVT::CreateImage(unsigned & i)
{

    frameReady = true;

    // Ожидание изображения с камеры
    while ((errCode = PvCaptureWaitForFrameDone(_tCamera->Handle, &(_tCamera->Frame), 10)) == ePvErrTimeout)
    {
        ++i;
    }

    // Проверка вызова Stop()
    if (!started)
        return ePvErrSuccess;

    if (errCode != ePvErrSuccess)
    {
        qDebug() << "PvCaptureWaitForFrameDone: errCode = " << errCode;
        return errCode;
    }


    // Проверка фрейма
    if ((errCode = _tCamera->Frame.Status) != ePvErrSuccess)
    {
        qDebug() << "CameraAVT:CreateImage: FrameStatus = errSuccess!";
        return errCode;
    }


    // Подготовка очереди для следующего кадра
    if ((errCode = PvCaptureQueueFrame(_tCamera->Handle, &(_tCamera->Frame), NULL)) != ePvErrSuccess)
    {
        qDebug() << "CameraAVT:CreateImage: PvCaptureQueueFrame err = " << errCode;
        return errCode;
    }
    frameReady = false;
    return ePvErrSuccess;
}

//-----------------------------------------------------------------------------
// Функция создания файла формата FITS и его сохранения
//
bool Camera_AVT::SaveFitsImage()
{
    bool retval = true;
    char errtext[80];

    QString str;
    QByteArray ba;
    void* c_str;

    try
    {
        int status = 0;
        int nAxis = 2;
        long nAxes[2];
        nAxes[0] = _tCamera->Frame.Width;
        nAxes[1] = _tCamera->Frame.Height;
        fitsfile *fptr;
        long fpixel = 1;

        // Создание файла
        if ( fits_create_file(&fptr, _tCamera->Filename, &status) != 0 )
            throw(status);
        // Построение 16-bit изображения
         if ( fits_create_img(fptr, USHORT_IMG, nAxis, nAxes, &status) != 0 )
            throw(status);

        // Запись ключей со значениями в шапку файла
        str = QDateTime::currentDateTimeUtc().toString("yyyy-MM-ddTHH:mm:ss");
        ba = str.toLatin1();
        c_str = ba.data();
        if ( fits_update_key(fptr, TSTRING, "DATE", c_str, "date of file creation", &status) != 0 )
            throw(status);
        if ( fits_update_key(fptr, TSTRING, "DATE-OBS", c_str, "date of the observation", &status) != 0 )
            throw(status);

        long exposure = GetExposure();
         if ( fits_update_key(fptr, TLONG, "EXPOSURE", &exposure, "exposure time", &status) != 0 )
             throw(status);
         if (dailyAngle != 0)
         {
             if ( fits_update_key(fptr, TDOUBLE, "DELTA-ANGLE", &dailyAngle, "daily delta-angle rad.", &status) != 0 )
                 throw(status);
         }
         str = "Kislovodsk Solar Station";
         ba = str.toLatin1();
         c_str = ba.data();
         if ( fits_update_key(fptr, TSTRING, "ORIGIN", c_str, "organization responsible for the data", &status) != 0 )
             throw(status);

         str = parameters.cameraName;
         ba = str.toLatin1();
         c_str = ba.data();
         if ( fits_update_key(fptr, TSTRING, "DETNAM", c_str, "name of the detector used to make the observation", &status) != 0 )
             throw(status);

         // Запись массива изображения в файл
        long nelements = (_tCamera->Frame.Width) * (_tCamera->Frame.Height);
         if ( fits_write_img(fptr, TUSHORT, fpixel, nelements, (unsigned short *)_tCamera->Frame.ImageBuffer, &status) != 0 )
             throw(status);
         // Закрытие файла
         if ( fits_close_file(fptr, &status) != 0 )
             throw(status);
        fits_report_error(stderr, status); /* print out any error messages */
    }
    catch(int exeptval)
    {
        fits_get_errstatus(exeptval, errtext);
        qDebug() << "FitsFile:" << exeptval << errtext;
        retval = false;
        saving = false;
        emit sigScanPause();
        return false;
    }
    catch(...)
    {
        qDebug() << "FitsFile: Unhandle exception!";
        retval = false;
        saving = false;
        emit sigScanPause();
        return false;
    }

    return true;
}
