#include "camera_jai.h"
#include <fitsio.h>                         /* fits_create_file, fits_create_img, ... */
#include <io.h>                             /* _access */
#include <time.h>                           /* time, difftime */
#include <QImage>
#include <QThread>
#include <QDebug>
#include <QCoreApplication>
#include <QDateTime>
#include <QApplication>

FACTORY_HANDLE Camera_JAI::hFactory = NULL;
int Camera_JAI::nOpenedCameras = 0;
bool8_t Camera_JAI::camerasFound = false;
uint32_t Camera_JAI::nCameras = 0;
bool Camera_JAI::wait = false;

//-------------------------------------------------------------------------------------------------
// Конструктор камеры. Обнуляются основные атрибуты.
//
Camera_JAI::Camera_JAI(QObject *parent) :
    Camera(parent)
{
    setObjectName("GAI_Camera");
    hCamera = NULL;
    hNode = NULL;
    hThread = NULL;
    returnImage_t.pImageBuffer = NULL;
    buf8bit = NULL;
    ViewSize.cx = 0;
    ViewSize.cy = 0;
    opened = false;
    started = false;
    saving = false;
    frameReady = false;
    isFirst = false;
}

//-------------------------------------------------------------------------------------------------
// Деструктор. Очищается буфер с изображением.
//
Camera_JAI::~Camera_JAI()
{  
    Close();
}

//-------------------------------------------------------------------------------------------------
// Инициализация камеры.
// Происходит поиск подключенных камер, и их инициализация.
// Инициализируются только камеры, соответствующие выбранной модели
//
bool Camera_JAI::Open()
{
    if(opened)
        return true;

    while(wait == true)
    {
//        QCoreApplication::processEvents( QEventLoop::AllEvents, 15 );
        QApplication::processEvents( QEventLoop::AllEvents, 15 );
        Sleep(10);
    }
    {
        wait = true;

        if(hFactory == NULL)
        {
            // Открываем оболочку над камерой (Factory)
            retval = J_Factory_Open((int8_t*)J_REG_DATABASE_V2_0 , &hFactory);
            if (retval != J_ST_SUCCESS)
            {
                qDebug() << "Camera_JAI::Ошибка открытия оболочки камеры гида";
                if(J_Factory_Close(hFactory) != J_ST_SUCCESS)
                    return false;
            }
        }

        qDebug() << "Camera_JAI::Open()" << parameters.cameraName;

        // Поиск подключенных камер по сети
        if(camerasFound == false)
        {
            retval = J_Factory_UpdateCameraList(hFactory, &camerasFound);
            if(retval != J_ST_SUCCESS)
            {
                 qDebug() << "Camera_JAI::Ошибка получения списка камер";
                 return false;
            }
//            qDebug() << "Camera_JAI::" << camerasFound << parameters.cameraName;
        }
//        qDebug() << "Loop!!!!!!!!!!";
        if(nCameras == 0)
        {
            // Получаем количество подключенных камер.
            // Одна камера может быть найдена дважды, из-за особенности драйверов.
            // Поэтому нужна дополнительная фильтрация.
            retval = J_Factory_GetNumOfCameras(hFactory, &nCameras);

            if (retval != J_ST_SUCCESS || nCameras == 0)
            {
                qDebug() << "Camera_JAI::Ошибка получения количества камер" << parameters.cameraName;
                return false;
            }
//            else
//                qDebug() << "Camera_JAI::Подключенных камер менее:" << nCameras;
        }
        wait = false;
    }

    // Проходим по списку найденных камер и последовательно инициализируем их
    for (uint32_t index = 0; index < nCameras; index++)
    {
        sizeID = sizeof(sCameraID);

        // Получаем ID камеры
        retval = J_Factory_GetCameraIDByIndex(hFactory, index, sCameraID,
                                              &sizeID );
        if (retval != J_ST_SUCCESS)
        {
            qDebug() << "Camera_JAI::Ошибка получения индекса камеры";
        }
        else
        {
            // Получаем имя камеры
            sizeCameraInfo = sizeof(sCameraInfo);
            retval = J_Factory_GetCameraInfo(hFactory, sCameraID,
                                             CAM_INFO_MODELNAME,
                                             sCameraInfo, &sizeCameraInfo);
            if (retval == J_ST_SUCCESS)
            {
                QByteArray ba_cameraName = parameters.cameraName.toLatin1();
                const char *c_cameraName = ba_cameraName.data();
//                qDebug() << "Camera_JAI::Камера:" << index << "Имя:" << (char*)sCameraInfo;
                sigStatusMessage((char*)sCameraInfo);
                if (parameters.cameraName != "")
                {
                    if(strcmp((const char*)sCameraInfo, c_cameraName) != 0)
                    {
//                        qDebug() << "Camera_JAI::Камера не подходит под заданное имя";
                        continue;
                    }
                }
                else
                {
                    qDebug() << "Camera_JAI::Модель камеры не задана, выбирается автоматически";
                    parameters.cameraName = (const char*)sCameraInfo;
                }
            }

            // Открываем камеру
            retval = J_Camera_Open(hFactory, sCameraID, &hCamera);
            if (retval != J_ST_SUCCESS)
            {
                qDebug() << "Camera_JAI::Ошибка открытия камеры";
            }
            else
            {
                qDebug() << "Camera_JAI::Камера" << parameters.cameraName << "открыта";
                opened = true;
                nOpenedCameras++;
                sigCameraOpened();
                return true;
            }
        }
    }
    return true;
}

//-------------------------------------------------------------------------------------------------
// Закрытие камеры
//
bool Camera_JAI::Close()
{
    qDebug() << "Camera_JAI::Close" << opened;
    if(!opened)
        return true;
    if(started)
        Stop();

    // Закрытие камеры
    if(hCamera)
    {
        retval = J_Camera_Close(hCamera);
        if (retval != J_ST_SUCCESS){
            return FALSE;
            qDebug("Camera_JAI::Ошибка закрытия камеры");
        }
    }

    // Закрытие оболочки factory
    if(hFactory && nOpenedCameras <= 1)
    {
        retval = J_Factory_Close(hFactory);
        if (retval != J_ST_SUCCESS){
            return FALSE;
            qDebug("Camera_JAI::Ошибка закрытия оболочки камеры");
        }
    }

    //  Очистка буфера, освобождение памяти
    if(J_Image_Free(&returnImage_t) != J_ST_SUCCESS)
        qDebug() << "Camera_JAI::Ошибка освобождения буфера";
    else
        qDebug() << "Camera_JAI::Буфер освобожден";

    nOpenedCameras--;
    opened = false;
    return true;
}

bool Camera_JAI::Reset()
{
    // заглушка
    return true;
}

void Camera_JAI::Restart()
{
    return;
}

//-------------------------------------------------------------------------------------------------
// Запуск камеры
//
bool Camera_JAI::Start()
{
    //qDebug() << "Camera_JAI::Start" << started;
    if(started)
        return true;

    int64_t int64Val = 0;
    isFirst = true;

    // Включается режим экспозиции
    if(SetParams((char*)NODE_NAME_EXPOSURE_MODE, (char*)NODE_VALUE_EXPOSURE_MODE))

    // Получение ширины кадра
    if (J_Camera_GetValueInt64(hCamera, (int8_t*)NODE_NAME_WIDTH, &int64Val) != J_ST_SUCCESS)
        return FALSE;
    ViewSize.cx = (LONG)int64Val;
    if(ViewSize.cx == 0)
        ViewSize.cx = parameters.width;

    // Получение высоты кадра
    if (J_Camera_GetValueInt64(hCamera, (int8_t*)NODE_NAME_HEIGHT, &int64Val) != J_ST_SUCCESS)
        return FALSE;
    ViewSize.cy = (LONG)int64Val;
    if(ViewSize.cy == 0)
        ViewSize.cy = parameters.height;

    // Рассчитать количество битов на пиксель
    //    int pixelformat = (int)int64Val;
    int bpp = DEFAULT_BITS_PER_PIXEL;
    //int bpp = J_BitsPerPixel(pixelformat);

    // Запуск потока с callback-функцией
    retval = J_Image_OpenStream(hCamera, 0,
                                reinterpret_cast<J_IMG_CALLBACK_OBJECT>(this),
                                reinterpret_cast<J_IMG_CALLBACK_FUNCTION>(&Camera_JAI::StreamCBFunc),
                                &hThread,
                                (ViewSize.cx*ViewSize.cy*bpp)/8);   // размер буфера
    if (retval != J_ST_SUCCESS)
    {
        qDebug("Camera_JAI::Ошибка открытия потока приема данных");
        return false;
    }

    // Проверка на наличие потоков
    uint32_t numStreams = 0;
    retval = J_Camera_GetNumOfDataStreams(hCamera, &numStreams);
    if (retval != J_ST_SUCCESS)
    {
        qDebug() << "Camera_JAI::Ошибка получения количества потоков.";
        return false;
    }
    if ( numStreams == 0)
    {
        qDebug() << "Camera_JAI::Num streams == 0";
        return false;
    }

    // Подача команды запуска на камеру
    if(J_Camera_ExecuteCommand(hCamera, (int8_t*)NODE_NAME_ACQSTART) != J_ST_SUCCESS)
        return false;

    qDebug() <<"Camera_JAI::Размер изображения:" << (ViewSize.cx*ViewSize.cy*bpp) / 8  << "Б";

    buf8bit = new unsigned char[ViewSize.cx*ViewSize.cy];

    started = true;
    emit sigCameraStarted();
    return true;
}

//-------------------------------------------------------------------------------------------------
// Остановка камеры
//
bool Camera_JAI::Stop()
{
    qDebug() << "Camera_JAI::Stop" << started;
    if(!started)
        return true;

    // Посылка на камеру команды остановки
    if (J_Camera_ExecuteCommand(hCamera, (int8_t*)NODE_NAME_ACQSTOP) != J_ST_SUCCESS)
    {
        qDebug() << "Camera_JAI::Ошибка остановки камеры";
        //return false;
    }

    // Закрытие потока
    if(retval = J_Image_CloseStream(hThread) != J_ST_SUCCESS)
    {
        qDebug() << "Camera_JAI::Ошибка закрытия потока";
        //return false;
    }
    if(buf8bit)
        delete buf8bit;
    isFirst = false;
    started = false;
    return true;
}

//-------------------------------------------------------------------------------------------------
// Сохранить изображение с камеры (в настоящий момент метод не используется, заглушка)
//
int Camera_JAI::SaveImage()
{
    if(!started || !opened)
        return -1;
    if(parameters.savePath == "")
        parameters.savePath = "F://";

    //qDebug() << "Camera_JAI::SaveImage()" << parameters.savePath;
    // Конвертация пути в нужный формат
    std::string str = parameters.savePath.toStdString();
    // Запись имени файла в структуру камеры
    // 04 - означает заполнение нулями до 4-х значного числа, если само число меньше.
    // l - модификатор длинного числа (long)
    // u - беззнакового целого.
    //const char fname[] = "F://frames/%04lu.tif";
    str.append("%04lu.fit[compress]");
    sprintf_s(fileName, sizeof(fileName), str.c_str(), counter++);

    // Проверка существования файла
    if (_access(str.c_str(),0) != -1)
        if (!remove(fileName))
        {
            emit sigWarningMessage("Camera_JAI::Не удаётся удалить файл");
            return 1;
        }

    // Сохранения файла
    SaveFitsImage();

//    // Проверка на целостность кадра
//    if (_tCamera->Frame.Status == ePvErrDataMissing)
//    {
//        emit sigWarningMessage("Camera_AVT::Dropped packets. Possible improper network card settings:\n See GigE Installation Guide.");
//        return false;
//    }
    return 0;
}

//-----------------------------------------------------------------------------
// Функция создания файла формата FITS и его сохранения
//
void Camera_JAI::SaveFitsImage()
{
    //qDebug() << "Camera_JAI::SaveFitsImage()";

    bool retval = true;
    char errtext[80];

    QString str;
    QByteArray ba;
    void* c_str;

    try
    {
        int status = 0;
        int naxis = 2;
        long naxes[2];
        naxes[0] = GetFrameWidth();
        naxes[1] = GetFrameHeight();
        fitsfile *fptr;
        long fpixel = 1;

        // Создание файла
        if ( fits_create_file(&fptr, fileName, &status) != 0 )
            throw(status);
        // Построение 16-bit изображения
         if ( fits_create_img(fptr, USHORT_IMG, naxis, naxes, &status) != 0 )
            throw(status);

         // Запись ключей со значениями в шапку файла
         str = QDateTime::currentDateTimeUtc().toString("yyyy-MM-ddTHH:mm:ss");
         ba = str.toLatin1();
         c_str = ba.data();
         if ( fits_update_key(fptr, TSTRING, "DATE", c_str, "date of file creation", &status) != 0 )
             throw(status);
         if ( fits_update_key(fptr, TSTRING, "DATE-OBS", c_str, "date of the observation", &status) != 0 )
             throw(status);
         long exposure = GetExposure();
          if ( fits_update_key(fptr, TLONG, "EXPOSURE", &exposure, "exposure time", &status) != 0 )
              throw(status);
//         if (dailyAngle != 0)
//         {
//             if ( fits_update_key(fptr, TDOUBLE, "DELTA-ANGLE", &dailyAngle, "Daily delta-angle rad.", &status) != 0 )
//                 throw(status);
//         }
          str = "Kislovodsk Solar Station";
          ba = str.toLatin1();
          c_str = ba.data();
          if ( fits_update_key(fptr, TSTRING, "ORIGIN", c_str, "organization responsible for the data", &status) != 0 )
              throw(status);

          str = parameters.cameraName;
          ba = str.toLatin1();
          c_str = ba.data();
          if ( fits_update_key(fptr, TSTRING, "DETNAM", c_str, "name of the detector used to make the observation", &status) != 0 )
              throw(status);

         // Запись массива изображения в файл
        long nelements = (GetFrameWidth()) * (GetFrameHeight());
         if ( fits_write_img(fptr, TUSHORT, fpixel, nelements, (unsigned short *)image_t->pImageBuffer, &status) != 0 )
             throw(status);
         // Закрытие файла
         if ( fits_close_file(fptr, &status) != 0 )
             throw(status);
        fits_report_error(stderr, status); /* print out any error messages */
    }
    catch(int exeptval)
    {
        fits_get_errstatus(exeptval, errtext);
        qDebug() << "FitsFile:" << exeptval << errtext;
        retval = false;
    }
    catch(...)
    {
        qDebug() << "FitsFile: Unhandle exception!";
        retval = false;
    }

    return;
}

//-------------------------------------------------------------------------------------------------
// Call back sample function / Основная функция получения данных с камеры.
// Функция ответа, работает циклично в потоке все время пока запущена камера.
// Запускается в функции Start(), останавливается в Stop()
//
void Camera_JAI::StreamCBFunc(J_tIMAGE_INFO *pAqImageInfo)
{
    uint32_t iSize = pAqImageInfo->iSizeX*pAqImageInfo->iSizeY*DEFAULT_BITS_PER_PIXEL/8;

    // Проверка на наличие и целостность
    if(pAqImageInfo->pImageBuffer != NULL &&
            pAqImageInfo->iImageSize == iSize)
    {
        CreateImageBuf(pAqImageInfo);
        if(saving)
            SaveImage();
    }
    else if(pAqImageInfo->pImageBuffer == NULL)
        qDebug() << "Camera_JAI::StreamCBFunc: pAqImageInfo->pImageBuffer == NULL!";

    else if (pAqImageInfo->iImageSize != iSize)
        qDebug() << "Camera_JAI::StreamCBFunc: pAqImageInfo->iImageSize != iSize";

}

//-------------------------------------------------------------------------------------------------
// Преобразование сырого буфера изображения, в готовый буфер
//
bool Camera_JAI::CreateImageBuf(J_tIMAGE_INFO *pAqImageInfo)
{
    frameReady = false;

    if(isFirst)
    {
        // Выделяем память под буфер изображения
        if(J_Image_Malloc(pAqImageInfo, &returnImage_t) != J_ST_SUCCESS)
        {
            qDebug("Camera_JAI::CreateImage(): Буфер не создан");
            return FALSE;
        }
        else
            qDebug() << "Camera_JAI::Буфер создан";
        isFirst = false;
    }

    // Отражение изображения
    //    if(J_Image_Flip(&rawImageInfo, J_FLIP_HORIZONTAL) != J_ST_SUCCESS)
    if(J_Image_Flip(pAqImageInfo, J_FLIP_HORIZONTAL) != J_ST_SUCCESS)
    {
       qDebug() << "Camera_JAI::CreateImageBuf: J_Image_Flip != J_ST_SUCCESS" ;
       return FALSE;
    }

 //   qDebug() << "Pixel format " << pAqImageInfo->iPixelType << "MONO12" << J_PF_MONO12;

    // Преобразование сырых данных в формат изображения
//    if(J_Image_FromRawToImage(pAqImageInfo, &returnImage_t) != J_ST_SUCCESS)
//        return FALSE;
    image_t = pAqImageInfo;
    //qDebug() << "Intensity" << GetAverageIntensity();
    // Отправка буфера с изображением
    //imageBuf = returnImage_t.pImageBuffer;
    frameReady = true;
    //emit sigImage16BitGot(static_cast<unsigned char*>(returnImage_t.pImageBuffer),GetFrameWidth(),GetFrameHeight());

    ConvertTo8Bit(pAqImageInfo);

    return TRUE;
}

//-------------------------------------------------------------------------------------------------
// Преобразование 16 битного буфера изображения в 8 битный для вывода на экран
//
void Camera_JAI::ConvertTo8Bit(J_tIMAGE_INFO *pAqImageInfo)
{
    uint16_t * p16t; // Указатель на массив данных камеры

    p16t = (uint16_t *) pAqImageInfo->pImageBuffer;

    // Каждые 2 байта записываются в 1 байт
    for ( unsigned int i = 0; i < pAqImageInfo->iImageSize / 2; i++ )
    {
        buf8bit[i] = unsigned char(p16t[i] >> 4);
    }
    emit sigImageGot(GetImageBuf(),GetFrameWidth(),GetFrameHeight());
}

//-------------------------------------------------------------------------------------------------
// Получить изображение из буфера
//
QImage Camera_JAI::GetImage()
{
    if(GetImageBuf() != NULL)
    {
        return QImage(buf8bit,GetFrameWidth(),GetFrameHeight(),
                           QImage::Format_Grayscale8,0,0);
    }
    else
    {
        sigStatusMessage("Изображение не получено");
        qDebug() << "Camera_JAI::Изображение не получено";
        return QImage();
    }
}

//-------------------------------------------------------------------------------------------------
// Получение буфера с изображением
//
unsigned char* Camera_JAI::GetImageBuf()
{
    time_t timer_start, timer_end;
    time(&timer_start);
    while(!frameReady)
    {
        QApplication::processEvents(QEventLoop::AllEvents, 25);
        time(&timer_end);
        if( difftime(timer_end, timer_start) >= 1)
            break;
    }
    if(buf8bit)
        return buf8bit;
    else
        return NULL;
}

//-------------------------------------------------------------------------------------------------
// Нахождение среднего значения интенсивности в кадре (или области кадра)
//
int Camera_JAI::GetAverageIntensity()
{
    J_tBGR48 intensityRGB;
    RECT frameRect;
    frameRect.left = 0;
    frameRect.right = GetFrameWidth();
    frameRect.top = 0;
    frameRect.bottom = GetFrameHeight();

    if(J_Image_GetAverage(image_t, &frameRect, &intensityRGB) != J_ST_SUCCESS)
    {
        qDebug() << "Camera_JAI::Ошибка нахождения среднего значения";
        return FALSE;
    }
    return (int)(intensityRGB.B16);
}

//-------------------------------------------------------------------------------------------------
// Установка значений параметров камеры целочисленного типа
//
bool Camera_JAI::SetParams(char *name, int value)
{
    if (J_Camera_SetValueInt64(hCamera, reinterpret_cast<int8_t*>(name), static_cast<int64_t>(value)) != J_ST_SUCCESS)
    {
        qDebug() << "Camera_JAI::Значение типа int = " << value << " не установлено";
        return FALSE;
    }
    return TRUE;
}

//-------------------------------------------------------------------------------------------------
// Установка значений параметров камеры строкового типа
//
bool Camera_JAI::SetParams(char* name, char* value)
{
    if(J_Camera_SetValueString(hCamera, reinterpret_cast<int8_t*>(name), reinterpret_cast<int8_t*>(value)) != J_ST_SUCCESS)
    {
        qDebug() << "Camera_JAI::Значение типа char[] = " << reinterpret_cast<char*>(value) << " не установлено";
        return FALSE;
    }
    return TRUE;
}

//-------------------------------------------------------------------------------------------------
// Установка значений параметров камеры типа с плавающей точкой
//
bool Camera_JAI::SetParams(char *name, double value)
{
    if (J_Camera_SetValueDouble(hCamera, reinterpret_cast<int8_t*>(name), value) != J_ST_SUCCESS)
    {
        qDebug() << "Camera_JAI::Значение типа double = " << value << " не установлено";
        return FALSE;
    }
    return TRUE;
}

//-------------------------------------------------------------------------------------------------
// Установка значений параметров из структуры CameraSettings_t
//
bool Camera_JAI::SetParams()
{
    if (!Stop())
        return false;

    SetParams((char*)NODE_NAME_WIDTH,    parameters.width);
    SetParams((char*)NODE_NAME_HEIGHT,   parameters.height);
    SetParams((char*)NODE_NAME_OFFSET_X, parameters.offsetX);
    SetParams((char*)NODE_NAME_OFFSET_Y, parameters.offsetY);
    SetParams((char*)NODE_NAME_BINNING_X,parameters.binningX);
    SetParams((char*)NODE_NAME_BINNING_Y,parameters.binningY);
    SetParams((char*)NODE_NAME_FPS,      parameters.fps);
//    SetParams((char*)NODE_NAME_GAIN,     parameters.gain);
    SetParams((char*)NODE_NAME_PIXELFORMAT, (char*) NODE_VALUE_MONO12);
    SetExposure(static_cast<double>(parameters.exposure));

    emit sigStatusMessage("Параметры установлены");

    if (!Start())
        return false;

    return true;
}

//-------------------------------------------------------------------------------------------------
// Получение значений параметров камеры целочисленного типа
//
double Camera_JAI::GetParams(char *name) const
{
    double doubleVal;
    if (J_Camera_GetValueDouble(hCamera, reinterpret_cast<int8_t*>(name), &doubleVal) != J_ST_SUCCESS)
    {
        qDebug("Camera_JAI::Ошибка получения значения параметра");
        return 0;
    }
    return doubleVal;
}

//-------------------------------------------------------------------------------------------------
//  Установка параметров со значениями по умолчанию (этот метод не используется)
//
void Camera_JAI::SetDefaultSettings()
{
    SetParams((char*)NODE_NAME_BINNING_X, DEFAULT_BINNING_X);
    SetParams((char*)NODE_NAME_BINNING_Y, DEFAULT_BINNING_Y);
    SetParams((char*)NODE_NAME_FPS, DEFAULT_FPS);
    SetParams((char*)NODE_NAME_EXPOSURE_MODE, (char*)NODE_VALUE_EXPOSURE_MODE);
    SetExposure(DEFAULT_EXPOSURE);
    SetParams((char*)NODE_NAME_BINNING_X, DEFAULT_BINNING_X);
    SetParams((char*)NODE_NAME_BINNING_Y, DEFAULT_BINNING_Y);
    SetParams((char*)NODE_NAME_GAIN, DEFAULT_GAIN);
    SetParams((char*)NODE_NAME_HEIGHT, DEFAULT_HEIGHT);
    SetParams((char*)NODE_NAME_WIDTH, DEFAULT_WIDTH);
}

//-------------------------------------------------------------------------------------------------
//  Включение/выключение режима сохранения кадров (для сканирования)
//
void Camera_JAI::SetSaveMode(bool saveMode)
{
    saving = saveMode;
    if(saveMode == false)
        counter = 0;
}

//-------------------------------------------------------------------------------------------------
// Установка режима автоматической или ручной экспозиции
//
bool Camera_JAI::SetAutoExposure(bool autoMode)
{
    qDebug() << "Camera_JAI::SetAutoExposure, opened =" << opened;
    // Если параметр функции true - включается режим авто экспозиции
    if(autoMode)
    {
        if(SetParams((char*)NODE_NAME_EXPOSURE_AUTO, (char*)NODE_VALUE_CONTINUOUS))
        {
            return TRUE;
        }
        else
        {
            emit sigWarningMessage("Ошибка установки режима авто экспозиции");
            return FALSE;
        }
    }
    // Если параметр функции false - включается режим ручной экспозиции
    else
    {
        if(SetParams((char*)NODE_NAME_EXPOSURE_AUTO, (char*)NODE_VALUE_OFF))
        {
            return TRUE;
        }
        else
        {
            emit sigWarningMessage("Ошибка установки режима ручной экспозиции");
            return FALSE;
        }
    }
}

//-------------------------------------------------------------------------------------------------
// Установка значения экспозиции
//
bool Camera_JAI::SetExposure(int val)
{
    if(SetParams((char*)NODE_NAME_EXPOSURE, static_cast<double>(val)))
        return TRUE;
    else
        return FALSE;
}

//-------------------------------------------------------------------------------------------------
// Получение значения текущей экспозиции
//
int Camera_JAI::GetExposure() const
{
    return static_cast<int>(GetParams((char*)NODE_NAME_EXPOSURE));
}

//-------------------------------------------------------------------------------------------------
// Получение текущей ширины кадра
//
int Camera_JAI::GetFrameWidth() const
{
    int64_t int64Val;
    if (J_Camera_GetValueInt64(hCamera, const_cast<int8_t*>(reinterpret_cast<const int8_t*>(NODE_NAME_WIDTH)), &int64Val) != J_ST_SUCCESS)
    {
        qDebug("Camera_JAI::Camera width get error");
        return 0;
    }
    return static_cast<int>(int64Val);
}

//-------------------------------------------------------------------------------------------------
// Получение текущей высоты кадра
//
int Camera_JAI::GetFrameHeight() const
{
    int64_t int64Val;
    if (J_Camera_GetValueInt64(hCamera, const_cast<int8_t*>(reinterpret_cast<const int8_t*>(NODE_NAME_HEIGHT)), &int64Val) != J_ST_SUCCESS)
    {
        qDebug("Camera_JAI::Camera height get error");
        return 0;
    }
    return static_cast<int>(int64Val);
}

//-----------------------------------------------------------------------------
// Получить размер буфера изображения
//
int Camera_JAI::GetImageSize() const
{
    return static_cast<int>(image_t->iImageSize);
}
