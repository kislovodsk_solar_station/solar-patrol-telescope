#include "filefits.h"
#include <QDebug>

//-----------------------------------------------------------------------------
//
//
FileFits::FileFits()
    : Image()
{
    status = 0 ;
    fpixel = 1;
    naxis = 2;
    exposure = 0;
    naxes[0] = 10;
    naxes[1] = 10;
}

//-----------------------------------------------------------------------------
//
//
FileFits::FileFits(Image &img)
    :Image(img)
{
    qDebug() << "FileFits::FileFits(Image &img)";
    //Image::operator =(img);
}


//-----------------------------------------------------------------------------
//
//
FileFits::~FileFits()
{

}

//-----------------------------------------------------------------------------
//
//
bool FileFits::Open(QString fname, int hdunum)
{
    //qDebug() << "FileFits::Open";
    //fits_read_pix ( fptr, USHORT_IMG,)
    name = fname;
    int bitpix, anynul;
    char errtext[80];
    status = 0;
    fpixel = 1;
    naxis = 2;
    exposure = 0;
    naxes[0] = 0;
    naxes[1] = 0;
    bitpix = 16;
    int hdutype;
    try
    {
        Clear();
//        if( fits_open_file(&fptr, fname.toStdString().c_str(), READONLY, &status))
//            throw (status);
        // открываем файл, как изображение.
        if(fits_open_image(&fptr, fname.toStdString().c_str(), READONLY, &status))
            throw(status);
        //if(hdunum > 1)
        //    fits_movabs_hdu(fptr, hdunum, &hdutype, &status);
        //fits_get_img_size(fptr, 2, naxes, &status);
        fits_get_img_param(fptr, 2, &bitpix, &naxis, naxes, &status);
        Image::Init(naxes[0], naxes[1]);
        fits_read_img(fptr, USHORT_IMG, fpixel, Size(), NULL, Buffer(), &anynul, &status );
        fits_close_file(fptr, &status);
        fits_report_error(stderr, status);
    }
    catch(int exeptval)
    {
        fits_get_errstatus(exeptval, errtext);
        qDebug() << "FileFits:" << exeptval << errtext;
        return false;
    }
    catch(...)
    {
        qDebug() << "FileFits: Unhandle exception!";
        return false;
    }
    return true;
}

//-----------------------------------------------------------------------------
//
//
bool FileFits::Save(QString fname)
{
    qDebug() << "FileFits::Save" << fname;
    QFile(fname).remove();
    //fname.append("[compress]");
    name = fname;
    bool retval = true;
    char errtext[80];
    status = 0;
    fpixel = 1;
    status = 0; /* initialize status before calling fitsio routines */
    naxis = 2;
    naxes[0] = width;
    naxes[1] = height;
    int bitpix = 16;
    int anynul;

    try
    {
        if ( fits_create_file(&fptr, fname.toStdString().c_str(), &status) != 0 ) /* create new file */
            throw(status);
        /* Create the primary array image (16-bit short integer pixels */
        if ( fits_create_img(fptr, USHORT_IMG, naxis, naxes, &status) != 0 )
            throw(status);
        /* Write a keyword; must pass the ADDRESS of the value */
        exposure = 1500.0;
        if ( fits_update_key(fptr, TLONG, "EXPOSURE", &exposure, "Total Exposure Time", &status) != 0 )
            throw(status);
        /* Write the array of integers to the image */

        if ( fits_write_img(fptr, TUSHORT, fpixel, Size() , Buffer(), &status) != 0 )
            throw(status);

        //fits_insert_img(fptr, 16, naxis, naxes, &status);
        //fits_write_2d_usht(fptr,1,naxes[0], naxes[0], naxes[1], buffer(), &status);
        //fits_write_img(fptr, TUSHORT, fpixel, size , buffer(), &status);
//        long extvers = 2;
//        fits_write_key(fptr, TLONG, "EXTVER", &extvers, "extension version number", &status);
//        fits_insert_img(fptr, bitpix, naxis, naxes, &status);
//        //fits_write_2d_usht(fptr, 1, 19, naxes[0], naxes[1], buffer(), &status);
//        fits_write_img(fptr, TUSHORT, fpixel, size , buffer(), &status);
        //ffp2di()
        if ( fits_close_file(fptr, &status) != 0 ) /* close the file */
            throw(status);
        fits_report_error(stderr, status); /* print out any error messages */
    }
    catch(int exeptval)
    {
        fits_get_errstatus(exeptval, errtext);
        qDebug() << "FileFits:" << exeptval << errtext;
        retval = false;
    }
    catch(...)
    {
        qDebug() << "FileFits: Unhandle exception!";
        retval = false;
    }
    return retval;
}

//-----------------------------------------------------------------------------
//
//
void FileFits::AddImage(QString fname, Image & img)
{
    qDebug() << "FileFits::AddImage";
    //fits_read_pix ( fptr, USHORT_IMG,)
    int bitpix = 16;
    char errtext[80];
    status = 0;
    fpixel = 1;
    naxis = 2;
    exposure = 0;
    naxes[0] = img.Width();
    naxes[1] = img.Height();
    try
    {
        if( fits_open_file(&fptr, fname.toStdString().c_str(), READWRITE, &status))
            throw (status);
        //fits_get_img_size(fptr, 2, naxes, &status);
        //fits_get_img_param(fptr, 2, &bitpix, &naxis, naxes, &status);
        //Image::Init(naxes[0], naxes[1]);
        //fits_read_img(fptr, USHORT_IMG, fpixel, size, NULL, buffer(), &anynul, &status );
        /* Create the primary array image (16-bit short integer pixels */
        //if ( fits_create_img(fptr, USHORT_IMG, naxis, naxes, &status) != 0 )
        //    throw(status);
        /* Write a keyword; must pass the ADDRESS of the value */
        //exposure = 1500.0;
        //if ( fits_update_key(fptr, TLONG, "EXPOSURE", &exposure, "Total Exposure Time", &status) != 0 )
        //    throw(status);
        /* Write the array of integers to the image */

        //if ( fits_write_img(fptr, TUSHORT, fpixel, size , img.buffer(), &status) != 0 )
        //    throw(status);
        long extvers = 2;
        //fits_movabs_hdu(fptr, extvers, &hdutype, &status);
        //fits_write_key(fptr, TLONG, "EXTVER", &extvers, "extension version number", &status);
        fits_create_img(fptr, USHORT_IMG, naxis, naxes, &status);
        //fits_insert_img(fptr, bitpix, naxis, naxes, &status);
        //fits_write_2d_usht(fptr, 1, 19, naxes[0], naxes[1], img.buffer(), &status);
        fits_write_img(fptr, TUSHORT, fpixel, img.Size(), img.Buffer(), &status);
//        extvers = 3;
//        fits_write_key(fptr, TLONG, "EXTVER", &extvers, "extension version number", &status);
//        fits_create_img(fptr, USHORT_IMG, naxis, naxes, &status);
//        //fits_insert_img(fptr, bitpix, naxis, naxes, &status);
//        //fits_write_2d_usht(fptr, 1, 19, naxes[0], naxes[1], img.buffer(), &status);
//        fits_write_img(fptr, TUSHORT, fpixel, img.Size(), img.buffer(), &status);
        //        fits_insert_img(fptr, bitpix, naxis, naxes, &status);
        //        fits_write_2d_usht(fptr,1,naxes[0], naxes[0], naxes[1], img.buffer(), &status);
        //fits_write_img(fptr, TUSHORT, fpixel, size , buffer(), &status);

        //ffp2di()

        fits_close_file(fptr, &status);
        fits_report_error(stderr, status);
    }
    catch(int exeptval)
    {
        fits_get_errstatus(exeptval, errtext);
        qDebug() << "FileFits:" << exeptval << errtext;
    }
    catch(...)
    {
        qDebug() << "FileFits: Unhandle exception!";
    }
}

//-----------------------------------------------------------------------------
//
//
void FileFits::operator=(const Image & img)
{
    //qDebug() << "FileFits::operator=";
    Image::operator =(img);
}

//-----------------------------------------------------------------------------
// Чтение записей из fits файла
//
// Возвращает пустую строку в случае отсутствия записи.
QString FileFits::GetParam(QString fname, QString param)
{
    QString str;
    str.clear();
    int fitsKeys = 0;
    char keyname[80];
    char val[80];
    char comm[80];
    status = 0;
    Clear();
    fits_open_image( &fptr, fname.toStdString().c_str(), READONLY, &status);
    fits_get_hdrspace( fptr, &fitsKeys, NULL, &status);
    for ( int i = 0; i < fitsKeys; i++)
    {
        fits_read_keyn(fptr, i, keyname, val, comm, &status);
        if ( param.compare(keyname) == 0 )
        {
            str.append(val);
            break;
        }
    }
    fits_close_file(fptr, &status);
    fits_report_error(stderr, status);
    return str;
}

