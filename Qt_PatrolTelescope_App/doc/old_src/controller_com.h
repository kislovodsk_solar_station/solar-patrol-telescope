//---------------------------------------------------------------------------------------
//  Controller_COM - класс управления контроллером шаговых двигателей SMDP
//  Разработка: Пащенко Михаил, Чернов Ярослав
//---------------------------------------------------------------------------------------

#ifndef CONTROLLER_COM_H
#define CONTROLLER_COM_H

#include <iostream>
#include <windows.h>
#include <string>
#include <QObject>
#include <QString>

class Controller_COM : public QObject
{
    Q_OBJECT

    LPCWSTR lsPortName;
    DCB dcbComParams;
    HANDLE hCom;

public:
    explicit Controller_COM(QObject *parent = 0);

public slots:
    bool InitCOM(QString sPortName);
    void ReadCOM(char sReceivedChar);
    bool WriteCOM();
    bool CloseCOM();
};

#endif CONTROLLER_COM_H
