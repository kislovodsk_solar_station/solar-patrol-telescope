#include "controller_com.h"
#include <QDebug>

using namespace std;
//using namespace PatrolTelescope;
//using namespace System::Runtime::InteropServices;

Controller_COM::Controller_COM(QObject *parent) : QObject(parent)
{
	
}

bool Controller_COM::InitCOM(QString sPortName)
{
    //IntPtr iPortName = Marshal::StringToHGlobalUni(sPortName.toStdString());
    //lsPortName = (LPCWSTR)iPortName.ToPointer();
    lsPortName = reinterpret_cast<LPCWSTR>(sPortName.utf16());

	hCom = CreateFile(lsPortName, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);	//FILE_ATTRIBUTE_NORMAL
	if (hCom == INVALID_HANDLE_VALUE)
	{
		if (GetLastError() == ERROR_FILE_NOT_FOUND)
		{
            qDebug("Com port does not exist.");
			//system("pause");
			return false;
		}
        qDebug("some other error occurred.");
		//system("pause");
		return false;
	}


	DCB dcbComParams = { 0 };
//	dcbComParams.DCBlength = sizeof(dcbComParams);
	if (!GetCommState(hCom, &dcbComParams))
	{
        qDebug("getting state error");
	}
	dcbComParams.BaudRate = CBR_9600;
	dcbComParams.ByteSize = 8;
	dcbComParams.StopBits = ONESTOPBIT;
	dcbComParams.Parity = EVENPARITY;
	if (!SetCommState(hCom, &dcbComParams))
	{
        qDebug("error setting Com port state");
		//system("pause");
		return false;
	}

	return true;
}

void Controller_COM::ReadCOM(char sReceivedChar)
{
	setlocale(LC_ALL, "rus");
	DWORD iSize;
	
	//while (true)
	//{
		ReadFile(hCom, &sReceivedChar, 1, &iSize, 0);
		//if (iSize > 0)
			//sReceivedChar;
		return;
	//}
}

bool Controller_COM::WriteCOM()
{
//	String^ s_COM;
	char data[] = "ST1*";	// ������� �����
	//	char parameter;
	char sReceivedChar;
	DWORD dwSize = sizeof(data);
	DWORD dwBytesWritten;
	//LPOVERLAPPED ov;
	//if (parameter == 1)	{
	if (BOOL iRet = WriteFile(hCom, data, dwSize, &dwBytesWritten, NULL) == false)
	{
        qDebug("Error write to COM-port");
		return false;
	}

    //qDebug(dwSize + " Bytes in string. " + dwBytesWritten + " Bytes sended. ");

	//int i;
	for (int i = 1; i<4; i++)
	{
		ReadCOM(sReceivedChar);
		//s_COM += sReceivedChar;
        //qDebug(s_COM);
	}

    //qDebug("COM-port is activated, command:" + sReceivedChar);
	return true;
}

bool Controller_COM::CloseCOM()
{
	if (!CloseHandle(hCom))
	{
        qDebug("Error COM-port closing");
		return false;
	}
    qDebug("COM-port is closed");
	return true;

}
