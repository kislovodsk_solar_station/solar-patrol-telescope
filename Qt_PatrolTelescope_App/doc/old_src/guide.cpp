//-------------------------------------------------------------------------
//  �������� ����������� �� ����� ��� ���������� ������ ����� ������
//
void SolarGuide::FindCenterFromFile(QString filename)
{
    image = new QImage;
    image->load(filename);
    FindCenter(*image);
    delete image;
    return;
}

//---------------------------------------------------------------------------------------
//  ����� ������������ ��������� ����� � ������ ��������� � ��������� ����
//
void SolarGuide::Watch(QString filename)
{
    fileInfo = new QFileInfo(filename);
    dTime = new QDateTime;
    *dTime = QDateTime::currentDateTimeUtc();
    QString str_Time;
    QString format = "yyyyMMdd_HHmmss";
    QString name = "SolarPositions_";
    name.append(dTime->toString(format));
    name.append(".txt");
    QFile file(name);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    //  ���� �������� �� ������
    while(true)
    {
        QCoreApplication::processEvents( QEventLoop::AllEvents, 50 );
        this->thread()->msleep(10);
        fileInfo->refresh();
        *dTime = fileInfo->lastModified();
        if( str_Time != dTime->toString(format) && fileInfo->exists())
        {
            qDebug() << "----------------------------------------------";
            QFile imageFile(filename);
            while(!imageFile.open(QIODevice::ReadOnly))
            {
                qDebug() << "�������� �����...";
                this->thread()->msleep(20);
            }   
            // ������ ���������� �����
            str_Time = dTime->toString(format);
            qDebug() << dTime->toString(format);
            FindCenterFromFile(filename);
            QTextStream out(&file);
               out << "Time: " << str_Time
                   << " Position: " << posX << " " << posY
                   << " Offset: " << offsetX << " " << offsetY
                   << "\n";
        }
    }
}