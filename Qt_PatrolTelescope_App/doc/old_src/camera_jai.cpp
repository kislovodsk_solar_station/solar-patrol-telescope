//-------------------------------------------------------------------------------------------------
// ��������� ����������� � ������ (� ��������� ������ ����� �� ������������)
//
bool Camera_JAI::SaveImage()
{
    if( saveMode )
    {
        J_tIMAGE_INFO convertImageInfo;     // ��������� c ����������� �� �����������

        // �������� ������ ��� ���������� �����������
        if(J_Image_Malloc(&rawImageInfo, &convertImageInfo) != J_ST_SUCCESS)
        {
            qDebug("Camera_JAI::������ �������� ������");
            return FALSE;
        }

        // �������������� ����� ������ � ������ �����������
        if(J_Image_FromRawToImage(&rawImageInfo, &convertImageInfo) != J_ST_SUCCESS)
        {
            sigWarningMessage("������ �������������� ������ � �����������");
            return FALSE;
        }

        std::string str = "./frame0_";
        str.append(".jpg");
 
        QFile imageFile("./frame00.jpg");
        if ( imageFile.exists())
            imageFile.remove();
//        // ���������� ����������� � ������� .tif
        //if(J_Image_SaveFileA(&convertImageInfo, str.c_str()) != J_ST_SUCCESS)
//            return FALSE;
       // ���������� ����������� � ������� JPG
        if (J_Image_SaveFileExA(&convertImageInfo, str.c_str(), J_FF_JPEG) != J_ST_SUCCESS)
        {
            sigWarningMessage("������ ���������� �����������");
            return FALSE;
        }

        // ������� ������, ������������ ������
        if(J_Image_Free(&convertImageInfo) != J_ST_SUCCESS)
        {
            sigWarningMessage("�� ������� ���������� �����");
            return FALSE;
        }

        QFile imageFile1(str.c_str());
        imageFile1.rename("./frame00.jpg");
        qDebug() << "Camera_JAI::SaveImage(), " << imageFile1.fileName();
        emit sigImageSaved(imageFile1.fileName());
    }
    return true;
}